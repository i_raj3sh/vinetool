<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 7/8/18
 * Time: 8:33 PM
 */

if (!$argc)
    die('invalid');

require dirname(__DIR__) . '/lib/include.php';

$ap = new \model\ApproveOffers();
$ap->setProperties([
    'status' => 1
])->setQueryParameters($ap);

$get = $ap->query();

foreach ($get as $item){

    $arr = [];

    $arr['ID'] = $item->ID;
    $arr['offers_ID'] = $item->offers_ID;
    $arr['affiliate_ID'] = $item->affiliate_ID;
    $arr['percentage'] = $item->percentage;
    $arr['cap'] = $item->cap;
    $arr['status'] = $item->status;
    $arr['requested'] = strtotime($item->added.'00');
    $arr['approved'] = strtotime($item->added.'00');
    $arr['added'] = $item->added;
    $arr['traffic'] = $item->traffic;
    $arr['api'] = $item->api;

    $aa = new \model\ApproveOffers();
    $aa->setProperties($arr)
        ->update($aa);
}