<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 8/12/18
 * Time: 8:41 AM
 */
ini_set('memory_limit', '1024M');
set_time_limit(0);

use helper\Mysql;
use model\HourlyStats;

if (!$argc)
    die('invalid');

require dirname(__DIR__) . '/lib/include.php';

echo "\n -> $argv[1] \n";

$argv[1]();

function offers(){

    $sql = 'SELECT * FROM vine_adserver_db.offers';
    $data = Mysql::select($sql, (new \model\Offers())->getMyClass());

    foreach ($data as $v):

        $sql = "SELECT g.code name FROM vine_adserver_db.country g JOIN vine_adserver_db.offers_country og ON g.ID = og.country_ID WHERE og.offers_ID = '$v->ID'";
        $geo = Mysql::select($sql, (new \model\Country())->getMyClass());

        $sql = "SELECT d.name FROM vine_adserver_db.device d JOIN vine_adserver_db.offers_device od ON d.ID = od.device_ID WHERE od.offers_ID = '$v->ID'";
        $dev = Mysql::select($sql, (new \model\Device())->getMyClass());

        $sql = "SELECT c.name FROM vine_adserver_db.category c JOIN vine_adserver_db.offers_category oc ON c.ID = oc.category_ID WHERE oc.offers_ID = '$v->ID'";
        $cat = Mysql::select($sql, (new \model\Category())->getMyClass());

        $v = (array) $v;
        $keys = array_keys($v);
        $vals = array_slice(array_values($v), 3);
        $keys = mysqlKeys($keys);
        $v = array_combine($keys, $vals);
        $v['country=?'] = getNames($geo);
        $v['device=?'] = getNames($dev);
        $v['category=?'] = getNames($cat);
//print_r($v);die;

        Mysql::put('INSERT', 'offers', $v);
    endforeach;
}

function postback(){
    $sql = 'SELECT * from vine_adserver_db.postback';
    $data = Mysql::select($sql,(new \model\Postback())->getMyClass());

    foreach ($data as $v){
        $v = (array) $v;
        $keys = array_keys($v);
        $vals = array_slice(array_values($v), 3);
        $keys = mysqlKeys($keys);
        $v = array_combine($keys, $vals);
        $v['url=?'] = str_replace('{sub1}', '{clickid}', $v['url=?']);

        Mysql::put('INSERT', 'postback', $v);
    }
}

function affiliates(){
    $sql = 'SELECT * from vine_adserver_db.affiliate';
    $data = Mysql::select($sql,(new \model\Affiliate())->getMyClass());

    foreach ($data as $v){
        $v = (array) $v;
        $keys = array_keys($v);
        $vals = array_slice(array_values($v), 3);
        $keys = mysqlKeys($keys);
        $v = array_combine($keys, $vals);
        $v['last_updated=?']=0;

        Mysql::put('INSERT', 'affiliate', $v);
    }
}

function networks(){
    $sql = 'SELECT * from vine_adserver_db.network';
    $data = Mysql::select($sql,(new \model\Network())->getMyClass());

    foreach ($data as $v){
        $v = (array) $v;
        $keys = array_keys($v);
        $vals = array_slice(array_values($v), 3);
        $keys = mysqlKeys($keys);
        $v = array_combine($keys, $vals);
        $v['last_updated=?']=0;

        Mysql::put('INSERT', 'network', $v);
    }
}

function approved(){
    $sql = 'SELECT * from vine_adserver_db.affiliate_offers';
    $data = Mysql::select($sql,(new \model\ApproveOffers())->getMyClass());
    foreach ($data as $v){
        $v = (array) $v;
        $keys = array_keys($v);
        $vals = array_slice(array_values($v), 3);
        $keys = mysqlKeys($keys);
        $v = array_combine($keys, $vals);
        $v['requested=?']=strtotime($v['added'].'00');
        $v['approved=?']=strtotime($v['added'].'00');
        Mysql::put('INSERT', 'affiliate_offers', $v);
    }
}

function conversions(){
    $sql = '
      SELECT c.*,o.name offer,o.ID offerID,n.ID networkID,n.email network,a.ID affiliateID,a.email affiliate,s.* FROM vine_adserver_db.sale s
      JOIN vine_adserver_db.click c ON c.ID = s.stats_ID
      JOIN vine_adserver_db.affiliate_offers ao ON ao.ID = c.affiliate_offers_ID
      JOIN vine_adserver_db.offers o ON o.ID = ao.offers_ID
      JOIN vine_adserver_db.affiliate a ON a.ID = ao.affiliate_ID
      JOIN vine_adserver_db.network n ON n.ID = o.network_ID
      WHERE c.day_click >= 20180601 AND c.day_click <= 20180731
    ';
}

function stats(){
    $sql = '
        SELECT h.*,SUM(clicks) visits,SUM(sales) conversions,SUM(h.charge) inside,SUM(h.payout) outside, o.name campaign,n.email network,n.ID network_ID,a.email affiliate,a.admin_ID FROM vine_adserver_db.stats_grouped_hourly h
        JOIN vine_adserver_db.offers o ON h.offers_ID = o.ID
        JOIN vine_adserver_db.affiliate a ON h.affiliate_ID = a.ID
        JOIN vine_adserver_db.network n ON o.network_ID = n.ID
        WHERE month_ = 201808
        GROUP BY h.day_, h.hour_, h.affiliate_ID, h.offers_ID, h.country, h.device
    ';

    $data = Mysql::select($sql, (new HourlyStats())->getMyClass());
//echo count($data);
    Mysql::setDbName(Mysql::STATS);
    foreach ($data as $item){
        $params = [
            'day_' => $item->day_,
            'hour_' => $item->hour_,
            'month_' => $item->month_,
            'clicks' => $item->visits,
            'sales' => $item->conversions,
            'charge' => $item->inside,
            'payout' => $item->outside,
            'offers_ID' => $item->offers_ID,
            'affiliate_ID' => $item->affiliate_ID,
            'network_ID' => $item->network_ID,
            'admin_ID' => $item->admin_ID,
            'campaign' => $item->campaign,
            'affiliate' => $item->affiliate,
            'network' => $item->network,
            'device' => $item->device,
            'country' => $item->country
        ];

        $gh = new HourlyStats();
        $gh->setProperties($params);
        $gh->insert($gh);
//	print_r($item);
//	break;
    }
    Mysql::setDbName();
}

function getNames(array $a):string {
    $n = [];
    foreach ($a as $v){
        $n[] = $v->name;
    }
    asort($n);
    $n = array_values($n);
    return json_encode($n);
}

function mysqlKeys(array $keys):array{
    $n = [];
    foreach ($keys as $v){
        if (false !== strpos($v, 'model')){
            continue;
        }
        $n[] = $v.'=?';
    }
    return $n;
}
