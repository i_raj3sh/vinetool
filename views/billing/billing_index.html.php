<?php

use controller\AffiliateBilling as AB;
use helper\Request as R;
use helper\Tool;

$of = R::getField('of', 'GET');
$st = R::getField('st', 'GET');
$date = R::getField('date', 'GET');

?>

<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<style>
    table#final_billing tbody td {
        border-bottom: 1px solid #aaa;
    }
    .timepicker-picker,.noshow{
        display: none;
    }
    .ded_input{
        width: 70px;
    }
    .datepicker-days {
        display: none !important;
    }
    .datepicker-months{
        display: block !important;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Final Monthly Billing</h3>
        <form class="row py-3">
            <?=tknInp()?>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Offer ID:</label>
                    <input type="text" class="form-control" name="of" placeholder="Offer ID" value="<?= $of ?>">
                </div>
            </div>
            <div class="col-md-2">
                <label>Month:</label>
                <input id="date" class="form-control" type="text" name="date" autocomplete="off" value="<?= $date ?>"
                       placeholder="Month: YYYYMM">
            </div>
            <div class="col-md-2">
                <label>Status:</label>
                <select name="st" class="form-control">
                    <option value="-">All</option>
                    <option value="0" <?php if ($st === '0') echo 'selected' ?>>Unconfirmed</option>
                    <option value="1" <?php if ($st === '1') echo 'selected' ?>>Confirmed</option>
                </select>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <span class="float-right"><strong>Total</strong>: <label class="badge badge-info"><?= $form['total'] ?></span>
        <div class="table-responsive">
            <table id="final_billing" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>Month</th>
                    <th>AffiliateID</th>
                    <th>OfferID</th>
                    <th>Offer</th>
                    <th>Sales</th>
                    <th>Earning</th>
                    <th>Deduction</th>
                    <th>Total</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):

                    if (!$v->total) {
                        AB::total($v);
                    }

                    ?>
                    <tr>
                        <td <?= $v->bd ?>>
                            <?php if ($v->affiliate_ID) { ?>
                                <?= Tool::getMonthName((int)substr($v->month, 4, 2)) ?>
                            <?php } ?>
                        </td>
                        <td <?= $v->bd ?>><?= $v->affiliate_ID ?></td>
                        <td <?= $v->bd ?>><?= $v->offers_ID ?></td>
                        <td <?= $v->bd ?>><?= $v->offer ?></td>
                        <td <?= $v->bd ?>><?= number_format($v->sales) ?></td>
                        <td <?= $v->bd ?>><code>$</code><?= number_format($v->payout, 2) ?></td>
                        <td <?= $v->bd ?>><?= $v->deduction ?></td>
                        <td <?= $v->bd ?>><code>$</code><strong><?= number_format($v->total, 2) ?></strong></td>
                        <td <?= $v->bd ?>>
                            <?php if ($v->affiliate_ID) { ?>
                                <?= AB::status($v->status) ?>
                            <?php } ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $form['pages']; echo $form['perPage'] ?>
        </div>
    </div>
</section>
<script type="text/javascript">
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });
    $('#date').datetimepicker( {
        format:'YYYYMM',
        changeMonth: true,
        showButtonPanel: true
    });

    $('table').tableExport({
        headers: true,
        // footers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'bottom',
        // ignoreRows: null,
        // ignoreCols: null,
        trimWhitespace: true
    });
</script>