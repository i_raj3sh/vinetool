<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/14/17
 * Time: 8:04 AM
 */
?>
<section class="row">

    <div class="col-md-7 pt-5">
        <form name="add_postback" method="post" action="<?=s() ?>postback/add<?=tkn()?>">
            <input type="hidden" value="<?php $form->get('ID') ?>" name="id">
            <h3>Set your postback</h3>
            <div class="form-group pt-3">
                <label>Global postback</label>
                <input type="url" class="form-control" name="posturl" value="<?php $form->get('url') ?>" placeholder="Global Postback" required/>
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit" name="postback" value="_"><i class="fa fa-edit"></i> Update</button>
            </div>
        </form>
    </div>
    <div class="col-md-5">
        <div class="jumbotron">
            <h2>My Postback URL</h2>
            <p class="lead">Use this macros in your postback url.</p>
            <hr class="my-4">
            <ul class="list-group">
                <li class="list-group-item">Callback click: <span class="float-right text-danger">{clickid}</span></li>
                <li class="list-group-item">Affiliate source: <span class="float-right text-danger">{sub_affid}</span></li>
                <li class="list-group-item">Payout: <span class="float-right text-danger">{payout}</span></li>
                <li class="list-group-item">Campaign ID: <span class="float-right text-danger">{offer}</span></li>
                <li class="list-group-item">Country: <span class="float-right text-danger">{geo}</span></li>
                <li class="list-group-item">OS: <span class="float-right text-danger">{os}</span></li>
                <li class="list-group-item">IP address: <span class="float-right text-danger">{ip}</span></li>
                <li class="list-group-item">Store app id: <span class="float-right text-danger">{appid}</span></li>
                <li class="list-group-item">Device ID: <span class="float-right text-danger">{device_id}</span></li>
                <li class="list-group-item">Event ID: <span class="float-right text-danger">{event_id}</span></li>
                <li class="list-group-item">Event name: <span class="float-right text-danger">{event_name}</span></li>
                <li class="list-group-item">Extra 1: <span class="float-right text-danger">{extra1}</span></li>
                <li class="list-group-item">Extra 2: <span class="float-right text-danger">{extra2}</span></li>
                <li class="list-group-item">Extra 3: <span class="float-right text-danger">{extra3}</span></li>
                <li class="list-group-item">Extra 4: <span class="float-right text-danger">{extra4}</span></li>
            </ul>
        </div>
    </div>
</section>
