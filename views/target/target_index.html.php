<?php

use helper\Request as R;
use helper\Template as T;

function status($v)
{
    $status = 'text-danger';
    if ($v->status) {
        $status = 'text-success';
    }
    return '<a class="btn btn-sm" data-status="' . $status . '" href="#!" title="Status" onclick="smash(' . $v->ID . ',this)"><i class="fa fa-power-off ' . $status . '"></i></a>';
}

?>
<?= datatables() ?>
<?= select2() ?>
<style>
    #nameTarget .select2-container {
        width: 300px !important;
    }

    .fa-trash {
        font-size: 1.5em;
    }

    .pointer {
        cursor: pointer;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Targets <a class="btn btn-sm btn-primary float-right" href="<?= s() ?>target/edit"><i
                        class="fa fa-plus"></i> target</a></h3>
        <form class="row py-3">
            <div class="col-md-3">
                <?= affiliate() ?>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Geos:</label>
                    <?php
                    $geo = R::getField('geo', 'GET');

                    $g = new \model\Country();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select id="gea" name="geo" class="form-control selected5">
                        <option value="">select</option>
                        <?php foreach ($all as $item): ?>
                            <option value="<?= $item->code ?>" <?php T::option($geo, $item->code) ?> ><?= $item->code . ': ' . $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>OS:</label>
                    <?php
                    $geo = R::getField('os', 'GET');

                    $g = new \model\Device();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select id="davi" name="os" class="form-control selected5">
                        <option value="">select</option>
                        <?php foreach ($all as $item): ?>
                            <option value="<?= $item->name ?>" <?php T::option($geo, $item->name) ?> ><?= $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Category:</label>
                    <?php
                    $geo = R::getField('c', 'GET');

                    $g = new \model\Category();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select id="coti" name="c[]" class="form-control selected5" multiple>
                        <?php foreach ($all as $item): ?>
                            <option value="<?= $item->name ?>" <?php T::option($geo, $item->name) ?> ><?= $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="table-responsive">
            <table id="tabular" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Pub</th>
                    <th>Geo</th>
                    <th>Region</th>
                    <th>Device</th>
                    <th>OS</th>
                    <!--                    <th>Category</th>-->
                    <th>Offer</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form as $v):
                    ?>
                    <tr>
                        <td><?= $v->ID ?></td>
                        <td id="aff_<?= $v->ID ?>" data-val="<?= $v->affiliate_ID ?>"><?= $v->affiliate_ID ?></td>
                        <td id="geo_<?= $v->ID ?>" data-val="<?= $v->geo ?>"><?= $v->geo ?></td>
                        <td><?= $v->region ?></td>
                        <td><?= $v->device ?></td>
                        <td id="os_<?= $v->ID ?>"
                            data-val="<?= $v->os ?>"><?= T::wordBreak(T::devIcons([$v->os], 0), BR) ?></td>
                        <!--<td><? /*=$v->category*/
                        ?></td>-->
                        <td>
                            <div class="btn-group"><span
                                        class="btn btn-sm btn-disabled"><strong><?= $v->o ?></strong></span>
                                <a class="btn btn-info btn-sm" href="#!" onclick="popBack(<?= $v->ID ?>)"><i
                                            class="fa fa-plus"></i></a>
                            </div>
                        </td>
                        <td style="display: inline-flex">
                            <?= status($v) ?>
                            <div class="btn-group">
                                <a class="btn btn-info btn-sm" href="<?= s() ?>target/edit?id=<?= $v->ID ?>"><i
                                            class="fa fa-pencil"></i></a>
                                <a class="btn btn-danger btn-sm" href="#!"
                                   onclick=delet("<?= s() ?>target/del?id=<?= $v->ID ?>")><i
                                            class="fa fa-remove"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $form['pages'];
            echo $form['perPage'] ?>
        </div>
    </div>
</section>
<div class="modal fade" id="nameTarget" role="dialog" aria-labelledby="nameTarget" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Assign campaigns to this target.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="bodyTarget" class="modal-body">
                <input type="hidden" id="target_id" name="target_id" value="">
                <table class="table">
                    <thead>
                    <tr>
                        <th width="15%">ID</th>
                        <th width="80%">Name</th>
                        <th width="5%"></th>
                        <!--                                <th>Weight</th>-->
                    </tr>
                    </thead>
                    <tbody id="well_body">
                    </tbody>
                </table>
                <hr/>
                <div class="row">
                    <div class="col-10">
                        <div class="form-group">
                            <label for="campaign">Offer:</label>
                            <select id="campaign" name="of" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-2">
                        <button id="plus-offer" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function delet(loc, conf) {
        conf = confirm("Are you sure? Do you want to delete?");

        if (conf) {
            window.location = loc;
        }
    }

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
        // ,templateSelection: formatState
    });

    function smash(s, t) {
        var v = $(t), w = v.data('status');
        if (w === 'text-success') {
            v.data('text-danger');
            v.removeClass('text-success');
            v.addClass('text-danger')
        } else {
            v.data('text-success');
            v.removeClass('text-danger');
            v.addClass('text-success')
        }
    }

    function popBack(i) {
        $('#well_body').html('');
        $.getJSON("<?=s()?>target/pullCampaigns?id=" + i, function (res) {
            for (var k in res) {
                $('#well_body').append('<tr id="tar_' + i + '_' + res[k].ID + '">' +
                    '<td><strong>#' + res[k].ID + '</strong></td>' +
                    '<td>' + res[k].name + '</td>' +
                    '<td><span class="d-block pointer" onclick="delTarOff(' + i + ',' + res[k].ID + ')"><i class="fa fa-trash text-danger"></i></span></td>' +
                    '</tr>')
            }
        });
        $('#target_id').val(i);
        $('#nameTarget').modal();
    }

    $('#plus-offer').on('click', function () {
        var cid = $('#campaign');
        var t = $('#target_id').val();
        if (!cid.val()) return;
        $.get("<?=s()?>target/pushCampaigns?id=" + t + '&cid=' + cid.val(), function (res) {
            if (res === 'true') {
                var s = cid.text().split(" ");
                var i = s[0];
                var k = i.replace('#', '');
                delete s[0];
                s = s.join(" ");
                $('#well_body').append('<tr id="tar_' + t + '_' + k + '">' +
                    '<td>' + i + '</td>' +
                    '<td>' + s + '</td>' +
                    '<td><span class="d-block pointer" onclick="delTarOff(' + t + ',' + k + ')"><i class="fa fa-trash text-danger"></i></span></td>' +
                    '</tr>')
            }
        });
    });

    $('#copyBack').on('click', function () {
        var s = document.getElementById('popUrl');
        s.select();
        document.execCommand('copy');
    }).tooltip({
        trigger: 'click'
    });
    $('#tabular').DataTable();

    $("#campaign").select2({
        ajax: {
            url: "<?= s() ?>target/offer",
            dataType: "json",
            delay: 250,
            data: function (params) {
                var t = $('#target_id').val();
                var g = $('#geo_' + t).data('val');
                var d = $('#os_' + t).data('val');
                var a = $('#aff_' + t).data('val');
                return {
                    q: params.term,
                    g: g,
                    p: a,
                    d: d
                };
            },
            processResults: function (data, params) {
                return {
                    results: $.map(data, function (obj) {
                        return {
                            id: obj.id,
                            text: obj.value
                        };
                    })
                }
            },
            cache: true
        },
        placeholder: "--select--",
        allowClear: true,
        minimumInputLength: 3
    });

    function delTarOff(i, o) {
        $.get('<?=s()?>target/delOffs?i=' + i + '&o=' + o, function (res) {
            $('tr#tar_' + i + '_' + o).remove();
        });
    }
</script>