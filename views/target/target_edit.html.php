<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/16/19
 * Time: 6:55 PM
 */

use model\Affiliate;

?>
<?= select2() ?>
<section class="row">
    <div class="col-md-9 py-3">
        <h3>Update Target</h3>

        <div class="pt-3">
            <form name="add_target" method="post" action="<?php echo MY_SERVER ?>target/add">
                <input type="hidden" name="id" value="<?php $form->get('ID') ?>">
                <div class="form-group">
                    <label>Affiliate</label>
                    <?php
                    $g = new Affiliate();
                    $g->setProperties(['active' => 1])->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-bank"></i></span>
                        </div>
                        <select name="affiliate_ID" class="form-control selected5">
                            <?php foreach ($all as $item): ?>
                                <option value="<?php echo $item->ID ?>" <?php $form->select('affiliate_ID', $item->ID) ?> ><?php echo $item->email ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>Geos</label>
                    <?php

                    $g = new \model\Country();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-globe fa-spin"></i></span>
                        </div>
                        <select name="geo" class="form-control selected5">
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->code ?>" <?php $form->select('geo', $item->code) ?> ><?php echo $item->code . ': ' . $item->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>Region</label>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-map"></i></span>
                        </div>
                        <select name="region" class="form-control selected5">
                            <option value="All">All</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>Device Type</label>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-tablet"></i></span>
                        </div>
                        <select name="device" class="form-control selected5">
                            <option value="Both" <?php $form->select('device', 'Both') ?>>Both</option>
                            <option value="SmartPhone" <?php $form->select('device', 'SmartPhone') ?>>SmartPhone
                            </option>
                            <option value="Tablet" <?php $form->select('device', 'Tablet') ?>>Tablet</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>OS</label>
                    <?php
                    $g = new \model\Device();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-gear fa-spin"></i></span>
                        </div>
                        <select name="os" class="form-control selected5">
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->name ?>" <?php $form->select('os', $item->name) ?> ><?php echo $item->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <?php
                    $g = new \model\Category();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-tasks"></i></span>
                        </div>
                        <select name="category[]" class="form-control selected5" multiple>
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->name ?>" <?php $form->in_array(json_decode($form->fetch('category')), $item->name) ?> ><?php echo $item->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <label>Status</label>
                <div class="input-group input-group-sm">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fa fa-play-circle"></i></span>
                    </div>
                    <select name="status" class="form-control selected5">
                        <option value="1" <?php $form->select('status', '1') ?> >active</option>
                        <option value="0" <?php $form->select('status', '0') ?> >pause</option>
                    </select>
                </div>
                <div class="form-group mt-4">
                    <button type="submit" class="btn btn-info" name="target" value="_"><i class="fa fa-save"></i>
                        Proceed
                    </button>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
        // ,templateSelection: formatState
    });

</script>