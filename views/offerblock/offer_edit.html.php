<?php

use helper\Request as R;

?>
<?=select2()?>
<section class="row">
    <div class="col-md-7 py-3">
        <h3>Update blocked by offer</h3>

        <div>
            <form name="add_" method="post" action="<?=s() ?>offerBlock/add">
                <div class="form-group">
                    <?=affiliate()?>
                </div>
                <div class="form-group">
                    <div class="form-group">
                        <label>OfferID:</label>
                        <input type="text" class="form-control" name="of" value="<?= R::getField('i','GET') ?>" placeholder="Offer ID">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info" name="oblocked" value="_">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });
</script>