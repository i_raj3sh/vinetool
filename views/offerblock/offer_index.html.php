<?php

?>
<?=select2()?>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Blocked By Offer <a class="btn btn-sm btn-primary float-right" href="<?=s() ?>offerBlock/edit"><i class="fa fa-plus"></i> block</a></h3>
        <form class="row py-3">
            <div class="col-md-3"><?=affiliate()?></div>
            <div class="col-md-3">
                <?= campaign() ?>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="table-responsive">
            <span class="float-right"><strong>Total</strong>: <label class="badge badge-info"><?=$form['total'] ?></span>
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>AffiliateID</th>
                    <th>Affiliate</th>
                    <th>OfferID</th>
                    <th>Offer</th>
                    <th>At</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):?>
                <tr>
                    <td><?=$v->affiliate_ID?></td>
                    <td><?=$v->affiliate?></td>
                    <td><?=$v->offers_ID?></td>
                    <td><?=$v->offer?></td>
                    <td><?=date('M d, Y H:i',$v->at)?></td>
                    <td><a class="btn btn-danger btn-sm" href="#!" onclick=delet("<?=s()?>offerBlock/del?id=<?=$v->affiliate_ID?>_<?=$v->offers_ID?>") data-toggle="tooltip" title="delete"><i class="fa fa-remove"></i></a></td>
                </tr>
                <?php endforeach;
                ?>
                </tbody>
            </table>
                <?php echo $form['pages']; echo $form['perPage'] ?>
        </div>
    </div>
</section>
<script>
    $('a#rules').addClass('active');
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    function delet(loc, conf) {
        conf = confirm("Are you sure? Do you want to delete?");

        if (conf){
            window.location = loc;
        }
    }
</script>