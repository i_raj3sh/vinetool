<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/13/17
 * Time: 10:47 PM
 */
?>
<section class="row">
    <div class="col-md-7 py-3">
        <h3>Edit Account</h3>

        <div class="pt-3">
            <form name="edit_account" method="post" action="<?=s() ?>account/add<?=tkn()?>">
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" type="text" name="name" placeholder="Name" value="<?php $form->get('name') ?>" required>
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input class="form-control" type="text" name="username" placeholder="Username" value="<?php $form->get('username') ?>" required>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="email" name="email" placeholder="Email" value="<?php $form->get('email') ?>" required>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" type="password" name="password" placeholder="Password" value="" required>
                </div>
                <div class="form-group">
                    <label>Skype</label>
                    <input class="form-control" type="text" name="skype" placeholder="Skype" value="<?php $form->get('skype') ?>">
                </div>
                <div class="form-group">
                    <label>Company Name</label>
                    <input class="form-control" type="text" name="company" placeholder="Company Name" value="<?php $form->get('company') ?>">
                </div>
                <div class="form-group">
                    <label>Website. [include http(s)]</label>
                    <input class="form-control" type="url" name="website" placeholder="Website" value="<?php $form->get('website') ?>">
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <?php
                    $g = new \model\Country();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select name="country" class="form-control">
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->code ?>" <?php $form->select('country',$item->code) ?> ><?php echo $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info" name="account" value="_">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</section>
