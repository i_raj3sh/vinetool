<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 5:37 PM
 */

$g = $form['group']
?>
<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<?= datatables() ?>

<style>
    .dashboard-stat {
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number {
        font-size: 2rem;
    }

    .number cite {
        font-size: 1.5rem;
        color: #ddd;
    }

    .dashboard-stat .desc {
        padding: .3rem .7rem;
    }

    .number {
        padding: .8rem .6rem .2rem;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Statistics by scrubbed group</h3>
        <hr/>
        <form class="row py-3">
            <div class="col-md-2">
                <?= network() ?>
            </div>
            <div class="col-md-2">
                <?= affiliate() ?>
            </div>
            <div class="col-md-3">
                <?= campaign() ?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="grouped">Group with:</label>
                    <select id="grouped" class="form-control" name="g">
                        <option value="day" <?php if ($g === 'day') echo 'selected' ?>>Day</option>
                        <option value="month" <?php if ($g === 'month') echo 'selected' ?>>Month</option>
                        <option value="network_ID" <?php if ($g === 'network') echo 'selected' ?>>Network</option>
                        <option value="affiliate_ID" <?php if ($g === 'affiliate') echo 'selected' ?>>Affiliate</option>
                        <option value="offers_ID" <?php if ($g === 'offers') echo 'selected' ?>>Offer</option>
                    </select>
                </div>
            </div>
            <?= dateSpan() ?>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <div id="stats_block"></div>
        <hr/>
        <div class="table-responsive">
            <h4 class="my-4">Stats breakdown based on scrubbed sales</h4>
            <table id="Affiliate_Stats" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th><?= ucfirst($g) ?></th>
                    <th>Sales</th>
                    <th>Payout</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $series = $cate = [];
                $series[0]['name'] = 'Sales';
                $series[1]['name'] = 'Earnings';
                $series[2]['name'] = '';
                foreach ($form['table'] as $v):

                    if ($v->conver > 0) {
                        $cate[] = $v->$g;
                        $series[0]['data'][] = (int)$v->conver;
                        $series[1]['data'][] = (float)$v->pull;
                    }

                    echo '<td>' . $v->$g . '</td>';
                    echo '<td>' . number_format($v->conver) . '</td>';
                    echo '<td class="text-success"><strong>$' . number_format($v->pull, 4) . '</strong></td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="<?= s() ?>assets/js/stats.js"></script>
<script type="text/javascript">

    var cate = <?php echo json_encode($cate) ?>,
        series = <?php echo json_encode($series) ?>;

    bar2Spline('stats_block', cate, series, 'Scrubbed Group', 'Sales', '');

    $('a#stats').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    $('#grouped').on('change', function () {
        if ($(this).val() === 'day') {
            $('select#date_type').val('t').trigger('change')
        } else if ($(this).val() === 'month') {
            $('select#date_type').val('m').trigger('change')
        }
    });

    var table = $('#Affiliate_Stats');
    table.tableExport({
        headers: true,
        // footers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'top',
        // ignoreRows: null,
        // ignoreCols: null,
        trimWhitespace: true
    });
    var order = [[2, "desc"]];
    if ('day' === '<?=$g?>') {
        order = [[0, "asc"]]
    }
    table.DataTable({
        "order": order
    });
</script>