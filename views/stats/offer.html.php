<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 12:10 PM
 *///ini_set('display_errors',1); error_reporting(E_ALL);
use helper\Template as T;

$affy = false;
if (\controller\Controller::getSessionType() === 'ADMIN'){
    $affy = true;
}

?>
<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<?= datatables() ?>
<style>
    .dashboard-stat{
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number{
        font-size: 2rem;
    }
    .number cite{
        font-size: 1.5rem;
        color: #ddd;
    }
    .dashboard-stat .desc{
        padding: .3rem .7rem;
    }
    .number{
        padding: .8rem .6rem .2rem;
    }

    .select2 {
        width: 100% !important;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Statistics by offer</h3>
        <hr/>
        <form class="row py-3">
            <?=tknInp()?>
            <?php if ($affy){ ?>
                <div class="col-md-3">
                    <?= network() ?>
                </div>
                <div class="col-md-3">
                    <?= affiliate() ?>
                </div>
            <?php } ?>
            <div class="col-md-3">
                <div class="form-group">
                    <?= campaign() ?>
                </div>
            </div>
            <?= dateSpan() ?>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <div id="stats_block"></div>
        <hr/>
        <div class="table-responsive">
            <h4 class="my-4">Stats breakdown based on offers
                <?php if ($affy) { ?>
                    <a id="checkedAssign" class="btn btn-sm btn-success mb-1 float-right"
                       style="font-size: .7rem;padding: .2rem .6rem" href="#!"><i class="fa fa-plus"></i> assign to pub</a>
                <?php } ?>
            </h4>
            <table id="Offers_Stats" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <?php if ($affy) { ?>
                        <th class="tableexport-ignore"><input id="allbox" class="form-control" type="checkbox"></th>
                    <?php } ?>
                    <th class="tableexport-ignore"><i class="fa fa-circle" style="font-size: 1rem"></i></th>
                    <th>ID</th>
                    <th>Offer</th>
                    <th>Visits (Total)</th>
                    <th>Visits (Verified)</th>
                    <th>Sales</th>
                    <th>Payout</th>
                    <?php if ($affy){ ?>
                        <th>Profit</th>
                        <th>Charged</th>
                        <th>Network</th>
                    <?php } ?>
                    <th>EPC</th>
                    <th>CR%</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $series = $cate = [];
                $series[0]['name'] = 'Visits';
                $series[1]['name'] = 'Earnings';
                $series[2]['name'] = 'CR';
                foreach ($form as $v):

                    if (!$v->offers_ID) {
                        continue;
                    }

                    if($v->visit == 0) $v->visit = 1;

                    $cr = ($v->conver / $v->visit) * 100;

                    if ($v->pull > 0) {
                        $cate[] = "($v->offers_ID) $v->called";
                        $series[0]['data'][] = (int)$v->visit;
                        if ($affy) {
                            $series[1]['data'][] = $v->pull - $v->push;
                        } else {
                            $series[1]['data'][] = (float)$v->push;
                        }
                        $series[2]['data'][] = $cr;
                    }

                    echo '<tr>';
                    if ($affy) {
                        echo '<td class="tableexport-ignore"><input id="O$' . $v->offers_ID . '" class="form-control checker" type="checkbox"></td>';
                    }
                    echo '<td>' . T::state($v->active) . '</td>';
                    echo '<td>'.$v->offers_ID.'</td>';
                    echo '<td><a href="' . MY_SERVER . 'offers/view?id=' . $v->offers_ID . tkn('&') . '">' . $v->called . '</td>';
                    echo '<td>' . number_format($v->visit + $v->lost) . '</td>';
                    echo '<td>'.number_format($v->visit).'</td>';
                    echo '<td>'.number_format($v->conver).'</td>';
                    echo '<td class="text-success"><strong>$'.number_format( $v->push,4).'</strong></td>';
                    $amount = $v->push;
                    if ($affy) {
                        $amount = $v->pull;
                        echo '<td class="text-primary"><strong>$' . number_format( $v->pull - $v->push,4) . '</strong></td>';
                        echo '<td>$' . number_format($v->pull,4) . '</td>';
                        echo '<td>' . $v->network . '</td>';
                    }
                    echo '<td>'.sprintf('%.4f', $amount / $v->visit).'</td>';
                    echo '<td>' . sprintf('%.4f', $cr) . '</td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<?php if ($affy) { ?>
    <div class="modal fade" id="assignToPub" role="dialog" aria-labelledby="assignToPub" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Assign to the selected affiliate</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="bodyAssign" class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <?= affiliate() ?>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <input id="percentAssign" class="form-control" type="number" placeholder="Percentage"
                                       value="<?= COMMISSION * 100 ?>"/>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <input id="capAssign" class="form-control" type="number"
                                       placeholder="Cap (Default: Unlimited)"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="saveModelAssign" type="button" class="btn btn-primary">Assign</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('input#allbox').on('change', function () {
            var c = $('.checker');
            if (this.checked) {
                c.prop('checked', true);
            } else {
                c.prop('checked', false);
            }
        });
        $('a#checkedAssign').on('click', function () {
            var p = [];
            var c = $('.checker');
            c.each(function (i, j) {
                if (j.checked)
                    p.push(j.id.split('$')[1]);
            });
            var w = p + '';
            if (w) {
                $('#assignToPub').modal('show');
            }
        });

        $('#saveModelAssign').on('click', function () {
            var af = $('#bodyAssign').find('select[name="af"]').val(), per = $('#percentAssign').val(),
                cap = $('#capAssign').val(), p = [];
            $('.checker').each(function (i, j) {
                if (j.checked)
                    p.push(j.id.split('$')[1]);
            });
            var w = p + '';
            var data = 'af=' + af + '&p=' + per + '&c=' + cap + '&ids=' + w;
            $.getJSON("<?=s()?>approveOffers/assign?" + data, function (res) {
                if (res.error) {
                    toastr["error"](res.error, "Error!");
                } else {
                    toastr["success"]("Assigned", "Splendid!");
                }
                $('#assignToPub').modal('hide');
            });
        });
    </script>
<?php } ?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="<?= s() ?>assets/js/stats.js"></script>
<script type="text/javascript">

    var cate = <?php echo json_encode($cate) ?>,
        series = <?php echo json_encode($series) ?>;

    bar2Spline('stats_block', cate, series, 'Offers', 'Visits', 'CR');

    $('a#stats').addClass('active');
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    var table = $('#Offers_Stats');
    table.tableExport({
        headers: true,
        // footers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'top',
        // ignoreRows: null,
        // ignoreCols: null,
        trimWhitespace: true
    });
    table.DataTable({
        "order": [[6, "desc"]]
    });
</script>