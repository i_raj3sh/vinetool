<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 5:37 PM
 */

$who = \helper\Request::getField('w') === 'Network' ? 'Network' : 'Affiliate';
?>
<?= select2() ?>
<?= datepicker() ?>
<?= datatables() ?>

<style>
    .dashboard-stat {
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number {
        font-size: 2rem;
    }

    .number cite {
        font-size: 1.5rem;
        color: #ddd;
    }

    .dashboard-stat .desc {
        padding: .3rem .7rem;
    }

    .number {
        padding: .8rem .6rem .2rem;
    }

</style>
<section class="row">
    <div class="col-12">
        <form class="row py-3">
            <div class="col-md-3">
                <?= admin() ?>
            </div>
            <div class="col-md-3">
                <label for="who_am">Manager of:</label>
                <select id="who_am" name="w" class="form-control select5">
                    <option value="Affiliate" <?php if ($who === 'Affiliate') echo 'selected' ?> >Affiliate</option>
                    <option value="Network" <?php if ($who === 'Network') echo 'selected' ?>>Network</option>
                </select>
            </div>
            <?= dateSpan()?>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <h5 class="py-3">Statistics by <?= $who ?>'s AM</h5>
        <div id="stats_block"></div>
        <hr/>
        <div class="table-responsive">
            <table id="AM_Stats" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>Admin</th>
                    <th>Visits</th>
                    <th>Sales</th>
                    <th>Revenue</th>
                    <th>Profit</th>
                    <th>Incentive</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $series = $cate = [];
                $series[0]['name'] = 'Revenue';
                $series[1]['name'] = 'Incentive';
                $series[2]['name'] = 'Profit';
                foreach ($form as $v):


                    if ($v->conver > 0) {
                        $cate[] = $v->name;
                        if ($who === 'Network') {
                            $series[0]['data'][] = (float)$v->pull;
                        } else {
                            $series[0]['data'][] = (float)$v->push;
                        }

                        $v->profit = $v->pull - $v->push;
                        if ($v->profit >= 100) {
                            $v->incentive = $v->profit * .02;
                        }

                        $series[1]['data'][] = $v->incentive;
                        $series[2]['data'][] = $v->profit;
                    }

                    echo '<td>' . $v->name . '</td>';
                    echo '<td><span class="float-right">' . number_format($v->visit) . '</span></td>';
                    echo '<td><span class="float-right">' . number_format($v->conver) . '</span></td>';
                    echo '<td><span class="float-right">$' . number_format($v->push, 4) . '</span></td>';
                    echo '<td class="text-primary"><span class="float-right" style="font-weight: 600;">$' . number_format($v->profit, 4) . '</span></td>';
                    echo '<td class="text-success"><strong class="float-right">$' . number_format($v->incentive, 4) . '</strong></td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">

    var cate = <?php echo json_encode($cate) ?>,
        series = <?php echo json_encode($series) ?>;

    function bar2Spline(id, cate, series, statsfor, s1, s2) {
        Highcharts.chart(id, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Stats for ' + statsfor
            },
            xAxis: [{
                categories: cate,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} $',
                    style: {
                        color: Highcharts.getOptions().colors[5]
                    }
                },
                title: {
                    text: s1,
                    style: {
                        color: Highcharts.getOptions().colors[5]
                    }
                }

            }, { // Secondary yAxis
                gridLineWidth: 0,
                title: {
                    text: 'Incentive',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} $',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }, { // Tertiary yAxis
                gridLineWidth: 0,
                title: {
                    text: s2,
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                labels: {
                    format: '{value} $',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            series: [{
                name: series[1].name,
                type: 'column',
                yAxis: 1,
                data: series[1].data,
                tooltip: {
                    valueSuffix: ' $'
                }

            }, {
                name: series[2].name,
                type: 'bar',
                yAxis: 2,
                data: series[2].data,
                marker: {
                    enabled: false
                },
                dashStyle: 'shortdot',
                tooltip: {
                    valueSuffix: ' $'
                }

            }, {
                name: series[0].name,
                type: 'spline',
                data: series[0].data,
                tooltip: {
                    valueSuffix: ' $'
                },
                color: Highcharts.getOptions().colors[5]
            }]
        });
    }

    bar2Spline('stats_block', cate, series, "<?=$who?>'s AM", 'Revenue', 'Profit');

    $('a#stats').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    var table = $('#AM_Stats');
    table.DataTable({
        "order": [[2, "desc"]]
    });
</script>