<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 2:17 PM
 */

use helper\Request as R;
use helper\Tool;

$affy = false;
if (\controller\Controller::getSessionType() === 'ADMIN') {
    $affy = true;
}
$date = R::getField('date', 'GET');

?>
<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<?= datatables() ?>
<style>
    .dashboard-stat {
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number {
        font-size: 2rem;
    }

    .number cite {
        font-size: 1.5rem;
        color: #ddd;
    }

    .dashboard-stat .desc {
        padding: .3rem .7rem;
    }

    .number {
        padding: .8rem .6rem .2rem;
    }

    .timepicker-picker, .noshow {
        display: none;
    }

    .ded_input {
        width: 70px;
    }

    .datepicker-days, .datepicker-months {
        display: none !important;
    }

    .datepicker-years {
        display: block !important;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Monthly Statistics</h3>
        <hr/>
        <form class="row py-3">
            <?= tknInp() ?>
            <?php if ($affy) { ?>
                <div class="col-md-3">
                    <?= network() ?>
                </div>
                <div class="col-md-3">
                    <?= affiliate() ?>
                </div>
            <?php } ?>
            <div class="col-md-1">
                <label>Year:</label>
                <input id="date" class="form-control" type="text" name="date" autocomplete="off" value="<?= $date ?>"
                       placeholder="Month: YYYYMM">
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <div id="stats_block"></div>
        <hr/>
        <div class="table-responsive">
            <h4 class="my-4">Stats breakdown based on month</h4>
            <table id="Daily_Stats" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th width="1%"></th>
                    <th>Month</th>
                    <th>Visits (Total)</th>
                    <th>Visits (Verified)</th>
                    <th>Sales</th>
                    <th>Payout</th>
                    <?php if ($affy) { ?>
                        <th>Profit</th>
                        <th>Charged</th>
                    <?php } ?>
                    <th>EPC</th>
                    <th>CR%</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $series = $cate = [];
                $series[0]['name'] = 'Visits';
                $series[1]['name'] = 'Earnings';
                $series[2]['name'] = 'CR';
                foreach ($form as $v):

                    $month = Tool::getMonthName((int)substr($v->month_, 4, 2));
                    $cr = ($v->conver / $v->visit) * 100;

                    $cate[] = $month;
                    $series[0]['data'][] = (int)$v->visit;
                    if ($affy) {
                        $series[1]['data'][] = $v->pull - $v->push;
                    } else {
                        $series[1]['data'][] = (float)$v->push;
                    }
                    $series[2]['data'][] = $cr;
                    echo '<tr><td></td>';
                    if ($affy) {
                        echo '<td><a href="' . s() . '/dailyStats?q=c&date=' . $v->month_ . '01&date1=' . $v->month_ . '31">' . $month . '</td>';
                    } else {
                        echo '<td><a href="' . s() . '/dailyStats?q=c&date=' . $v->month_ . '01&date1=' . $v->month_ . '31' . tkn('&') . '">' . $month . '</td>';
                    }
                    echo '<td>' . number_format($v->visit + $v->lost) . '</td>';
                    echo '<td>' . number_format($v->visit) . '</td>';
                    echo '<td>' . number_format($v->conver) . '</td>';
                    echo '<td class="text-success"><strong>$' . number_format($v->push, 4) . '</strong></td>';
                    $amount = $v->push;
                    if ($affy) {
                        $amount = $v->pull;
                        echo '<td class="text-primary"><strong>$' . number_format($v->pull - $v->push, 4) . '</strong></td>';
                        echo '<td>$' . number_format($v->pull, 4) . '</td>';
                    }
                    echo '<td>' . sprintf('%.4f', $amount / $v->visit) . '</td>';
                    echo '<td>' . sprintf('%.4f', $cr) . '</td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="<?php echo MY_SERVER ?>assets/js/stats.js"></script>
<script type="text/javascript">

    var cate = <?php echo json_encode($cate) ?>,
        series = <?php echo json_encode($series) ?>;

    bar2Spline('stats_block', cate, series, 'Monthly', 'Visits', 'CR');

    $('a#stats').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    $('#date').datetimepicker({
        format: 'YYYY',
        changeMonth: true,
        showButtonPanel: true
    });

    var table = $('#Daily_Stats');
    table.tableExport({
        headers: true,
        // footers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'top',
        // ignoreRows: null,
        // ignoreCols: null,
        trimWhitespace: true
    });
    table.DataTable();
</script>