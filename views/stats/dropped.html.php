<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 2:17 PM
 */

use helper\Template as T;

?>
<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<?= datatables() ?>
<?= datatables_responsive() ?>
<style>
    .dashboard-stat{
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number{
        font-size: 2rem;
    }
    .number cite{
        font-size: 1.5rem;
        color: #ddd;
    }
    .dashboard-stat .desc{
        padding: .3rem .7rem;
    }
    .number{
        padding: .8rem .6rem .2rem;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Dropped Statistics</h3>
        <hr/>
        <form class="row py-3">
            <div class="col-md-3">
                <?= campaign() ?>
            </div>
            <div class="col-md-3">
                <?= affiliate() ?>
            </div>
            <div class="col-md-3">
                <?= network() ?>
            </div>
            <?= dateSpan() ?>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <div class="table-responsive">
            <h4 class="my-4">Stats breakdown based on conversions (dropped)</h4>
            <table id="Conversion_Stats" class="table table-striped dt-responsive">
                <thead class="thead-dark">
                <tr>
                    <th>-</th>
                    <th>#</th>
                    <th>ID</th>
                    <th>TID</th>
                    <th>Offer</th>
                    <th>Payout</th>
                    <th>Profit</th>
                    <th>Charged</th>
                    <th>Affiliate</th>
                    <th>Source</th>
                    <th class="none">Network</th>
                    <th class="none">Admin_id</th>
                    <th>Reason</th>
                    <th>Time click</th>
                    <th>Time sale</th>
                    <th class="none">Offer</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form as $k => $v):
                    $tkn = '';
                    if (isset($_GET[TKN])){
                        $tkn = '&'.TKN;
                    }
                    ?>
                    <tr>
                        <td></td>
                        <td><?=$k+1?></td>
                        <td><?=$v->click_id?></td>
                        <td><?=$v->tid?></td>
                        <td><a href="./offers/view?id=<?=$v->offers_ID .$tkn?>"><?=$v->offers_ID?></a></td>
                        <td class="text-success"><code>$</code><strong><?=sprintf('%.4f', $v->payout)?></strong></td>
                        <td class="text-info"><code>$</code><strong><?=sprintf('%.4f', $v->charge - $v->payout)?></strong></td>
                        <td><code>$</code><strong><?=sprintf('%.4f', $v->charge)?></strong></td>
                        <td><?=$v->affiliate?></td>
                        <td><?= $v->source ?></td>
                        <td><?=$v->network?></td>
                        <td><?=$v->admin_ID?></td>
                        <td><?=$v->reason?></td>
                        <td><?=T::getDate($v->date_click)?></td>
                        <td><?=T::getDate($v->date_sale)?></td>
                        <td><?=$v->offer?></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script type="text/javascript">

    $('a#stats').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    var table = $('#Conversion_Stats');
    table.tableExport({
        headers: true,
        // footers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'top',
        // ignoreRows: null,
        // ignoreCols: null,
        trimWhitespace: true
    });
    var dt = table.DataTable();
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'hover click',
        placement: 'top'
    });
    dt.on('draw', function () {
        $('[data-toggle="popover"]').popover({
            html: true,
            trigger: 'hover click',
            placement: 'top'
        });
    });
</script>