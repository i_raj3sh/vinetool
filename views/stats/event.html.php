<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 5:37 PM
 */
?>
<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<?= datatables() ?>

<style>
    .dashboard-stat {
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number {
        font-size: 2rem;
    }

    .number cite {
        font-size: 1.5rem;
        color: #ddd;
    }

    .dashboard-stat .desc {
        padding: .3rem .7rem;
    }

    .number {
        padding: .8rem .6rem .2rem;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Statistics by event</h3>
        <hr/>
        <form class="row py-3">
            <div class="col-md-2">
                <?= network() ?>
            </div>
            <div class="col-md-2">
                <?= affiliate() ?>
            </div>
            <div class="col-md-3">
                <?= campaign() ?>
            </div>
            <div class="col-md-2">
                <?= event() ?>
            </div>
            <?= dateSpan() ?>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <div id="stats_block"></div>
        <hr/>
        <div class="table-responsive">
            <h4 class="my-4">Stats breakdown based on events</h4>
            <table id="Affiliate_Stats" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>Event</th>
                    <th>Sales</th>
                    <th>Charge</th>
                    <th>Payout</th>
                    <th>Profit</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $series = $cate = [];
                $series[0]['name'] = 'Sales';
                $series[1]['name'] = 'Profit';
                $series[2]['name'] = '';
                foreach ($form['table'] as $v):

                    if ($v->conver > 0) {
                        $cate[] = $v->event;
                        $series[0]['data'][] = (int)$v->conver;
                        $series[1]['data'][] = $v->pull - $v->push;
                    }

                    echo '<td>' . $v->event . '</td>';
                    echo '<td>' . number_format($v->conver) . '</td>';
                    echo '<td>$' . number_format($v->pull, 4) . '</td>';
                    echo '<td>$' . number_format($v->push, 4) . '</td>';
                    echo '<td class="text-success"><strong>$' . number_format($v->pull - $v->push, 4) . '</strong></td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="<?= s() ?>assets/js/stats.js"></script>
<script type="text/javascript">

    var cate = <?php echo json_encode($cate) ?>,
        series = <?php echo json_encode($series) ?>;

    bar2Spline('stats_block', cate, series, 'Events', 'Sales', '');

    $('a#stats').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    var table = $('#Affiliate_Stats');
    table.tableExport({
        headers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'top',
        trimWhitespace: true
    });
    table.DataTable({
        "order": [[4, "desc"]]
    });
</script>