<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 26/1/18
 * Time: 7:20 PM
 */

use helper\Request;

?>
<?= select2() ?>
<?= datepicker() ?>
<style>
    .dashboard-stat{
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number{
        font-size: 2rem;
    }
    .number cite{
        font-size: 1.5rem;
        color: #ddd;
    }
    .dashboard-stat .desc{
        padding: .3rem .7rem;
    }
    .number{
        padding: .8rem .6rem .2rem;
    }
    .timepicker-picker,.dater,.dated{display: none}
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Compare Statistics</h3>
        <hr/>
        <form class="row py-3">
            <div class="col-6">
                <div class="row">
                    <div class="col-md-12">
                        <label>Stats from:</label>
                        <select id="date_type" class="form-control" name="q">
                            <option value="t" data-val="<?php echo date('Ymd') ?>" <?php if (Request::getField('q','GET') === 't') echo 'selected' ?>>Today</option>
                            <option value="y" data-val="<?php echo date('Ymd',strtotime('-1 day')) ?>" <?php if (Request::getField('q','GET') === 'y') echo 'selected' ?>>Yesterday</option>
                            <option value="w"
                                    data-val="<?php echo date('Ymd', strtotime('-6 days')) . '_' . date('Ymd') ?>" <?php if (Request::getField('q', 'GET') === 'w') echo 'selected' ?>>
                                Last 7 days
                            </option>
                            <option value="p"
                                    data-val="<?php echo date('Ymd', strtotime('-13 days')) . '_' . date('Ymd', strtotime('-7 days')) ?>" <?php if (Request::getField('q', 'GET') === 'p') echo 'selected' ?>>
                                Last Week
                            </option>
                            <option value="m" data-val="<?php echo date('Ym') ?>" <?php if (Request::getField('q','GET') === 'm') echo 'selected' ?>>This Month</option>
                            <option value="l" data-val="<?php echo date('Ym',strtotime('last month')) ?>" <?php if (Request::getField('q','GET') === 'l') echo 'selected' ?>>Last Month</option>
                            <option value="c" data-val="" <?php if (Request::getField('q','GET') === 'c') echo 'selected' ?>>Custom</option>
                        </select>
                    </div>
                    <div class="w-100 dater"></div>
                    <div class="col-md-6 dater">
                        <label>Start Date:</label>
                        <input id="date0" class="form-control" type="text" name="date" value="<?php echo Request::getField('date','GET') ?>" placeholder="Date Start: YYYYMMDD">
                    </div>
                    <div class="col-md-6 dater">
                        <label>End Date:</label>
                        <input id="date1" class="form-control" type="text" name="date1" value="<?php echo Request::getField('date1','GET') ?>" placeholder="Date End: YYYYMMDD">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-md-12">
                        <label>Stats to:</label>
                        <select id="date_type1" class="form-control" name="qq">
                            <option value="tt" data-val="<?php echo date('Ymd') ?>" <?php if (Request::getField('qq','GET') === 'tt') echo 'selected' ?>>Today</option>
                            <option value="yy" data-val="<?php echo date('Ymd',strtotime('-1 day')) ?>" <?php if (Request::getField('qq','GET') === 'yy') echo 'selected' ?>>Yesterday</option>
                            <option value="ww"
                                    data-val="<?php echo date('Ymd', strtotime('-6 days')) . '_' . date('Ymd') ?>" <?php if (Request::getField('qq', 'GET') === 'ww') echo 'selected' ?>>
                                Last 7 days
                            </option>
                            <option value="pp"
                                    data-val="<?php echo date('Ymd', strtotime('-13 days')) . '_' . date('Ymd', strtotime('-7 days')) ?>" <?php if (Request::getField('qq', 'GET') === 'pp') echo 'selected' ?>>
                                Last Week
                            </option>
                            <option value="mm" data-val="<?php echo date('Ym') ?>" <?php if (Request::getField('qq','GET') === 'mm') echo 'selected' ?>>This Month</option>
                            <option value="ll" data-val="<?php echo date('Ym',strtotime('last month')) ?>" <?php if (Request::getField('qq','GET') === 'll') echo 'selected' ?>>Last Month</option>
                            <option value="cc" data-val="" <?php if (Request::getField('qq','GET') === 'cc') echo 'selected' ?>>Custom</option>
                        </select>
                    </div>
                    <div class="w-100 dated"></div>
                    <div class="col-md-6 dated">
                        <label>Start Date:</label>
                        <input id="date00" class="form-control" type="text" name="date0" value="<?php echo Request::getField('date0','GET') ?>" placeholder="Date Start: YYYYMMDD">
                    </div>
                    <div class="col-md-6 dated">
                        <label>End Date:</label>
                        <input id="date11" class="form-control" type="text" name="date10" value="<?php echo Request::getField('date10','GET') ?>" placeholder="Date End: YYYYMMDD">
                    </div>
                </div>
            </div>
            <div class="col-md-6 pt-3">
                <div class="form-group">
                    <label>Offer ID:</label>
                    <input type="text" name="of" value="<?php echo Request::getField('of','GET') ?>" class="form-control" placeholder="Offer ID" />
                </div>
            </div>
            <div class="col-md-5 pt-3">
                <?= affiliate() ?>
            </div>
            <div class="col-md-1">
                <div style="margin: 2.6rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
            <hr/>
        <div id="stats_block" class="mt-5"></div>
        <div id="stats_block0" class="mt-5"></div>
    </div>
</section>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="<?php echo MY_SERVER ?>assets/js/stats.js"></script>
<script type="text/javascript">
    var cate = <?php echo $form['cate'] ?>,
        series = <?php echo $form['series'] ?>,
        cate0 = <?php echo $form['cate0'] ?>,
        series0 = <?php echo $form['series0'] ?>,
        statsfor = '<?php echo $form['for'] ?>',
        statsfor0 = '<?php echo $form['for0'] ?>';
    dash('stats_block',cate,series,statsfor);
    dash('stats_block0',cate0,series0,statsfor0);

    $('a#stats').addClass('active');
    $('#date0,#date1,#date00,#date11').datetimepicker( {
        format:'YYYYMMDD'
    });

    $('.selector').select2({
        placeholder: "--select--",
        allowClear: true
    });

    $('#date_type').on('change',function (e) {

        var vu = $(this).find(':selected').data('val')+'', $v = vu.split('_'),
            $date0 = $('#date0'),
            $date1 = $('#date1'),
            $dater = $('.dater');

        if ($v.length === 2){
            $date0.val($v[0]);
            $date1.val($v[1]);
        }else{
            $date0.val($v);
            $date1.val('');
        }

        if (this.value === 'c'){
            $dater.slideDown().find("input").attr("required","true");
        }else {
            $dater.slideUp().find("input").removeAttr("required");
        }
    });
    $('#date_type1').on('change',function (e) {

        var vu = $(this).find(':selected').data('val')+'', $v = vu.split('_'),
            $date0 = $('#date00'),
            $date1 = $('#date11'),
            $dater = $('.dated');

        if ($v.length === 2){
            $date0.val($v[0]);
            $date1.val($v[1]);
        }else{
            $date0.val($v);
            $date1.val('');
        }

        if (this.value === 'cc'){
            $dater.slideDown().find("input").attr("required","true");
        }else {
            $dater.slideUp().find("input").removeAttr("required");
        }
    });
    <?php if (Request::getField('q','GET') === 'c') echo '$(".dater").slideDown().find("input").attr("required","true");' ?>
    <?php if (Request::getField('qq','GET') === 'cc') echo '$(".dated").slideDown().find("input").attr("required","true");' ?>
</script>