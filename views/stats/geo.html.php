<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 4:47 PM
 */


$affy = false;
if (\controller\Controller::getSessionType() === 'ADMIN'){
    $affy = true;
}

?>
<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<?= datatables() ?>
<style>
    .dashboard-stat{
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number{
        font-size: 2rem;
    }
    .number cite{
        font-size: 1.5rem;
        color: #ddd;
    }
    .dashboard-stat .desc{
        padding: .3rem .7rem;
    }
    .number{
        padding: .8rem .6rem .2rem;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Statistics by geo</h3>
        <hr/>
        <form class="row py-3">
            <?=tknInp()?>
            <?php if ($affy){ ?>
                <div class="col-md-3">
                    <?= affiliate() ?>
                </div>
            <?php } ?>
            <?= dateSpan() ?>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <div id="stats_block"></div>
        <hr/>
        <div class="table-responsive">
            <h4 class="my-4">Stats breakdown based on countries</h4>
            <table id="Geo_Stats" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>Country</th>
                    <th>Visits (Total)</th>
                    <th>Visits (Verified)</th>
                    <th>Sales</th>
                    <th>Payout</th>
                    <?php if ($affy){ ?>
                        <th>Profit</th>
                        <th>Charged</th>
                    <?php } ?>
                    <th>EPC</th>
                    <th>CR%</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $series = $cate = [];
                $series[0]['name'] = 'Visits';
                $series[1]['name'] = 'Earnings';
                $series[2]['name'] = 'CR';
                foreach ($form as $v):

if($v->visit == 0) $v->visit = 1;

                    $cr = ($v->conver / $v->visit) * 100;

                    if ($v->pull > 0) {
                        $cate[] = $v->country;
                        $series[0]['data'][] = (int)$v->visit;
                        if ($affy) {
                            $series[1]['data'][] = $v->pull - $v->push;
                        } else {
                            $series[1]['data'][] = (float)$v->push;
                        }
                        $series[2]['data'][] = $cr;
                    }

                    echo '<td>'.$v->country.'</td>';
                    echo '<td>' . number_format($v->visit + $v->lost) . '</td>';
                    echo '<td>'.number_format($v->visit).'</td>';
                    echo '<td>'.number_format($v->conver).'</td>';
                    echo '<td class="text-success"><strong>$'.number_format( $v->push,4).'</strong></td>';
                    $amount = $v->push;
                    if ($affy) {
                        $amount = $v->pull;
                        echo '<td class="text-primary"><strong>$' . number_format($v->pull - $v->push,4) . '</strong></td>';
                        echo '<td>$' . number_format( $v->pull,4) . '</td>';
                    }
                    echo '<td>'.sprintf('%.4f', $amount / $v->visit).'</td>';
                    echo '<td>' . sprintf('%.4f', $cr) . '</td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="<?php echo MY_SERVER ?>assets/js/stats.js"></script>
<script type="text/javascript">

    var cate = <?php echo json_encode($cate) ?>,
        series = <?php echo json_encode($series) ?>;

    bar2Spline('stats_block', cate, series, 'Geo', 'Visits', 'CR');

    $('a#stats').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    var table = $('#Geo_Stats');
    table.tableExport({
        headers: true,
        // footers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'top',
        // ignoreRows: null,
        // ignoreCols: null,
        trimWhitespace: true
    });
    table.DataTable({
        "order": [[3, "desc"]]
    });
</script>