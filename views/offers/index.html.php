<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 3:52 PM
 */

use controller\Controller as C;
use controller\OfferBlock as OB;
use helper\Request as R;
use helper\Template as T;
use helper\Tool as G;

$ses_type = (C::getSessionType() === C::ADMIN);


function offer_name($n, $p){
    $n = mb_strimwidth($n, 0, 40, '&hellip;');
    if($p === '1'){
        return '<strong>'.$n.'</strong>';
    }
    return $n;
}

?>

<?= select2() ?>
<?= tableexport() ?>
<?= datepicker() ?>

<style>
    .select2{
        width: 100% !important;
    }
    .table th a{
        color: white;
    }
    .table th a:hover{
        text-decoration: none;
    }
    .ellipsis {
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
    }
    .popert-adder{
        max-height:200px;
        overflow-y: auto;
    }

    span.filter-tag {
        margin-left: 5px;
        background: #6f736e;
        color: #dee4e8;
        padding: .4rem .8rem .4rem .5rem;
        border-radius: 15px;
        cursor: pointer;
    }

    span.filter-tag b {
        margin: .3rem .5rem .4rem .2rem;
        font-size: 1rem;
        color: white;
    }
    .copy_cat {
        display: flex;
        background: #eee;
        border: 1px solid #bbb;
        padding: 10px 12px;
        border-radius: 5px;
        margin-top: 5px;
        font-family: monospace !important;
    }

    .copy_cat textarea {
        width: 100%;
        border: none;
        background: none;
        outline: none;
        resize: none;
        font-size: 0.8rem;
    }
</style>
<script>
    var offerIds = [];
</script>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">All Available Offers
            <?php if ($ses_type) { ?>
                <a class="btn btn-sm btn-primary float-right" href="<?= MY_SERVER ?>offers/edit"><i class="fa fa-plus"></i> Offer</a>
            <?php } ?>
        </h3>
        <div class="mb-3">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#offerFilter"
                    aria-expanded="false" aria-controls="Offer Filter">
                <i class="fa fa-filter text-light"> Filter</i>
            </button>
            <?= $form['tags'] ?></div>
        <div id="offerFilter" class="collapse">
            <form id="killer" class="row pb-3">
                <?=tknInp()?>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Offer type:</label>
                        <select id="kinda" name="t" class="form-control selected5">
                            <option value="">--select--</option>
                            <option value="CPI" <?php T::option(R::getField('t'),'CPI') ?>>CPI</option>
                            <option value="CPA" <?php T::option(R::getField('t'),'CPA') ?>>CPA</option>
                            <option value="CPL" <?php T::option(R::getField('t'),'CPL') ?>>CPL</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-5">
                    <?= campaign() ?>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Active:</label>
                        <select id="state" name="h" class="form-control selected5">
                            <option value="">--select--</option>
                            <option value="1" <?php T::option(R::getField('h'),'1') ?> >active</option>
                            <option value="0" <?php T::option(R::getField('h'),'0') ?> >inactive</option>
                            <option value="2" <?php T::option(R::getField('h'), '2') ?> >paused</option>
                            <option value="3" <?php T::option(R::getField('h'), '3') ?> >capped</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label style="width: 100%">Payout:</label>
                        <input id="above" type="text" class="form-control" name="r"
                               value="<?= R::getField('r', 'GET') ?>" placeholder="min $"
                               style="display: inline-block;width: 45%">
                        <input id="below" type="text" class="form-control" name="j"
                               value="<?= R::getField('j', 'GET') ?>" placeholder="max $"
                               style="display: inline-block;width: 45%">
                    </div>
                </div>
                <div class="w-100"></div>
                <?php if ($ses_type) { ?>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Adv ID:</label>
                            <input id="adv" type="text" class="form-control" name="m"
                                   value="<?= R::getField('m', 'GET') ?>"
                                   placeholder="Adv ID">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label>Network:</label>
                            <?php
                            $geo = R::getField('f', 'GET');

                            $g = new \model\Network();
                            $g->setProperties([
                                'active' => 1
                            ]);
                            $g->setQueryParameters($g);
                            $all = $g->query();
                            ?>
                            <select id="netw" name="f[]" class="form-control selected5" multiple>
                                <?php foreach ($all as $item): ?>
                                    <option value="<?= $item->ID ?>" <?php T::option($geo, $item->ID) ?> ><?= $item->name . " ($item->company)" ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Assigned:</label>
                            <select id="assign" name="e" class="form-control selected5">
                                <option value="">--select--</option>
                                <option value="1" <?php T::option(R::getField('e'), '1') ?> >assigned</option>
                                <option value="2" <?php T::option(R::getField('e'), '2') ?> >not assigned</option>
                            </select>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Name:</label>
                        <input id="nami" type="text" class="form-control" name="n"
                               value="<?= R::getField('n', R::GET) ?>" placeholder="Offer name">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Geos:</label>
                        <?php
                        $geo = R::getField('g','GET');

                        $g = new \model\Country();
                        $g->setQueryParameters($g);
                        $all = $g->query();
                        ?>
                        <select id="gea" name="g[]" class="form-control selected5" multiple>
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->code ?>" <?php T::option($geo,$item->code) ?> ><?= $item->code.': '.$item->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Devices:</label>
                        <?php
                        $geo = R::getField('d','GET');

                        $g = new \model\Device();
                        $g->setQueryParameters($g);
                        $all = $g->query();
                        ?>
                        <select id="davi" name="d[]" class="form-control selected5" multiple>
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->name ?>" <?php T::option($geo,$item->name) ?> ><?= $item->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Category:</label>
                        <?php
                        $geo = R::getField('c','GET');

                        $g = new \model\Category();
                        $g->setQueryParameters($g);
                        $all = $g->query();
                        ?>
                        <select id="coti" name="c[]" class="form-control selected5" multiple>
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->name ?>" <?php T::option($geo,$item->name) ?> ><?= $item->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Bundle:</label>
                        <input id="appi" type="text" class="form-control" name="z"
                               value="<?= R::getField('z', R::GET) ?>" placeholder="Bundle id">
                    </div>
                </div>
                <?= dateSpan('Date added', true) ?>
                <div class="col-md-1">
                    <div style="margin: 1.7rem"></div>
                    <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </div>
        <div class="w-100"></div>
        <span><?php if (!$ses_type) {?><label style="font-size: 1.1rem">With selected:</label>
                <a id="checkedReq" class="btn btn-sm btn-info mb-1" style="font-size: .7rem;padding: .2rem .6rem" href="#!"><i class="fa fa-share"></i> request</a>
                <script>
            $(document).ready(function () {
                var c = $('.checker');
                $('input#allbox').on('change', function () {
                    if (this.checked) {
                        c.prop('checked', true);
                    } else {
                        c.prop('checked', false);
                    }
                });
                $('a#checkedReq').on('click', function () {
                    var p = [];
                    c.each(function (i, j) {
                        if (j.checked)
                            p.push(j.id.split('$')[1]);
                    });
                    jax('reqAll', p);
                });
            });
        </script>
            <?php } else { ?>
            <label style="font-size: 1.1rem">With selected:</label>
<!--                <a id="checkedDel" class="btn btn-sm btn-info mb-1" style="font-size: .7rem;padding: .2rem .6rem" href="#!"><i class="fa fa-close"></i> delete</a>-->
                <a id="checkedPause" class="btn btn-sm btn-warning mb-1" style="font-size: .7rem;padding: .2rem .6rem" href="#!"><i class="fa fa-pause"></i> pause</a>
                <a id="checkedAssign" class="btn btn-sm btn-success mb-1" style="font-size: .7rem;padding: .2rem .6rem" href="#!"><i class="fa fa-plus"></i> assign to pub</a>
            <script>
            $(document).ready(function () {
                var c = $('.checker');
                $('input#allbox').on('change',function () {
                    if (this.checked){
                        c.prop('checked',true);
                    }else{
                        c.prop('checked',false);
                    }
                });
                $('a#checkedDel').on('click',function () {
                    var p = [];
                    c.each(function (i,j) {
                        if(j.checked)
                            p.push(j.id.split('$')[1]);
                    });
                    jax('delAll',p);
                });
                $('a#checkedPause').on('click', function () {
                    var p = [];
                    c.each(function (i, j) {
                        if (j.checked)
                            p.push(j.id.split('$')[1]);
                    });
                    jax('pauseAll', p);
                });

                $('a#checkedAssign').on('click', function () {
                    var p = [];
                    c.each(function (i, j) {
                        if (j.checked)
                            p.push(j.id.split('$')[1]);
                    });
                    var w = p+'';
                    if (w) {
                        $('#assignToPub').modal('show');
                    }
                });
            });
        </script>
            <?php } ?>
            <span class="float-right"><strong>Total</strong>: <label id="atTotal"
                                                                     class="badge badge-info"><?= $form['total'] ?></span>
        <div class="table-responsive">
            <table id="Vinetool_Offers" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <?php if (!$ses_type) {?>
                        <th class="tableexport-ignore"><input id="allbox" class="form-control" type="checkbox"></th>
                    <?php }else{ ?>
                        <th class="tableexport-ignore"><input id="allbox" class="form-control" type="checkbox"></th>
                        <th class="tableexport-ignore"><i class="fa fa-circle"  style="font-size: 1rem"></i></th>
                    <?php } ?>
                    <th><a <?= G::getSort('ID') ?>> ID</nobr></a></th>
                    <th class="tableexport-ignore">Logo</th>
                    <th><a <?= G::getSort('name') ?>> Title</nobr></a></th>
                    <th><a <?= G::getSort('charge') ?>> Payout</nobr></a></th>
                    <th><a <?= G::getSort('caps') ?>> Cap</nobr></a></th>
                    <th><a <?= G::getSort('geo') ?>> Geo</nobr></a></th>
                    <th><a <?= G::getSort('dev') ?>> OS</nobr></a></th>
                    <?php if ($ses_type) { ?>
                        <th class="tableexport-ignore">Charge</th>
                        <th class="tableexport-ignore">AdvID</th>
                        <th class="tableexport-ignore">Adv</th>
                    <?php } else { ?>
                        <th class="tableexport-ignore">Preview</th>
                    <?php } ?>
                    <th class="tableexport-ignore">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):?>
                <tr>
                    <?php if (!$ses_type) {

                        if ($v->charge < 1) {
                            continue;
                        }

                        if (OB::isBlocked($v->ID)) {
                            continue;
                        }
                        ?>
                        <td class="tableexport-ignore"><input id="O$<?=$v->ID?>" class="form-control checker" type="checkbox"></td>
                    <?php }else { ?>
                        <td class="tableexport-ignore"><input id="O$<?=$v->ID?>" class="form-control checker" type="checkbox"></td>
                        <td class="tableexport-ignore" style="padding-left: .7rem !important"><?=T::state($v->active)?></td>
                    <?php }?>
                    <td><strong><?=$v->ID?></strong></td>
                    <td class="tableexport-ignore lazy-load" data-src="<?=$v->icon?>" data-toggle="popover" data-content="<img src='<?=$v->icon?>' width=100%  />"></td>
                    <td><a href="<?php C::href('offers/view?id=' . $v->ID) ?>" title="<?= $v->name ?>"
                           data-toggle="tooltip" class="<?php if ($v->device_id === '1') {
                            echo 'text-danger';
                        } ?>"><?= offer_name($v->name, $v->private) ?></a></td>
                    <td style="font-weight: 600"><code>$</code><?=($v->charge * COMMISSION)?></td>
                    <td><?= $v->sale ?><?= T::nums($v->caps) ?></td>
                    <td data-toggle="tooltip"
                        title="<?= T::wordBreak($v->geo, ', ') ?>"><?= T::geoFormat($v->geo) ?></td>
                    <td><?= T::wordBreak(T::devIcons($v->dev, $v->min_os_ver), BR) ?></td>
                    <?php if ($ses_type) {
                        $test = T::offerTest($v);
                        $state = T::stateButton($v); ?>

                        <td class="tableexport-ignore"><code>$</code><?=$v->charge?></td>
                        <td class="tableexport-ignore"><?=$v->advID?></td>
                        <td class="tableexport-ignore"><?=$v->network_ID?></td>
                        <td class="tableexport-ignore">
                            <div class="btn-group">

<!--                                <a href="#!" class="btn btn-success popert"-->
<!--                                   title="Tracking link"-->
<!--                                   data-content="<span class='copy_cat'><textarea id='popUrl' rows='2' readonly>--><!--</textarea><br/><button type='button' id='copyBack' class='btn btn-dark btn-sm' title='Link is copied'><i class='fa fa-copy'></i></button></span>"><i class="fa fa-link"></i></a>-->

                                <a href="<?=$v->preview?>" target="_blank" class="btn btn-primary grey-bg" data-toggle="tooltip" title="preview"><i class="fa fa-eye"></i></a>
                                <a class="btn btn-info btn-sm" href="<?=s()?>offers/edit?id=<?=$v->ID?>" data-toggle="tooltip" title="edit"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn-danger btn-sm" href="#!" onclick=delet("<?=s()?>offers/del?id=<?=$v->ID?>") data-toggle="tooltip" title="delete"><i class="fa fa-remove"></i></a>
                                <?=$state?>
                                <a href="<?=$test?>" target="_blank" class="btn btn-default purple-bg text-white" data-toggle="tooltip" title="test tracking link"><i class="fa fa-circle-o"></i></a>
                                <a href="#!" class="btn btn-default popert" data-toggle="tooltip"
                                   title="publishers assigned"
                                   data-content="<div id='pub_list-<?= $v->ID ?>' class='popert-adder' ></div><script>callPub('<?= $v->ID ?>',<?= $v->charge ?>);</script>"><i
                                            class="fa fa-user"></i> <span id="badge-count-<?= $v->ID ?>"
                                                                          class="badge badge-dark"></span></a>
                            </div></td>
                    <?php }else{
                        echo '<td class="tableexport-ignore"><a href="' . $v->preview . '" target="_blank">preview</a></td>';
                        T::affiliateStatus($v->ID);
                    }?>
                </tr>
                <script>
                    offerIds.push(<?=$v->ID?>);
                </script>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?= $form['pages']; echo $form['perPage'] ?>
        </div>
    </div>
</section>
<div class="modal fade" id="assignToPub" role="dialog" aria-labelledby="assignToPub" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Assign to the selected affiliate</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="bodyAssign" class="modal-body">
                <?=affiliate()?>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <input id="percentAssign" class="form-control" type="number" placeholder="Percentage"
                                   value="<?= COMMISSION * 100 ?>"/>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <input id="capAssign" class="form-control" type="number" placeholder="Cap (Default: Unlimited)"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="saveModelAssign" type="button" class="btn btn-primary">Assign</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function delet(loc, conf) {
        conf = confirm("Are you sure? Do you want to delete?");

        if (conf){
            window.location = loc;
        }
    }
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true,
        templateSelection: formatState
    });
    function formatState (state) {
        if (state.text.indexOf(':') === -1) {
            return state.text;
        }
        var code = state.text.toLowerCase().split(':')[0];
        return $(
            '<span class="f16"><i class="flag ' + code + '"></i> ' + state.text + '</span>'
        );
    };

    $('table').tableExport({
        headers: true,
        // footers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'bottom',
        // ignoreRows: null,
        // ignoreCols: null,
        trimWhitespace: true
    });

    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover',
        placement: 'top'
    });

    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'hover',
        placement: 'right'
    });

    function jax(a, b) {
        b = b+'';
        if (b) {
            window.location = '<?=s()?>offers/' + a + '?ids=' + b + '<?=tkn('&')?>';
        }
    }

    setTimeout(function () {
        var lazy = $('.lazy-load');
       lazy.each(function (i,j) {
           $(j).html('<img src="'+$(j).data('src')+'" width="20" height="20" />')
       })
    },2000);

    <?php if($ses_type){ ?>

    $('.popert').popover({
        html: true,
        trigger: 'click',
        placement: 'top'
    })
    //     .on('show.bs.popover', function() {
    //     var s = document.getElementById('popUrl');
    //     s.select();
    //     document.execCommand('copy');
    //     $('body').find('#copyBack').on('click', function () {
    //     }).tooltip({
    //         trigger: 'click'
    //     });
    // })

    function callPub(id,c) {
        var o = $('#pub_list-'+id), str = 'Not assigned', fa;
        $.getJSON("<?=s()?>approveOffers/getPub?id="+id,function (data) {
            if (data){
                str = '<table class="table table-striped">' +
                    '<thead><tr>' +
                    '<th>AffID</th>'+
                    '<th>Payout</th>'+
                    '<th>Action</th>'+
                    '</tr></thead>'+
                    '<tbody>';
                for(var i in data){
                    if (data[i].status == 1){
                        fa = 'pause text-warning';
                    }else{
                        fa = 'play text-success';
                    }
                    str += '<tr><td>'+data[i].affid+'</td>' +
                        '<td><code>$</code>'+(data[i].per * c)+'</td>' +
                        '<td><a href="#!" class="btn btn-default btn-sm"><i class="fa fa-'+fa+'" data-line="'+data[i].status+'" onclick="toggleStatus(this,'+data[i].id+')"></i></a></td></tr>';
                }
                str += '</tbody></table>';
            }
            o.html(str);
        });
    }

    function toggleStatus(t, id, v) {
        v = $(t).data('line');
        if (v == '2') {
            v = '1';
        }else{
            v = '2';
        }
        var data = 'i=' + id + '&s=' + v;
        $.getJSON("<?=s()?>approveOffers/toggle?" + data, function (res) {
            if (res.error) {
                toastr["error"](res.error,"Error!");
            } else {
                if (v === '1'){
                    $(t).removeClass('text-success').removeClass('fa-play').addClass('text-warning').addClass('fa-pause');
                    toastr["success"]("Active","Splendid!");
                }else{
                    $(t).removeClass('text-warning').removeClass('fa-pause').addClass('text-success').addClass('fa-play');
                    toastr["success"]("Paused","Splendid!");
                }
                $(t).data('line',v);
            }
        });
    }

    $('#saveModelAssign').on('click',function () {
        var af = $('#bodyAssign').find('select[name="af"]').val(), per = $('#percentAssign').val(), cap = $('#capAssign').val(), p = [];
        $('.checker').each(function (i, j) {
            if (j.checked)
                p.push(j.id.split('$')[1]);
        });
        var w = p + '';
        var data = 'af='+af+'&p='+per+'&c='+cap+'&ids='+w;
        $.getJSON("<?=s()?>approveOffers/assign?" + data, function (res) {
            if (res.error) {
                toastr["error"](res.error,"Error!");
            } else {
                toastr["success"]("Assigned","Splendid!");
            }
            $('#assignToPub').modal('hide');
        });
    });

    $('body').on('click', function (e) {
        $('.popert').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    $.post('<?=s()?>/approveOffers/countPub','ids='+offerIds, function (res) {
        for (var i in res){
            $('#badge-count-'+res[i].id).text(res[i].count);
        }
    },'json');

    <?php } ?>

    $('.filter-tag').on('click', function () {
        let t = $(this).data('filter'),
            i = $(this).data('what');
        if (t === 'select') {
            $('#' + i).empty().trigger('change');
        }
        else if (t === 'date') {
            $('#date,#date1').val('');
            $('#date_type').empty();
        }
        else {
            $('#' + i).val('');
        }
        $(this).remove();
        setTimeout(function () {
            $('#killer').submit()
        }, 250);
    });
</script>
