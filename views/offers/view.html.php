<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/14/17
 * Time: 7:47 AM
 */

$ses_type = (\controller\Controller::getSessionType() === 'ADMIN');

use helper\Template as T;

?>
<style>
    .dex-offer{
        font-size: .8rem;
        background: #f1f1f1;
        border-radius: 5px;
        padding: .5rem 1rem;
    }

    .copy_cat {
        display: flex;
        background: #eee;
        border: 1px solid #bbb;
        padding: 10px 12px;
        border-radius: 5px;
        margin-top: 5px;
        font-family: monospace !important;
    }

    .copy_cat textarea {
        width: 100%;
        border: none;
        background: none;
        outline: none;
        resize: none;
        font-size: 0.8rem;
    }
</style>
<section class="row">
    <div class="col-12 pt-5">
        <div class="row">
            <div class="col-10">
                <p>Offer ID: #<?php $form->get('ID'); if($ses_type) { ?>
                    &nbsp;&nbsp;&nbsp;
                Adv ID: #<?php $form->get('advID'); } ?>
                </p>
            </div>
            <div class="col-2">
                <span class="float-right">
                    <?= T::offerState($form->fetch('active')) ?>
                </span>
            </div>
            <div class="col-md-2">
                <img src="<?php $form->get('icon') ?>" width="100%" />
            </div>
            <div class="col-md-9 pt-md-0 pt-3">
                <h3><?php $form->get('name') ?></h3>
                <p><?php $form->get('type') ?>&nbsp;&nbsp;&nbsp;<strong>$<?php $form->get('charge') ?></strong></p>
                <p><a href="<?php $form->get('preview') ?>" target="_blank"><?php $form->get('preview') ?></a></p>
            </div>
            <div class="w-100"></div>
            <div class="col-12 pt-md-5">
                <strong>Description:</strong>
                <div class="dex-offer"><?php $form->get('description') ?></div>
            </div>
            <div class="w-100 py-3"></div>
            <div class="col-12">
                <strong>Devices: </strong>&nbsp;<?php echo T::wordBreak(T::devIcons($form->fetch('device'), $form->fetch('min_os_ver')), '') ?>
            </div>
            <div class="col-12 py-3">
                <strong>Countries: </strong>&nbsp;<?php $form->get('country') ?>
            </div>
            <div class="col-12">
                <strong>Categories: </strong>&nbsp;<?php $form->get('category') ?>
            </div>
            <div class="w-100 pt-3"></div>
            <div class="col-md-4">
                <p><strong>Device ID Required: </strong>
                    <?= $form->fetch('device_id') ?></p>
                <p><strong>Incent: </strong>
                    <?php echo T::bool($form->fetch('incent')) ?></p>
                <p><strong>Expiry: </strong>
                    <?php echo T::expiry($form->fetch('expiry')) ?></p>
                <p><strong>Daily Cap: </strong>
                    <?php echo $form->fetch('sale') . T::nums($form->fetch('caps')) ?>
                </p>
                <p><strong>Monthly Cap: </strong>
                    <?php echo T::nums($form->fetch('monthly_cap')) ?>
                </p>
            </div>
            <div class="col-md-8">
                <strong class="w-100 d-block">Tracking: </strong>
                <?php $form->get('tracking') ?>
                <?php $form->get('macros') ?>
            </div>
            <div class="w-100 pt-3"></div>
            <div class="col-12">
                <?php $form->get('button') ?>
            </div>
            <hr class="my-4">
        </div>
    </div>
</section>
<div class="modal fade" id="creaMode" role="dialog" aria-labelledby="creaMode" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="creaName">Creatives of
                    #<?php $form->get('ID') ?> <?php $form->get('name') ?>.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="bodyCrea" class="modal-body"></div>
        </div>
    </div>
</div>
<script>
    $('#copyBack').on('click', function () {
        var s = document.getElementById('popUrl');
        s.select();
        document.execCommand('copy');
    }).tooltip({
        trigger: 'click'
    });

    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover',
        placement: 'top'
    });

    $('#creaMode').on('shown.bs.modal', function () {
        $('#bodyCrea').load('/async/view?p=creative&i=<?php $form->get("ID") ?>')
    });
</script>