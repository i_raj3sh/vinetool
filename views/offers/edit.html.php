<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 4:32 PM
 */
?>

<?= select2() ?>
<?= datepicker() ?>
<?= ckeditor() ?>

<style>
    .popover{
        max-width: 500px !important;
        border: 1px solid #111111;
    }
    .popover.bs-popover-top .arrow::before, .popover.bs-popover-auto[x-placement^="top"] .arrow::before, .popover.bs-popover-top .popover-header::before, .popover.bs-popover-auto[x-placement^="top"] .popover-header::before  {
        border-top-color: #000000;
    }
    .popover.bs-popover-bottom .arrow::before, .popover.bs-popover-auto[x-placement^="bottom"] .arrow::before, .popover.bs-popover-bottom .popover-header::before, .popover.bs-popover-auto[x-placement^="bottom"] .popover-header::before{
        border-bottom-color: #000000;
    }
    .popover.bs-popover-bottom .arrow::after, .popover.bs-popover-auto[x-placement^="bottom"] .arrow::after {
        border-bottom-color: #111111;
    }
    .popover-header {
        background-color: #111111;
        border-bottom: 1px solid #000;
        color: #ffffff;
    }
</style>

<section class="row">
    <div class="col-md-9 py-3">
        <h3>Update Offers</h3>

        <div>
            <form name="add_offers" method="post" action="<?php echo MY_SERVER ?>offers/add">
                <input type="hidden" name="id" value="<?php $form->get('ID') ?>">
                <div class="form-group">
                    <label>Offer Name</label>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-font"></i></span>
                        </div>
                    <input class="form-control" type="text" name="name" placeholder="Offer Name" value="<?php $form->get('name') ?>" data-toggle="popover" title="Best Practice" data-content="<ul><li>[Name] <b>-</b> [Geo] <b>-</b> [Device] <b>-</b> [Incent | Non-Incent | Adult] <b>-</b> [CPI | CPA | CPL]</li></ul>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea id="editor1" class="form-control" name="desc" placeholder="Description"><?php $form->get('description') ?></textarea>
                </div>
                <section class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Offer ID</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-bolt"></i></span>
                                </div>
                            <input class="form-control" type="text" name="advID" placeholder="Offer ID by advertiser" value="<?php $form->get('advID') ?>" data-toggle="popover" title="Advertiser Campaign ID" data-content="<ul><li>This is the offer ID appear in Advertiser panel</li></ul>" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Network</label>
                            <?php
                            $g = new \model\Network();
                            $g->setProperties(['active'=>1])->setQueryParameters($g);
                            $all = $g->query();
                            ?>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-bank"></i></span>
                                </div>
                                <select id="networkEv" name="network" class="form-control selected5">
                                <?php foreach ($all as $item): ?>
                                    <option value="<?php echo $item->ID ?>" <?php $form->select('network_ID', $item->ID) ?> ><?php echo $item->email ?></option>
                                <?php endforeach; ?>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Type</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-tags"></i></span>
                                </div>
                            <select name="type" class="form-control selected5">
                                <option value="CPI" <?php $form->select('type','CPI') ?> >CPI</option>
                                <option value="CPA" <?php $form->select('type','CPA') ?> >CPA</option>
                                <option value="CPL" <?php $form->select('type','CPL') ?> >CPL</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="api_trigger" class="form-group" data-toggle="popover" title="API updated" data-content="<ul><li>Enable this option only if this offer is to be updated by an API.</li><li>API will automatically update the details of this offer.</li><li>This option only work if API is integrated with the Advertiser.</li><li><strong class='text-danger'>IMPORTANT:</strong> Please do not fuck with this option if you are not sure what it is.</li></ul>">
                            <label>API</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-download"></i></span>
                                </div>
                            <select name="api" class="form-control selected5">
                                <option value="0" <?php $form->select('cron','0') ?> >Nope</option>
                                <option value="1" <?php $form->select('cron','1') ?> >Yep</option>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4" id="ifa_trigger" data-toggle="popover"
                         title="GAID / IDFA required in TL"
                         data-content="<ul><li>Set this is <b>YES</b> if GAID/IDFA is strictly required for this campaign.</li><li>Traffic will be moved to fallback if not provided by publisher.</li></ul>">
                        <label>Device ID required</label>
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-circle"></i></span>
                            </div>
                            <select name="device_id" class="form-control selected5">
                                <option value="0" <?php $form->select('device_id', '0') ?> >no</option>
                                <option value="1" <?php $form->select('device_id', '1') ?> >yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Offer Charged</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input id="charged" class="form-control" type="text" name="charge" placeholder="Offer charged"
                                       value="<?php $form->get('charge') ?>" data-toggle="popover"
                                       title="Payout payed by advertiser"
                                       data-content="<ul><li>Must be in USD always.</li><li>Publisher will see 50% of this payout by default.</li><li>If this offer updated by an API. This perhaps can change.</li></ul>"
                                       required>
                                <div class="input-group-append">
                            <span class="input-group-text">publisher PO:&nbsp;&nbsp;$<em
                                        id="pay_pub"><?php echo($form->fetch('charge') * COMMISSION) ?></em></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Expiry</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                </div>
                                <input id="datetime0" class="form-control date" type="text" name="expiry" placeholder="Offer expiry date: YYYYMMDDHHmmss" value="<?php $form->get('expiry') ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Cap</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-scissors"></i></span>
                                </div>
                            <input class="form-control" type="text" name="caps" placeholder="Set install cap" value="<?php $form->get('caps','0') ?>" data-toggle="popover" title="Daily Conversion Cap" data-content="<ul><li>Offer will be paused after reaching this limit.</li><li>Default is Zero for unlimited cap limit.</li><li>Cap resets at UTC time daily.</li></ul>" required>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Monthly Cap</label>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-scissors"></i></span>
                                </div>
                            <input class="form-control" type="text" name="monthly_cap" placeholder="Set install monthly cap" data-toggle="popover" title="Monthly Conversion Cap" data-content="<ul><li>Offer will be paused after reaching this limit.</li><li>Default is Zero for unlimited cap limit.</li><li>Cap resets on 1st of every month.</li></ul>" value="<?php $form->get('monthly_cap','0') ?>">
                        </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Restrict Fallback</label>
                            <div id="fallback_trigger" class="input-group input-group-sm" data-toggle="popover"
                                 title="Fallback settings"
                                 data-content="<ul><li>If you set this field to <b>yep</b>, backfill traffic will <b class='text-danger'>NOT</b> go to this campaign.</li><li>This field is useless if network level restriction is used.</li></ul>">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-share"></i></span>
                                </div>
                                <select name="fallback" class="form-control selected5">
                                    <option value="0" <?php $form->select('fallback', '0') ?> >Nope</option>
                                    <option value="1" <?php $form->select('fallback', '1') ?> >Yep</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="form-group">
                    <label>Preview</label>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-eye"></i></span>
                        </div>
                    <input class="form-control" type="url" name="preview" placeholder="Preview url" value="<?php $form->get('preview') ?>" data-toggle="popover" title="Campaign preview URL" data-content="<ul><li>Preview of the content presented to users.</li><li>Must start with http(s).</li></ul>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Icon</label>
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-image"></i></span>
                        </div>
                    <input class="form-control" type="url" name="icon" placeholder="Icon url" value="<?php $form->get('icon') ?>" data-toggle="popover" title="Brand Logo" data-content="<ul><li>Brand Logo of the offer.</li><li>Must start with http(s).</li></ul>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label>Tracking</label>
                    <div class="input-group input-group-sm">
                        <div id="openMacro" class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-cog fa-spin"></i></span>
                        </div>
                        <input id="trackingUrl" class="form-control" type="url" name="tracking"
                               placeholder="Tracking url"
                               value="<?php $form->get('tracking') ?>" data-toggle="popover"
                               title="Tracking URL to the offer"
                               data-content="<ul><li>Use appropriate URL defined at Advertiser's panel for this offer.</li><li>Must append <b>{CLICK}</b> to the tracking parameter.</li><li>Double check the URL before proceeding.</li><li>Must start with http(s).</li><li>Following are Optional Parameters.<ul><li>{PUB}</li><li>{APP}</li><li>{IDFA}</li><li>{GAID}</li></ul></li></ul>"
                               required>
                    </div>
                </div>
                <section class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Geos</label>
                            <?php

                            $g = new \model\Country();
                            $g->setQueryParameters($g);
                            $all = $g->query();
                            ?>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-globe fa-spin"></i></span>
                                </div>
                                <select name="country[]" class="form-control selected5" multiple>
                                    <?php foreach ($all as $item): ?>
                                        <option value="<?= $item->code ?>" <?php $form->in_array(json_decode($form->fetch('country')), $item->code) ?> ><?php echo $item->code . ': ' . $item->name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Events</label>
                            <?php

                            $g = new \model\NetworkEvent();
                            $g->setProperties(['network_ID' => $form->fetch('network_ID')])
                                ->setQueryParameters($g);
                            $all = $g->query();
                            ?>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-bell"></i></span>
                                </div>
                                <select id="event" name="event[]" class="form-control selected5" multiple>
                                    <?php foreach ($all as $item): ?>
                                        <option value="<?= $item->ID ?>" <?php $form->in_array($form->fetch('events'), $item->ID) ?> ><?php echo $item->event_id . ': ' . $item->event_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>OS</label>
                            <?php
                            $g = new \model\Device();
                            $g->setQueryParameters($g);
                            $all = $g->query();
                            ?>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-tablet"></i></span>
                                </div>
                            <select name="device[]" class="form-control selected5" multiple>
                                <?php foreach ($all as $item): ?>
                                    <option value="<?=$item->name ?>" <?php $form->in_array(json_decode($form->fetch('device')), $item->name) ?> ><?php echo $item->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Category</label>
                            <?php
                            $g = new \model\Category();
                            $g->setQueryParameters($g);
                            $all = $g->query();
                            ?>
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-tasks"></i></span>
                                </div>
                            <select name="category[]" class="form-control selected5" multiple>
                                <?php foreach ($all as $item): ?>
                                    <option value="<?=$item->name ?>" <?php $form->in_array(json_decode($form->fetch('category')), $item->name) ?> ><?php echo $item->name ?></option>
                                <?php endforeach; ?>
                            </select>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="row">
                    <div class="form-group col-md-6">
                        <label>Min OS Version</label>
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-thermometer-half"></i></span>
                            </div>
                            <input class="form-control" type="number" name="min_os_ver" placeholder="Min OS Version"
                                   step="any" min="0"
                                   value="<?php $form->get('min_os_ver') ?>" data-toggle="popover"
                                   title="Min OS version allowed for this offer"
                                   data-content="<ul><li>Must be numeric.</li><li>Decimals are allowed.</li><li>Set it to zero if not applicable.</li></ul>"
                                   required>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Incent</label>
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-money"></i></span>
                            </div>
                            <select name="incent" class="form-control selected5">
                                <option value="0" <?php $form->select('incent', '0') ?> >Non-incent</option>
                                <option value="1" <?php $form->select('incent', '1') ?> >Incent</option>
                            </select>
                        </div>
                    </div>
                </section>
                <section class="row">
                    <div class="form-group col-md-6">
                        <label>Active</label>
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-play-circle"></i></span>
                            </div>
                            <select name="active" class="form-control selected5">
                                <option value="1" <?php $form->select('active', '1') ?> >active</option>
                                <option value="2" <?php $form->select('active', '2') ?> >pause</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Private</label>
                        <div class="input-group input-group-sm">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <select name="private" class="form-control selected5">
                                <option value="0" <?php $form->select('private', '0') ?> >No</option>
                                <option value="1" <?php $form->select('private', '1') ?> >Yep</option>
                            </select>
                        </div>
                    </div>
                </section>
                <div class="form-group">
                    <button type="submit" class="btn btn-info" name="offers" value="_"><i class="fa fa-save"></i> Proceed</button>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="modal fade" id="setMacro" role="dialog" aria-labelledby="assignToPub" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Set macros with tracking link.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="bodyMacro" class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <input id="mainUrl" class="form-control" type="text" placeholder="Tracking url" value=""/>
                        </div>
                    </div>
                    <div class="col-12">
                        <label>Advertiser's parameters:</label>
                        <ul class="list-group">
                            <li class="list-group-item">click:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
                                        id="clickM" type="text" placeholder="" value=""/></li>
                            <li class="list-group-item">pub:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
                                        id="pubM" type="text" placeholder="" value=""/></li>
                            <li class="list-group-item">
                                idfa:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="idfaM" type="text"
                                                                                                  placeholder=""
                                                                                                  value=""/></li>
                            <li class="list-group-item">
                                gaid:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="gaidM" type="text"
                                                                                                  placeholder=""
                                                                                                  value=""/></li>
                            <li class="list-group-item">app name: <input id="appM" type="text" placeholder="" value=""/>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="saveMacro" type="button" class="btn btn-primary">Set</button>
            </div>
        </div>
    </div>
</div>

<script>
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
        // ,templateSelection: formatState
    });
    function formatState (state) {
        if (state.text.indexOf(':') === -1) {
            return state.text;
        }
        var code = state.text.toLowerCase().split(':')[0];
        return $(
            '<span class="f16"><i class="flag ' + code + '"></i> ' + state.text + '</span>'
        );
    };

    $('#openMacro').on('click', function () {
        var url = $('#trackingUrl').val();
        var link = [];
        if (url) {
            var parts = url.split('&');
            for (var i in parts) {
                addM('#clickM', parts[i], '{CLICK}');
                addM('#pubM', parts[i], '{PUB}');
                addM('#gaidM', parts[i], '{GAID}');
                addM('#idfaM', parts[i], '{IDFA}');
                addM('#appM', parts[i], '{APP}');
                if (parts[i].indexOf('{') === -1) {
                    link.push(parts[i])
                }
            }
            $('#mainUrl').val(link.join('&'));
        }
        $('#setMacro').modal();
    });

    function addM(m, id, a) {
        if (id.indexOf(a) > -1) {
            $(m).val(id.split('=')[0])
        }
    }

    $('#datetime0').datetimepicker( {
        format:'YYYY-MM-DD HH:mm:ss',
        useSeconds:true
    });

    $('#api_trigger,#ifa_trigger,#fallback_trigger').popover({
        html: true,
        trigger: 'hover',
        placement: 'top'
    });

    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'focus',
        placement: 'top'
    });

    var commission = <?php echo COMMISSION ?>;
    
    $('#charged').on('keyup', function () {
        try {
            var v = parseFloat(this.value);
            if (v > 0) {
                $('#pay_pub').text(parseFloat(v * commission + '').toFixed(3));
                return;
            }
        }catch(e){
            console.error(e.message);
        }
        $('#pay_pub').text(0);
    });

    $('select#networkEv').on('change', function () {
        var id = $(this).val();
        $.ajax({
            url: "<?=s()?>offers/eventsList",
            data: "nid=" + id,
            success: function (h) {
                $('#event').html(h).trigger('change');
            }
        });
    });

    CKEDITOR.replace( 'editor1' );
</script>