<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 25/1/18
 * Time: 11:03 PM
 */
?>

<section class="row">
    <div class="col-md-5">
        <h3 class="pt-3">Test Affiliate Link</h3>
        <form id="test_link" class="row py-3" method="post">
            <div class="col-12">
                <div class="form-group">
                    <label>Tracking ID:</label>
                    <input type="number" class="form-control" name="t" value="<?php echo \helper\Request::getField('t','POST') ?>" placeholder="Enter Tracking ID" required>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label>Test link:</label>
                    <input type="text" class="form-control" name="l" id="test_url" value="<?php echo \helper\Request::getField('l','POST') ?>" placeholder="Enter Afiliate Link" required>
                </div>
            </div>
            <div class="col-1">
                <button type="submit" class="btn btn-warning"><i class="fa fa-circle"></i> Test</button>
            </div>
        </form>
        <p class="text-danger"><?php echo $form['er'] ?></p>
        <hr/>
        <form method="post">
            <input type="hidden" name="pb" value="<?php echo $form['pb'] ?>">
            <button type="submit" class="btn btn-success" <?php if(!$form['pb']) echo 'disabled' ?>><i class="fa fa-circle"></i> Postback</button>
            <br/>
            <p class="text-success"><?php echo $form['msg'] ?></p>
            <br/>
            <p class="text-info"><?php echo $form['res'] ?></p>
        </form>
    </div>
    <div class="col-md-5">
        <?php if ($form['pb']) { ?>
            <h5 class="pt-4 pb-2">Affiliate's parameters sent</h5>
            <ul class="list-group">
                <li class="list-group-item">clickid=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $form['clickid'] ?></span>
                </li>
                <li class="list-group-item">sub_affid=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $form['sub_affid'] ?></span>
                </li>
                <li class="list-group-item">sub_sourceid=&nbsp;<span
                            class="text-danger"><?= $form['sub_sourceid'] ?></span></li>
                <li class="list-group-item">app_name=&nbsp;<span
                            class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;<?= $form['app_name'] ?></span></li>
                <li class="list-group-item">idfa=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $form['idfa'] ?></span>
                </li>
                <li class="list-group-item">gaid=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $form['gaid'] ?></span>
                </li>
            </ul>
        <?php } ?>
    </div>
</section>
<script>
    $('a#tools').addClass('active');
    $('#test_link').on('submit', function(e){
        e.preventDefault();
        var strWindowFeatures = "location=yes,height=100,width=100,scrollbars=no,status=yes";
        var URL = $('#test_url').val();
        var win = window.open(URL, "_blank", strWindowFeatures);
        var me = this;
        setTimeout(function(){
            me.submit();
        },3000);
    });
</script>