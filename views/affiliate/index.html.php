<?= datatables() ?>
<?php

use helper\Template;

?>

<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Affiliate panel <a class="btn btn-sm btn-primary float-right" href="<?php echo MY_SERVER ?>affiliate/edit"><i class="fa fa-plus"></i> affiliate</a></h3>
        <form class="row py-3">
            <div class="col-md-3">
                <div class="form-group">
                    <label>ID:</label>
                    <input type="text" class="form-control" name="id"
                           value="<?php echo \helper\Request::getField('id', 'GET') ?>" placeholder="Affiliate ID">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Name / company:</label>
                    <input type="text" class="form-control" name="n"
                           value="<?php echo \helper\Request::getField('n', 'GET') ?>"
                           placeholder="Affiliate name / company">
                </div>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="table-responsive">
            <table id="tabular" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Skype</th>
                    <th>Country</th>
                    <th>Company</th>
                    <th>Website</th>
                    <th>Manager</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):
                    echo '<tr>';
                    echo '<td>'.$v->ID.'</td>';
                    echo '<td>'.$v->name.'</td>';
                    echo '<td>'.$v->username.'</td>';
                    echo '<td>'.$v->email.'</td>';
                    echo '<td>'.$v->skype.'</td>';
                    echo '<td>'.$v->country.'</td>';
                    echo '<td>'.$v->company.'</td>';
                    echo '<td>'.$v->website.'</td>';
                    echo '<td>' . $v->manager . '</td>';
                    echo '<td>' . \helper\Template::state($v->active) . '</td>'; ?>
                    <td>
                        <div class="btn-group">
                            <a class="btn btn-warning btn-sm" href="#!" onclick="passcode(<?= $v->ID ?>)"><i
                                        class="fa fa-key text-dark"></i></a>
                            <?php echo Template::entityStateButton($v, 'affiliate'); ?>
                            <a class="btn btn-info btn-sm" href="<?= s() ?>affiliate/edit?id=<?= $v->ID ?>"><i
                                        class="fa fa-pencil"></i></a>
                            <a class="btn btn-danger btn-sm" href="#!"
                               onclick=delet("<?= s() ?>affiliate/del?id=<?= $v->ID ?>")><i
                                        class="fa fa-remove"></i></a>
                            <a class="btn btn-success btn-sm" title="Access account" data-toggle="tooltip"
                               href="<?= s() ?>login?<?= TKN ?>=<?= $v->ID ?>" target="affy"><i class="fa fa-user"></i></a>
                        </div>
                    </td>
                    <?php echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
            <?php echo $form['pages']; echo $form['perPage'] ?>
        </div>
    </div>
</section>
<div class="modal fade" id="passcode" role="dialog" aria-labelledby="popBack" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="bodyBack" class="modal-body">
                <div class="row">
                    <input type="hidden" id="passId">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="password" id="passVal" class="form-control" placeholder="New password">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="password" id="passAnd" class="form-control" placeholder="Repeat password">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="passButt" class="btn btn-info">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function delet(loc, conf) {
        conf = confirm("Are you sure? Do you want to delete?");

        if (conf){
            window.location = loc;
        }
    }

    var dt = $('#tabular').DataTable();

    function passcode(id) {
        $('#passId').val(id);
        $('#passcode').modal()
    }

    $('#passButt').on('click', function () {
        var np = $('#passVal').val(), rp = $('#passAnd').val();
        if (np !== rp) {
            toastr['error']('Passwords do not match.', 'Error!');
            return
        }
        $.ajax({
            url: '<?=s()?>affiliate/password',
            data: 'i=' + $('#passId').val() + '&p=' + np,
            success: function (h) {
                if (h === 'OK') {
                    toastr['success']('Password changed', 'Splendid!')
                } else {
                    toastr['error']('Password not updated.', 'Error!')
                }
            }
        });
    });

    /*$('[data-toggle="tooltip"]').tooltip();

    dt.on( 'draw', function () {
        $('[data-toggle="tooltip"]').tooltip();
    } );*/
</script>