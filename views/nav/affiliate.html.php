<?php

use controller\Controller as C;

?>
<header class="navbar navbar-expand-md navbar-dark bg-danger flex-column flex-md-row">
    <a class="navbar-brand mr-0 mr-md-2" href="./" aria-label="VineTool">
        <strong>Vinetool</strong>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <nav class="navbar-nav-scroll pl-md-3 collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-md-0">
            <li class="nav-item">
                <a id="dashboard" class="nav-link" href="<?php C::href() ?>">Dashboard</a>
            </li>
            <li class="nav-item">
                <a id="account" class="nav-link " href="<?php C::href('account') ?>">My account</a>
            </li>
            <li class="nav-item">
                <a id="offers" class="nav-link " href="<?php C::href('offers') ?>">Offers</a>
            </li>
            <li class="nav-item">
                <a id="myOffers" class="nav-link " href="<?php C::href('myOffers') ?>">My Offers</a>
            </li>
            <li class="nav-item">
                <a id="postback" class="nav-link " href="<?php C::href('postback') ?>">Postback</a>
            </li>
            <li class="nav-item">
                <a id="apiKey" class="nav-link" href="<?php C::href('apiKey') ?>">API</a>
            </li>
            <li class="nav-item">
                <a id="billing" class="nav-link " href="<?php C::href('billing') ?>">Billing</a>
            </li>
            <li class="nav-item">
                <a id="invoice" class="nav-link " href="<?php C::href('invoice') ?>">Invoice <span
                            class="badge"><?= $invoice_count ?></span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="stats" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Stats
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="offerStats" class="dropdown-item" href="<?php C::href('offerStats') ?>">By Offer</a>
                    <a id="monthlyStats" class="dropdown-item" href="<?php C::href('monthlyStats') ?>">By Month</a>
                    <a id="dailyStats" class="dropdown-item" href="<?php C::href('dailyStats?q=w') ?>">By Date</a>
                    <a id="geoStats" class="dropdown-item" href="<?php C::href('geoStats') ?>">By Geo</a>
                    <a id="osStats" class="dropdown-item" href="<?php C::href('osStats') ?>">By OS</a>
                    <a id="sourceStats" class="dropdown-item" href="<?php C::href('sourceStats') ?>">By Source</a>
                    <a id="conversionsStats" class="dropdown-item" href="<?php C::href('conversionsStats') ?>">By Conversion</a>
                    <div class="dropdown-divider"></div>
                </div>
            </li>
            <li class="nav-item">
                <a id="postbackLogs" class="nav-link" href="<?php C::href('postbackLogs') ?>">Postback Logs</a>
            </li>
            <li class="nav-item last">
                <span id="login_name" style="display: inline;color:#ffc107"><?php $nm = explode(' ',C::getSessionName(),2)[0]; echo $nm ?></span>
                <?php if (empty($_GET[TKN])) { ?>
                <a class="nav-link " href="<?php echo MY_SERVER.'logout.php' ?>" style="display: inline">Logout</a>
                <?php } ?>
                <div id="clock"></div>
            </li>
        </ul>
    </nav>
    <script>
        $('a#<?php \helper\Template::getActive() ?>').addClass('active');
    </script>
</header><?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/13/17
 * Time: 2:21 PM
 */