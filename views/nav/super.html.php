<header class="navbar navbar-expand-md navbar-dark bg-danger flex-md-row navbar-fixed">
    <a class="navbar-brand mr-0 mr-md-2" href="./" aria-label="VineTool">
        <strong>Vinetool</strong>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <nav class="navbar-nav-scroll pl-md-3 collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-md-0">
            <li class="nav-item">
                <a id="dash" class="nav-link" href="<?php echo MY_SERVER ?>">Dashboard</a>
            </li>
            <li class="nav-item">
                <a id="admin" class="nav-link " href="<?php echo MY_SERVER.'admin' ?>">Admins</a>
            </li>
            <li class="nav-item">
                <a id="offers" class="nav-link " href="<?php echo MY_SERVER.'offers' ?>">Offers</a>
            </li>
            <li class="nav-item">
                <a id="affiliate" class="nav-link " href="<?php echo MY_SERVER.'affiliate' ?>">Affiliates</a>
            </li>
            <li class="nav-item">
                <a id="approveOffers" class="nav-link " href="<?php echo MY_SERVER . 'approveOffers' ?>">Assigned</a>
            </li>
            <li class="nav-item">
                <a id="network" class="nav-link " href="<?php echo MY_SERVER.'network' ?>">Networks</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="stats" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Stats
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="offerStats" class="dropdown-item" href="<?php echo MY_SERVER.'offerStats' ?>">By Offer</a>
                    <a id="monthlyStats" class="dropdown-item" href="<?= s() . 'monthlyStats' ?>">By Month</a>
                    <a id="dailyStats" class="dropdown-item" href="<?= s() . 'dailyStats?q=w' ?>">By Date</a>
                    <a id="geoStats" class="dropdown-item" href="<?= s() . 'geoStats' ?>">By Geo</a>
                    <a id="osStats" class="dropdown-item" href="<?= s() . 'osStats' ?>">By OS</a>
                    <a id="sourceStats" class="dropdown-item" href="<?= s() . 'sourceStats' ?>">By
                        Source</a>
                    <a id="conversionsStats" class="dropdown-item" href="<?= s() . 'conversionsStats' ?>">By
                        Conversion</a>
                    <div class="dropdown-divider"></div>
                    <a id="affiliateStats" class="dropdown-item" href="<?= s() . 'affiliateStats' ?>">By Affiliate</a>
                    <a id="networkStats" class="dropdown-item" href="<?= s() . 'networkStats' ?>">By
                        Network</a>
                    <a id="aMStats" class="dropdown-item" href="<?= s() . 'AMStats' ?>">By Manager</a>
                    <a id="eventGroupedStats" class="dropdown-item" href="<?= s() . 'EventGroupedStats' ?>">By
                        Event (beta)</a>
                    <a id="droppedStats" class="dropdown-item" href="<?= s() . 'droppedStats' ?>">By Dropped</a>
                    <a id="droppedGroupedStats" class="dropdown-item" href="<?= s() . 'droppedGroupedStats?q=w' ?>">By
                        Dropped
                        Group</a>
                    <a id="scrubbedStats" class="dropdown-item" href="<?= s() . 'scrubbedStats' ?>">By Scrubbed</a>
                    <a id="scrubbedGroupedStats" class="dropdown-item" href="<?= s() . 'scrubbedGroupedStats?q=w' ?>">By
                        Scrubbed
                        Group</a>
                    <!--                    <a id="compareStats" class="dropdown-item" href="-->
                    <? //= s() . 'compareStats' ?><!--">Compare</a>-->
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="rules" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Rules
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="networkBlock" class="dropdown-item" href="<?=s().'networkBlock' ?>">Blocked by network</a>
                    <a id="offerBlock" class="dropdown-item" href="<?=s().'offerBlock' ?>">Blocked by offer</a>
                    <a id="sourceBlock" class="dropdown-item" href="<?=s().'sourceBlock' ?>">Blocked by source</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="tools" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Tools
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="networkMonthly" class="dropdown-item" href="<?= s() . 'networkMonthly' ?>">Network Monthly
                        Report</a>
                    <a id="affiliateMonthly" class="dropdown-item" href="<?= s() . 'affiliateMonthly' ?>">Affiliate
                        Monthly Report</a>
                    <a id="invoiceAffiliate" class="dropdown-item" href="<?= s() . 'invoiceAffiliate' ?>">Affiliate
                        Invoice <span
                                class="badge badge-danger"><?= $invoice_count ?></span></a></a>
                    <a id="passback" class="dropdown-item" href="<?= s() . 'passback' ?>">Traffic (beta)</a>
                    <a id="testAffiliateLink" class="dropdown-item" href="<?= s() . 'testAffiliateLink' ?>">Test
                        Link</a>
                    <a id="apiKey" class="dropdown-item" href="<?= s() . 'apiKey' ?>">Pub API</a>
                    <a id="affiliateBilling" class="dropdown-item" href="<?= s() . 'affiliateBilling' ?>">Pub
                        Billing</a>
<!--                    <a id="invoiceNetwork" class="dropdown-item" href="--><?php //echo MY_SERVER.'invoiceNetwork' ?><!--">Network Invoice</a>-->
                    <div class="dropdown-divider"></div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="logs" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Logs
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="activityLog" class="dropdown-item" href="<?= s() . 'activityLog' ?>">Activity Logs</a>
                    <a id="postbackLogs" class="dropdown-item" href="<?= s() . 'postbackLogs' ?>">Postback Logs</a>
                    <a id="clickLogs" class="dropdown-item" href="<?= s() . 'clickLogs' ?>">Click Logs</a>
                </div>
            </li>
			<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    API
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/mobilda.php' ?>">Run
                        Mobilda</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/mobs10media.php' ?>">Run
                        Mobs10Media</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/slaviamobile.php' ?>">Run
                        Slaviamobile</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/lemmonet.php' ?>">Run
                        Lemmonet</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/motorads.php' ?>">Run
                        Motorads</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/calltoadagency.php' ?>">Run
                        Call2AdAgency</a>
					<a class="dropdown-item" target="_blank" href="<?=s().'cron/api/blammobi.php' ?>">Run BlamMobi</a>
					<a class="dropdown-item" target="_blank" href="<?=s().'cron/api/polemedia.php' ?>">Run Polemedia</a>
					<a class="dropdown-item" target="_blank" href="<?=s().'cron/api/mobvista.php' ?>">Run MobVista</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/avazu.php' ?>">Run Avazu</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/mobusi.php' ?>">Run Mobusi</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/mobisummer.php' ?>">Run
                        MobiSummer</a>
                    <a class="dropdown-item" target="_blank" href="<?= s() . 'cro\n/api/nposting.php' ?>">Run
                        NPosting
                    </a><a class="dropdown-item" target="_blank" href="<?= s() . 'cron/api/applift.php' ?>">Run
                        Applift</a>
				</div>
			</li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="extra" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Extra
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="target" class="dropdown-item" href="<?= s() . 'target' ?>">Targets</a>
                    <a id="tags" class="dropdown-item" href="<?= s() . 'tags' ?>">Tags</a>
                </div>
            </li>
            <li class="nav-item last">
                <span id="login_name" style="display: inline;color:#ffc107"><?php echo \controller\Controller::getSessionName()?></span>
                <a class="nav-link " href="<?= s() . 'logout.php' ?>" style="display: inline">Logout</a>
                <div id="clock"></div>
            </li>
        </ul>
    </nav>
    <script>
        $('a#<?php \helper\Template::getActive() ?>').addClass('active');
    </script>
</header>