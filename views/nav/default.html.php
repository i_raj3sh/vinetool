<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 7:03 PM
 */
?>


<header class="navbar navbar-expand-md navbar-dark bg-danger flex-md-row navbar-fixed">
    <a class="navbar-brand mr-0 mr-md-2" href="./" aria-label="VineTool">
        <strong>Vinetool</strong>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <nav class="navbar-nav-scroll pl-md-3 collapse navbar-collapse" id="navbarTogglerDemo03">
        <ul class="navbar-nav mr-auto mt-2 mt-md-0">
            <li class="nav-item">
                <a id="dash" class="nav-link" href="<?php echo MY_SERVER ?>">Dashboard</a>
            </li>
            <li class="nav-item">
                <a id="offers" class="nav-link " href="<?php echo MY_SERVER.'offers' ?>">Offers</a>
            </li>
            <li class="nav-item">
                <a id="affiliate" class="nav-link " href="<?php echo MY_SERVER.'affiliate' ?>">Affiliates</a>
            </li>
            <li class="nav-item">
                <a id="approveOffers" class="nav-link " href="<?php echo MY_SERVER . 'approveOffers' ?>">Assigned</a>
            </li>
            <li class="nav-item">
                <a id="network" class="nav-link " href="<?php echo MY_SERVER.'network' ?>">Networks</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="stats" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Stats
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="offerStats" class="dropdown-item" href="<?php echo MY_SERVER.'offerStats' ?>">By Offer</a>
                    <a id="monthlyStats" class="dropdown-item" href="<?= s() . 'monthlyStats' ?>">By Month</a>
                    <a id="dailyStats" class="dropdown-item" href="<?php echo MY_SERVER.'dailyStats?q=w' ?>">By Date</a>
                    <a id="geoStats" class="dropdown-item" href="<?php echo MY_SERVER.'geoStats' ?>">By Geo</a>
                    <a id="osStats" class="dropdown-item" href="<?php echo MY_SERVER.'osStats' ?>">By OS</a>
                    <a id="sourceStats" class="dropdown-item" href="<?php echo MY_SERVER . 'sourceStats' ?>">By
                        Source</a>
                    <a id="conversionsStats" class="dropdown-item" href="<?php echo MY_SERVER.'conversionsStats' ?>">By Conversion (beta)</a>
                    <div class="dropdown-divider"></div>
                    <a id="affiliateStats" class="dropdown-item" href="<?php echo MY_SERVER.'affiliateStats' ?>">By Affiliate</a>
                    <a id="networkStats" class="dropdown-item" href="<?php echo MY_SERVER . 'networkStats' ?>">By
                        Network</a>
                    <a id="aMStats" class="dropdown-item" href="<?php echo MY_SERVER . 'AMStats' ?>">By
                        Manager</a>
                    <a id="droppedStats" class="dropdown-item" href="<?php echo MY_SERVER.'droppedStats' ?>">By Dropped</a>
                    <a id="compareStats" class="dropdown-item" href="<?php echo MY_SERVER.'compareStats' ?>">Compare</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="rules" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Rules
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="networkBlock" class="dropdown-item" href="<?=s().'networkBlock' ?>">Blocked by network</a>
                    <a id="offerBlock" class="dropdown-item" href="<?= s() . 'offerBlock' ?>">Blocked by offer</a>
                    <a id="sourceBlock" class="dropdown-item" href="<?= s() . 'sourceBlock' ?>">Blocked by source</a>
                </div>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="offerBlock" class="dropdown-item" href="<?=s().'offerBlock' ?>">Blocked by offer</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="tools" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Tools
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="passback" class="dropdown-item" href="<?php echo MY_SERVER.'passback' ?>">Traffic (beta)</a>
                    <a id="testAffiliateLink" class="dropdown-item" href="<?php echo MY_SERVER.'testAffiliateLink' ?>">Test Link</a>
                    <a id="apiKey" class="dropdown-item" href="<?php echo MY_SERVER.'apiKey' ?>">Pub API</a>
                    <a id="affiliateBilling" class="dropdown-item" href="<?php echo MY_SERVER.'affiliateBilling' ?>">Pub Billing</a>
                    <div class="dropdown-divider"></div>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="logs" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    Logs
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a id="activityLog" class="dropdown-item" href="<?= s() . 'activityLog' ?>">Activity Logs</a>
                    <a id="postbackLogs" class="dropdown-item" href="<?= s() . 'postbackLogs' ?>">Postback Logs</a>
                    <a id="clickLogs" class="dropdown-item" href="<?= s() . 'clickLogs' ?>">Click Logs</a>
                </div>
            </li>
            <li class="nav-item last">
                <span id="login_name" style="display: inline;color:#ffc107"><?php echo \controller\Controller::getSessionName()?></span>
                <a class="nav-link " href="<?php echo MY_SERVER.'logout.php' ?>" style="display: inline">Logout</a>
                <div id="clock"></div>
            </li>
        </ul>
    </nav>
    <script>
        $('a#<?php \helper\Template::getActive() ?>').addClass('active');
    </script>
</header>