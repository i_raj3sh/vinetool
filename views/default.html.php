<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/9/17
 * Time: 9:09 PM
 */
/**
 * @var $template \helper\Template
 */

use controller\Controller as C;
use helper\Message as M;

?>

<!doctype html>
<html lang="en">
<head>
    <title id="titleTop"><?php $template->getTemplateField('title') ?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#cb2434">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#cb2434">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#cb2434">

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet"
          type="text/css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= s() ?>assets/css/main.css"/>
    <link rel="stylesheet" href="<?= s() ?>assets/css/toastr.min.css"/>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link id="fav-icon" rel="icon" type="image/png" sizes="32x32" href="<?php echo MY_SERVER ?>images/vv.png">
    <script type="text/javascript" src="<?= s() ?>assets/js/toastr.min.js"></script>
    <script type="text/javascript" src="<?= s() ?>assets/js/clock.js"></script>
    <style>
        .highcharts-credits, .amcharts-chart-div a {
            display: none !important;
        }

        body, input, h1, h2, h3, h4, h5, h6, span, div, label, a, button, table {
            font-family: "Work Sans" !important;
        }
        .fixed{
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
        }
    </style>
</head>
<body>
<?php $template->getNav() ?>

<section class="container-fluid">
    <div class="row flex-xl-nowrap">
        <main class="col-md-11 pt-md-4 bd-content">
            <?php $template->getHTML($form)  ?>
        </main>
        <aside id="sidebar" class="col-md-1" style="background: #212529; border-left: 5px solid #111">
            <?php $template->getSideBar() ?>
        </aside>
    </div>
</section>
<div class="modal fade" id="aliveLogin" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Session expired; please login again.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group px-3">
                    <div class="input-group input-group-sm border-0">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                        </div>
                        <input class="form-control" type="text" id="username" value="" placeholder="Username" required>
                    </div>
                </div>
                <div class="form-group px-3">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-key"></i></span>
                        </div>
                        <input class="form-control" type="password" id="password" value="" placeholder="********" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="aliveLoginBtn" type="button" class="btn btn-primary">Login</button>
            </div>
        </div>
    </div>
</div>
<?php $template->getFoot() ?>

<?php if (C::tknSession()){ ?>
    <div class="py-2 px-4 text-white blue-bg blue-bd fixed text-center">
        You are accessing an affiliate account.
    </div>
<?php } ?>

<script>
    <?php
            if(C::getSessionType() === 'ADMIN'){
    $ao = new \model\ApproveOffers();
    $o = new \model\Offers();
    $a = new \model\Affiliate();

    $args = [
        'status' => 0,
        'affiliate.active' => 1,
        'offers.active' => 1
    ];

    $o->setProperties($args);
    $ao->setQueryParameters($o, ['offers.ID OID',]);
    $ao->setQueryParameters($ao, ['affiliate_offers.ID AOID', 'status', 'percentage']);
    $ao->setQueryParameters($a, ['affiliate.ID AID', 'affiliate.name pub', 'affiliate.active state']);
    $get = $ao->count();

    if ($get){
    ?>
    $('#fav-icon').attr('href','<?php echo MY_SERVER ?>images/vn.png');
    $("#approveOffers").append('<span class="badge text-white ml-1" style="background:#333"><?php echo $get ?></span>');
    var t = $("#titleTop");
    t.text('(<?php echo $get ?>) '+t.text());
    <?php
    }else{ ?>
        $('#fav-icon').attr('href','<?php echo MY_SERVER ?>images/vv.png');
    <?php
    }
    }
    ?>

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-left",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    <?php
    M::display_error();
    M::display_success();
    ?>

    var table = $('.table-responsive').find('table');
    if (!table.find('tbody').find('tr').length){
        table.after('<div style="margin-top: -1rem"><span class="py-3 grey-bg text-white text-center d-block">Nothing so far</span></div>');
    }

    $('#clock').jClocksGMT({
        title: '',
        offset: '0',
        digital: true,
        analog: false,
        timeformat: 'hh:mm:ss A',
        date: true,
        dateformat: 'DDD, MMM D'
    });

    $('#aliveLoginBtn').on('click',function () {
        var u = $('#username').val(), p = $('#password').val();
        $.post('<?=s()?>/login/relive','u='+u+'&p='+btoa(p), function (res) {
            if (res.status){
                toastr['success']('Login successful.','Splendid!');
                $('#aliveLogin').modal('hide');
            }else{
                toastr['error']('Login failed. Please try again.','Error!');
            }
        },'json');
    });
    var inTime = setInterval(function () {

        if ($('#main_login_content').length){
            clearInterval(inTime); return;
        }

        $.getJSON("<?=s()?>login/dead",function (res) {
            if (res.status) {
                $('#aliveLogin').modal();
                clearInterval(inTime);
            }
        });
    },60000)
</script>
</body>
</html>