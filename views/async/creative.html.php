<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/21/19
 * Time: 8:11 PM
 */
?>
<link href="<?= s() ?>assets/css/dropzone.css" rel="stylesheet" type="text/css"/>
<style>
    .img-wide {
        max-width: 100%;
    }

    .dropzone {
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }

    .dropzone-file-area {
        border: 2px dashed #028AF4;
        background: white;
        padding: 20px;
        margin: 0 auto;
        text-align: center;
    }

    .dz-hidden-input {
        left: 0;
    }

    @media (max-width: 768px) {
        /* 768px */
        .dropzone-file-area {
            width: auto;
        }
    }
</style>
<section>
    <input type="hidden" id="offers_id" name="offers_id" value="<?= $form['id'] ?>">
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Creative</th>
            <th>Size</th>
        </tr>
        </thead>
        <tbody id="well_body">
        <?php foreach ($form['table'] as $v): ?>
            <tr>
                <td><?= $v->format_ID . '_' . $v->offers_ID ?></td>
                <td width="70%"><img class="img-wide" src="<?= $v->url ?>" width="100%"/></td>
                <td><strong><?= $v->size ?></strong></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <hr/>
    <div class="row">
        <div class="col-12">
            <div class="dropzone dropzone-file-area" id="my-dropzone"></div>
        </div>
        <div class="col-12">
            <p>Uploaded images must be in proper sizes eg. 320x50, 300x50, 320x480, 480x320.</p>
            <p>Incorrect sizes will be dropped. Duplicate formats will be over-written.</p>
        </div>
    </div>
</section>
<script src="<?= s() ?>assets/js/dropzone.js" type="text/javascript"></script>
<script>
    var myDropzone = new Dropzone("div#my-dropzone", {url: "<?=s()?>tools/upload.php?oid=<?= $form['id'] ?>"});
    var out = [];
    var error = false;

    myDropzone.on("addedfile", function (file) {
        var removeButton = Dropzone.createElement("<a href='javascript:;' class='btn red btn-sm btn-block'>Remove</a>");
        var _this = this;
        removeButton.addEventListener("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            _this.removeFile(file);
        });
        file.previewElement.appendChild(removeButton);
    });

    myDropzone.on("success", function (i, j) {
        out = j.split("|");
        $('#well_body').append('<tr>' +
            '<td>' + out[0] + '_<?= $form["id"] ?></td>' +
            '<td width="70%"><img class="img-wide" src="' + out[1] + '" width="100%"/></td>' +
            '<td><strong>' + out[2] + '</strong></td>' +
            '</tr>'
        );
        toastr["success"]("Creative successfully uploaded.", "Splendid!");
        error = false
    });

    myDropzone.on("complete", function (i) {
        var _this = this;
        setTimeout(function () {
            if (error) _this.removeFile(i);
        }, 1000);
    });
    myDropzone.on("error", function (i, j) {
        toastr["error"](j, "Error!");
        error = true;
    });

    myDropzone.on("removedfile", function () {
        $('#').load("<?=s()?>tools/delete.php?o=<?= $form['id'] ?>&f=" + out[0]);
    })

</script>
