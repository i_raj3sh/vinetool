<style>
    .timepicker-picker, .noshow {
        display: none;
    }

    .ded_input {
        width: 70px;
    }

</style>
<?= select2() ?>
<?= datepicker() ?>
<section class="row">
    <div class="col-md-7 py-3">
        <h3>Update Invoice for Affiliate</h3>

        <div>
            <form name="add_affiliate" method="post" action="<?php echo MY_SERVER ?>invoiceAffiliate/add">
                <input type="hidden" name="id" value="<?php $form->get('ID') ?>">
                <div class="form-group">
                    <label>Affiliate</label>
                    <?php
                    $g = new \model\Affiliate();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select name="af" class="form-control selected5">
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->ID ?>" <?php $form->select('affiliate_ID', $item->ID) ?> ><?php echo $item->email ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Month</label>
                    <input id="month" class="form-control" type="text" name="month" placeholder="Invoice for month"
                           value="<?php $form->get('month') ?>" required autocomplete="off">
                </div>
                <div class="form-group">
                    <label>Amount</label>
                    <input class="form-control" type="number" name="amount" placeholder="Amount" value="<?php $form->get('amount') ?>" required>
                </div>
                <div class="form-group">
                    <label>Paid</label>
                    <select id="paid_it" name="paid" class="form-control selected5">
                        <option value="0" <?php $form->select('status', '0') ?> >No</option>
                        <option value="1" <?php $form->select('status', '1') ?> >Yes</option>
                    </select>
                </div>
                <div class="noshow">
                    <div class="form-group">
                        <label>Payment method</label>
                        <select name="payment_method" class="form-control">
                            <option value="PayPal" <?php $form->select('payment_method', 'PayPal') ?> >PayPal</option>
                            <option value="Payoneer" <?php $form->select('payment_method', 'Payoneer') ?> >Payoneer
                            </option>
                            <option value="Wired" <?php $form->select('payment_method', 'Wired') ?> >Wired</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Paid on</label>
                        <input id="date0" class="form-control" type="text" name="paid_on" placeholder="Paid On"
                               value="<?php $form->get('paid_on') ?>" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Transaction Id</label>
                        <input class="form-control" type="text" name="transaction_id"
                               placeholder="Payment transaction id" value="<?php $form->get('transaction_id') ?>">
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info" name="invoice_aff" value="_">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $('#date0').datetimepicker({
        format: 'YYYYMMDD'
    });
    $('#month').datetimepicker({
        format: 'YYYYMM',
        changeMonth: true,
        showButtonPanel: true
    });

    $('.selected5').select2();

    $('#paid_it').on('change', function () {
        if ($(this).val() === '1') {
            $('.noshow').slideDown();
        } else {
            $('.noshow').slideUp();
        }
    });

    $('#month').on('focus', function () {
        $('.datepicker-days').attr('style', 'display:none !important;');
        $('.datepicker-months').attr('style', 'display:block !important;');
    }).on('blur', function () {
        $('.datepicker-days').attr('style', 'display:block !important;');
        $('.datepicker-months').attr('style', 'display:none !important;');
    });

    <?php if($form->fetch('status') === '1'){ ?>
    $('.noshow').slideDown();
    <?php } ?>
</script>