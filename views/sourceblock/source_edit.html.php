<?php

?>
<?=select2()?>
<style>
    .select2-container {
        width: 100% !important;
    }
</style>
<link rel="stylesheet" href="<?= s() ?>assets/css/tags.css"/>
<section class="row">
    <div class="col-md-7 py-3">
        <h3>Update blocked by source</h3>

        <div>
            <form name="add_" method="post" action="<?= s() ?>sourceBlock/add">
                <div class="form-group">
                    <?=affiliate()?>
                </div>
                <div class="form-group">
                    <label>Source:</label>
                    <select class="form-control" name="so[]" multiple data-role="tagsinput"
                            data-placeholder="block sources"></select>
                </div>
                <div class="form-group">
                    <label for="what_block">Block by:</label>
                    <select id="what_block" class="form-control">
                        <option value="of" selected>Campaign</option>
                        <option value="nw">Network</option>
                    </select>
                </div>
                <div id="_of" class="form-group">
                    <?= campaign() ?>
                </div>
                <div id="_nw" class="form-group" style="display: none">
                    <?= network() ?>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info" name="sblocked" value="_">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</section>
<script type="text/javascript" src="<?= s() ?>assets/js/tags.js"></script>
<script>
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });
    $('#what_block').on('change', function () {
        var s = $(this).val();
        if (s === 'of') {
            $('#_nw').hide();
            $('#_of').slideDown();
        } else {
            $('#_of').hide();
            $('#_nw').slideDown();
        }
    })
</script>