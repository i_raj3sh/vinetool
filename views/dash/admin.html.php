<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/15/17
 * Time: 9:25 PM
 */

?>
<?= select2() ?>
<?= datepicker() ?>
<style>
    .dashboard-stat{
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number{
        font-size: 1.5rem;
    }
    .number cite{
        font-size: 1.5rem;
        color: #ddd;
    }
    .dashboard-stat .desc{
        padding: .3rem .7rem;
    }
    .number{
        padding: .8rem .6rem .2rem;
    }

    .details {
        position: relative;
    }

    .stick {
        position: absolute;
        top: -5px;
        left: -5px;
        padding: 0rem .3rem;
        background: yellow;
        color: brown;
        border: 1px solid #dede05;
        font-weight: 500;
        box-shadow: 2px 1px 2px #555;
    }

    #container {
        height: 500px;
        margin: 0 auto;
    }

    .loading {
        margin-top: 10em;
        text-align: center;
        color: gray;
    }
</style>
<section class="row pt-3 pt-md-0">
    <div class="col-12">
        <div class="row">
            <div class="col-md-2 col-6">
                <div class="dashboard-stat blue-bg">
                    <div class="details">
                        <span class="stick">Revenue</span>
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="a_today">0</span>
                        </div>
                        <div class="desc blue-bd"> Today </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat purple-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="a_yester">0</span>
                        </div>
                        <div class="desc purple-bd"> Yesterday </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat grey-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="a_week">0</span>
                        </div>
                        <div class="desc grey-bd"> Last 7 days</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat red-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="a_month">0</span>
                        </div>
                        <div class="desc red-bd"> This Month </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat green-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="a_lmonth">0</span>
                        </div>
                        <div class="desc green-bd"> Last month </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat yellow-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="a_total">0</span>
                        </div>
                        <div class="desc yellow-bd"> Total </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-6">
                <div class="dashboard-stat blue-bg">
                    <div class="details">
                        <span class="stick">Profit</span>
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="w_today">0</span>
                        </div>
                        <div class="desc blue-bd"> Today</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat purple-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="w_yester">0</span>
                        </div>
                        <div class="desc purple-bd"> Yesterday</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat grey-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="w_week">0</span>
                        </div>
                        <div class="desc grey-bd"> Last 7 days</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat red-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="w_month">0</span>
                        </div>
                        <div class="desc red-bd"> This Month</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat green-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="w_lmonth">0</span>
                        </div>
                        <div class="desc green-bd"> Last month</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat yellow-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="w_total">0</span>
                        </div>
                        <div class="desc yellow-bd"> Total</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 col-6">
                <div class="dashboard-stat blue-bg">
                    <div class="details">
                        <span class="stick">Scrub</span>
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="s_today">0</span>
                        </div>
                        <div class="desc d-block d-md-none blue-bd"> Today</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat purple-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="s_yester">0</span>
                        </div>
                        <div class="desc d-block d-md-none purple-bd"> Yesterday</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat grey-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="s_week">0</span>
                        </div>
                        <div class="desc d-block d-md-none grey-bd"> Last 7 days</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat red-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="s_month">0</span>
                        </div>
                        <div class="desc d-block d-md-none red-bd"> This Month</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat green-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="s_lmonth">0</span>
                        </div>
                        <div class="desc d-block d-md-none green-bd"> Last month</div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-6">
                <div class="dashboard-stat yellow-bg">
                    <div class="details">
                        <div class="number">
                            <cite>$</cite><span class="counter-number" data-counter="counterup" id="s_total">0</span>
                        </div>
                        <div class="desc d-block d-md-none yellow-bd"> Total</div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <form class="row py-3">
            <div class="col-md-3">
                <?= campaign() ?>
            </div>
            <div class="col-md-3">
                <?= affiliate() ?>
            </div>
            <div class="col-md-3">
                <?= network() ?>
            </div>
            <?= dateSpan() ?>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <!-- <div id="geos" style="height: 600px"></div> -->
        <div id="container">
            <div class="loading">
                Loading data...
            </div>
        </div>
        <hr/>
        <div class="row">
            <div id="contain_Offer_top" class="col-md-6"></div>
            <div id="contain_Offer" class="col-md-6"></div>
        </div>
        <hr/>
        <div class="row">
            <div id="contain_Affiliate_top" class="col-md-6"></div>
            <div id="contain_Affiliate" class="col-md-6"></div>
        </div>
        <hr/>
        <div class="row">
            <div id="contain_Network_top" class="col-md-6"></div>
            <div id="contain_Network" class="col-md-6"></div>
        </div>
        <hr/>
        <div class="row">
            <div id="contain_PAM_top" class="col-md-6"></div>
            <div id="contain_Publisher Team's" class="col-md-6"></div>
        </div>
        <hr/>
        <div class="row">
            <div id="contain_AAM_top" class="col-md-6"></div>
            <div id="contain_Advertiser Team's" class="col-md-6"></div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                <div id="stats_block_visits"></div>
            </div>
            <div class="col-md-6">
                <div id="stats_block_sales"></div>
            </div>
        </div>
        <hr/>
        <div class="table-responsive">
            <h4 class="my-4"><?= $form['title'] ?></h4>
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th><?php echo $form['type'] ?></th>
                    <th>Clicks (Total)</th>
                    <th>Clicks (Verified)</th>
                    <th>Sales</th>
                    <th>Profit</th>
                    <th>Payout</th>
                    <th>Charged</th>
                    <th>EPC</th>
                    <th>CR%</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):
                    echo '<tr>';
                    echo '<td>' . $v['hour'] . '</td>';
                    echo '<td>' . $v['total'] . '</td>';
                    echo '<td>' . $v['visit'] . '</td>';
                    echo '<td>' . $v['convert'] . '</td>';
                    echo '<td class="text-primary">' . $v['profit'] . '</td>';
                    echo '<td>' . $v['push'] . '</td>';
                    echo '<td>' . $v['pull'] . '</td>';
                    echo '<td>' . $v['EPC'] . '</td>';
                    echo '<td>' . $v['CR'] . '</td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/variable-pie.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/maps/modules/map.js"></script>
<script src="https://code.highcharts.com/mapdata/custom/world-highres3.js"></script>
<!--<script type="text/javascript" src="--><?php //echo MY_SERVER ?><!--assets/js/ammap.js"></script>-->
<!--<script type="text/javascript" src="--><?php //echo MY_SERVER ?><!--assets/js/world.js"></script>-->
<script type="text/javascript" src="<?php echo MY_SERVER ?>assets/js/stats.js"></script>
<script type="text/javascript">
    var cate = <?php echo $form['cate'] ?>,
        visits = <?php echo $form['visits'] ?>,
        sales = <?php echo $form['sales'] ?>,
        forVisits = '<?php echo $form['forVisits'] ?>',
        forSales = '<?php echo $form['forSales'] ?>',
        geos_ = <?php echo $form['geos'] ?>;
    dash('stats_block_visits',cate,visits,forVisits);
    barSpline('stats_block_sales', cate, sales, forSales, 'Conversions');

    $('.selector').select2({
        placeholder: "--select--",
        allowClear: true
    });

    //if (window.orientation === undefined) {
    //getGeoChart(geos_, "Earnings based on countries");
    /*}else{
        $('#geos').hide();
    }*/
    highmap(geos_);

    $.getJSON("<?=s()?>async/widget", function (result) {
        //Profit
        $('#w_today').easy_number_animate({end_value: result.pToday});
        $('#w_yester').easy_number_animate({end_value: result.pYesterday});
        $('#w_week').easy_number_animate({end_value: result.pWeek});
        $('#w_month').easy_number_animate({end_value: result.pMonth});
        $('#w_lmonth').easy_number_animate({end_value: result.pMonthL});
        $('#w_total').easy_number_animate({end_value: result.pTotal});

        //Revenue
        $('#a_today').easy_number_animate({end_value: result.rToday});
        $('#a_yester').easy_number_animate({end_value: result.rYesterday});
        $('#a_week').easy_number_animate({end_value: result.rWeek});
        $('#a_month').easy_number_animate({end_value: result.rMonth});
        $('#a_lmonth').easy_number_animate({end_value: result.rMonthL});
        $('#a_total').easy_number_animate({end_value: result.rTotal});

        //Scrubbed
        $('#s_today').easy_number_animate({end_value: result.sToday});
        $('#s_yester').easy_number_animate({end_value: result.sYesterday});
        $('#s_week').easy_number_animate({end_value: result.sWeek});
        $('#s_month').easy_number_animate({end_value: result.sMonth});
        $('#s_lmonth').easy_number_animate({end_value: result.sMonthL});
        $('#s_total').easy_number_animate({end_value: result.sTotal});
    });

    <?php if (isset($_GET['date'])){
        $m = substr($_GET['date'],0,6);
        $mm = (int)substr($m,4,2) - 1;
    }else{
        $m = date('Ym');
        $mm = date('n') - 1;
    }
    ?>
    $.getJSON("<?php echo MY_SERVER ?>async/dash?m=<?php echo $m ?>",function (res) {
        pieStats(res.o<?php echo ','.$mm ?>,'Offer');
        pieStats(res.a<?php echo ','.$mm ?>,'Affiliate');
        pieStats(res.n<?php echo ','.$mm ?>,'Network');
        pieStats(res.pam<?php echo ',' . $mm ?>, 'Publisher Team\'s');
        pieStats(res.aam<?php echo ',' . $mm ?>, 'Advertiser Team\'s');
        pieMulti(res.ot,'Today\'s Top 5 converting offers','Offer');
        pieMulti(res.at,'Today\'s Top 5 performing affiliates','Affiliate');
        pieMulti(res.nt,'Today\'s Top 5 performing networks','Network');
        bar('contain_PAM_top', 'Publisher Team\'s performance for Today', res.pamt);
        bar('contain_AAM_top', 'Advertiser Team\'s performance for Today', res.aamt);
    });

</script>