<?php

use helper\Template as T;

?>

<section class="row">
    <div class="col-12">
        <h3 class="pt-3">My Offers</h3>
        <form class="row py-3">
            <?=tknInp()?>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Search TID:</label>
                    <input type="text" class="form-control" name="t"
                           value="<?php echo \helper\Request::getField('t', 'GET') ?>" placeholder="TID">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Search Offer ID:</label>
                    <input type="text" class="form-control" name="i"
                           value="<?php echo \helper\Request::getField('i', 'GET') ?>" placeholder="Offer ID">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Search name:</label>
                    <input type="text" class="form-control" name="n" value="<?php echo \helper\Request::getField('n','GET') ?>" placeholder="Offer Name">
                </div>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="table-responsive">
            <?php
            if (empty($form))
                echo '<p class="text-warning">No approved offers</p>';
            else {
            ?>
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>TID</th>
                    <th>Offer ID</th>
                    <th>Offer</th>
                    <th>Type</th>
                    <th>Payout</th>
                    <th>Cap</th>
                    <th>OS</th>
                    <th>Geo</th>
                    <th>Active</th>
                    <th>Approved</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):
                    echo '<tr>';
                    echo '<td>' . $v->TID . '</td>';
                    echo '<td>'.$v->ID.'</td>';
                    echo '<td><a href="./offers/view?id=' . $v->ID . tkn('&') . '">' . $v->name . '</a></td>';
                    echo '<td>'.$v->type.'</td>';
                    echo '<td>$'.($v->charge * $v->percentage/100).'</td>';
                    echo '<td>'.T::cap($v->cap, $v->caps).'</td>';
                    echo '<td>' . T::wordBreak(T::devIcons(json_decode($v->device), $v->min_os_ver), BR) . '</td>';
                    echo '<td data-toggle="tooltip" title="' . T::wordBreak(json_decode($v->country), ', ') . '">' . T::geoFormat(json_decode($v->country)) . '</td>';
                    echo '<td>'.T::state($v->active).'</td>';
                    echo '<td>'.T::approval($v->status).'</td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
                <?php echo $form['pages']; echo $form['perPage'] ?>
            <?php } ?>
        </div>
    </div>
</section>

<script>
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover',
        placement: 'top'
    });
</script>