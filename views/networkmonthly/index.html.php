<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 9:01 PM
 */
?>

<?php

use helper\Request;

$af = Request::getField('nw','GET');
?>
<?= select2() ?>
<?= datatables() ?>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Network Monthly Report for
            <?php if(Request::getField('y','GET')) echo Request::getField('y','GET'); else echo date('Y') ?>
        </h3>
        <form class="row py-3">
            <div class="col-md-5">
                <?= network() ?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Year:</label>
                    <input type="text" class="form-control" name="y" value="<?php echo Request::getField('y','GET') ?>" placeholder="Year">
                </div>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning">Get Report</button>
            </div>
        </form>
        <div class="table-responsive">
            <?php if (Request::getField('nw','GET')): ?>
                <table class="table table-striped tabular">
                <thead class="thead-dark">
                <tr>
                    <th></th>
                    <th>Month</th>
                    <th>Advertiser</th>
                    <th>Visits</th>
                    <th>Sale</th>
                    <th>Payout</th>
                    <th>Due</th>
                    <th>Total</th>
                    <th>Paid</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form as $k=>$v):
                    echo '<tr>';
                    echo '<td>'.($k).'</td>';
                    echo '<td>'.$v->month.'</td>';
                    echo '<td>'.$v->person.'</td>';
                    echo '<td>'.$v->visits.'</td>';
                    echo '<td>'.$v->sales.'</td>';
                    echo '<td>'.$v->amount.'</td>';
                    echo '<td>'.$v->due.'</td>';
                    echo '<td>'.$v->total.'</td>';
                    if ($v->paid === '-'){
                        echo '<td>-</td>';
                    }elseif($v->paid == 1){
                        echo '<td><span class="text-success">Paid</span></td>';
                    }else {
                        echo '<td><span class="text-danger">Pending</span></td>';
                    }
                if ($v->paid === '-'){
                    echo '<td>-</td>';
                }
                    elseif ($v->paid == 0){
                        echo '<td><a class="btn btn-sm btn-info" href="'.MY_SERVER.'networkMonthly/tick?id=' . $v->ID . '&p=1"><i class="fa fa-check"></i> Paid</a></td>';
                    }elseif ($v->paid == 1){
                        echo '<td><a class="btn btn-sm btn-danger" href="'.MY_SERVER.'networkMonthly/tick?id=' . $v->ID . '&p=0"><i class="fa fa-close"></i> Unpaid</a></td>';
                    }

                endforeach;
                ?>
                </tbody>
            </table>
            <?php else: ?>
                <table class="table table-striped tabular">
                    <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Advertiser</th>
                        <th>Payout</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($form as $v):
                        echo '<tr>';
                        echo '<td>'.$v->network_ID.'</td>';
                        echo '<td>'.$v->network.'</td>';
                        echo '<td><i class="fa fa-dollar text-danger"></i> '.number_format($v->amount,4).'</td>';
                        echo '<td><a class="btn btn-sm btn-info" href="'.MY_SERVER.'networkMonthly/?nw=' . $v->network_ID . '"><i class="fa fa-eye"></i> View</a></td>';
                        echo '</tr>';
                    endforeach;
                    ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</section>
<script>
    $('a#tools').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    $('.tabular').DataTable();
</script>