<?php

use helper\Request;
use helper\Template as T;

$af = Request::getField('af','GET');
$nw = Request::getField('nw','GET');
$ap = Request::getField('ap','GET');
$of = Request::getField('of','GET');
?>
<style>
    input[type="checkbox"]{
        width: auto;
    }
</style>
<?= datepicker() ?>
<?= select2() ?>
<?= datatables() ?>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Assigned offers<a class="btn btn-sm btn-primary float-right"
                                           href="<?php echo MY_SERVER ?>approveOffers/edit"><i class="fa fa-plus"></i>
                Assign</a></h3>
        <form class="row py-3">
            <div class="col-md-3">
                <?php
                $g = new \model\Affiliate();
                $g->setProperties([
                    'active' => 1
                ]);
                $g->setQueryParameters($g);
                $all = (array)$g->query();
                ?>
                <label>Affiliate:</label>
                <select name="af" class="form-control selected5">
                    <option value="" selected>--select-affiliate--</option>
                    <?php foreach ($all as $item): ?>
                        <option value="<?php echo $item->ID ?>" <?php if ($af == $item->ID) echo 'selected' ?>><?php echo $item->name." ($item->company)" ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-3">
                <?=network()?>
            </div>
            <div class="col-md-1">
                <label>TID:</label>
                <input type="text" class="form-control" name="ap" placeholder="TID" value="<?php echo $ap ?>">
            </div>
            <div class="col-md-1">
                <label>Offer ID:</label>
                <input type="text" class="form-control" name="of" placeholder="Offer ID" value="<?php echo $of ?>">
            </div>
            <?= dateSpan('Assigned on') ?>
            <div class="col-md-1" style="margin-top: 2.3rem">
                <input type="checkbox" style="display: inline-flex" class="form-control"
                       name="as" <?php if (isset($_GET['as'])) echo 'checked'; ?> >
                <label class="float-right">Assigned</label>
            </div>
            <div class="col-md-1">
                <button style="margin-top: 1.7rem" type="submit" class="btn btn-warning">Get List</button>
            </div>
        </form>

        <span><label style="font-size: 1.1rem">With selected:</label>
            <a id="checkedTick" class="btn btn-sm btn-info mb-1" style="font-size: .7rem;padding: .2rem .6rem" href="#!"><i class="fa fa-check"></i></a>
            <a id="checkedDel" class="btn btn-danger btn-sm mb-1 ml-1" style="font-size: .7rem;padding: .2rem .6rem" href="#!"><i class="fa fa-close"></i></a></span>
        <script>
            $(document).ready(function () {
                $('input#allbox').on('change',function () {
                    var c = $('.checker');
                    if (this.checked){
                        c.prop('checked',true);
                    }else{
                        c.prop('checked',false);
                    }
                });
                $('a#checkedTick').on('click',function () {
                    var c = $('.checker'), p = [];
                    c.each(function (i,j) {
                        if(j.checked)
                            p.push(j.id.split('$')[1]);
                    });
                    jax('tickAll',p);
                });
                $('a#checkedDel').on('click',function () {
                    var c = $('.checker'), p = [];
                    c.each(function (i,j) {
                        if(j.checked)
                            p.push(j.id.split('$')[1]);
                    });
                    jax('delAll',p);
                });
                function jax(a,b){
                    b = b+'';
                    if (b) {
                        window.location = '<?=s()?>approveOffers/' + a + '?ids=' + b;
                    }
                }
            });
        </script>

        <div class="table-responsive mt-1">
            <table id="tabular" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th><input id="allbox" class="form-control" type="checkbox"></th>
                    <th>TID</th>
                    <th>OID</th>
                    <th>Offer</th>
                    <th>Affiliate</th>
                    <th>NetworkID</th>
                    <th>Type</th>
                    <th>Payout</th>
                    <th>Charge</th>
                    <th>RequestedAt</th>
                    <th>ApprovedAt</th>
                    <th>Approved</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form as $v):

                    echo '<tr>';
                    echo '<td><input id="AO$'.$v->AOID.'" class="form-control checker" type="checkbox"></td>';
                    echo '<td>'.$v->AOID.'</td>';
                    echo '<td>'.$v->ID.'</td>';
                    echo '<td><a href="'.MY_SERVER.'offers/view?id=' . $v->ID . '">'.$v->name.'</a></td>';
                    echo '<td>'.$v->pub.'</td>';
                    echo '<td>'.$v->network_ID.'</td>';
                    echo '<td>'.$v->type.'</td>';
                    echo '<td><code>$</code>' . ($v->charge * $v->percentage / 100) . '</td>';
                    echo '<td><code>$</code>' . $v->charge . '</td>';
                    echo '<td>'.T::time($v->requested).'</td>';
                    echo '<td>'.T::time($v->approved).'</td>';
                    echo '<td>'.T::approval($v->status,$v->active).'</td>';
                    if(!$v->status){
                    if ($v->api && !$af)
                        echo '<td><span class="badge badge-info">requested</span></td>';
                    else
                        echo '<td><div class="btn-group"><a class="btn btn-sm btn-info" href="'.MY_SERVER.'approveOffers/tick?id='.$v->AOID.'"><i class="fa fa-check"></i></a><a class="btn btn-danger btn-sm" href="#!" onclick=delet("'.MY_SERVER.'approveOffers/del?id='.$v->AOID.'")><i class="fa fa-close"></i></a></div></td>';
                    }else {
                        echo '<td><div class="btn-group"><a class="btn btn-info btn-sm" href="'.MY_SERVER.'approveOffers/edit?id='.$v->AOID.'"><i class="fa fa-pencil"></i></a><a class="btn btn-danger btn-sm" href="#!" onclick=delet("'.MY_SERVER.'approveOffers/del?id='.$v->AOID.'")><i class="fa fa-close"></i></a></div> </td>';
                        echo '</tr>';
                    }
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script type="text/javascript">
    function delet(loc, conf) {
        conf = confirm("Are you sure? Do you want to delete?");

        if (conf){
            window.location = loc;
        }
    }
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });
    $('#tabular').DataTable();

</script>