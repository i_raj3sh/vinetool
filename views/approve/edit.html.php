<?= select2() ?>

<section class="row">
    <div class="col-md-7 py-3">
        <h3>Assign offer</h3>

        <div class="pt-3">
            <form name="add_approve" method="post" action="<?php echo MY_SERVER ?>approveOffers/add">
                <input type="hidden" name="id" value="<?php $form->get('AOID') ?>">
                <div class="form-group">
                    <label>Affiliate</label>
                    <?php
                    $g = new \model\Affiliate();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select name="affiliate" class="form-control selected5">
                        <option value="" selected>--select-affiliate--</option>
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->ID ?>" <?php $form->select('AID',$item->ID) ?> ><?php echo $item->name." ($item->company)" ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Offer ID requested</label>
                    <input class="form-control" type="text" name="offer" placeholder="Offer ID #" value="<?php $form->get('OID') ?>">
                </div>
                <div class="form-group">
                    <label>Cap [leave empty for unlimited]</label>
                    <input class="form-control" type="text" name="cap" placeholder="Cap" value="<?php $form->get('cap') ?>">
                </div>
                <div class="form-group">
                    <label>Commission Adjust on (% | $)</label>
                    <select id="adjust" class="form-control">
                        <option value="%" selected >Percentage</option>
                        <option value="$" >USD value</option>
                    </select>
                </div>
                <div id="percent" class="form-group">
                    <label>Percentage commission</label>
                    <input id="cutt" class="form-control" type="text" name="percent" placeholder="Commission" value="<?php $form->get('percentage') ?>" maxlength="5" max="85" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;">
                </div>
                <div id="valuable" style="display: none">
                    <div class="form-group">
                        <label>Advertiser charged</label>
                        <input id="advC" class="form-control" type="text" placeholder="Advertiser charged for the offer" value="" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;">
                    </div>
                    <div class="form-group">
                        <label>Publisher payout&nbsp;&nbsp;<strong id="shoot"></strong></label>
                        <input id="pubP" class="form-control" type="text" placeholder="Publisher should get for the offer" value="" onkeypress="if ( isNaN(this.value + String.fromCharCode(event.keyCode) )) return false;">
                    </div>
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control selected5">
                        <option value="1" <?php $form->select('status','1') ?> >Approve</option>
                        <option value="2" <?php $form->select('status','2') ?> >Pause</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info" name="approve" value="_">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</section>
<script>
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    $('#adjust').on('change',function () {
       if (this.value === '%'){
           $('#percent').slideDown();
           $('#valuable').slideUp();
       }else{
           $('#valuable').slideDown();
           $('#percent').slideUp();
       }
    });

    $('#advC').on('keyup change',function () {
        var that = $('#pubP').val();
        together(this.value, that);
    })
    $('#pubP').on('keyup change',function () {
        var that = $('#advC').val();
        together(that, this.value);
    })
    function together(adv, pub, per) {
        if (adv !== '' && pub !== ''){
            per = pub/adv;
            $('#cutt').val(per * 100);
            $('#shoot').text((per * 100)+'%');
        }
    }
</script>