<section class="row">
    <div class="col-md-7 py-3">
        <h3>Update Admin</h3>

        <div>
            <form name="add_admin" method="post" action="<?php echo MY_SERVER ?>admin/add">
                <input type="hidden" name="id" value="<?php $form->get('ID') ?>">
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" type="text" name="name" placeholder="Name" value="<?php $form->get('name') ?>" required>
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input class="form-control" type="text" name="username" placeholder="Username" value="<?php $form->get('username') ?>" required>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="email" name="email" placeholder="Email" value="<?php $form->get('email') ?>" required>
                </div>
                <?php if (!$form->fetch('ID')) { ?>
                    <div class="form-group">
                        <label>Password</label>
                        <input class="form-control" type="password" name="password" placeholder="Password" value="" required>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label>Super Admin</label>
                    <select name="type" class="form-control">
                        <option value="SUPER" <?php $form->select('type','SUPER') ?> >Super admin</option>
                        <option value="DEFAULT" <?php $form->select('type','DEFAULT') ?>  >Default </option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Active</label>
                    <select name="active" class="form-control">
                        <option value="1" <?php $form->select('active','1') ?> >active</option>
                        <option value="0" <?php $form->select('active','0') ?> >inactive</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info" name="admin" value="_">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</section>