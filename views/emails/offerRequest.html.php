<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/21/17
 * Time: 4:20 PM
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no" />
</head>
<body style="font-family: verdana,arial,sans-serif">
<table align="center" style="margin-top: 16px;width: 100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td style="font-size:12px; line-height:30px;color:#FFF;background-color: #dc3545;padding: 50px 10px 0 30px">
            <h1 style="margin-top:0; padding-left: 10px">VineTool</h1>
        </td>
    </tr>
    <tr>
        <td valign="top" style="padding: 40px;background: #dedede;border-bottom: 1px solid #aaa;">
            Hello <strong>Team</strong>,
            <p>The following offer is request by <strong><?php echo $arr['name'] ?></strong></p>
            <br/>
            <p><img src="<?php echo $arr['icon'] ?>" alt="VineTool:<?php echo $arr['title'] ?>" width="300px" height="300px" /></p>
            <p><strong>Offer ID: </strong>#<?php echo $arr['ID'] ?><br/>
            <p><strong>Offer title: </strong><?php echo $arr['title'] ?><br/>
            <br/>
            <br/>
            <strong style="color: #d24">VineTool</strong>
        </td>
    </tr>
</table>
</body>
</html>
