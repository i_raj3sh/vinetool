<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/25/17
 * Time: 7:02 PM
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no" />
</head>
<body style="font-family: verdana,arial,sans-serif">
<table align="center" style="margin-top: 16px;width: 100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td style="font-size:12px; line-height:30px;color:#FFF;background-color: #dc3545;padding: 50px 10px 0 30px">
            <h1 style="margin-top:0; padding-left: 10px">VineTool</h1>
        </td>
    </tr>
    <tr>
        <td valign="top" style="padding: 40px;background: #dedede;border-bottom: 1px solid #aaa;">
            Hello <strong><?php echo $arr['name'] ?></strong>,
            <p>The following offers are <strong>Re-Activated</strong>. Please start sending traffic from your end.</p>
            <br/>
            <?php foreach ($arr['OID'] as $k=>$item) {?>
                <p><strong>#<?php echo $item ?></strong>&nbsp;-&nbsp;<?php echo $arr['title'][$k] ?></p>
            <?php } ?>
            <br/>
            <br/>
            <p style="color: #777">Please contact account manager for more details.</p>
            <p style="color: #555">Thanking You,</p>
            <strong style="color: #d24">VineTool</strong>
        </td>
    </tr>
</table>
</body>
</html>