<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/19/17
 * Time: 2:04 PM
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no" />
</head>
<body style="font-family: verdana,arial,sans-serif">
<table align="center" style="margin-top: 16px;width: 100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td style="font-size:12px; line-height:30px;color:#FFF;background-color: #dc3545;padding: 50px 10px 0 30px">
            <h1 style="margin-top:0; padding-left: 10px">VineTool</h1>
        </td>
    </tr>
    <tr>
        <td valign="top" style="padding: 40px;background: #dedede;border-bottom: 1px solid #aaa;">
            Dear <strong><?php echo $arr['name'] ?></strong>,
            <p>An offer has been approved for you. Following are the details.</p>
            <br/>
            <p><img src="<?php echo $arr['icon'] ?>" alt="VineTool:<?php echo $arr['title'] ?>" width="300px" height="300px" /></p>
            <p><strong>Offer ID: </strong>#<?php echo $arr['ID'] ?><br/>
            <p><strong>Offer title: </strong><?php echo $arr['title'] ?><br/>
            <p><strong>Offer payout: $</strong><?php echo $arr['payout'] ?><br/>
            <p><strong>Offer cap: </strong><?php echo $arr['cap'] ?><br/>
            <p><strong>Offer tracking: </strong><?php echo $arr['tracking'] ?><br/><br/>
            <p><strong>Macros: </strong><br/>
                <?php echo $arr['macros'] ?><br/>
            </p>
            <br/>
            <br/>
            <p style="color: #777">Please contact account manager for more details.</p>
            <br/>
            <p style="color: #555">Thanking You,</p>
            <strong style="color: #d24">VineTool</strong>
        </td>
    </tr>
    <tr>
        <td style="padding:10px;font-size:10px;color:#aaa">This email can't receive replies.</td>
    </tr>
</table>
</body>
</html>
