<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/17/17
 * Time: 7:07 PM
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no" />
</head>
<body style="font-family: verdana,arial,sans-serif">
<table align="center" style="margin-top: 16px;width: 100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td style="font-size:12px; line-height:30px;color:#FFF;background-color: #dc3545;padding: 50px 10px 0 30px">
            <h1 style="margin-top:0; padding-left: 10px">VineTool</h1>
        </td>
    </tr>
    <tr>
        <td valign="top" style="padding: 40px;background: #dedede;border-bottom: 1px solid #aaa;">
            Dear <strong><?php echo $arr['name'] ?></strong>,
            <p>Here are some new campaigns. Find out more on <a href="https://op.vinetool.com/offers">VineTool.com</a>.</p>
            <br/>
            <table style="width: 100%;font-size: .8rem;border-collapse: collapse; text-align: left" border="1" cellpadding="2">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Logo</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Payout</th>
                    <th>Cap</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($arr['offers'] as $item) {?>
                    <tr>
                        <td><?php echo $item->ID ?></td>
                        <td><img src="<?php echo $item->icon ?>" width="30" height="30" /></td>
                        <td><a href="https://op.vinetool.com/offers/view?id=<?php echo $item->ID ?>"><?php echo $item->name ?></a></td>
                        <td><?php echo $item->type ?></td>
                        <td>$<?php echo ($item->charge * 0.5) ?></td>
                        <td><?php if ($item->caps == 0) echo 'n/a'; else echo $item->caps ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <p style="color: #777">Please contact account manager for more details.</p>
            <br/>
            <p style="color: #555">Thanking You,</p>
            <strong style="color: #d24">VineTool</strong>
        </td>
    </tr>
    <tr>
        <td style="padding:10px;font-size:10px;color:#aaa">This email can't receive replies.</td>
    </tr>
</table>
</body>
</html>
