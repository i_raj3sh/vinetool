<?= datatables() ?>
<?php

use helper\Template;

?>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Network panel <a class="btn btn-sm btn-primary float-right" href="<?php echo MY_SERVER ?>network/edit"><i class="fa fa-plus"></i> network</a></h3>
        <form class="row py-3">
            <div class="col-md-3">
                <div class="form-group">
                    <label>ID:</label>
                    <input type="text" class="form-control" name="id"
                           value="<?php echo \helper\Request::getField('id', 'GET') ?>" placeholder="Network ID">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Name / company:</label>
                    <input type="text" class="form-control" name="n"
                           value="<?php echo \helper\Request::getField('n', 'GET') ?>"
                           placeholder="Network name / company">
                </div>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="table-responsive">
            <table id="tabular" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Skype</th>
                    <th>Country</th>
                    <th>Company</th>
                    <th>Website</th>
                    <th>Manager</th>
                    <th>Active</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):
                    echo '<tr>';
                    echo '<td>'.$v->ID.'</td>';
                    echo '<td>'.$v->name.'</td>';
                    echo '<td>'.$v->email.'</td>';
                    echo '<td>'.$v->skype.'</td>';
                    echo '<td>'.$v->country.'</td>';
                    echo '<td>'.$v->company.'</td>';
                    echo '<td>'.$v->website.'</td>';
                    echo '<td>' . $v->manager . '</td>';
                    echo '<td>'.\helper\Template::state($v->active).'</td>';
                    ?>
                    <td style="display: inline-flex">
                        <a class="btn btn-sm" href="#!" onclick="smash(<?= $v->ID ?>)"><i
                                    class="fa fa-bell text-primary"></i></a>
                        <div class="btn-group">
                            <a class="btn btn-sm btn-success" href="#!"
                               onclick="popBack(<?= $v->ID ?>,'<?= str_replace(' ', '.', $v->company) ?>')"><i
                                        class="fa fa-link"></i></a>
                            <?php echo Template::entityStateButton($v, 'network'); ?>
                            <a class="btn btn-info btn-sm" href="<?= s() ?>network/edit?id=<?= $v->ID ?>"><i
                                        class="fa fa-pencil"></i></a>
                            <a class="btn btn-danger btn-sm" href="#!"
                               onclick=delet("<?= s() ?>network/del?id=<?= $v->ID ?>")><i class="fa fa-remove"></i></a>
                        </div>
                    </td>
                    <?php
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
            <?php echo $form['pages']; echo $form['perPage'] ?>
        </div>
    </div>
</section>
<div class="modal fade" id="nameEvents" role="dialog" aria-labelledby="nameEvents" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="modal-content" action="network/events">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Enter event IDs wrt their names.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="bodyEvents" class="modal-body">
                <input type="hidden" id="network_id" name="network_id" value="">
                <div class="row">
                    <?= \model\Event::getEventListHtml() ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="saveModelEvent" type="submit" name="nameEvents" value="_" class="btn btn-primary">Add
                </button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="popBack" role="dialog" aria-labelledby="popBack" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Callback url for network.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="bodyBack" class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <textarea class="form-control" id="popUrl" rows="5" readonly></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="copyBack" class="btn btn-info" title="Postback is copied">Copy</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function delet(loc, conf) {
        conf = confirm("Are you sure? Do you want to delete?");

        if (conf){
            window.location = loc;
        }
    }

    function smash(i) {
        $('.event_in').val('');
        $.getJSON("<?=s()?>network/pullEvents?id=" + i, function (res) {
            for (var k in res) {
                $('#' + res[k].id).val(res[k].val);
            }
        });
        $('#network_id').val(i);
        $('#nameEvents').modal();
    }

    function popBack(i, n) {
        var url = 'https://postback.vinetool.com/?nid=' + i + '&brand=' + n + '&click=[CLICK]&cam=[YOUR_OFFER_ID]&payout=[PAYOUT]&eid=[EVENT_ID]&enm=[EVENT_NAME]&ip=[IP]&ua=[UA]&ifa=[DEVICE_ID]';
        $('#popUrl').val(url);
        $('#popBack').modal();
    }

    $('#copyBack').on('click', function () {
        var s = document.getElementById('popUrl');
        s.select();
        document.execCommand('copy');
    }).tooltip({
        trigger: 'click'
    });
    $('#tabular').DataTable();
</script>