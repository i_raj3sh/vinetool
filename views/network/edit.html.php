<section class="row">
    <div class="col-md-7 py-3">
        <h3>Update Network</h3>

        <div>
            <form name="add_affiliate" method="post" action="<?php echo MY_SERVER ?>network/add">
                <input type="hidden" name="id" value="<?php $form->get('ID') ?>">
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" type="text" name="name" placeholder="Name" value="<?php $form->get('name') ?>" required>
                </div>
                <div class="form-group">
                    <label>Username</label>
                    <input class="form-control" type="text" name="username" placeholder="Username" value="<?php $form->get('username') ?>" required>
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="email" name="email" placeholder="Email" value="<?php $form->get('email') ?>" required>
                </div>
                <?php if (!$form->fetch('ID')) { ?>
                    <div class="form-group">
                        <label>Password</label>
                        <input class="form-control" type="password" name="password" placeholder="Password" value=""
                               required>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label>Skype</label>
                    <input class="form-control" type="text" name="skype" placeholder="Skype" value="<?php $form->get('skype') ?>">
                </div>
                <div class="form-group">
                    <label>Company Name</label>
                    <input class="form-control" type="text" name="company" placeholder="Company Name"
                           value="<?php $form->get('company') ?>" required>
                </div>
                <div class="form-group">
                    <label>Website. [include http(s)]</label>
                    <input class="form-control" type="url" name="website" placeholder="Website" value="<?php $form->get('website') ?>">
                </div>
                <div class="form-group">
                    <label>API Key</label>
                    <input class="form-control" type="text" name="apiKey" placeholder="API Key"
                           value="<?php $form->get('apiKey') ?>">
                </div>
                <div class="form-group">
                    <label>API Domain. [include http(s)]</label>
                    <input class="form-control" type="url" name="apiDomain" placeholder="Api Domain"
                           value="<?php $form->get('apiDomain') ?>">
                </div>
                <div class="form-group">
                    <label>Country</label>
                    <?php
                    $g = new \model\Country();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select name="country" class="form-control">
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->code ?>" <?php $form->select('country',$item->code) ?> ><?php echo $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Manager</label>
                    <?php
                    $g = new \model\Admin();
                    $g->setProperties([
                        'active' => 1
                    ]);
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select name="admin" class="form-control">
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->ID ?>" <?php $form->select('admin_ID', $item->ID) ?> ><?php echo $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Restrict Fallback</label>
                    <select name="fallback" class="form-control selected5">
                        <option value="0" <?php $form->select('fallback', '0') ?> >Nope</option>
                        <option value="1" <?php $form->select('fallback', '1') ?> >Yep</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Active</label>
                    <select name="active" class="form-control">
                        <option value="1" <?php $form->select('active','1') ?> >active</option>
                        <option value="0" <?php $form->select('active','0') ?> >inactive</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info" name="network" value="_">Proceed</button>
                </div>
            </form>
        </div>
    </div>
</section>