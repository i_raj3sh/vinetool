<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 10/21/18
 * Time: 12:20 PM
 */

?>
<style>
    .bd-1 {
        border-bottom: 1px solid #ccc
    }

    .bt-1 {
        border-top: 1px solid #ccc
    }

    .lb {
        font-weight: 500;
    }

    .large {
        font-size: 1rem;
    }
</style>
<div class="col-12">
    <h3 class="pb-3 pl-3 bd-1">Invoice</h3>
</div>
<section class="row">
    <div class="col-1"></div>
    <div class="col-10">
        <div class="row">
            <div class="col-9 pt-4">
                <strong><?= $form->af->company ?></strong><br/>
                <?= $form->af->country ?><br/>
                <?= $form->af->email ?>
            </div>
            <div class="col-3 pt-4">
                <?php if ($form->status === '0') { ?>
                    <span class="float-right badge badge-danger large">Unpaid</span>
                <?php } else { ?>
                    <span class="float-right badge badge-success large">Paid</span>
                <?php } ?>
            </div>
            <div class="col-12 pt-3">
                <h5 class="pt-2">Bill to:</h5>
            </div>
            <div class="col-md-6">
                <img src="../images/vv.png" width="16" height="16" alt="Vinetool media logo tm"
                     style="margin-top: -4px"/>
                <strong style="margin-left: -3px">inetool media</strong> <br/>
                5/117 Viraj Khand, Gomti Nagar<br/>
                Lucknow - IN <br/>
                <a href="mailto:support@vinetool.com">support@vinetool.com</a>
            </div>
            <div class="col-md-6 text-right">
                Invoice #: <strong><?= $form->af->ID . $form->month ?></strong> <br/>
                Invoice date: <strong><?= date('M d, Y', $form->at) ?></strong> <br/>
                Invoice for: <strong><?= \helper\Tool::getMonthName(substr($form->month, 4, 2)) ?></strong>
            </div>
            <div class="w-100 pt-4"></div>
            <div class="col-12">
                <div class="table table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Campaign</th>
                            <th>Sales</th>
                            <th>Payout</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $amount = 0 ?>
                        <?php foreach ($form->confirmed as $v): ?>
                            <tr>
                                <td>#<?= $v->offers_ID ?>&nbsp;<?= $v->offer ?></td>
                                <td><?= number_format($v->sales) ?></td>
                                <td><code>$</code><?= $v->per ?></td>
                                <td><code>$</code><?= number_format($v->payout, 2) ?></td>
                            </tr>
                            <?php $amount += $v->payout ?>
                        <?php endforeach; ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><strong class="float-right">Total:</strong></td>
                            <td><code>$</code><span class="lb"><?= number_format($amount, 2) ?></span></td>
                        </tr>
                        <?php if (!empty($form->unconfirmed)): ?>
                            <tr>
                                <td><strong>Unconfirmed</strong></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php foreach ($form->unconfirmed as $v): ?>
                                <tr style="color: #777">
                                    <td>#<?= $v->offers_ID ?>&nbsp;<?= $v->offer ?></td>
                                    <td><?= number_format($v->f_sales) ?></td>
                                    <td><code>$</code><?= $v->per ?></td>
                                    <td><code>$</code><?= number_format($v->f_payout, 2) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if (!empty($form->deducted)): ?>
                            <tr>
                                <td><strong>Deductions</strong></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php foreach ($form->deducted as $v): ?>
                                <tr class="text-danger">
                                    <td>#<?= $v->offers_ID ?>&nbsp;<?= $v->offer ?></td>
                                    <td>-<?= number_format($v->d_sales) ?></td>
                                    <td><code>$</code><?= $v->per ?></td>
                                    <td>-<code>$</code><?= number_format($v->d_payout, 2) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php
                        if ($form->lastInvoice): ?>

                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><strong>Amount added from previous unpaid invoice</strong></td>
                                <td></td>
                                <td><strong class="float-right">+</strong></td>
                                <td><code>$</code><span class="lb"><?= number_format($form->lastInvoice, 2) ?></span>
                                </td>
                            </tr>
                        <?php
                        endif;
                        ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><strong class="float-right">Total payable:</strong></td>
                            <td><code>$</code><span class="lb"><?= number_format($form->amount, 2) ?></span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>