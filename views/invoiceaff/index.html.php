<?php

use helper\Request;
use helper\Template as TM;
use helper\Tool as T;

$paid = Request::getField('paid', 'GET');
?>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Invoice</h3>
        <form class="row py-3">
            <div class="col-md-2">
                <label>Status</label>
                <select id="paid" name="paid" class="form-control selected5">
                    <option value="">All</option>
                    <option value="0" <?php if ($paid === '0') echo 'selected' ?> >No</option>
                    <option value="1" <?php if ($paid === '1') echo 'selected' ?> >Yes</option>
                </select></div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning">Get Invoice</button>
            </div>
        </form>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Affiliate_ID</th>
                    <th>Month</th>
                    <th>Amount</th>
                    <th>Paid</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form as $v):
                    echo '<tr>';
                    echo '<td>' . $v->affiliate_ID . $v->month . '</td>';
                    echo '<td>' . $v->affiliate_ID . '</td>';
                    echo '<td>' . T::getMonthName(substr($v->month, 4, 2)) . '</td>';
                    echo '<td><code>$</code><strong>' . number_format($v->amount, 2) . '</strong></td>';
                    echo '<td>' . TM::approval($v->status) . $v->pop() . '</td>';
                    echo '<td><div class="btn-group"><a class="btn btn-info btn-sm" href="' . s() . 'invoice/view?i=' . $v->ID . tkn('&') . '"><i class="fa fa-eye"></i> view</a></div></td>';
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script type="text/javascript">
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'hover',
        placement: 'right'
    });
</script>