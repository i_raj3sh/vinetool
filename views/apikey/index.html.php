<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 17/3/18
 * Time: 2:23 PM
 */

use helper\Request;

$af = Request::getField('id','GET');
$ses_type = (\controller\Controller::getSessionType() === 'ADMIN');
?>

<?= select2() ?>

<section class="row">

    <div class="col-md-12">
        <div class="jumbotron">
            <h2>Generate API key</h2>
            <p class="lead">Vinetool API documentation can be found <a href="https://op.vinetool.com/doc/VineTool_API_1_0.pdf" target="_blank">here</a>. </p>
            <?php if ($ses_type){ ?>
                <div class="row">
                    <div class="col-5">
                        <label style="font-size: larger">Affiliate:</label>
                        <?php
                        $g = new \model\Affiliate();
                        $g->setProperties([
                            'active' => 1
                        ]);
                        $g->setQueryParameters($g);
                        $all = (array)$g->query();
                        ?>
                        <select class="form-control selector">
                            <option value="" selected>--select-affiliate--</option>
                            <?php foreach ($all as $item): ?>
                                <option value="<?php echo $item->ID ?>" <?php if ($af === $item->ID) echo 'selected' ?>><?php echo $item->name." ($item->company)" ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            <?php } if ($af || !$ses_type) { ?>
            <hr class="my-4">
            <form name="add_apikey" method="post" action="<?php echo MY_SERVER ?>apiKey/add<?=tkn()?>">
                <input type="hidden" value="<?php $form->get('ID') ?>" name="id">
                <div class="panel panel-default panel-article">
                    <?php if ($form->fetch('akey')) {?>
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item"><strong>Key:</strong> <span class="text-danger"><?php $form->get('akey') ?></span></li>
                        </ul>
                    </div>
                    <?php } ?>
                    <div class="panel-footer">
                        <?php if ($form->fetch('akey')) {?>
                        <button class="btn btn-primary" name="generate" value="_">Re-Generate</button>
                        <?php }else{ ?>
                        <button class="btn btn-success" name="generate" value="_">Generate</button>
                        <?php } ?>
                    </div>
                </div>
            </form>
            <?php } ?>
        </div>
    </div>
</section>
<script>
    $('.selector').select2({
        placeholder: "--select--",
        allowClear: true
    }).on('select2:select',function () {
        window.location = '<?php echo MY_SERVER ?>apiKey?id='+this.value;
    });
    $('a#tools').addClass('active');
</script>