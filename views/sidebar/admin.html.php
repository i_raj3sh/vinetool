<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/16/17
 * Time: 11:48 AM
 */

$data = json_decode($sidebarData, true);

?>
<style>
    table.sidebarAside tbody tr td {
        border-color: #111;
    }
    table.sidebarAside tbody tr td label{
        display: block;
        text-align: center;
    }
</style>
<div class="row">
    <h6 class="p-3 m-0" style="background: #111;width: 100%;color: aliceblue;padding-left: .5rem !important;
font-weight: bolder;"><?= date('F') ?></h6>
    <h6 class="p-2 m-0 text-white" style="font-size: .8rem;font-weight: 900;">Today</h6>
    <table class="table table-dark table-hover sidebarAside m-0">
        <tbody>
        <tr>
            <td width="20%"><strong><i class="fa fa-mouse-pointer"></i></strong></td>
            <td><label class="text-warning"><?= $data['impressions'] ?></label></td>
        </tr>
        <tr>
            <td><strong<i class="fa fa-download"></i></strong></td>
            <td><label class="text-warning"><?= $data['installs'] ?></label></td>
        </tr>
        <tr style="border-bottom: 5px solid #111">
            <td><strong><i class="fa fa-usd"></i></strong></td>
            <td><label class="text-warning"><?= $data['payout'] ?></label></td>
        </tr>
        </tbody>
    </table>
    <h6 class="p-2 m-0 text-white" style="font-size: .8rem;font-weight: 900;">Total</h6>
    <table class="table table-dark table-hover sidebarAside m-0">
        <tbody>
        <tr>
            <td width="20%"><strong><i class="fa fa-mouse-pointer"></i></strong></td>
            <td><label class="text-warning"><?= $data['impressionsT'] ?></label></td>
        </tr>
        <tr>
            <td><strong><i class="fa fa-download"></i></strong></td>
            <td><label class="text-warning"><?= $data['installsT'] ?></label></td>
        </tr>
        <tr style="border-bottom: 5px solid #111">
            <td><strong><i class="fa fa-usd"></i></strong></td>
            <td><label class="text-warning"><?= $data['payoutT'] ?></label></td>
        </tr>
        </tbody>
    </table>
</div>
<?php if(\controller\Controller::getSessionType() !== 'ADMIN'){
    $getAcc = \controller\Controller::getAccountManager();
    $name = $getAcc->name;
    $mail = $getAcc->email;
    ?>
<div class="row justify-content-center pb-4">
    <h5 class="text-center pt-4 text-light">Account Manager</h5>
        <figure class="col-7 pt-2 m-0">
            <img style="width: 100%" src="<?= MY_SERVER ?>images/dp.png">
            <figcaption class="pt-1 text-center text-warning" style="font-size: .7rem"><?= $name ?></figcaption>
        </figure>
    <a href="mailto:<?= $mail ?>" class="text-center text-info" style="width: 100%; font-size: 2rem"><i
                class="fa fa-envelope"></i></a>
</div>
<?php } ?>
