<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 28/4/18
 * Time: 9:23 AM
 */

use helper\Request;

?>

<?= select2() ?>
<?= datepicker() ?>
<style>
    .timepicker-picker, .dater {
        display: none
    }

    .piece {
        background: #fafafa;
        color: #82949a;
        margin-bottom: .5rem;
    }

    .ikon {
        color: white;
        padding: .3rem .7rem;
        font-weight: 900;
        margin-right: .5rem;
    }

    .piece-line, .piece-time {
        padding: .3rem;
    }

    .tabs a {
        color: #aaa;
        font-weight: 900;
        display: block;
        float: left;
        width: 50px;
        text-align: center;
    }

    .tabs a:hover, .tab-selected {
        color: #333 !important;
        text-decoration: none;
    }

    .card-height {
        height: 500px;
        overflow-y: auto;
    }
</style>
<section class="row">
    <div class="col-12">
        <form class="row py-3">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Account Manager:</label>
                    <?php
                    $g = new \model\Admin();
                    $g->setProperties([
                        'active' => 1
                    ])
                        ->setQueryParameters($g);
                    $all = (array)$g->query();
                    ?>
                    <select name="am" class="form-control selected5">
                        <option value="" selected>--select-am--</option>
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->ID ?>" <?php if (Request::getField('am', 'GET') == $item->ID) echo 'selected' ?>><?php echo $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Affiliate:</label>
                    <?php
                    $g = new \model\Affiliate();
                    $g->setProperties([
                        'active' => 1
                    ])
                        ->setQueryParameters($g);
                    $all = (array)$g->query();
                    ?>
                    <select name="af" class="form-control selector selected5">
                        <option value="" selected>--select-affiliate--</option>
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->ID ?>" <?php if (Request::getField('af', 'GET') == $item->ID) echo 'selected' ?>><?php echo $item->name . " ($item->company)" ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Type:</label>
                    <select name="t" class="form-control selector selected5">
                        <option value="">--select-type--</option>
                        <option value="LOGIN" <?php if (Request::getField('t', 'GET') === 'LOGIN') echo 'selected' ?> >
                            LOGIN
                        </option>
                        <option value="INSERT" <?php if (Request::getField('t', 'GET') === 'INSERT') echo 'selected' ?> >
                            INSERT
                        </option>
                        <option value="UPDATE" <?php if (Request::getField('t', 'GET') === 'UPDATE') echo 'selected' ?> >
                            UPDATE
                        </option>
                        <option value="DELETE" <?php if (Request::getField('t', 'GET') === 'DELETE') echo 'selected' ?> >
                            DELETE
                        </option>
                        <option value="OPERATION" <?php if (Request::getField('t', 'GET') === 'OPERATION') echo 'selected' ?> >
                            TASK
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <label>Date:</label>
                <select id="date_type" class="form-control" name="q">
                    <option value="t"
                            data-val="<?php echo date('Ymd') ?>" <?php if (Request::getField('q', 'GET') === 't') echo 'selected' ?>>
                        Today
                    </option>
                    <option value="y"
                            data-val="<?php echo date('Ymd', strtotime('-1 day')) ?>" <?php if (Request::getField('q', 'GET') === 'y') echo 'selected' ?>>
                        Yesterday
                    </option>
                    <option value="c" data-val="" <?php if (Request::getField('q', 'GET') === 'c') echo 'selected' ?>>
                        Custom
                    </option>
                </select>
            </div>
            <div class="w-100 dater"></div>
            <div class="col-md-3 dater">
                <label>Date:</label>
                <input id="date0" class="form-control" type="text" name="date"
                       value="<?php echo Request::getField('date', 'GET') ?>" placeholder="Date: YYYYMMDD">
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
    </div>
    <div class="col-md-8">
        <div class="card mb-3">
            <div class="card-header">
                <span style="font-size: 1rem"><i class="fa fa-book"></i> Activity Logs</span>
                <div class="float-right tabs">
                    <a href="#!" id="piece" class="tab-selected">All</a>
                    <a href="#!" id="pub">Affiliate</a>
                    <a href="#!" id="adm">Admin</a>
                </div>
            </div>
            <div class="card-body card-height">
                <?php foreach ($form as $item):

                    $class = 'pub';
                    if ($item->AID) {
                        $class = 'adm';
                    }

                    ?>
                    <div class="piece w-100 float-left <?php echo $class ?>">
                        <?php if ($item->type === 'LOGIN'): ?>
                            <div class="ikon blue-bg blue-bd float-left" data-toggle="tooltip" title="Login">L</div>
                            <div class="piece-line float-left"><strong><?php echo $item->name ?></strong> logged in from
                                IP <strong class="text-dark"><?php echo $item->extra->ip ?></strong>.
                            </div>
                        <?php elseif ($item->type === 'UPDATE'): ?>
                            <div class="ikon purple-bg purple-bd float-left" data-toggle="tooltip" title="Update">U
                            </div>
                            <div class="piece-line float-left"><strong><?php echo $item->name ?></strong> has updated
                                <strong class="text-dark"><?php echo $item->extra->what ?></strong>.
                            </div>
                        <?php elseif ($item->type === 'INSERT'): ?>
                            <div class="ikon green-bg green-bd float-left" data-toggle="tooltip" title="Add">A</div>
                            <div class="piece-line float-left"><strong><?php echo $item->name ?></strong> has added
                                <strong class="text-dark"><?php echo $item->extra->what ?></strong>.
                            </div>
                        <?php elseif ($item->type === 'DELETE'): ?>
                            <div class="ikon red-bg red-bd float-left" data-toggle="tooltip" title="Delete">D</div>
                            <div class="piece-line float-left"><strong><?php echo $item->name ?></strong> has deleted
                                <strong class="text-dark"><?php echo $item->extra->what ?></strong>.
                            </div>
                        <?php elseif ($item->type === 'OPERATION'): ?>
                            <div class="ikon grey-bg grey-bd float-left" data-toggle="tooltip" title="Task">T</div>
                            <div class="piece-line float-left"><strong><?php echo $item->name ?></strong> has <strong
                                        class="text-dark"><?php echo $item->extra->what ?></strong>.
                            </div>
                        <?php endif; ?>
                        <div class="piece-time float-right"><?php echo date('M d, Y H:i:s', $item->time) ?></div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-md-3" id="chat-box-1546264789214" style="display:none">
        <h4>Who's online</h4>
        <hr/>
        <div class="card mb-2">
            <div class="card-header">
                <h6>Admin</h6>
            </div>
            <div class="card-body admin-online"></div>
        </div>
        <div class="card mb-3">
            <div class="card-header">
                <h6>Affiliate</h6>
            </div>
            <div class="card-body affiliate-online"></div>
        </div>
    </div>
</section>
<script>
    $('#date0').datetimepicker({
        format: 'YYYYMMDD'
    });

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover',
        placement: 'right'
    });

    $('#date_type').on('change', function (e) {

        var vu = $(this).find(':selected').data('val') + '', $date0 = $('#date0'), $v = vu.split('_'),
            $dater = $('.dater');

        $date0.val($v);
        if (!vu) {
            $dater.slideDown();
        } else {
            $dater.slideUp();
        }
    });

    var tb = $('.tabs a');
    tb.on('click', function () {
        tb.removeClass('tab-selected');
        $(this).addClass('tab-selected');
        if (this.id !== 'piece') {
            $('.piece').hide(200);
        }
        $('.' + this.id).show(200);
    });

    <?php if (Request::getField('q', 'GET') === 'c') echo '$(".dater").slideDown().find("input").attr("required","true");' ?>
</script>
