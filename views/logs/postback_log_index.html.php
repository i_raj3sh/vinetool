<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 2:17 PM
 */

use helper\Request;

$affy = false;
if (\controller\Controller::getSessionType() === 'ADMIN') {
    $affy = true;
}

?>
<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<?= datatables() ?>
<style>
    .dashboard-stat {
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number {
        font-size: 2rem;
    }

    .number cite {
        font-size: 1.5rem;
        color: #ddd;
    }

    .dashboard-stat .desc {
        padding: .3rem .7rem;
    }

    .number {
        padding: .8rem .6rem .2rem;
    }

    .dater, .noshow {
        display: none
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Postback Logs</h3>
        <hr/>
        <form class="row py-3">
            <?= tknInp() ?>
            <div class="col-md-3">
                <?= campaign() ?>
            </div>
            <?php if ($affy) { ?>
                <div class="col-md-3">
                    <?= affiliate() ?>
                </div>
            <?php } ?>
            <div class="col-md-2">
                <label>Date:</label>
                <select id="date_type" class="form-control" name="q">
                    <option value="t"
                            data-val="<?php echo date('Ymd') ?>" <?php if (Request::getField('q', 'GET') === 't') echo 'selected' ?>>
                        Today
                    </option>
                    <option value="y"
                            data-val="<?php echo date('Ymd', strtotime('-1 day')) ?>" <?php if (Request::getField('q', 'GET') === 'y') echo 'selected' ?>>
                        Yesterday
                    </option>
                    <option value="c" data-val="" <?php if (Request::getField('q', 'GET') === 'c') echo 'selected' ?>>
                        Custom
                    </option>
                </select>
            </div>
            <div class="w-100 dater"></div>
            <div class="col-md-3 dater">
                <label>Date:</label>
                <input id="date0" class="form-control" type="text" name="day"
                       value="<?php echo Request::getField('day', 'GET') ?>" placeholder="Date: YYYYMMDD">
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <div class="table-responsive">
            <table id="PostbackLogs" class="table table-striped dt-responsive">
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Offer</th>
                    <?php if ($affy) { ?>
                        <th>Affiliate</th>
                    <?php } ?>
                    <th>Payout</th>
                    <th class="tableexport-ignore">Postback</th>
                    <th class="tableexport-ignore">Response</th>
                    <th class="noshow">Postback</th>
                    <th class="noshow">Response</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form as $k => $v):
                    ?>
                    <tr>
                        <td><?= $k + 1 ?></td>
                        <td><?= $v->date ?></td>
                        <td>
                            <a href="./offers/view?id=<?= $v->offers_ID . tkn('&') ?>">#<?= $v->offers_ID . ' ' . $v->offer ?></a>
                        </td>
                        <?php
                        if ($affy) { ?>
                            <td>#<?= $v->affiliate_ID . ' ' . $v->affiliate ?></td>
                        <?php } ?>
                        <td class="text-success"><code>$</code><strong><?= sprintf('%.4f', $v->payout) ?></strong></td>
                        <td class="tableexport-ignore">
                            <a href="#!" class="btn btn-sm btn-success" data-toggle="popover"
                               data-content="<code><strong><?= $v->url ?></strong></code>"><i
                                        class="fa fa-caret-up"></i></a>
                            <?= mb_strimwidth($v->url, 0, 60, '&hellip;') ?></td>
                        <td class="tableexport-ignore"><a href="#!" class="btn btn-sm btn-info"
                                                          onclick="showPop('<?= base64_encode($v->response) ?>')"><i
                                        class="fa fa-caret-up"></i></a> <?= mb_strimwidth(htmlentities($v->response), 0, 60, '&hellip;') ?>
                        </td>
                        <td class="noshow"><?= $v->url ?></td>
                        <td class="noshow"><?= htmlentities($v->response) ?></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<div class="modal fade" id="popRes" role="dialog" aria-labelledby="assignToPub" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div id="bodyRes" class="modal-body"><code id="coder"></code></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $('a#logs').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    $('#date0').datetimepicker({
        format: 'YYYYMMDD'
    });

    $('#date_type').on('change', function (e) {
        var vu = $(this).find(':selected').data('val') + '', $date0 = $('#date0'), $v = vu.split('_'),
            $dater = $('.dater');

        $date0.val($v);
        if (!vu) {
            $dater.slideDown();
        } else {
            $dater.slideUp();
        }
    });

    <?php if (Request::getField('q', 'GET') === 'c') echo '$(".dater").slideDown().find("input").attr("required","true");' ?>

    var table = $('#PostbackLogs');
    table.tableExport({
        headers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'top',
        trimWhitespace: true
    });
    var dt = table.DataTable();
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'click',
        placement: 'top'
    });
    dt.on('draw', function () {
        $('[data-toggle="popover"]').popover({
            html: true,
            trigger: 'click',
            placement: 'top'
        });
    });

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    function showPop(r) {
        r = atob(r);
        if (r.length > 999) {
            $('#bodyRes').find('#coder').text(r)
        } else {
            $('#bodyRes').html(r);
        }
        $('#popRes').modal()
    }
</script>