<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 2:17 PM
 */

use helper\Request;
use helper\Template as T;

?>
<?= select2() ?>
<?= datepicker() ?>
<?= tableexport() ?>
<?= datatables() ?>
<?= datatables_responsive() ?>
<style>
    .dashboard-stat {
        color: #FFFFFF;
        text-align: right;
    }

    .counter-number {
        font-size: 2rem;
    }

    .number cite {
        font-size: 1.5rem;
        color: #ddd;
    }

    .dashboard-stat .desc {
        padding: .3rem .7rem;
    }

    .number {
        padding: .8rem .6rem .2rem;
    }

    .noshow {
        display: none
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="py-3">Click Logs</h3>
        <hr/>
        <form id="click_log" class="row py-3">
            <div class="col-md-2">
                <label for="tid">TID:</label>
                <input id="tid" type="number" class="form-control" name="t" placeholder="TID"
                       value="<?= \helper\Request::getField('t', 'GET') ?>">
            </div>
            <div class="col-md-4">
                <?= campaign() ?>
            </div>
            <div class="col-md-3 noshow">
                <?= affiliate() ?>
            </div>
            <div class="col-md-1">
                <label for="hours">Hours:</label>
                <select id="hours" name="h" class="form-control">
                    <?php for ($i = 0; $i < 24; $i++) {
                        $h = (int)Request::getField('h', Request::GET); ?>
                        <option value="<?php echo $i; ?>" <?php if ($i == $h) {
                            echo "selected";
                        } ?>><?php echo $i; ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-1">
                <label for="records">Records:</label>
                <select id="records" name="l" class="form-control">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                </select>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search text-light"></i></button>
            </div>
        </form>
        <hr/>
        <div class="table-responsive">
            <table id="ClickLogs" class="table table-striped dt-responsive">
                <thead class="thead-dark">
                <tr>
                    <th>-</th>
                    <th>#</th>
                    <th>ID</th>
                    <th>TID</th>
                    <th>Offer</th>
                    <th>Charge</th>
                    <th>Margin</th>
                    <th>Affiliate</th>
                    <th class="none">AffiliateID</th>
                    <th>Network</th>
                    <th class="none">NetworkID</th>
                    <th>Time click</th>
                    <th class="none">Offer</th>
                    <th>Drop</th>
                    <th>Admin</th>
                    <th>clickid</th>
                    <th>sub_affid</th>
                    <th>gaid</th>
                    <th>idfa</th>
                    <th>App_Name</th>
                    <th>Device Type</th>
                    <th>OS</th>
                    <th>Version</th>
                    <th>Brand</th>
                    <th>Browser</th>
                    <th>Geo</th>
                    <th>Region</th>
                    <th>City</th>
                    <th>ISP</th>
                    <th>Carrier</th>
                    <th>IP</th>
                    <th>UA</th>
                    <th>Ref</th>
                    <th>Reason</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form as $k => $v):
                    ?>
                    <tr>
                        <td></td>
                        <td><?= $k + 1 ?></td>
                        <td><?= $v->_id ?></td>
                        <td><?= $v->tid ?></td>
                        <td><a href="./offers/view?id=<?= $v->offerID . tkn('&') ?>"><?= $v->offerID ?></a></td>
                        <td class="text-info"><code>$</code><strong><?= sprintf('%.4f', $v->charge) ?></strong></td>
                        <td><?= $v->margin ?>%</td>
                        <td><?= $v->affiliate ?></td>
                        <td><?= $v->affiliateID ?></td>
                        <td><?= $v->network ?></td>
                        <td><?= $v->networkID ?></td>
                        <td><?= T::getDate($v->dateClick) ?></td>
                        <td><?= $v->offer ?></td>
                        <td><?= $v->drop ? '<b class="text-danger">Yes</b>' : '' ?></td>
                        <td><?= $v->adminID ?></td>
                        <td><?= $v->subID ?></td>
                        <td><?= $v->source ?></td>
                        <td><?= $v->GAID ?></td>
                        <td><?= $v->IDFA ?></td>
                        <td><?= $v->appName ?></td>
                        <td><?= $v->deviceType ?></td>
                        <td><?= $v->device ?></td>
                        <td><?= $v->platformVersion ?></td>
                        <td><?= $v->hardwareVendor ?></td>
                        <td><?= $v->browserName ?></td>
                        <td><?= $v->country ?></td>
                        <td><?= $v->region ?></td>
                        <td><?= $v->city ?></td>
                        <td><?= $v->isp ?></td>
                        <td><?= $v->carrier ?></td>
                        <td><?= $v->ip ?></td>
                        <td><?= $v->ua ?></td>
                        <td><?= $v->page ?></td>
                        <td><?= $v->reason ?></td>
                    </tr>
                <?php
                endforeach;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</section>
<script type="text/javascript">

    $('a#logs').addClass('active');

    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });

    var table = $('#ClickLogs');
    table.tableExport({
        headers: true,
        formats: ['csv'],
        filename: 'id',
        bootstrap: true,
        position: 'top',
        trimWhitespace: true
    });
    var dt = table.DataTable();
    $('[data-toggle="popover"]').popover({
        html: true,
        trigger: 'hover click',
        placement: 'top'
    });
    dt.on('draw', function () {
        $('[data-toggle="popover"]').popover({
            html: true,
            trigger: 'hover click',
            placement: 'top'
        });
    });

    $('#click_log').on('submit', function (e) {
        e.preventDefault();
        var t = $('#tid').val(), af = $('#affiliate').val(), of = $('#campaign').val();
        if (t || af || of) {
            this.submit();
        } else {
            alert('Required one of these TID or Offer.');
        }
    });
</script>