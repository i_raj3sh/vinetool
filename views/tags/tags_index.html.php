<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 17/3/18
 * Time: 2:23 PM
 */

use helper\Request as R;

$af = R::getField('af', R::GET);
$t = R::getField('t', R::GET);
$ses_type = (\controller\Controller::getSessionType() === 'ADMIN');
?>

<?= select2() ?>
<style>
    .show-none {
        display: none
    }

    hr {
        width: 100%;
        float: left;
    }

    .copy_cat {
        display: flex;
        background: #eee;
        border: 1px solid #bbb;
        padding: 10px 12px;
        border-radius: 5px;
        margin-top: 5px;
        font-family: monospace !important;
    }

    .copy_cat textarea {
        width: 100%;
        border: none;
        background: none;
        outline: none;
        resize: none;
        font-size: 0.8rem;
    }
</style>
<section class="row">

    <div class="col-md-12">
        <div class="jumbotron">
            <h2>Generate Tags</h2>
            <p class="lead">Vinetool SDK documentation can be found <a
                        href="https://op.vinetool.com/doc/vinetool_android_sdk.html"
                        target="_blank">here</a>. </p>
            <div class="row">
                <?php if ($ses_type) { ?>
                    <div class="col-7">
                        <label style="font-size: larger">Affiliate:</label>
                        <?php
                        $g = new \model\Affiliate();
                        $g->setProperties([
                            'active' => 1
                        ]);
                        $g->setQueryParameters($g);
                        $all = (array)$g->query();
                        ?>
                        <select id="affili" class="form-control selector">
                            <option value="" selected>--select-affiliate--</option>
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->ID ?>" <?php if ($af === $item->ID) echo 'selected' ?>><?php echo $item->name . " ($item->company)" ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <hr class="my-4">
                <?php }
                if ($af) { ?>
                    <div class="col-7">
                        <label style="font-size: larger">Target:</label>
                        <?php
                        $g = new \model\Target();
                        $g->setProperties([
                            'affiliate_ID' => $af,
                            'status' => 1
                        ]);
                        $g->setQueryParameters($g);
                        $all = (array)$g->query();
                        ?>
                        <select id="target_" class="form-control selector">
                            <option value="" selected>--select-target--</option>
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->ID ?>" <?php if ($t === $item->ID) echo 'selected' ?>><?= "#$item->ID $item->geo-$item->os-$item->device" ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <hr class="my-4">
                <?php }
                if ($t) { ?>
                    <div class="col-7">
                        <label style="font-size: larger">Format:</label>
                        <?php
                        $g = new \model\Format();
                        $g->setQueryParameters($g);
                        $all = (array)$g->query();
                        ?>
                        <select id="format" class="form-control selector">
                            <option value="" selected>--select-format--</option>
                            <?php foreach ($all as $item): ?>
                                <option value="<?= $item->size ?>"><?= $item->size ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <hr class="my-4">
                <?php } ?>
                <div class="col-10 show-none">
                    <h5>Js Tag</h5>
                    <span class="copy_cat"><textarea id="popUrl" rows="5" readonly="">hello world!</textarea><br><button
                                type="button" id="copyBack" class="btn btn-dark btn-sm" title=""
                                data-original-title="JS Tag is copied"><i class="fa fa-copy"></i></button></span>
                    <hr class="my-4">
                    <h5>SDK Tag</h5>
                    <span class="copy_cat"><textarea id="popUrl2" rows="2" readonly="">hello world!</textarea><br><button
                                type="button" id="copyBack2" class="btn btn-dark btn-sm" title=""
                                data-original-title="SDK Tag is copied"><i class="fa fa-copy"></i></button></span>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('.selector').select2({
        placeholder: "--select--",
        allowClear: true
    });
    $('#affili').on('select2:select', function () {
        window.location = '<?=s() ?>tags?af=' + this.value;
    });
    $('#target_').on('select2:select', function () {
        window.location = window.location.href + '&t=' + this.value;
    });
    $('#format').on('select2:select', function () {
        var f = $('#format').val();
        $('#popUrl').text('<ins class="__vines_imps__" data-pub="<?=$af?>" data-size="' + f + '"></ins>' +
            '<scr' + 'ipt>(function(v){v.type="text/javascript";v.id="__vine__tool__";v.async=true;v.src="/assets/js/vt.js";if(!document.getElementById("__vine__tool__"))(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(v)})(document.createElement("script"))</scr' + 'ipt>');
        $('#popUrl2').text('<?=$af?>-' + f);
        $('.show-none').show(1000);
    });
    $('a#extra').addClass('active');

    $('#copyBack').on('click', function () {
        var s = document.getElementById('popUrl');
        s.select();
        document.execCommand('copy');
    }).tooltip({
        trigger: 'click'
    });
    $('#copyBack2').on('click', function () {
        var s = document.getElementById('popUrl2');
        s.select();
        document.execCommand('copy');
    }).tooltip({
        trigger: 'click'
    });
</script>