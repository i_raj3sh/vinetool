<?php

use controller\AffiliateBilling as AB;
use helper\Request as R;
use helper\Tool;

$of = R::getField('of', 'GET');
$af = R::getField('af', 'GET');
$st = R::getField('st', 'GET');
$date = R::getField('date', 'GET');

?>

<?= select2() ?>
<?= datepicker() ?>
<style>
    table#passback-table tbody td {
        padding: 1rem .7rem !important;
        border-bottom: 1px solid #aaa;
    }
    .timepicker-picker,.noshow{
        display: none;
    }
    .ded_input{
        width: 70px;
    }
    .datepicker-days {
        display: none !important;
    }
    .datepicker-months{
        display: block !important;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Affiliate Monthly Billing</h3>
        <form class="row py-3">
            <div class="col-md-3">
                <?= affiliate() ?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Offer ID:</label>
                    <input type="text" class="form-control" name="of" placeholder="Offer ID" value="<?= $of ?>">
                </div>
            </div>
            <div class="col-md-2">
                <label>Month:</label>
                <input id="date" class="form-control" type="text" name="date" autocomplete="off" value="<?= $date ?>"
                       placeholder="Date Start: YYYYMM">
            </div>
            <div class="col-md-2">
                <label>Status:</label>
                <select name="st" class="form-control">
                    <option value="-">All</option>
                    <option value="0" <?php if ($st === '0') echo 'selected' ?>>Unconfirmed</option>
                    <option value="1" <?php if ($st === '1') echo 'selected' ?>>Confirmed</option>
                </select>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <span class="float-right"><strong>Total</strong>: <label class="badge badge-info"><?= $form['total'] ?></span>
        <div class="table-responsive">
            <table id="passback-table" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>Month</th>
                    <th>AffiliateID</th>
                    <th>Affiliate</th>
                    <th>OfferID</th>
                    <th>Offer</th>
                    <th>Network</th>
                    <th>Sales</th>
                    <th>Earning</th>
                    <th>Deduction</th>
                    <th>Total</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):

                    if (!$v->total) {
                        AB::total($v);
                    }

                    ?>
                    <tr>
                        <td <?= $v->bd ?>>
                            <?php if ($v->affiliate_ID) { ?>
                                <?= Tool::getMonthName((int)substr($v->month, 4, 2)) ?>
                            <?php } ?>
                        </td>
                        <td <?= $v->bd ?>><?= $v->affiliate_ID ?></td>
                        <td <?= $v->bd ?>><?= $v->affiliate ?></td>
                        <td <?= $v->bd ?>><?= $v->offers_ID ?></td>
                        <td <?= $v->bd ?>><?= $v->offer ?></td>
                        <td <?= $v->bd ?>><?= $v->network ?></td>
                        <td id="sal_<?= $v->ID ?>"
                            data-sales="<?= $v->sales ?>" <?= $v->bd ?>><?= number_format($v->sales) ?></td>
                        <td <?= $v->bd ?>><code>$</code><?= number_format($v->payout, 2) ?></td>
                        <td <?= $v->bd ?>>
                            <div id="ded_show_<?=$v->ID?>"><?=$v->deduction?></div>
                            <div id="ded_edit_<?=$v->ID?>" class="noshow">
                                <input class="form-group ded_input" id="ded_inp_<?=$v->ID?>" placeholder="- sales" type="number" value="<?=$v->deduction?>" />
                            </div>
                        </td>
                        <td id="tot_<?= $v->ID ?>" <?= $v->bd ?>>
                            <code>$</code><strong><?= number_format($v->total, 2) ?></strong></td>
                        <td <?= $v->bd ?>>
                            <?php if ($v->affiliate_ID) { ?>
                                <div class="btn-group"><a id="ded_<?= $v->ID ?>" class="btn btn-sm btn-info case_ded"
                                                          href="#!"><i class="fa fa-pencil"></i></a><a
                                            class="btn btn-sm" href="#!" onclick='toggle("<?= $v->ID ?>")'><i
                                                id="stat_<?= $v->ID ?>" data-line="<?= $v->status ?>"
                                                class="fa <?= AB::uiStatus($v->status) ?>"></i></a></div>
                            <?php } ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php echo $form['pages']; echo $form['perPage'] ?>
        </div>
    </div>
</section>
<script type="text/javascript">
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });
    $('#date').datetimepicker( {
        format:'YYYYMM',
        changeMonth: true,
        showButtonPanel: true
    });
    $('a#tools').addClass('active');

    $('a.case_ded').on('click', function () {
        var d_i = this.id.split('_')[1], d_s = $('#ded_show_'+d_i), d_e = $('#ded_edit_'+d_i), d_x = $('#ded_inp_'+d_i), d_o = $(this).find('.fa');
        if (d_o.hasClass('fa-pencil')) {
            d_s.hide();
            d_e.fadeIn();
            d_o.removeClass('fa-pencil').addClass('fa-save');
        }else{
            d_e.hide();
            d_s.fadeIn();
            var d = parseInt(d_x.val()), s = parseInt($('#sal_'+d_i).data('sales'));
            d_o.removeClass('fa-save').addClass('fa-pencil');
            if (d > s){
                toastr["error"]("deduction cannot be more than sales","Error!");
                return;
            }
            add(d_i,d);
        }
    });

    function add(x, v) {
        var data = 'i=' + x + '&v=' + v;
        $.getJSON("<?=s()?>affiliateBilling/add?" + data, function (res) {
            if (res.error) {
                toastr["error"](res.error,"Error!");
            } else {
                $('#ded_inp_'+x).val(res.deduction);
                $('#ded_show_'+x).text(res.deduction);
                $('#tot_'+x).html(res.total);
                toastr["success"]("Result updated successfully","Splendid!");
            }
        });
    }
    
    function toggle(i) {
        var f = $('#stat_'+i), v = f.data('line');
        if (v == '0'){
            v = '1';
        }else{
            v = '0';
        }
        var data = 'i=' + i + '&v=' + v;
        $.getJSON("<?=s()?>affiliateBilling/toggle?" + data, function (res) {
            if (res.error) {
                toastr["error"](res.error,"Error!");
            } else {
                if (v === '1'){
                    f.removeClass('text-success').removeClass('fa-play').addClass('text-danger').addClass('fa-stop');
                    toastr["success"]("Status changed to confirmed","Splendid!");
                }else{
                    f.removeClass('text-danger').removeClass('fa-stop').addClass('text-success').addClass('fa-play');
                    toastr["success"]("Status changed to unconfirmed","Splendid!");
                }
                f.data('line',v);
            }
        });
    }
</script>