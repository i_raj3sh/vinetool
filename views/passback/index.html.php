<?= select2() ?>
<script>
    var peet = false;
</script>
<style>
    table#passback-table tbody td{
        padding: 1rem .7rem !important;
        border-bottom: 1px solid #aaa;
    }
</style>
<section class="row">
    <div class="col-12">
        <h3 class="pt-3">Passback Traffic (beta)</h3>
        <form class="row py-3">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Search Geo:</label>
                    <?php
                    $geo = \helper\Request::getField('g','GET');

                    $g = new \model\Country();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select name="g" class="form-control selected5">
                        <option value="">--select--</option>
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->code ?>" <?php \helper\Template::option($geo,$item->code) ?>  ><?php echo $item->code.': '.$item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Search Device:</label>
                    <?php
                    $geo = \helper\Request::getField('d','GET');

                    $g = new \model\Device();
                    $g->setQueryParameters($g);
                    $all = $g->query();
                    ?>
                    <select name="d" class="form-control selected5">
                        <option value="">--select--</option>
                        <?php foreach ($all as $item): ?>
                            <option value="<?php echo $item->name ?>" <?php \helper\Template::option($geo,$item->name) ?> ><?php echo $item->name ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-md-1">
                <div style="margin: 1.7rem"></div>
                <button type="submit" class="btn btn-warning"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="table-responsive">
            <table id="passback-table" class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>ID</th>
                    <th>Geo</th>
                    <th>Device</th>
                    <th>Traffic</th>
                    <th>Route</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($form['table'] as $v):
                    echo '<tr>';
                    echo '<td>'.$v->ID.'</td>';
                    echo '<td>'.$v->geo.'</td>';
                    echo '<td>'.$v->OS.'</td>';
                    echo '<td>'.$v->count.'</td>';

                    $r = new \model\ReRoute();
                    $r->setProperties([
                       'passback_ID' => $v->ID
                    ]);
                    $r->setQueryParameters($r);
                    $ro = $r->query();
                    ?>
                <td><div id="block-<?php echo $v->ID ?>">
                <?php
                $last = 0;
                    foreach ($ro as $k=>$item){
                ?>
                        <div id="row-<?php echo $item->ID ?>" class="row"><div class="col-8"><input id="tot-<?php echo $item->ID ?>" type="url" class="form-control" name="tot" value="<?php echo $item->tot ?>" placeholder="Our Tracking Link"></div>
                            <div class="col-2"><input id="wei-<?php echo $item->ID ?>" type="text" class="form-control wei" name="wei" value="<?php echo $item->weight ?>" max="100" placeholder="Weight 0-100"></div>
                            <div class="col-2"><div class="btn-group btn-sm"><a href="#!" id="add-<?php echo $item->ID ?>" class="btn btn-sm btn-warning" onclick="add(<?php echo $v->ID ?>,this.id)"><i class="fa fa-check"></i> </a><a href="#!" id="del-<?php echo $item->ID ?>" class="btn btn-sm btn-danger" onclick="del(<?php echo $v->ID ?>,this.id)"><i class="fa fa-remove"></i> </a></div></div></div>
                <?php $last = $item->ID + 1;
                    }
                    if ($last){?>
                        <script>
                            peet = true;
                        </script>
                    <?php } ?>
                    <div class="w-100"></div>
                    </div>
                    <a href="#!" id="plus-<?php echo $v->ID.'-'.$last ?>" class="btn btn-sm btn-primary plus"><i class="fa fa-plus"></i> </a>
                </td>
                <?php
                    echo '</tr>';
                endforeach;
                ?>
                </tbody>
            </table>
            <?php echo $form['pages']; echo $form['perPage'] ?>
        </div>
    </div>
</section>
<script type="text/javascript">
    $('.selected5').select2({
        placeholder: "--select--",
        allowClear: true
    });
    $('a#tools').addClass('active');
    $('a.plus').on('click',function () {
        var id = this.id.split('-');
        if (!id[2]){
            id[2] = 1;
        }else{
            id[2]++;
        }
        this.id = id.join('-');
        $('#block-'+id[1]).append('<div id="row-'+id[2]+'" class="row"><div class="col-8"><input id="tot-'+id[2]+'" type="url" class="form-control" name="tot" value="" placeholder="Our Tracking Link"></div>' +
            '<div class="col-2"><input id="wei-'+id[2]+'" type="text" class="form-control wei" name="wei" value="" max="100" placeholder="Weight 0-100"></div>' +
            '<div class="col-2"><div class="btn-group btn-sm"><a href="#!" id="add-'+id[2]+'" class="btn btn-sm btn-warning" onclick="add('+id[1]+',this.id)"><i class="fa fa-check"></i> </a><a href="#!" id="del-'+id[2]+'" class="btn btn-sm btn-danger" onclick="del('+id[1]+',this.id)"><i class="fa fa-remove"></i> </a></div></div></div>');
    });
    function del(b,k) {
        k = k.split('-')[1];
        var conf = confirm("Are you sure? Do you want to delete?"), d =$('#block-'+b);

        if (conf){
            if (peet && d.find('#tot-'+k).val()){
                window.location = '<?php echo MY_SERVER ?>passback/del?p='+b+'&k='+k;
            }
            d.find('#row-'+k).remove();
        }
    }
    function add(x,k) {
        k = k.split('-')[1];
        var b = $('#block-'+x), t = b.find('#tot-'+k).val(), w = b.find('#wei-'+k).val(),a=0;
        b.find('.wei').each(function (i, j) {
            a += parseInt(j.value);
        });
        if (!t){
            alert('tracking link is empty. why?')
        }
        if(a === 100) {
            var data = 'k='+k+'&p='+x+'&t=' + t + '&w=' + w;
            $.getJSON("<?php echo MY_SERVER ?>passback/add?"+data,function (res) {
                peet = true;
                if (res.error){
                    alert('Error: '+res.error);
                }else {
                    alert('Done: '+res.success);
                    if (res.id){
                        b.find('#tot-'+k).attr('id','tot-'+res.id);
                        b.find('#wei-'+k).attr('id','wei-'+res.id);
                        b.find('#row-'+k).attr('id','row-'+res.id);
                        b.find('#del-'+k).attr('id','del-'+res.id);
                        b.find('#add-'+k).attr('id','add-'+res.id);
                    }
                }
            });
        }else{
            alert('Total weight must be 100');
        }
    }
</script>