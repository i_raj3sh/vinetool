<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/9/17
 * Time: 2:39 PM
 */


use controller\Controller;
use helper\MyException;

require_once "./lib/include.php";

// phpinfo();

$x = isset($_REQUEST['x']) ? ucfirst(rtrim($_REQUEST['x'], '/')) : 'Home';
$action = isset($_REQUEST['a']) ? rtrim($_REQUEST['a'], '/') : 'index';

if ($x !== 'Cb' && $x !== 'Api' && $x !== 'Tracking' && $x !== 'Login' && Controller::getSession() === false) {
	
    $_SESSION['DIRECT'] = $_SERVER['REQUEST_URI'];
    Controller::redirect('login');
}else {
    Controller::verifyAccess($x);
    $controller = "\\controller\\" . $x;
    $controller::$action();
    MyException::unsetSessionMessages();
}
