<?php

/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/06/2016
 * Time: 19:42
 */
namespace helper;
class MyException
{
    private static $error;
    private static $code;

    public static function setError($message, $code)
    {
        self::$error = $message;
        self::$code = $code;
    }

    public static function critcalError($message, $code, $text)
    {
        throw new HttpException(500, $code . ": " . $message, $text);
    }
    public static function showError()
    {
        throw new HttpException(500, self::$code . ": " . self::$error);
    }

    public static function isError()
    {
        if (self::$code == null)
            return false;
        return true;
    }

    public static function setSessionError($message)
    {
        $_SESSION[md5("E")] = $message;
    }

    public static function setSessionSuccess($message)
    {
        $_SESSION[md5("S")] = $message;
    }

    public static function isSessionError()
    {
        if (isset($_SESSION[md5("E")]))
            return true;
        return false;
    }

    public static function isSessionSuccess()
    {
        if (isset($_SESSION[md5("S")]))
            return true;
        return false;
    }

    public static function unsetSessionMessages()
    {
        unset($_SESSION[md5("S")]);
        unset($_SESSION[md5("E")]);
    }
}