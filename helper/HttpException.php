<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/28/17
 * Time: 1:58 PM
 */

namespace helper;


class HttpException extends \Exception
{

    /**
     * Override the parent constructor
     *
     * @param int $code HTTP status code
     * @param string $error
     * @param string $text
     * @param boolean $log Set to false not to log here
     */
    public function __construct($code, $error = "", $text = "", $log = true)
    {

        // switch between error codes
        switch ($code) {
            case 404:
                if ($log)
                    Log::writeLog("404", $_SERVER["HTTP_REFERER"] . " >> " . $_SERVER["REQUEST_URI"], "httperrors");
                $this->notFound();
                break;
            default:
                if ($log)
                    Log::writeLog("500", $_SERVER["HTTP_REFERER"] . " >> " . $_SERVER["REQUEST_URI"] . " ==>  $error", "httperrors");
                $this->serverError($text); // we also default to 500
        }
        die();
    }

    /**
     * 404 page not found
     */
    public function notFound()
    {
//        header("HTTP/1.1 404 Not Found");
        require MY_BASE . "views/httperrors/404.tpl.php";
    }

    /**
     * 500 server error
     */
    public function serverError($error)
    {
//        header('HTTP/1.1 500 Internal Server Error');
        require MY_BASE . "views/httperrors/500.tpl.php";
    }

}