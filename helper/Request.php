<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/28/17
 * Time: 6:04 PM
 */

namespace helper;


use Exception;

class Request
{
    public const GET = 'GET';
    public const POST = 'POST';

    public static function getField($key,$kind = '')
    {
        switch ($kind) {
            case 'GET':
                if (isset($_GET[$key]) && $_GET[$key] !== '')
                    return $_GET[$key];
                break;
            case 'POST':
                if (isset($_POST[$key]) && $_POST[$key]  !== '')
                    return $_POST[$key];
                break;
            default:
                if (isset($_REQUEST[$key]) && $_REQUEST[$key] !== '')
                    return $_REQUEST[$key];
        }

        return '';
    }

    /**
     * @param $key
     * @param string $kind
     * @return string|array
     */
    public static function getRequiredField($key, $kind = '')
    {
        switch ($kind) {
            case 'GET':
                if (isset($_GET[$key]) && $_GET[$key] !== '')
                    return $_GET[$key];
                break;
            case 'POST':
                if (isset($_POST[$key]) && $_POST[$key]  !== '')
                    return $_POST[$key];
                break;
            default:
                if (isset($_REQUEST[$key]) && $_REQUEST[$key] !== '')
                    return $_REQUEST[$key];
        }

//        $url = './';
//        if (isset($_SERVER["HTTP_REFERER"]))
//            $url = $_SERVER["HTTP_REFERER"];
//        MyException::setSessionError($key . " field cannot be empty");
//        header("Location: " . $url);
        die('<strong>ERROR: invalid request or not enough parameters sent</strong><p>please check and try again. If this problem persists, contact: <a href="mailto:tech@vinetool.com">tech@vinetool.com</a></p>');
    }

    public static function getHastKeys($key){
        return md5($key);
    }

    /**
     * @param $type
     * @return array
     */
    public static function all($type){
        switch ($type){
            case 'GET': return $_GET;
            case 'POST': return $_POST;
            default: return $_REQUEST;
        }
    }

    public static function curlURL($url): ?array
    {
        try {
            $ch = curl_init();

            if (FALSE === $ch)
                throw new Exception('Failed'.__CLASS__.' to init');

            curl_setopt($ch, CURLOPT_URL,$url);

            curl_setopt($ch, CURLOPT_NOBODY, true);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);

            if (FALSE === $result)
                throw new Exception(curl_error($ch), curl_errno($ch));

            return ['code' => curl_getinfo($ch, CURLINFO_HTTP_CODE)];

        } catch(Exception $e) {
            return ['code'=>$e->getCode()];
        }
    }
}
