<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/5/18
 * Time: 8:23 PM
 */

namespace helper;


class RedisFy
{
    private static $con;

    public function __construct()
    {
        if (self::$con === null) {
            try{
                self::$con = new \Redis();
                self::$con->connect('127.0.0.1', 6379);
            }catch (\RedisException $exception){
                Log::writeLog('connection failed', $exception->getMessage(), 'redis.log');
                die('Failed'.__CLASS__.'');
            }
        }
    }

    public function getCon(): \Redis
    {
        return self::$con;
    }

    public function setKV($key, $value): void
    {
        self::$con->set($key, $value);
    }

    public function setKVex($key, $value, int $time): void
    {
        self::$con->setex($key, $time, $value);
    }

    public function getV($key): string
    {
        if (($val = self::$con->get($key)) === false) {
            return '';
        }
        return $val;
    }

    public function getAll(): array
    {
        return self::$con->keys('*');
    }

    public function getStart($key): array
    {
        return self::$con->keys($key . '*');
    }

    public function remove($key): void
    {
        self::$con->delete($key);
    }
}
