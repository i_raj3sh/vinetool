<?php

namespace helper;
class Message
{
    public static function display_error($text_only = false):void
    {

        $md = md5('E');
        if (isset($_SESSION[$md])) {
            if ($text_only) echo $_SESSION[$md];
            else {
                self::toastr('error', 'Error!', $_SESSION[$md]);
            }
            unset($_SESSION[$md]);
        }

    }

    public static function display_success($text_only = false):void
    {

        $md = md5('S');
        if (isset($_SESSION[$md])) {
            if ($text_only) echo $_SESSION[$md];
            else {
                self::toastr('success', 'Splendid!', $_SESSION[$md]);
            }
            unset($_SESSION[$md]);
        }

    }

//    public static function display_success($text_only = false){
//
//        if (isset($_SESSION[md5("S")])) {
//            if ($text_only == true) echo $_SESSION[md5("S")];
//            else echo "<div id='popop' class='alert alert-success' >" . $_SESSION[md5("S")] . "</div>";
//            unset($_SESSION[md5("S")]);
//        }
//    }

    private static function toastr(string $type, string $title, string $msg = ''){
        echo 'toastr["'.$type.'"]("'.$msg.'","'.$title.'");';
    }

}