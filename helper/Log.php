<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/28/17
 * Time: 12:30 PM
 */

namespace helper;


class Log
{

    public static function writeLog($type, $message, $file = 'panel')
    {
        if (filesize(LOG_PATH . "$file") > 100000) {
            unlink(LOG_PATH . "$file");
        }
        $f = fopen(LOG_PATH . "$file", 'ab');
        $str = date('Y-m-d H:i:s') . "|----| $type |----| $message |\n\n";
        fwrite($f, $str);
        fclose($f);
    }
}