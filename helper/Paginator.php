<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/23/17
 * Time: 7:32 AM
 */

namespace helper;

class Paginator {
    public $itemsPerPage;
    public $range;
    public $currentPage;
    public $total;
    public $textNav;
    public $itemSelect;
    private $_navigation;
    private $_link;
    private $_pageNumHtml;
    private $_itemHtml;
    private $join = '?';
    /**
     * Constructor
     */
    public function __construct()
    {
        //set default values
        $this->itemsPerPage = 10;
        $this->range        = 20;
        $this->currentPage  = 1;
        $this->total        = 0;
        $this->textNav      = true;
//        $this->itemSelect   = array(10,25,50,100,'All');
        $this->itemSelect   = array(10,25,50,100);
        //private values
        $this->_navigation  = array(
            'next'=>'Next',
            'pre' =>'Pre',
            'ipp' =>'Item per page'
        );
        $this->_link         = filter_var($_SERVER['PHP_SELF'], FILTER_SANITIZE_STRING);
        $this->_pageNumHtml  = '';
        $this->_itemHtml     = '';
    }

    public function setDate(int $items):void{
        $this->itemsPerPage = $items;
        $this->range = $items * 3;
    }

    public function setLink(): void
    {
        $this->_link = Tool::getLink('current');
    }

    /**
     * paginate main function
     *
     * 
     * @access              public
     */
    public function paginate(): void
    {
        //get current page
        if(isset($_GET['current'])){
            $this->currentPage  = $_GET['current'];
        }
        //get item per page
        if(isset($_GET['item'])){
            $this->itemsPerPage = $_GET['item'];
            $this->range = $this->itemsPerPage * 3;
        }
        if (false !== strpos($this->_link, '?')){
            $this->join = '&amp;';
        }
        //get page numbers
        $this->_pageNumHtml = $this->_getPageNumbers();
        //get item per page select box
        $this->_itemHtml    = $this->_getItemSelect();
    }

    /**
     * return pagination numbers in a format of UL list
     *
     * 
     * @access              public
     * @return string
     * @internal param type $parameter
     */
    public function pageNumbers(): string
    {
        if(empty($this->_pageNumHtml)){
            exit('Please call function paginate() first.');
        }
        return $this->_pageNumHtml;
    }

    /**
     * return jump menu in a format of select box
     *
     * 
     * @access              public
     * @return              string
     */
    public function itemsPerPage(): string
    {
        if(empty($this->_itemHtml)){
            exit('Please call function paginate() first.');
        }
        return $this->_itemHtml;
    }

    /**
     * return page numbers html formats
     *
     * 
     * @access              public
     * @return              string
     */
    private function  _getPageNumbers(): string
    {
        $html  = '<ul class="pagination pagination-sm">';
        //previous link button
        if($this->textNav&&($this->currentPage>1)){
            $html .= '<li class="page-item"><a class="page-link" href="'.$this->_link .$this->join.'current='.($this->currentPage-1).'"';
            $html .= '>'.$this->_navigation['pre'].'</a></li>';
        }else{
            $html .= '<li class="page-item disabled">
      <span class="page-link">Pre</span>
    </li>';
        }

        //do ranged pagination only when total pages is greater than the range
        if($this->total > $this->range){
            $start = ($this->currentPage <= $this->range/$this->itemsPerPage)?1:($this->currentPage - $this->range/$this->itemsPerPage);
            $end   = (int)((floor($this->total/$this->itemsPerPage) - $this->currentPage > 0)?($this->currentPage+$this->range/$this->itemsPerPage): ceil($this->total/$this->itemsPerPage));

        }else{
            $start = 1;
            $end   = ceil($this->total/$this->itemsPerPage);
        }

        //loop through page numbers
        for($i = $start; $i <= $end; $i++){
            if($i==$this->currentPage){
                $html .= '<li class="page-item active">
      <span class="page-link">
        '.$i.'
        <span class="sr-only">(current)</span>
      </span>
    </li>';continue;
            }
            $html .= '<li class="page-item"><a class="page-link" href="'.$this->_link .$this->join.'current='.$i.'"';
            $html .= '>'.$i.'</a></li>';
        }
        $over = ceil($this->total/$this->itemsPerPage);
        //next link button
        if($this->textNav&&($this->currentPage<=$over)){
            $html .= '<li class="page-item ';
            if (!($this->currentPage+1<=$over)) $html .= 'disabled';
            $html .= '"><a class="page-link" href="'.$this->_link .$this->join.'current='.($this->currentPage+1).'"';
            $html .= '>'.$this->_navigation['next'].'</a></li>';
        }
        $html .= '</ul>';
        return $html;
    }

    /**
     * return item select box
     *
     * 
     * @access              public
     * @return              string
     */
    private function  _getItemSelect(): string
    {
        $items = '';
        $ippArray = $this->itemSelect;
        foreach($ippArray as $ippOpt){
            $items .= ($ippOpt == $this->itemsPerPage) ? "<option selected value=\"$ippOpt\">$ippOpt</option>\n":"<option value=\"$ippOpt\">$ippOpt</option>\n";
        }
        $url = Tool::getLink('item');
        if (false !== strpos($url, '?')){
            $join = '&';
        }else{
            $join = '?';
        }
        return "<div class='float-right' style='margin-top: -3rem'><span class=\"paginate\">".$this->_navigation['ipp']."</span>
            <select class=\"paginate\" onchange=\"window.location='".$url.$join."item='+this[this.selectedIndex].value;return false\">$items</select></div>";
    }

    public function getStart(): int {
        return ($this->currentPage-1)*$this->itemsPerPage;
    }

    public function isLast(): bool
    {
        return ceil($this->total / $this->itemsPerPage) == $this->currentPage;
    }
}