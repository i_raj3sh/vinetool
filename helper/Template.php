<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/9/17
 * Time: 9:13 PM
 */

namespace helper;


use controller\Controller;
use model\HourlyStats;
use model\InvoiceAffiliate;
use Twig_Environment;
use Twig_Loader_Filesystem;

class Template
{
    public const TITLE = 'title';
    public const HTML = 'html';
    private $title = 'Vinetool';
    private $html;
    private $controller;

    public function __construct($controller)
    {
        $this->controller = $controller;
    }

    public static function devIcons($dev, $ver): array
    {
        $ver = $ver > 0 ? '<b>' . $ver . '+</b>' : '';
        $arr = [];
        foreach ($dev as $os) {
            switch ($os) {
                case 'Android':
                    $arr[] = '<i class="fa fa-android fa-lg" style="color: #a4c639" data-toggle="tooltip" title="Android"></i> ' . $ver . '';
                    break;
                case 'iOS':
                    $arr[] = '<i class="fa fa-apple fa-lg" title="iOS" data-toggle="tooltip"></i> ' . $ver . '';
                    break;
                case 'Windows':
                    $arr[] = '<i class="fa fa-windows fa-lg" style="color: #008272" data-toggle="tooltip" title="Windows"></i> ' . $ver . '';
            }
        }
        if (empty($arr)) {
            $arr[] = 'All';
        }
        return $arr;
    }

    public static function getView($page, $form = null): void
    {
        require dirname(__DIR__) . '/views/async/' . $page . '.html.php';
    }

    public static function geoFormat($geo): string
    {
        if (\count($geo) > 3) {
            return implode(' ', \array_slice($geo, 0, 3)) . ' <i class="fa fa-ellipsis-h"></i>';
        }
        return implode(' ', $geo);
    }

    public function setTemplateField($key, $value): void
    {
        $this->$key = $value;
    }

    public function getHTML($form): void
    {
        include dirname(__DIR__) . '/views/' . $this->controller . '/' . $this->html;
    }

    public function getNav(): void
    {
        if ($type = Controller::getSessionType()) {
            switch ($type) {
                case 'AFFILIATE':
                    $nav = 'affiliate';
                    $args = [
                        'status' => 0,
                        'affiliate_ID' => Controller::getSession()
                    ];
                    break;
                case 'ADMIN':
                    $admin = Controller::getAdminType();
                    if ($admin === 'SUPER')
                        $nav = 'super';
                    else
                        $nav = 'default';

                    $args = [
                        'status' => 0
                    ];
            }
            $in = new InvoiceAffiliate();
            $in->setProperties($args)->setQueryParameters($in);
            $in->appendQuery('amount >= 100');
            Mysql::setDbName(Mysql::STATS);
            $invoice_count = $in->count();
            $invoice_count = $invoice_count > 0 ? $invoice_count : '';
            Mysql::setDbName();

            include dirname(__DIR__) . '/views/nav/' . $nav . '.html.php';
        }
    }

    public function getFoot(): void{
        echo
        '<footer style="background: #212529;border-top:5px solid #111">
            <div class="py-2 px-3">Vinetool Media. &copy; 2018</div>
        </footer>';
    }

    public function getTemplateField($key): void
    {
        echo $this->$key;
    }

    public function getSideBar(): void
    {
        if ($type = Controller::getSessionType()) {
            $nav = 'admin';
            $cObj = new Cache();

            if (!($sidebarData = $cObj->retrieve(__METHOD__))) {
                Mysql::setDbName(Mysql::STATS);
                $sidebarData = self::generateSideBarCounts($type);
                Mysql::setDbName();
                $cObj->enter(__METHOD__, $sidebarData);
            }
            include \dirname(__DIR__) . '/views/sidebar/' . $nav . '.html.php';
        }
    }

    public static function generateSideBarCounts($type):string {
        $day = date('Ymd');
        switch ($type) {
            case 'AFFILIATE':
                $gh = new HourlyStats();

                $params = [
                    'day_' => $day,
                    'affiliate_ID' => Controller::getSession()
                ];

                $gh->setProperties($params);
                $gh->setQueryParameters($gh, ['SUM(stats_grouped_hourly.clicks) visit,SUM(stats_grouped_hourly.sales) conver,SUM(stats_grouped_hourly.payout) push,SUM(stats_grouped_hourly.charge) pull']);

                $today = $gh->one();

                $gh = new HourlyStats();

                $params = [
                    'affiliate_ID' => Controller::getSession(),
                    'month_' => date('Ym')
                ];

                $gh->setProperties($params);
                $gh->setQueryParameters($gh, ['SUM(stats_grouped_hourly.clicks) visit,SUM(stats_grouped_hourly.sales) conver,SUM(stats_grouped_hourly.payout) push,SUM(stats_grouped_hourly.charge) pull']);

                $total = $gh->one();

                $data = [
                    'impressions' => number_format($today->visit),
                    'installs' => number_format($today->conver),
                    'payout' => number_format($today->push,2),
                    'impressionsT' => number_format($total->visit),
                    'installsT' => number_format($total->conver),
                    'payoutT' => number_format($total->push,2)
                ];
                return json_encode($data);

            case 'ADMIN':
                $gh = new HourlyStats();

                $params = [
                    'day_' => $day,
                ];

                $gh->setProperties($params);
                $gh->setQueryParameters($gh, ['SUM(stats_grouped_hourly.clicks) visit,SUM(stats_grouped_hourly.sales) conver,SUM(stats_grouped_hourly.payout) push,SUM(stats_grouped_hourly.charge) pull']);

                $today = $gh->one();

                $gh = new HourlyStats();
                $gh->setProperties([
                    'month_' => date('Ym')
                ]);
                $gh->setQueryParameters($gh, ['SUM(stats_grouped_hourly.clicks) visit,SUM(stats_grouped_hourly.sales) conver,SUM(stats_grouped_hourly.payout) push,SUM(stats_grouped_hourly.charge) pull']);

                $total = $gh->one();

                $data = [
                    'impressions' => number_format($today->visit),
                    'installs' => number_format($today->conver),
                    'payout' => number_format($today->pull - $today->push,2),
                    'impressionsT' => number_format($total->visit),
                    'installsT' => number_format($total->conver),
                    'payoutT' => number_format($total->pull - $total->push,2)
                ];

                return json_encode($data);

            default:
                return '';
        }
    }

    public function render($form = null): void
    {
        $template = $this;
        require \dirname(__DIR__) . '/views/default.html.php';
    }

    public static function getActive(): void
    {
        if (!isset($_REQUEST['x'])) {
            echo 'dash';
        } else {
            echo $_REQUEST['x'];
        }
    }

    public static function expiry($e): string
    {
        if ($e == 0) {
            return 'No Expiry';
        }
        return self::getDate($e);

    }

    public static function getDate($date): string
    {
        $year = substr($date, 0, 4);
        $m = (int)substr($date, 4, 2);
        $day = substr($date, 6, 2);
            $hour = substr($date, 8, 2);
            $minute = substr($date, 10, 2);
            $second = substr($date, 12, 2);

        switch ($m) {
            case 1:
                $month = 'Jan';
                break;
            case 2:
                $month = 'Feb';
                break;
            case 3:
                $month = 'Mar';
                break;
            case 4:
                $month = 'Apr';
                break;
            case 5:
                $month = 'May';
                break;
            case 6:
                $month = 'Jun';
                break;
            case 7:
                $month = 'Jul';
                break;
            case 8:
                $month = 'Aug';
                break;
            case 9:
                $month = 'Sep';
                break;
            case 10:
                $month = 'Oct';
                break;
            case 11:
                $month = 'Nov';
                break;
            default:
                $month = 'Dec';
        }

        if (date('Y') === '2018'){
            $end = $month . ' ' . $day.' ';
        }else{
            $end = $month . ' ' . $day . ', ' . $year;
        }

        if (\strlen($date) > 8) {
        $end .= ' ' . $hour . ':' . $minute . ':' . $second;
        }

        return $end;
    }

    public static function state($e): string
    {
        if ($e == 0) {
            return '<i data-toggle="tooltip" title="inactive" class="fa fa-circle text-danger" style="font-size: 1rem"><span style="display: none">In_Active</span></i>';
        }
        if ($e == 2) {
            return '<i data-toggle="tooltip" title="paused" class="fa fa-circle text-warning"  style="font-size: 1rem"><span style="display: none">Paused_</span></i>';
        }
        if ($e == 3) {
            return '<i data-toggle="tooltip" title="capped" class="fa fa-circle text-primary"  style="font-size: 1rem"><span style="display: none">Capped_</span></i>';
        }
        return '<i data-toggle="tooltip" title="active" class="fa fa-circle text-success"  style="font-size: 1rem"><span style="display: none">Active</span></i>';

    }

    public static function offerState($e): string
    {
        if ($e === '0') {
            return '<strong class="badge badge-danger text-white" style="font-size: .9rem">inactive</strong>';
        }
        if ($e === '2') {
            return '<strong class="badge badge-warning" style="font-size: .9rem">paused</strong>';
        }
        if ($e === '3') {
            return '<strong class="badge badge-primary" style="font-size: .9rem">capped</strong>';
        }
        return '<strong class="badge badge-success text-white" style="font-size: .9rem">active</strong>';

    }

    public static function stateButton($v){
        if ($v->active === '1') {
            $state = '<a class="btn btn-warning btn-sm" href="' . s() . 'offers/pause?id=' . $v->ID . '" data-toggle="tooltip" title="pause"><i class="fa fa-pause"></i></a>';
        } elseif ($v->active === '2') {
            $state = '<a class="btn btn-success btn-sm" href="' . s() . 'offers/resume?id=' . $v->ID . '" data-toggle="tooltip" title="resume"><i class="fa fa-play"></i></a>';
        } else {
            $state = '<button class="btn btn-default btn-disabled btn-sm" data-toggle="tooltip" title="inactive" aria-disabled="true"><i class="fa fa-stop text-danger"></i></button>';
        }
        return $state;
    }

    public static function entityStateButton($v, $entity)
    {
        if ($v->active === '1') {
            $state = '<a class="btn btn-outline-warning btn-sm" href="' . s() . $entity . '/deactivate?id=' . $v->ID . '" data-toggle="tooltip" title="deactivate"><i class="fa fa-stop text-danger"></i></a>';
        } else {
            $state = '<a class="btn btn-outline-success btn-sm" href="' . s() . $entity . '/activate?id=' . $v->ID . '" data-toggle="tooltip" title="activate"><i class="fa fa-play"></i></a>';
        }
        return $state;
    }

    public static function approval($e,$o=1): string
    {
        if ($o === '0'){
            return '<span class="badge badge-danger">inactive offer</span>';
        }
        if ($o === '2'){
            return '<span class="badge badge-warning">paused offer</span>';
        }
        if ($e == 0) {
            return '<span class="badge badge-danger">pending</span>';
        }
        if ($e == 2) {
            return '<span class="badge badge-warning">paused</span>';
        }
        return '<span class="badge badge-success">yes</span>';

    }

    public static function bool($e): string
    {
        if ($e == 0) {
            return '<span class="badge badge-danger">No</span>';
        }
        return '<span class="badge badge-success">Yes</span>';

    }

    public static function nums($e): string
    {
        if ($e == 0) {
            return '<span>-</span>';
        }
        return '<strong>' . $e . '</strong>';

    }

    public static function cap($v, $t):string{
        if ($v === '-1'){
            if ($t === '0'){
                return 'n/a';
            }
            return $t;
        }
        return $v;
    }

    public static function option($item, $val): void
    {
        if (\is_array($item)) {
                foreach ($item as $v) {
                    if ($v == $val) {
                        echo 'selected';
                        break;
                    }
                }
        } else {
            if ($item == $val)
                echo 'selected';
        }
    }

    public static function wordBreak($subject,$replace): string {
        return implode($replace, $subject);
    }

    public function getSidebarView():string {

    }

    public function twig($form=null):void{
        $loader = new Twig_Loader_Filesystem(__DIR__.'/../view');
        $twig = new Twig_Environment($loader, [ 'cache' => __DIR__.'/../cache/twig_compile'] );

        $obj = new \stdClass();
        $obj->title = $this->title;
        $obj->nav = $this->getSidebarView();
        try {
            echo $twig->render($this->html, ['obj'=>$obj]);
        } catch (\Twig_Error_Loader $e) {
        } catch (\Twig_Error_Runtime $e) {
        } catch (\Twig_Error_Syntax $e) {
        }
    }

    public static function time($t): string
    {
        if ($t){
            return date('M d, Y H:i', $t);
        }else{
            return '-';
        }
    }

    public static function offerTest($v): string
    {
        if ($v->dev[0] !== 'All') {
            $os = $v->dev[0] === 'iOS' ? 'iphone' : 'android';
            return 'https://offertest.net/offertest/testresult?country=' . strtolower($v->geo[0]) . '&os=' . $os . '&target=' . strtolower($v->dev[0]) . '&url=' . \rawurlencode($v->tracking);
        }
        return '#!';
    }

    public static function affiliateStatus($id): void
    {
        $a = new \model\ApproveOffers();
        $a->setProperties([
            'offers_ID' => $id,
            'affiliate_ID' => Controller::getSession()
        ]);
        $a->setQueryParameters($a);
        $d_ = $a->query();
        if (empty($d_)) {
            echo '<td><a class="btn btn-sm btn-info" href="offers/request?id=' . $id . tkn('&') . '">Request</a> </td>';
        } elseif ($d_[0]->status == 1) {
            echo '<td><span class="text-success">Approved</span> </td>';
        } else {
            echo '<td><span class="text-warning">Pending</span> </td>';
        }
    }
}