<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/6/18
 * Time: 11:46 AM
 */

namespace helper;

use controller\Controller;

class Cache
{
    public const STORE = 'Store-';
    private $cacheType = 'Cache-';

    public function __construct(string $type = '')
    {
        if ($type) {
            $this->cacheType = $type;
        }
    }

    private function adminKey($at): string
    {
        return $this->cacheType . '-Admin-' . $at;
    }

    private function affiliateKey($at): string
    {
        return $this->cacheType . '-Affiliate-' . Controller::getSession() . '-' . $at;
    }

    public function enter($at, $json): void
    {

        $redis = new RedisFy();
        if (Controller::getSessionType() === 'ADMIN') {
            $key = $this->adminKey($at);
        } else {
            $key = $this->affiliateKey($at);
        }
        $redis->setKV($key, $json);
    }

    public function enterTemp($at, $json, int $time): void
    {
        $redis = new RedisFy();
        if (Controller::getSessionType() === 'ADMIN') {
            $key = $this->adminKey($at);
        } else {
            $key = $this->affiliateKey($at);
        }
        $redis->setKVex($key, $json, $time);
    }

    public function retrieve($at): string
    {
        $redis = new RedisFy();
        if (Controller::getSessionType() === 'ADMIN') {
            $key = $this->adminKey($at);
        } else {
            $key = $this->affiliateKey($at);
        }
        return $redis->getV($key);
    }

    public function clearCache(): void
    {
        $redis = new RedisFy();
        $redis->remove($redis->getStart($this->cacheType));
    }

    public function clearAdminCache(): void
    {
        $redis = new RedisFy();
        $redis->remove($redis->getStart($this->cacheType . '-Admin-'));
    }

    public function clearAffiliateCache(): void
    {
        $redis = new RedisFy();
        $redis->remove($redis->getStart($this->cacheType . '-Affiliate-'));
    }

    public function set($at, $json): void
    {

        $redis = new RedisFy();
        $key = $this->cacheType . $at;
        $redis->setKV($key, $json);
    }

    public function get($at): string
    {

        $redis = new RedisFy();
        $key = $this->cacheType . $at;
        $redis->getV($key);
    }
}