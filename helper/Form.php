<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/20/17
 * Time: 6:39 PM
 */

namespace helper;


class Form
{
    public function get($k,$v='')
    {
        if (isset($this->$k))
            echo $this->$k;
        else echo $v;
    }

    public function set($k, $v)
    {
        $this->$k = $v;
    }

    public function setForm($info)
    {
        $info = (array) $info;
        foreach ($info as $k => $v):
            if (!strpos($k, "\\")) {
                $k = trim($k);
                $this->$k = $v;
            }
        endforeach;
    }

    public function getDate($date)
    {
        echo date('M d, Y', strtotime($date));
    }

    public static function getGenderString($num){
        switch ($num):
            case 0: return 'Unisex';
            case 1: return 'Male';
            default: return 'Female';
        endswitch;
    }

    public function select($k,$v){
        if ($this->$k == $v)
            echo 'selected';
    }

    public function fetch($k){
        return $this->$k;
    }

    public function option($item,$val){
        foreach ($item as $v){
            if ($v->ID == $val) {
                echo 'selected';
                break;
            }
        }
    }

    public function in_array($arr, string $val):void {
        if (\in_array($val, $arr, true)){
            echo 'selected';
        }
        echo '';
    }

}