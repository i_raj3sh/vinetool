<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/21/17
 * Time: 10:02 AM
 */

namespace helper;


class Image
{
    public static function upload()
    {

        $error = "Invalid image request";
        if (isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {

            require_once "lib/image.php";

            $userfile_name = $_FILES['image']['name'];
            $userfile_tmp = $_FILES['image']['tmp_name'];
            $userfile_size = $_FILES['image']['size'];
            $userfile_type = $_FILES['image']['type'];
            $n = explode(".", $_FILES['image']['name']);

            $file_ext = $n[1];

            $filename = md5($t);

            if ((!empty($_FILES["image"])) && ($_FILES['image']['error'] == 0)) {

                foreach ($allowed_image_types as $mime_type => $ext) {
                    if ($file_ext == $ext && $userfile_type == $mime_type) {
                        $error = "";
                        break;
                    } else {
                        $error = "Only " . $image_ext . " images accepted for upload";
                    }
                }
                if ($userfile_size > ($max_file * 1048576)) {
                    $error .= "Images must be under " . $max_file . "MB in size";
                }

            } else {
                $error = "No image / image with errors";
            }
            $EXT = "." . $file_ext;
            $large_image_location = $upload_path . $filename . $EXT;

            move_uploaded_file($userfile_tmp, $large_image_location);
            chmod($large_image_location, 0777);

            $width = getWidth($large_image_location);
            $height = getHeight($large_image_location);

            if ($width > $max_width) {
                $scale = $max_width / $width;
                resizeImage($large_image_location, $width, $height, $scale);
            } else {
                $scale = 1;
                resizeImage($large_image_location, $width, $height, $scale);
            }

            $scale = $thumb_width / $max_width;

            $thumb_height = getHeight($large_image_location);
            $x1 = 0;
            $y1 = 0;

            $thumb_image_location = $upload_path . "thumb/" . $filename . $EXT;

            resizeThumbnailImage($thumb_image_location, $large_image_location, $max_width, $thumb_height, $x1, $y1, $scale);
        } else {
            MyException::setSessionError("Banner Image is required. please upload it.");
        }

        if (!empty($error))
            MyException::setSessionError($error);
        else
            //return [$large_image_location, $thumb_image_location];
            return [$filename . $EXT];

        return [];
    }
}