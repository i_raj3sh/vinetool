<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/19/17
 * Time: 1:53 PM
 */

namespace helper;


class Mailer
{
    private $message='';
    private $headers;
    private $replyTo = 'support@vinetool.com';

    /**
     * @param string $from
     * @param mixed $cc
     * @param mixed $bcc
     * @return $this
     */
    public function setHeaders($from='no-reply@vinetool.com', $cc=[], $bcc=[]){
        $this->headers  = 'From: "' .$from. '" <' .$this->replyTo.">\r\n";
        $this->headers .= "MIME-Version: 1.0\r\n";
        $this->headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        if (!empty($cc)){
            $cc = implode(',',$cc);
            $this->headers .= "Cc: $cc\r\n";
        }
        if (!empty($bcc)){
            $bcc = implode(',',$bcc);
            $this->headers .= "Bcc: $bcc\r\n";
        }

        $this->headers .= 'X-Mailer: PHP/' . PHP_VERSION;

        return $this;
    }


    public function setMessage($file, $arr=[]){
        ob_start();

        include dirname(__DIR__).'/views/emails/'.$file.'.html.php';

        $this->message = ob_get_clean();

        return $this;
    }

    public function getHeaders(){
        return $this->headers;
    }

    public function getMessage(){
        return $this->message;
    }

    public function setReplyTo($replyTo){
        $this->replyTo = $replyTo;
        return $this;
    }

    public function send($to,$subject){
        $mail = mail($to,$subject,$this->getMessage(),$this->getHeaders());
        if($mail){
            return true;
        }
        return null;
    }

}