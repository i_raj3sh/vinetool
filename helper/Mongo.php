<?php
/**
 * Created by PhpStorm.
 * User: loot
 * Date: 14/3/18
 * Time: 8:13 PM
 */

namespace helper;


use model\Model;
use MongoDB\BSON\ObjectId;
use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Command;
use MongoDB\Driver\Cursor;
use MongoDB\Driver\Exception\Exception;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;

class Mongo
{
    private static $obj;
    private static $con = [
        'db' => 'db_dump',
        'server' => 'localhost',
        'timeout' => 900000,
    	'authentication' => 'admin'
    ];
    private $now;

    public function __construct(array $con=[]){
        if (!empty($con)){
            self::$con = $con;
        }
        try {
            $this->now = self::connect();
        }catch (Exception $e){
            MyException::setSessionError('DB handshake failed');
        }
    }

    /**
     *
     * @throws \MongoDB\Driver\Exception\RuntimeException
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     * @return Manager
     */
    private static function connect(): Manager{
        if (self::$obj[self::$con['server']] === null){
            $options = ['connect' => TRUE, 'socketTimeoutMS' => self::$con['timeout'],'connectTimeoutMS'=> 1000 ];
            self::$obj[self::$con['server']] = new Manager('mongodb://'. MONGO_DSN . self::$con['server'] . ':27017/'.self::$con['authentication'],$options);
        }
        return self::$obj[self::$con['server']];
    }

    /**
     * @param Model $model
     * @param $filter
     * @param array $options
     * @return Cursor
     */
    public function query(Model $model, $filter, array $options=[]): Cursor{
        try{
        if (empty($options)){
            $query = new Query($filter);
        }else{
            $query = new Query($filter, $options);
        }
            return $this->now->executeQuery(self::$con['db'].'.'.$model->getMyTable(), $query);
        } catch (Exception $e) {
            MyException::setSessionError('DB query handshake failed');
        }
        return null;
    }

    /**
     * @param Model $model
     * @param array $data
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     */
    public function insert(Model $model, array $data): void{
        $bulk = new BulkWrite;
        foreach ($data as $datum){
            $bulk->insert($datum);
        }
        $this->now->executeBulkWrite(self::$con['db'].'.'.$model->getMyTable(), $bulk);
    }

    /**
     * @param Model $model
     * @param array $data
     * @throws \MongoDB\Driver\Exception\InvalidArgumentException
     */
    public function save(Model $model, array $data): void{
        $bulk = new BulkWrite;
        foreach ($data as $datum){
            $datum['ID'] = (int)$datum['ID'];
            if ($id = $this->ifExist($model, 'ID', $datum['ID'])){
                unset($datum['ID']);
                $id = new ObjectId($id);
                $bulk->update(['_id'=>$id], $datum);
            }else{
                $bulk->insert($datum);
            }
        }
        $this->now->executeBulkWrite(self::$con['db'].'.'.$model->getMyTable(), $bulk);
    }

    /**
     * @param Model $model
     * @param string $key
     * @param $value
     * @return string
     */
    private function ifExist(Model $model, string $key, $value):string {
        try{
            $query = new Query([$key=>$value]);
            $cursor = $this->now->executeQuery(self::$con['db'].'.'.$model->getMyTable(), $query)->toArray();
            if (empty($cursor)){
                return '';
            }
            return $cursor[0]->{'_id'}->__toString();
        } catch (Exception $e) {
            MyException::setSessionError('DB query failed');
        }
        return null;
    }

    public function command(Model $model, string $command, array $query = []) {
        $run = [$command => $model->getMyTable(), 'query' => $query];
        try {
            $result = $this->now->executeCommand(self::$con['db'], new Command($run));
            return current($result->toArray());
        } catch (Exception $e) {
            MyException::setSessionError('DB command handshake failed');
        }
        return null;
    }

    public function aggregate(Model $model, array $query): Cursor {
        $run = [
            'aggregate' => $model->getMyTable(),
            'pipeline' => $query,
            'cursor' => $model,
            'allowDiskUse' => true
            ];
        try {
            return $this->now->executeCommand(self::$con['db'], new Command($run));
        } catch (Exception $e) {
            MyException::setSessionError('DB command handshake failed');
        }
        return null;
    }

    /**
     * @param Model $model
     * @param array $filter
     */
    public function delete(Model $model, array $filter = []): void
    {
        $bulk = new BulkWrite;
        $bulk->delete($filter);
        $this->now->executeBulkWrite(self::$con['db'] . '.' . $model->getMyTable(), $bulk);
    }
}
