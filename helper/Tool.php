<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/25/17
 * Time: 10:30 AM
 */

namespace helper;


use Exception;

class Tool
{
    private static $ch;

    public static function hitCurl($url, $setting=[
        'ct' => 'json',
    	'fl' => false
    ])
    {
        try {

            if (self::$ch) {
                $ch = self::$ch;
            } else {
                $ch = self::$ch = curl_init();
            }

            if (FALSE === $ch)
                throw new Exception('Failed'.__CLASS__.' to initialize');

            if ($setting['ct'] === 'json') {
                $headers = [
                    'Content-Type: application/json',
                    'Accept: application/json',
                ];
            }else{
                $headers = [];
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            if ($setting['fl']) {
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);

            if (FALSE === $result){
                throw new Exception(curl_error($ch), curl_errno($ch));
            }

            return $result;

        } catch (Exception $e) {

            return false;
        }

    }

    public static function getLink($append){
        $link = ltrim($_SERVER['REQUEST_URI'],'/');

        list($first,$second)  = explode($append.'=',$link,2);
        $first = rtrim($first,'?');
        $first = rtrim($first,'&');
        $second = explode('&',$second,2)[1];
        if ($second)
            $second = '?'.$second;
        $url = $first.$second;
        return MY_SERVER.$url;
    }

    public static function getSort($sortBy){
        $link = self::getLink('sort');
        $join = '?';
        if (false !== strpos($link, '?')){
            $join = '&';
        }
        if (isset($_GET['sort'])){
            if (false !== strpos($_GET['sort'], $sortBy.'_a')){
                $link .= $join.'sort='.$sortBy.'_d';
                $href = 'href="'.$link.'"><nobr><i class="fa fa-sort-asc"></i>';
            }elseif (false !== strpos($_GET['sort'], $sortBy.'_d')){
                $link .= $join.'sort='.$sortBy.'_a';
                $href = 'href="'.$link.'"><nobr><i class="fa fa-sort-desc"></i>';
            }
            else {
                $link .= $join.'sort='.$sortBy.'_a';
                $href = 'href="'.$link.'"><nobr><i class="fa fa-sort"></i>';
            }
        }
        else {
            $link .= $join.'sort='.$sortBy.'_a';
            $href = 'href="'.$link.'"><nobr><i class="fa fa-sort"></i>';
        }

        return $href;
    }

    public static function hourFormat($input): string
    {
        if ($input > 100){
            return Template::getDate($input);
        }
        $input = (int) $input;
        if ($input < 12){
            if ($input === 0){
                $hour = '12:00 AM  -->  01:00 AM';
            }else{
                $what = 'AM';
                if (($next = $input+1) === 12){
                    $what = 'PM';
                }
                if ($input < 10)
                    $input = '0'.$input;
                if ($next < 10)
                    $next = '0'.$next;

                $hour = $input.':00 AM  -->  '.$next.':00 '.$what;
            }
        }else{
            if ($input === 12){
                $hour = '12:00 PM  -->  01:00 PM';
            }else{
                if ($input > 12) {
                    $input -= 12;
                    $next = $input + 1;
                }

                $what = 'PM';
                if ($next === 12){
                    $what = 'AM';
                }

                if ($input < 10)
                    $input = '0'.$input;
                if ($next < 10)
                    $next = '0'.$next;

                $hour = $input.':00 PM  -->  '.$next.':00 '.$what;
            }
        }
        return $hour;
    }

    public static function getIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        if (strpos($ipaddress, ",")) $ipaddress = trim(explode(",", $ipaddress)[0]);
        return $ipaddress;
    }

    public static function dateOverflowCheck(&$date): void
    {
        $temp = $date . '';
        $day = (int)substr($temp, 6, 2);
        if ($day < 29) {
            return;
        }
        $month = (int)substr($temp, 4, 2);
        switch ($month) {
            case 2:
                $overflow = 28;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                $overflow = 30;
                break;
            default:
                $overflow = 31;
        }
        if ($day === $overflow) {
            $year = (int)substr($temp, 0, 4);
            $day = '00';
            if ($month === 12) {
                $month = '01';
                $year++;
                $temp = $year . $month . $day;
                $date = (int)$temp;
            } else {
                $adder = 100 - $overflow;
                $date += $adder;
            }
        }
    }

    public static function sort($a): array
    {
        asort($a);
        return array_values($a);
    }

    public static function getMonthName($m): string
    {
        switch ($m){
            case 1: $month = 'Jan'; break;
            case 2: $month = 'Feb'; break;
            case 3: $month = 'Mar'; break;
            case 4: $month = 'Apr'; break;
            case 5: $month = 'May'; break;
            case 6: $month = 'Jun'; break;
            case 7: $month = 'Jul'; break;
            case 8: $month = 'Aug'; break;
            case 9: $month = 'Sep'; break;
            case 10: $month = 'Oct'; break;
            case 11: $month = 'Nov'; break;
            default: $month = 'Dec';
        }
        return $month;
    }

    public static function hydrate($get):array {
        if (empty($get)){
            return [];
        }
        $arr = [];
        foreach ($get as $v){
            $arr[] = $v->network_ID;
        }
        return $arr;
    }

    public static function replicateOffers():void{
        $command = MY_BASE.'cron/replicateOffers.php';
        exec("php $command");
    }

    public static function getAppId(string $preview): string
    {
        if (false !== strpos($preview, 'https://play.google.com/store')) {
            $get = explode('id=', $preview)[1];
            return explode('&', $get)[0];
        }
        if (false !== strpos($preview, 'https://itunes.apple.com')) {
            $get = explode('/id', $preview)[1];
            return explode('?', $get)[0];
        }
        return '';
    }

    public static function getMinVer($os, string $type): string
    {
        if ($type === 'AFFISE') {
            foreach ($os as $v) {
                if (false !== strpos($v, '>')) {
                    return ltrim($v, '>');
                }
            }
        }
        return 0;
    }
}
