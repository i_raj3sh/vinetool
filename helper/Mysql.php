<?php

/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/06/2016
 * Time: 19:31
 */
namespace helper;
class Mysql
{
    public const PANEL = 'vine_ad_db';
    public const STATS = 'vine_ad_stats';
    private static $con = [];
    private static $dbName = Mysql::PANEL;

    public static function select($sql, $class, $arg = [])
    {
        if (SQL_DEBUG) {
            Log::writeLog("MYSQL QUERY", $sql);
        }
        try {
            $con = self::getCon();
            $stmt = $con->prepare($sql);
            $stmt->execute($arg);
            return $stmt->fetchAll(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $class);
        } catch (\PDOException $e) {
            MyException::setError($e->getMessage(), "select");
            return null;
        }
    }

    public static function setDbName(string $db = Mysql::PANEL): void
    {
        self::$dbName = $db;
    }

    public static function getCon(): \PDO
    {
        try {
            if (self::$con[self::$dbName]) {
                return self::$con[self::$dbName];
            }
            return self::$con[self::$dbName] = new \PDO('mysql:host=localhost;dbname=' . self::$dbName . ';charset=UTF8', MYSQL_USER, MYSQL_PSWD);

        } catch (\PDOException $e) {
            Log::writeLog('connection failed', $e->getMessage(), 'mysql.log');
            die('Failed'.__CLASS__.'');
        }
        return null;
    }

    public static function put($type, $table, $arg, $cond = "")
    {
        $key = implode(',', array_keys($arg));
        $val = array_values($arg);
        if (SQL_DEBUG) {
            Log::writeLog("MYSQL SAVE", "$type $table set $key $cond ||| ".json_encode($arg));
        }
        try {
            $con = self::getCon();
            $stmt = $con->prepare("$type $table set $key $cond");
            $stmt->execute($val);
            var_dump($stmt, $val);
            return $stmt->rowCount();
        } catch (\PDOException $e) {
            MyException::setError($e->getMessage(), $type);
            var_dump($e->getMessage());
        }
        return false;
    }

    public static function update($sql)
    {
        if (SQL_DEBUG) {
            Log::writeLog('MYSQL SAVE', $sql);
        }
        try {
            $con = self::getCon();
            $stmt = $con->query($sql);
            if($stmt)
                return $stmt->rowCount();
            else return 0;
        } catch (\PDOException $e) {
            MyException::setError($e->getMessage(), 'UPDATE D');
        }
        return false;
    }

    public static function lastId()
    {
        $con = self::getCon();
        return $con->lastInsertId();
    }

    public static function errorCode(){
        $con = self::getCon();
        return $con->errorCode();
    }

    public static function delete($sql, $arg = [])
    {
        if (SQL_DEBUG) {
            Log::writeLog("MYSQL DELETE", $sql);
        }
        try {
            $con = self::getCon();
            $stmt = $con->prepare($sql);
            return $stmt->execute($arg);
        } catch (\PDOException $e) {
            MyException::setError($e->getMessage(), "delete");
        }
        return false;
    }

    public static function numRows($sql, $arg)
    {
        try {
            $con = self::getCon();
            $stmt = $con->prepare($sql);
            $stmt->execute($arg);
            return $stmt->rowCount();
        } catch (\PDOException $e) {
            MyException::setError($e->getMessage(), 'numRows');
            return -1;
        }
    }

    public static function count($sql,$arg){
        try {
            $con = self::getCon();
            $stmt = $con->prepare($sql);
            $stmt->execute($arg);
            return $stmt->fetchColumn();
        } catch (\PDOException $e) {
            MyException::setError($e->getMessage(), 'count');
            return -1;
        }
    }
}
