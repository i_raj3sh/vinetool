<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 11:45 AM
 */

if (!$argc)
    die('invalid');

require dirname(__DIR__) . '/lib/include.php';

ini_set('display_errors', 1);
error_reporting(E_ERROR);

use controller\Async;
use helper\Cache;
use helper\Mysql;
use helper\Template;
use model\Sale;
use model\Visit;
use MongoDB\BSON\ObjectId;

$con = Mysql::getCon();
const BUFFER_FILE = 'buffer_log.sql';
$buffer = '';
clearBuffer();

function clearBuffer()
{
    global $buffer;
    $buffer = '';
//    file_put_contents(BUFFER_FILE, '');
}

function writeBuffer(array $data, int $i = 0): void
{
    global $buffer;
    $buffer .= implode(',', $data) . PHP_EOL;
    if ($i % 10 === 0) {
        file_put_contents(BUFFER_FILE, $buffer, FILE_APPEND);
        $buffer = '';
    }
}

const HOURLY_SQL = 'INSERT INTO vine_ad_stats.hourly_temp VALUES ';
function writeSql(array $data): void
{
    global $buffer;
    $buffer .= '("' . implode('","', $data) . '"),';
}

function groupBy($sql)
{
    $c = Mysql::getCon();
    $s = $c->query($sql);
    $result = $s->fetchAll(PDO::FETCH_ASSOC);

    foreach ($result as $item) {
        $item['ID'] = 0;
        writeSql($item);
    }
}

$id = file_get_contents(INDEX . 'click');
$sid = file_get_contents(INDEX . 'sale');

$dayClick = date('YmdH', strtotime('-5 minutes'));
$visitObj = new Visit($dayClick);
$saleObj = new Sale();

$db = new \helper\Mongo([
    'db' => 'db_dump',
    'server' => 'traffic',
    'timeout' => 900000,
    'authentication' => 'db_dump'
]);

$last = $db->query($visitObj, [], ['projection' => ['_id' => 1], 'sort' => ['_id' => -1], 'limit' => 1])->toArray();
if (empty($last)){
    die('Nothing');
}

Mysql::setDbName(Mysql::STATS);

$last = $last[0]->{'_id'}->__toString();
$lastSID = Mysql::select('SELECT ID FROM sale ORDER BY ID DESC LIMIT 1',(new Sale())->getMyClass(),[])[0]->ID;

if($id == 0){
    $match = [ '$lte' => new ObjectId($last) ];
}else{
    $match = [ '$gt' => new ObjectId($id), '$lte' => new ObjectId($last) ];
}

$visit = [
    [
        '$match' => [
            '_id' => $match
        ]
    ],
    [
        '$group' => [
            '_id' => [
                'affiliateID' => '$affiliateID',
                'offerID' => '$offerID',
                'hour' => '$hourClick',
                'country' => '$country',
                'device' => '$device',
                'drop' => '$drop'
            ],
            'clicks' => ['$sum' => 1],
            'affiliate' => ['$first' => '$affiliate'],
            'affiliateID' => ['$first' => '$affiliateID'],
            'campaign' => ['$first' => '$offer'],
            'offerID' => ['$first' => '$offerID'],
            'network' => ['$first' => '$network'],
            'networkID' => ['$first' => '$networkID'],
            'adminID' => ['$first' => '$adminID'],
            'hour' => ['$first' => '$hourClick'],
            'day' => ['$first' => '$dayClick'],
            'country' => ['$first' => '$country'],
            'device' => ['$first' => '$device'],
            'drop' => ['$first' => '$drop']
        ]
    ],
    [
        '$sort' => [ 'hour' => 1 ]
    ]
];

$visits = $db->aggregate($visitObj, $visit);

$j = false;

foreach ($visits as $k => $item) {

    $params = [
        'ID' => 0,
        'day_' => $item->day,
        'hour_' => (int)substr($item->hour, -2, 2),
        'month_' => (int)substr($item->day, 0, 6),
        'clicks' => 0,
        'dropped' => 0,
        'sales' => 0,
        'charge' => 0,
        'payout' => 0,
        'admin_ID' => $item->adminID,
        'offers_ID' => $item->offerID,
        'affiliate_ID' => $item->affiliateID,
        'network_ID' => $item->networkID,
        'campaign' => $item->campaign,
        'affiliate' => $item->affiliate,
        'network' => $item->network,
        'device' => $item->device,
        'country' => $item->country
    ];
    if ($item->drop) {
        $params['dropped'] = $item->clicks;
    } else {
        $params['clicks'] = $item->clicks;
    }

//    writeBuffer($params, $k);
    writeSql($params);
    $j = true;
}

if ($j) {
    file_put_contents(INDEX . 'click', $last);
//    $con->exec('LOAD DATA INFILE \'/web/server/'.BUFFER_FILE.'\' INTO TABLE vine_ad_stats.hourly_temp;');
    $sqlite = HOURLY_SQL . rtrim($buffer, ',');
    $con->exec($sqlite);
    clearBuffer();
} else {
    die;
}
/*
 * SALE HOURLY
 */
$sql = '
  SELECT offers_ID OID, affiliate_ID AID,network_ID NID,admin_ID MID,offer,affiliate,network,day_sale,hour_sale,device,country,SUM(charge) pull,SUM(payout) push,COUNT(ID) sales
  FROM vine_ad_stats.sale
  WHERE ID > :i AND ID <= :u AND affiliate_ID != 20 GROUP BY AID,OID,hour_sale
';

$args = [':i' => $sid,':u' => $lastSID];

$sales = Mysql::select($sql,(new Sale())->getMyClass(),$args);

$r = false;

foreach ($sales as $item){

    $params = [
        'ID' => 0,
        'day_' => $item->day_sale,
        'hour_' => (int)substr($item->hour_sale, -2, 2),
        'month_' => (int)substr($item->day_sale, 0, 6),
        'clicks' => 0,
        'dropped' => 0,
        'sales' => $item->sales,
        'charge' => $item->pull,
        'payout' => $item->push,
        'admin_ID' => $item->MID,
        'offers_ID' => $item->OID,
        'affiliate_ID' => $item->AID,
        'network_ID' => $item->NID,
        'campaign' => $item->offer,
        'affiliate' => $item->affiliate,
        'network' => $item->network,
        'device' => $item->device,
        'country' => $item->country
    ];

    writeSql($params);

    $r = true;
}

if ($r){
    file_put_contents(INDEX . 'sale', $lastSID);
    $sqlite = HOURLY_SQL . rtrim($buffer, ',');
    $con->exec($sqlite);
    clearBuffer();
}

groupBy('SELECT ID, day_, hour_, month_, SUM(clicks) clicks, SUM(dropped) dropped, SUM(sales) sales, SUM(charge) charge, SUM(payout) payout, admin_ID, offers_ID, affiliate_ID, network_ID, campaign, affiliate, network, device, country FROM vine_ad_stats.hourly_temp GROUP BY day_ , hour_, offers_ID, affiliate_ID, country, device');

$sqlite = 'INSERT INTO vine_ad_stats.stats_grouped_hourly VALUES ' . rtrim($buffer, ',');
$con->exec($sqlite);
clearBuffer();
$con->exec('TRUNCATE TABLE vine_ad_stats.hourly_temp');
/*
 * Stats By Source
 */

$visit = [
    [
        '$match' => [
            '_id' => $match,
            'drop' => false
        ]
    ],
    [
        '$group' => [
            '_id' => [
                'source' => '$source',
                'affiliateID' => '$affiliateID',
                'offerID' => '$offerID',
                'hour' => '$hourClick',
                'country' => '$country',
                'device' => '$device'
            ],
            'clicks' => ['$sum' => 1],
            'affiliate' => ['$first' => '$affiliate'],
            'affiliateID' => ['$first' => '$affiliateID'],
            'campaign' => ['$first' => '$offer'],
            'offerID' => ['$first' => '$offerID'],
            'network' => ['$first' => '$network'],
            'networkID' => ['$first' => '$networkID'],
            'adminID' => ['$first' => '$adminID'],
            'hour' => ['$first' => '$hourClick'],
            'day' => ['$first' => '$dayClick'],
            'country' => ['$first' => '$country'],
            'device' => ['$first' => '$device'],
            'source' => ['$first' => '$source']
        ]
    ]
];

$stats = $db->aggregate($visitObj, $visit);

$r = false;
foreach ($stats as $item) {

    $params = [
        'ID' => 0,
        'day_' => $item->day,
        'month_' => (int)substr($item->day, 0, 6),
        'clicks' => $item->clicks,
        'sales' => 0,
        'charge' => 0,
        'payout' => 0,
        'offers_ID' => $item->offerID,
        'affiliate_ID' => $item->affiliateID,
        'network_ID' => $item->networkID,
        'campaign' => $item->campaign,
        'affiliate' => $item->affiliate,
        'network' => $item->network,
        'source' => $item->source,
        'device' => $item->device,
        'country' => $item->country
    ];

    writeSql($params);
    $r = true;
}

if ($r) {
    $sqlite = 'INSERT INTO vine_ad_stats.source_temp VALUES ' . rtrim($buffer, ',');
    $con->exec($sqlite);
    clearBuffer();
}

$sql = '
  SELECT OID,AID,NID,offer,affiliate,network,day_sale,device,country,SUM(cs) pull,SUM(ps) push,SUM(ss) sales,source 
  FROM ( 
  SELECT offers_ID OID, affiliate_ID AID,network_ID NID,offer,affiliate,network,day_sale,device,country,SUM(charge) cs,SUM(payout) ps,COUNT(ID) ss,source 
  FROM vine_ad_stats.sale 
  WHERE ID > :i AND ID <= :u AND affiliate_ID != 20 
  GROUP BY hour_sale,source,OID,AID
  ) agg 
  GROUP BY source,OID,AID
';

$args = [':i' => $sid,':u' => $lastSID];

$sales = Mysql::select($sql,(new Sale())->getMyClass(),$args);

$r = false;
foreach ($sales as $item) {

        $params = [
            'ID' => 0,
            'day_' => $item->day_sale,
            'month_' => (int)substr($item->day_sale, 0, 6),
            'clicks' => 0,
            'sales' => $item->sales,
            'charge' => $item->pull,
            'payout' => $item->push,
            'offers_ID' => $item->OID,
            'affiliate_ID' => $item->AID,
            'network_ID' => $item->NID,
            'campaign' => $item->offer,
            'affiliate' => $item->affiliate,
            'network' => $item->network,
            'source' => $item->source,
            'device' => $item->device,
            'country' => $item->country,
        ];

    writeSql($params);
    $r = true;
}

if ($r) {
    $sqlite = 'INSERT INTO vine_ad_stats.source_temp VALUES ' . rtrim($buffer, ',');
    $con->exec($sqlite);
    clearBuffer();
}

groupBy('SELECT ID, day_, month_, SUM(clicks) clicks, SUM(sales) sales, SUM(charge) charge, SUM(payout) payout, offers_ID, affiliate_ID, network_ID, campaign, affiliate, network, source, device, country FROM vine_ad_stats.source_temp GROUP BY day_ , source, offers_ID, affiliate_ID, country, device');

$sqlite = 'INSERT INTO vine_ad_stats.stats_grouped_source VALUES ' . rtrim($buffer, ',');
$con->exec($sqlite);
clearBuffer();
$con->exec('TRUNCATE TABLE vine_ad_stats.source_temp');

$cObj = new Cache();
$cObj->clearCache();
Template::generateSideBarCounts('ADMIN');
Async::widget();
Async::revenue();
Async::scrubbed();
