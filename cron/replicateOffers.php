<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 14/03/18
 * Time: 10:50 PM
 */

use controller\Offers as O;
use helper\Mysql;

require dirname(__DIR__).'/lib/include.php';

ini_set('memory_limit', '1024M');
set_time_limit(0);

$sql = 'SELECT * FROM `offers`';

$param = [];
$offerObj = new \model\Offers();

$offers = Mysql::select($sql,$offerObj->getMyClass(),$param);

$toInsert = [];

foreach ($offers as $k=>$offer){

    $sale = '';
    if ($offer->caps > 0) {
        $sale = $offer->caps - $offer->count_daily;
        $sale = $sale > $offer->caps ? $offer->caps . ' / ' : $sale . ' / ';
    }

    $arr = [];
    $arr['ID'] = (int)$offer->ID;
    $arr['name'] = $offer->name;
    $arr['charge'] = (float)$offer->charge;
    $arr['date'] = $offer->date;
    $arr['caps'] = $offer->caps;
    $arr['sale'] = $sale;
    $arr['network_ID'] = $offer->network_ID;
    $arr['type'] = $offer->type;
    $arr['preview'] = $offer->preview;
    $arr['app_id'] = $offer->app_id;
    $arr['min_os_ver'] = $offer->min_os_ver;
    $arr['device_id'] = $offer->device_id;
    $arr['tracking'] = $offer->tracking;
    $arr['icon'] = $offer->icon;
    $arr['active'] = $offer->active;
    $arr['private'] = $offer->private;
    $arr['advID'] = $offer->advID;
    $arr['geo'] = json_decode($offer->country);
    $arr['cat'] = json_decode($offer->category);
    $arr['dev'] = json_decode($offer->device);
    $arr['assigned'] = O::assigned($offer->ID);
    $toInsert[] = $arr;
}

$db = new \helper\Mongo();
$db->delete($offerObj);

$db->insert($offerObj, $toInsert);

//\helper\Message::display_error();
//_r($toInsert);
