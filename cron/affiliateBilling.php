<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 9/2/18
 * Time: 5:39 PM
 */

use helper\Mysql;
use model\BillingAffiliate;
use model\HourlyStats;

if (!$argc)
    die('Invalid');

require dirname(__DIR__).'/lib/include.php';

$month = date('Ym', strtotime('-1 month'));

Mysql::setDbName(Mysql::STATS);
$gh = new HourlyStats();
$gh->setProperties([
    'month_' => $month
]);
$gh->setQueryParameters($gh,['*','SUM(sales) conver','SUM(payout) push'], '', 'GROUP BY month_,affiliate_ID,offers_ID HAVING push > 0', 'ORDER BY push DESC');

$result = $gh->query();

foreach ($result as $item){

    $ab = new BillingAffiliate();
    $ab->setProperties([
        'affiliate_ID' => $item->affiliate_ID,
        'affiliate' => $item->affiliate,
        'offers_ID' => $item->offers_ID,
        'offer' => $item->campaign,
        'network_ID' => $item->network_ID,
        'network' => $item->network,
        'month' => $month,
        'sales' => $item->conver,
        'payout' => $item->push
    ])
        ->insert($ab);

}

Mysql::setDbName();