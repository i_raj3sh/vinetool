<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 4:19 PM
 */


if (!$argc){
    die('Invalid');
}

require dirname(__DIR__).'/lib/include.php';

$date = date('YmdHis');

$args = ['active=?'=>0,'cron=?'=>1];

\helper\Mysql::put('update','offers',$args,"WHERE expiry < $date and expiry != 0");


$args = ['active=?'=>1,'cron=?'=>0];

\helper\Mysql::put('update','offers',$args, 'WHERE expiry = 0 and cron = 1');