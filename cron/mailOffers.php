<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/17/17
 * Time: 5:08 PM
 */

require dirname(__DIR__).'/lib/include.php';

$id = file_get_contents(INDEX.'mail');

$o = new \model\Offers();
$o->setProperties([
    'active' => 1
])
->setQueryParameters($o,['offers.ID','offers.name','offers.charge','offers.type','offers.icon','offers.caps'],'','','ORDER BY ID DESC','LIMIT 50');
$o->appendQuery('ID > '.$id);

$of = $o->query();

if (count($of) > 4){

    file_put_contents(INDEX.'mail', $of[0]->ID);

    $a = new \model\Affiliate();
    $a->setProperties([
        'active' => 1
    ])->setQueryParameters($a);

    $aa = $a->query();

    foreach ($aa as $item){

        $m = new \helper\Mailer();
        $m->setHeaders()
            ->setMessage('affiliateOffers',[
                'name' => $item->name,
                'offers' => $of
            ])
            ->send($item->email,'Today\'s New Offers');
    }
}