<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 7:26 PM
 */


use helper\Cache;
use helper\Mysql;
use model\AffiliateMonthly;
use model\HourlyStats;
use model\NetworkMonthly;

if (!$argc)
    die('Invalid');

require dirname(__DIR__).'/lib/include.php';

$date = date('YmdHis');
$dateM = date('Ym');
$month = date('M');
$year = date('Y');

$nm = new NetworkMonthly();
$am = new AffiliateMonthly();

Mysql::setDbName(Mysql::STATS);

$h = new HourlyStats();
$h->setProperties([
    'month_' => $dateM
])
    ->setQueryParameters($h,['network_ID','network','SUM(clicks) visits','SUM(sales) converts','SUM(charge) pull'],'','GROUP BY network_ID');
$get = $h->query();

foreach ($get as $item){
    $nm = new NetworkMonthly();
    $nm->setProperties([
        'network_ID' => $item->network_ID,
        'date_month' => $dateM
    ]);
    $nm->setQueryParameters($nm,['ID']);
    $nmID = $nm->one()->ID;

    $nm = new NetworkMonthly();
    $param = [
        'date' => $date,
        'date_month' => $dateM,
        'month' => $month,
        'year_' => $year,
        'visits' => $item->visits,
        'sales' => $item->converts,
        'amount' => $item->pull,
        'network_ID' => $item->network_ID,
        'network' => $item->network
    ];
    if ($nmID){
        $param['ID'] = $nmID;
    }
    $nm->setProperties($param);
    $nm->save($nm);
}

$h = new HourlyStats();
$h->setProperties([
    'month_' => $dateM
])
    ->setQueryParameters($h,['affiliate_ID','affiliate','SUM(clicks) visits','SUM(sales) converts','SUM(payout) push'],'','GROUP BY affiliate_ID');
$put = $h->query();

foreach ($put as $item){
    $am = new AffiliateMonthly();
    $am->setProperties([
        'affiliate_ID' => $item->affiliate_ID,
        'date_month' => $dateM
    ]);
    $am->setQueryParameters($am,['ID']);
    $amID = $am->one()->ID;

    $am = new AffiliateMonthly();
    $param = [
        'date' => $date,
        'date_month' => $dateM,
        'month' => $month,
        'year_' => $year,
        'visits' => $item->visits,
        'sales' => $item->converts,
        'amount' => $item->push,
        'affiliate_ID' => $item->affiliate_ID,
        'affiliate' => $item->affiliate
    ];
    if ($amID){
        $param['ID'] = $amID;
    }
    $am->setProperties($param);
    $am->save($am);
}

$cObj = new Cache(Cache::STORE);
$cObj->clearCache();