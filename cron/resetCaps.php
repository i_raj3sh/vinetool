<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/27/17
 * Time: 8:05 PM
 */

if (!$argc){
    die('Invalid');
}

require dirname(__DIR__).'/lib/include.php';

$of = new \model\Offers();
$of->setQueryParameters($of,['ID','caps','monthly_cap','cron','active']);
$of->appendQuery('(monthly_cap > 0 and count_monthly != monthly_cap) or (caps > 0 and count_daily != caps)');

$get = $of->query();

echo date('Y-m-d H:i:s') . "\n";

foreach ($get as $item):

    $param['ID'] = $item->ID;
    $param['count_daily'] = $item->caps;

    echo "$item->ID | $item->caps | ";

    if (date('j') == 1){
        $param['count_monthly'] = $item->monthly_cap;
    }
    if($item->active == 2){
        $param['active'] = 2;
        $param['cron'] = $item->cron;

        echo "A2 | $item->cron | ";
    }
    elseif ($item->cron == 0){
        $param['active'] = 1;
        $param['cron'] = 0;
        echo "A1 | 1 | ";
    }
    elseif ($item->cron == 2){
        $param['active'] = 0;
        $param['cron'] = 1;

        echo "A0 | 1 | ";
    }

    if ($item->active == 3) {
        $param['active'] = 1;

        echo "A3 | 1 | ";
    }


    $o = new \model\Offers();
    $o->setProperties($param);
    $o->update($o);

    print_r($param);

    echo "\n";

endforeach;