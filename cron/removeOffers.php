<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/25/17
 * Time: 8:42 PM
 */

use helper\Mysql;

if (!$argc){
    die('Invalid');
}

require dirname(__DIR__).'/lib/include.php';


$date = date('YmdHm',strtotime('-30 day'));

$sql_offer = "DELETE from offers where last_updated < '$date' and ID not in (select offers_ID from affiliate_offers)";

$sql_cat = 'DELETE from offers_category where offers_ID not in (select ID from offers)';
$sql_geo = 'DELETE from offers_country where offers_ID not in (select ID from offers)';
$sql_dev = 'DELETE from offers_device where offers_ID not in (select ID from offers)';

Mysql::delete($sql_offer);

Mysql::delete($sql_cat);
Mysql::delete($sql_geo);
Mysql::delete($sql_dev);