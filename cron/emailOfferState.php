<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/25/17
 * Time: 2:19 PM
 */

require dirname(__DIR__).'/lib/include.php';

$ao = new \model\ApproveOffers();
$o = new \model\Offers();
$a = new \model\Affiliate();

$ao->setProperties([
    'traffic' => 1,
    'status' => 1,
    'offers.active' => 0
])
    ->setQueryParameters($ao,['group_concat(affiliate_offers.ID) AID']);

$ao->setQueryParameters($a,['affiliate.name','affiliate.email']);

$ao->setQueryParameters($o,['group_concat(offers.ID) OID','group_concat(offers.name) title'],'','GROUP BY affiliate_ID');

$getInActive = $ao->query();

foreach ($getInActive as $item){

    $AID = explode(',',$item->AID);
    foreach ($AID as $v) {
        $afo = new \model\ApproveOffers();
        $afo->setProperties([
            'ID' => $v,
            'traffic' => 0
        ]);
        $afo->update($afo);
    }

    $m = new \helper\Mailer();
    $m->setHeaders()
        ->setMessage('emailOfferPaused',[
            'name' => $item->name,
            'title' => explode(',',$item->title),
            'OID' => explode(',',$item->OID)
        ])
        ->send($item->email,'Offer Paused Alert');
}



$ao = new \model\ApproveOffers();
$o = new \model\Offers();
$a = new \model\Affiliate();

$ao->setProperties([
    'traffic' => 0,
    'status' => 1,
    'offers.active' => 1
])
    ->setQueryParameters($ao,['group_concat(affiliate_offers.ID) AID']);

$ao->setQueryParameters($a,['affiliate.name','affiliate.email']);

$ao->setQueryParameters($o,['group_concat(offers.ID) OID','group_concat(offers.name) title'],'','GROUP BY affiliate_ID');

$getActive = $ao->query();

foreach ($getActive as $item){

    $AID = explode(',',$item->AID);
    foreach ($AID as $v) {
        $afo = new \model\ApproveOffers();
        $afo->setProperties([
            'ID' => $v,
            'traffic' => 1
        ]);
        $afo->update($afo);
    }

    $m = new \helper\Mailer();
    $m->setHeaders()
        ->setMessage('emailOfferActive',[
            'name' => $item->name,
            'title' => explode(',',$item->title),
            'OID' => explode(',',$item->OID)
        ])
    ->send($item->email,'Offer Re-Activated Alert');
}


