<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 10/2/18
 * Time: 12:09 PM
 */

if (!$argc){
    die('Invalid');
}

require dirname(__DIR__).'/lib/include.php';

$ao = new \model\ApproveOffers();
$o = new \model\Offers();

$ao->setProperties([
   'api' => 1,
   'status' => 0
]);
$ao->setQueryParameters($ao,['affiliate_offers.ID AOID']);
$ao->setQueryParameters($o,['tracking']);
$get = $ao->query();

foreach ($get as $v):

    if ($v->tracking !== ''){
        $aaoo = new \model\ApproveOffers();
        $aaoo->setProperties([
            'ID' => $v->AOID,
            'status' => 1
        ]);
        $aaoo->update($aaoo);
    }

endforeach;