<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 10/18/18
 * Time: 4:29 PM
 */

use helper\Mysql as M;

require_once dirname(__DIR__) . '/lib/include.php';

$month = date('Ym', strtotime('-1 month'));

$sql = "SELECT SUM(sales) s, SUM(payout) p, SUM(deduction) d, affiliate_ID af, affiliate email, month
FROM vine_ad_stats.billing_affiliate 
WHERE status = 1 AND affiliate_ID != 11 AND month = $month
GROUP BY affiliate_ID,offers_ID
HAVING s > 0";

M::setDbName(M::STATS);

$results = M::select($sql, (new \model\BillingAffiliate())->getMyClass());

$temp = $email = '';
foreach ($results as $o) {

    if ($temp === '') {
        $total = 0;
        $temp = $o->af;
        $email = $o->email;
    }

    if ($temp === $o->af) {
        $one = $o->p / $o->s;
        $final = $o->s - $o->d;
        $total += $one * $final;

    } else {

        if ($total > 0) {
            $ia = new \model\InvoiceAffiliate();
            $ia->setProperties([
                'affiliate_ID' => $temp,
                'affiliate' => $email,
                'month' => $month,
                'amount' => $total,
                'at' => time()
            ])
                ->insert($ia);
        }

        $temp = $o->af;
        $email = $o->email;
        $one = $o->p / $o->s;
        $final = $o->s - $o->d;
        $total = $one * $final;
    }
}


if ($total > 0 && count($results)) {
    $ia = new \model\InvoiceAffiliate();
    $ia->setProperties([
        'affiliate_ID' => $temp,
        'affiliate' => $email,
        'month' => $month,
        'amount' => $total,
        'at' => time()
    ])
        ->insert($ia);
}