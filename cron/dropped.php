<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 11:45 AM
 */

use helper\Mysql;
use model\Dropped;
use model\DroppedGroup;
use model\EventGroup;
use model\Sale;
use model\Scrubbed;
use model\ScrubbedGroup;

if (!$argc)
    die('invalid');

require dirname(__DIR__) . '/lib/include.php';

ini_set('display_errors', 1);
error_reporting(E_ERROR);

$today = date('Ymd', strtotime('-5 minutes'));

Mysql::setDbName(Mysql::STATS);

$gh = new DroppedGroup();
$gh->remove("day = $today", true);

$gh = new Dropped();
$gh->setProperties(['day_sale' => $today]);
$gh->setQueryParameters($gh, ['*', 'COUNT(ID) sale', 'SUM(charge) pull'], '', 'GROUP BY affiliate_ID, offers_ID', 'ORDER BY pull DESC');

$how = $gh->query();

foreach ($how as $item) {

    $params = [
        'day' => $item->day_sale,
        'month' => substr($item->day_sale, 0, 6),
        'offers_ID' => $item->offers_ID,
        'affiliate_ID' => $item->affiliate_ID,
        'network_ID' => $item->network_ID,
        'offers' => $item->offer,
        'affiliate' => $item->affiliate,
        'network' => $item->network,
        'sale' => $item->sale,
        'payout' => $item->pull
    ];

    $gh = new DroppedGroup();
    $gh->setProperties($params);
    $gh->insert($gh);
}
#
#
#
#
### Events grouping from sales ###

$gh = new EventGroup();
$gh->remove("day = $today", true);

$gh = new Sale();
$gh->setProperties(['day_sale' => $today]);
$gh->setQueryParameters($gh, ['*', 'COUNT(ID) sale', 'SUM(charge) pull', 'SUM(payout) push'], '', 'GROUP BY event, affiliate_ID, offers_ID', 'ORDER BY pull DESC');

$how = $gh->query();

foreach ($how as $item) {

    $params = [
        'day' => $item->day_sale,
        'month' => substr($item->day_sale, 0, 6),
        'offers_ID' => $item->offers_ID,
        'affiliate_ID' => $item->affiliate_ID,
        'network_ID' => $item->network_ID,
        'offers' => $item->offer,
        'affiliate' => $item->affiliate,
        'network' => $item->network,
        'event' => $item->event,
        'sale' => $item->sale,
        'charge' => $item->pull,
        'payout' => $item->push
    ];

    $gh = new EventGroup();
    $gh->setProperties($params);
    $gh->insert($gh);
}

#
#
#
#
### Scrubbed
$gh = new ScrubbedGroup();
$gh->remove("day = $today", true);

$gh = new Scrubbed();
$gh->setProperties(['day_sale' => $today]);
$gh->setQueryParameters($gh, ['*', 'COUNT(ID) sale', 'SUM(charge) pull'], '', 'GROUP BY affiliate_ID, offers_ID', 'ORDER BY pull DESC');

$how = $gh->query();

foreach ($how as $item) {

    $params = [
        'day' => $item->day_sale,
        'month' => substr($item->day_sale, 0, 6),
        'offers_ID' => $item->offers_ID,
        'affiliate_ID' => $item->affiliate_ID,
        'network_ID' => $item->network_ID,
        'offers' => $item->offer,
        'affiliate' => $item->affiliate,
        'network' => $item->network,
        'sale' => $item->sale,
        'payout' => $item->pull
    ];

    $gh = new ScrubbedGroup();
    $gh->setProperties($params);
    $gh->insert($gh);
}