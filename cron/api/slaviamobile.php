<?php
/**
 * Created by PhpStorm.
 * User: tiwari_ut
 * Date: 10/5/19
 * Time: 5:45 PM
 */

use helper\Tool;

require dirname(__DIR__) . '/../lib/include.php';
require_once 'AdvApi.php';

$url = 'http://api.slaviamobile.com/affiliate/offer/findAll/?token=x0xmBfIfWV1V1muW3SaAPTFh1sVzJzFU';
$network = 106;
$date = date('Ymd');

$api = new AdvApi($url, $network);

$data = $api->pull();

if ($data->success !== true) {
    die("API Error");
}

$data = $data->offers;

foreach ($data as $v) {

    if ($v->Tracking_url === null || $v->Status !== 'active') {
        continue;
    }

    $params = [
        'name' => $v->Name,
        'description' => $v->Description,
        'charge' => $v->Payout,
        'caps' => $v->Daily_cap,
        'monthly_cap' => $v->Monthly_cap
    ];

    if ($api->notExist($v->ID)) {

        if (strpos($v->Name, "CPA")) {
            $type = AdvApi::CPA;
        } else {
            $type = AdvApi::CPI;
        }

        $appId = ltrim($v->APP_ID, 'id');

        $device = '';
        $minOS = 0;
        $ifa = '';
        $device_id = infiltrate($v->Name);
        if (strpos($v->Platforms, "iPhone") !== false || strpos($v->Platforms, "iPad") !== false) {
            $device = '["iOS"]';
            $ifa = '{IDFA}';
            if (strpos($v->Name, "+")) {
                $minOS = get_string_between($v->Name, "(iPhone ", "+");
            }
        } else {
            $device = '["Android"]';
            $ifa = '{GAID}';
            if (strpos($v->Name, "+")) {
                $minOS = get_string_between($v->Name, "(Android ", "+");
            }
        }

        $tracking = $v->Tracking_url . '&aff_sub={CLICK}&aff_sub2={PUB}_{PUB2}&idfa=' . $ifa;

//        $category = explode(",", $v->Tags);

        if ($device_id === 0){
            $device_id = infiltrate($v->Original_name);
        }
        if ($device_id === 0){
            $device_id = infiltrate($v->Description);
        }


        $params['type'] = $type;
        $params['preview'] = $v->Preview_url;
        $params['app_id'] = $appId;
        $params['min_os_ver'] = $minOS;
        $params['device_id'] = $device_id;
        $params['joined'] = date('YmdHi');
        $params['icon'] = $v->Icon_url;
        $params['tracking'] = $tracking;
        $params['incent'] = $v->Type === 'Non incent' ? 0 : 1;
        $params['device'] = $device;
        $params['country'] = $api->makeJson($v->Countries, true);
        $params['category'] = '["General"]';
        $params['expiry'] = preg_replace("/[^a-zA-Z0-9]/", "", $v->Expiration_date);

        $api->addNew($params);

    } else {
        $api->updateExist($params);
        echo "<hr>";
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));

function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

function infiltrate($t)
{
    if (false !== stripos($t, 'GAID') || false !== stripos($t, 'IDFA')) {
        return 1;
    }
    return 0;
}
