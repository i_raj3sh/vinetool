<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/1/18
 * Time: 6:21 PM
 */
die;
use helper\Mysql;
use helper\Tool;

require dirname(__DIR__).'/../lib/include.php';

$url = 'https://www.mybestclick.mobi/api.php?method=getOffers&aid=1301&key=cbd7f6a2770724212ad83169d862fbf14222f380&images=true';
$network = 5;
$date = date('Ymd');

$getData = Tool::hitCurl($url);

if ($getData === false)
    die('Failed'.__CLASS__.'');

$getData = json_decode($getData);
if ($getData->error_code === 0){
    echo 'reached'.BR;
    $offers = $getData->offers;
    foreach ($offers as $v):
        $k = $v->id;
        echo $k.BR;

        if (in_array('Adult',$v->type_traffic)){
            continue;
        }

        $oo = new \model\Offers();
        $oo->setProperties([
            'advID' => $k,
            'network_ID' => $network
        ]);
        $oo->setQueryParameters($oo);
        echo ($oo->count()).BR;

        $replace = [
            '-',
            ' ',
            ':'
        ];
        $expiry = str_replace($replace, '', $v->expire);

        if (!$oo->count()){

            if($v->payout_type === 'Installation'){
                $type = 'CPI';
            }else{
                if (false !== strpos($v->name, 'CPL')){
                    $type = 'CPL';
                }else{
                    $type = 'CPA';
                }
            }

            $incent = 1;
            if (in_array('Non-Incentive',$v->type_traffic)){
                $incent = 0;
            }

            $tracking = $v->tracking_link.'&transaction_id={CLICK}';
            $oo->setProperties([
                'name' => $v->name,
                'description' => $v->description,
                'caps' => $v->daily_conversions,
                'count_daily' => $v->daily_conversions,
                'monthly_cap' => $v->monthly_conversions,
                'count_monthly' => $v->monthly_conversions,
                'charge' => $v->payout,
                'type' => $type,
                'active' => 1,
                'preview' => $v->preview,
                'icon' => $v->image_link,
                'tracking' => $tracking,
                'date' => date('YmdHis'),
                'joined' => $date,
                'expiry' => $expiry,
                'network_ID' => $network,
                'advID' => $k,
                'incent' => $incent,
                'last_updated' => $date
            ]);
            $ID = $oo->insert($oo);

            $gg = implode('","',$v->geo);
            $g = new \model\Country();
            $g->setQueryParameters($g);
            $g->appendQuery("code IN (\"$gg\")");
            $country = $g->query();

            if (in_array('iPad',$v->devices) || in_array('iPhone',$v->devices) || in_array('iPod',$v->devices)){
                $device = ['iOS'];
            }
            if (in_array('Android',$v->devices)){
                $device = ['Android'];
            }
            if(empty($v->devices)){
                $device = ['All'];
            }
            $dd = implode('","',$device);
            $d = new \model\Device();
            $d->setQueryParameters($d);
            $d->appendQuery("name IN (\"$dd\")");
            $device = $d->query();

            foreach ($country as $item) {
                Mysql::put('insert', 'offers_country', ['offers_ID=?' => $ID, 'country_ID=?' => $item->ID]);
            }
            foreach ($device as $item) {
                Mysql::put('insert', 'offers_device', ['offers_ID=?' => $ID, 'device_ID=?' => $item->ID]);
            }
            Mysql::put('insert', 'offers_category', ['offers_ID=?' => $ID, 'category_ID=?' => 393]);
        }else {
            $o = new \model\Offers();
            $o->setProperties([
                'active' => 1,
                'cron' => 0,
                'charge' => $v->payout,
                'last_updated' => $date,
                'expiry' => $expiry
            ]);
            $o->update($o, "WHERE advID = '$k' and network_ID = $network and cron = 1 and active != 2");
        }
    endforeach;

    $o = new \model\Offers();
    $o->setProperties([
        'active' => 0,
        'cron' => 1
    ]);
    $o->update($o, "WHERE last_updated < $date and network_ID = $network");
}
