<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 01/03/18
 * Time: 9:39 PM
 */

use helper\Tool;
use model\Offers;

require dirname(__DIR__).'/../lib/include.php';

$time = time();
$key = md5("0AG6VFLQIBLSSWL4H3VE".md5($time));
$url = 'http://3s.mobvista.com/v4.php?m=index&cb=cb12984&time='.$time.'&token='.$key.'';

$network = 23;
$date = date('YmdHi');

$getData = Tool::hitCurl($url);

//header('Content-Type: application/json');
//echo $getData; die;

if ($getData === false)
    die('Failed'.__CLASS__.'');

$getData = json_decode($getData);
if ($getData->sucess && !empty($getData->offers)) {
    echo 'reached---';
    $offers = $getData->offers;

    foreach ($offers as $v):

        echo $v->campid.'->';
        $oo = new Offers();
        $oo->setProperties([
            'advID' => $v->campid,
            'network_ID' => $network
        ]);
        $oo->setQueryParameters($oo);
        $count = $oo->count();
        echo  $count.']|';

        $expiry = 0;

        $sub = '';
        if ($v->platform === 'android')
            $sub = '&mb_gaid={GAID}';
        elseif ($v->platform === 'ios')
            $sub = '&mb_idfa={IDFA}';

        $tracking = $v->tracking_link.'&click={CLICK}&mb_subid={PUB}'.$sub;

        if (!$count){

            $title = $v->offer_name.' ('.$v->app_name.') - '.$v->traffic_source;

            $incent = 1;
            if (false !== strpos($v->traffic_source, 'non-incent')){
                $incent = 0;
            }

            $adult = 0;

            $msg = 'IDFA/GAID is suggested to avail 10% more margin if sent in tracking link.';
            if ($v->gid_idfa_needs === 'yes'){
                $msg = 'IDFA/GAID is strictly required to run this offer. 10% margin is added by default.';
            }

            $description = "
            <h2>$v->app_name</h2>
            <p>$v->app_desc</p>
            <p></p>
            <h5>Note:</h5>
            $v->note
            <p>Traffic allowed: <strong>$v->traffic_network</strong></p>
            <p>Exclude Traffic: <strong>$v->exclude_traffic</strong></p>
            <p>Platform: <strong>$v->platform</strong> min:<strong>$v->min_version</strong> - max:<strong>$v->max_version</strong></p>
            <p></p>
            <h5>Notice:</h5>
            <p>$msg</p>
            ";

            if ($v->end_time){
                $expiry = date('YmdHis',$v->end_time);
            }

            $geo = Tool::sort($v->geo);

            if ($v->platform === 'android') {
                $dd = '["Android"]';
                $app_id = $v->package_name;
            } elseif ($v->platform === 'ios') {
                $dd = '["iOS"]';
                $app_id = ltrim($v->package_name, 'id');
            } else {
                $dd = '["All"]';
                $app_id = '';
            }

            $params = [
                'name' => $title,
                'description' => $description,
                'caps' => $v->daily_cap,
                'count_daily' => $v->daily_cap,
                'monthly_cap' => 0,
                'count_monthly' => 0,
                'charge' => (float)$v->price,
                'type' => strtoupper($v->price_model),
                'active' => $v->status === 'running'?1:0,
                'preview' => $v->preview_link,
                'app_id' => $app_id,
                'min_os_ver' => $v->min_version ?? 0,
                'icon' => $v->icon_link,
                'tracking' => $tracking,
                'date' => date('YmdHis'),
                'joined' => $date,
                'expiry' => $expiry,
                'network_ID' => $network,
                'advID' => $v->campid,
                'incent' => $incent,
                'adult' => $adult,
                'cron' => 1,
                'last_updated' => $date,
                'category' => '["General"]',
                'device' => $dd,
                'country' => json_encode($geo)
            ];
//            print_r($params); die;
            $oo->setProperties($params);
            $ID = $oo->insert($oo);

        }else {
            $o = new Offers();
            $o->setProperties([
                'active' => $v->status === 'running'?1:0,
                'cron' => 1,
                'charge' => (float)$v->price,
                'caps' => $v->daily_cap,
                'tracking' => $tracking,
                'last_updated' => $date
            ]);
            $o->update($o, "WHERE advID = '$v->campid' and network_ID = $network and cron = 1 and active != 2");
        }
    endforeach;

    $o = new Offers();
    $o->setProperties([
        'active' => 0,
        'cron' => 1
    ]);
    $o->update($o, "WHERE last_updated < $date and network_ID = $network and active != 2 and cron = 1");
}

if(!isset($argc)){
    echo "\n<br> Offers Updated";
    Tool::replicateOffers();
}
