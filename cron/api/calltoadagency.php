<?php
/**
 * Created by PhpStorm.
 * User: tiwari_ut
 * Date: 18/3/19
 * Time: 1:16 PM
 */

require dirname(__DIR__) . '/../lib/include.php';
require_once 'AdvApi.php';

$url = 'http://calltoadagency.offer18.com/api/af/offers?mid=1543&aid=27541&key=aa0c04287a2932d65126bafff1fdbbcc&page=0';
$network = 38;

$api = new AdvApi($url, $network);

$data = $api->pull();

if (!$data->response) {
    die("API Error");
}
$data = $data->data;

foreach ($data as $v) {
    if ($v->status !== 'active' && $v->model !== 'CPI') {
        continue;
    }

    $params = [
        'name' => $v->name,
        'description' => $v->offer_terms . $v->offer_kpi,
        'charge' => $v->price,
        'caps' => $v->capping_conversion ?? 0,
        'monthly_cap' => 0
    ];

    if ($api->notExist($v->offerid)) {

        if ($v->os_allow == 'ios') {
            $os = '["iOS"]';
            $ifa = '&iosidfa={IDFA}';
        } elseif ($v->os_allow == 'android') {
            $os = '["Android"]';
            $ifa = '&googleaid={GAID}';
        }

        $params['type'] = AdvApi::CPI;
        $params['preview'] = $v->preview_url;
        $params['app_id'] = getAppId($v->preview_url);
        $params['min_os_ver'] = 0;
        $params['device_id'] = 0;
        $params['icon'] = $v->logo;
        $params['tracking'] = tracking($v->click_url, $ifa);
        $params['incent'] = 0;
        $params['device'] = $os;
        $params['country'] = $api->makeJson($v->country_allow, true);
        $params['category'] = '["' . $v->category . '"]';
        $params['expiry'] = $v->date_end;

        $api->addNew($params);
    } else {
        $api->updateExist($params);
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));

function tracking($url, $ifa): string
{
    return $url . '&aff_sub1={CLICK}&aff_sub4={PUB}_{PUB2}' . $ifa;
}

function getAppId(string $preview): string
{
    if (false !== strpos($preview, 'https://play.google.com/store')) {
        $get = explode('id=', $preview)[1];
        return explode('&', $get)[0];
    }
    if (false !== strpos($preview, 'https://itunes.apple.com')) {
        $get = explode('/id', $preview)[1];
        return explode('?', $get)[0];
    }
    return '';
}
