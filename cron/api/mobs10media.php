<?php
/**
 * Created by PhpStorm.
 * User: tiwari_ut
 * Date: 8/7/19
 * Time: 12:05 PM
 */

use helper\Tool;

require dirname(__DIR__) . '/../lib/include.php';

require_once 'AdvApi.php';

$network = 115;
$link = 'http://mobs10.offer18.com/api/af/offers?mid=1298&aid=24905&key=85d36d3f688ef08fdd88d20f406fbf95&page=';

$api = new AdvApi($link, $network);

$pull = $api->pull();

if ($pull->response !== '200') {
    die('Failed'.__CLASS__.'');
}

$data = $pull->data;

process($data, $api);

for ($i = 0; $i <= 200; $i++) {

    $lnk = $link . ($i + 1);
    $api = new AdvApi($lnk, $network);
    $pull = $api->pull();
    if ($pull->response !== '200') {
        if ($pull->response == '400') {
            echo "\n page: " . $i . "\n";
            break;
        } else {
            die('Failed'.__CLASS__.'');
        }
    }
    echo "\n page: " . $i . "\n";

    $data = $pull->data;
    process($data, $api);
}

/**
 * @param $data
 * @param $api
 */
function process($data, AdvApi $api)
{
    foreach ($data as $v) {

        if ($v->status !== 'active') {
            continue;
        }

        $appId = Tool::getAppId($v->preview_url);
        if ($appId === '') {
            continue;
        }

        $params = [
            'name' => $v->name,
            'description' => $v->offer_terms . "<br>" . $v->offer_kpi,
            'charge' => $v->price,
            'caps' => $v->capping_conversion ?? 0,
            'monthly_cap' => 0,
        ];

        if ($api->notExist($v->offerid)) {

            $minOS = 0;
            if (strpos($v->name, "iOS")) {
                $dev = '["iOS"]';
                $ifa = '&iosidfa={IDFA}';
                if (strpos($v->name, "+")) {
                    $minOS = get_string_between($v->name, "(iPhone ", "+");
                }
            } else {
                $dev = '["Android"]';
                $ifa = '&googleaid={GAID}';
                if (strpos($v->name, "+")) {
                    $minOS = get_string_between($v->name, "(Android ", "+");
                }
            }

            $incent = 0;
            if (strpos($v->offer_terms, "Incent allowed")) {
                $incent = 1;
            }

            $device_id = 0;
            if ((false !== strpos($v->name, 'GAID')) || (false !== strpos($v->name, 'IDFA'))) {
                $device_id = 1;
            }

            $geos = explode(',', $v->country_allow);

            $tracking = $v->click_url . '&aff_sub1={CLICK}&aff_sub2={PUB}_{PUB2}' . $ifa;

            $params['type'] = $v->model;
            $params['preview'] = $v->preview_url;
            $params['app_id'] = $appId;
            $params['min_os_ver'] = $minOS;
            $params['joined'] = date('YmdHi');
            $params['device_id'] = $device_id;
            $params['icon'] = $v->logo;
            $params['tracking'] = $tracking;
            $params['incent'] = $incent;
            $params['device'] = $dev;
            $params['country'] = $api->makeJson($geos);
            $params['category'] = '["General"]';
            $params['expiry'] = preg_replace("/[^0-9]/", "", $v->date_end);

            $api->addNew($params);
        } else {
            $api->updateExist($params);
            echo "<hr>";
        }
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));

function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}
