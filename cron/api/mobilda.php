<?php
/**
 * Created by PhpStorm.
 * User: tiwari_ut
 * Date: 9/9/19
 * Time: 5:53 PM
 */

require dirname(__DIR__) . '/../lib/include.php';

require_once 'AdvApi.php';

$network = 173;
$link = 'http://s.marsfeeds.com/xml/cpa_feeds/feed.php?feed_id=935&hash=297fea2b3e9cb28c4563c71e72573f57&format=json';

$api = new AdvApi($link, $network);

$pull = $api->pull();

if (!$pull->request) {
    die('Failed'.__CLASS__.'');
}

for ($i = 1; $i <= $pull->summary->total_pages; $i++) {

    $lnk = $link . "&page=" . $i;
    $api = new AdvApi($lnk, $network);
    $pull = $api->pull();

    if (!$pull->request) {
        die('Failed'.__CLASS__.'');
    }

    echo "\n page: " . $i . "\n";

    $data = $pull->products;
    process($data, $api);
}

/**
 * @param $data
 * @param $api
 */
function process($data, AdvApi $api)
{
    foreach ($data as $v) {

        if ($v->attributes->status !== 'Live') {
            continue;
        }

        if ($v->attributes->package_name === '') {
            continue;
        }

        $params = [
            'name' => $v->attributes->title,
            'description' => $v->attributes->description . "<br><strong>KPI: </strong>" . $v->attributes->kpi,
            'charge' => $v->attributes->rate,
            'caps' => $v->capping->cap_amount ?? 0,
            'monthly_cap' => 0,
        ];

        if ($api->notExist($v->attributes->id)) {


            if (in_array("iPad", $v->mobile_attributes->allowed_devices) || in_array("iPhone", $v->mobile_attributes->allowed_devices) || strpos($v->attributes->preview_url, "apple")) {
                $dev = '["iOS"]';
                $ifa = '&p6={IDFA}';
            } else {
                $dev = '["Android"]';
                $ifa = '&p4={GAID}';
            }

            $incent = 1;
            if (strpos($v->attributes->kpi, "Non-Incent") || strpos($v->attributes->kpi, "No incent")) {
                $incent = 0;
            }

            $device_id = 0;
            if (strpos($v->attributes->kpi, 'GAID') || strpos($v->attributes->kpi, 'IDFA')) {
                $device_id = 1;
            }

            $tracking = $v->attributes->tracking_url . '&pub_id=13352&p1={CLICK}&p2={PUB}_{PUB2}&p3={APP}' . $ifa;

            $params['type'] = $v->attributes->business_model;
            $params['preview'] = $v->attributes->preview_url;
            $params['app_id'] = $v->attributes->package_name;
            $params['min_os_ver'] = sizeof($v->mobile_attributes->MinOs_version) ? $v->mobile_attributes->MinOs_version[0] : 0;
            $params['joined'] = date('YmdHi');
            $params['device_id'] = $device_id;
            $params['icon'] = $v->attributes->thumbnail;
            $params['tracking'] = $tracking;
            $params['incent'] = $incent;
            $params['device'] = $dev;
            $params['country'] = in_array("", $v->targeting->countries) || sizeof($v->targeting->countries) >= 45 ? '["All"]' : json_encode($v->targeting->countries);
            $params['category'] = json_encode($v->targeting->categories);

            $api->addNew($params);
        } else {
            $api->updateExist($params);
            echo "<hr>";
        }
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));
