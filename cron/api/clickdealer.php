<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/2/19
 * Time: 8:34 PM
 */

require dirname(__DIR__) . '/../lib/include.php';

require_once 'AdvApi.php';

$network = 0;
$link = 'https://partners.clickdealer.com/affiliates/api/1/offers.asmx/GetCampaign?api_key=076a4f9a458bb0891f7032785cc4da&affiliate_id=80844';//&platform=3,4,6,7&tracking_type=2&adult=2&ssl=true&row_limit=';

$api = new AdvApi($link, $network);

$pull = $api->pull();

if ($data->success) {
    die('API error');
}


foreach ($data as $v) {
    if ($v->status !== 'live') {
        continue;
    }
    $params = [
        'name' => $v->title . ' ' . $v->traffic_type,
        'description' => $v->description . ' <br/><br/>KPI: ' . $v->kpi . ' <br/>Deduction:' . $v->deduction . ' <br/>Ad Model:' . $v->ad_model . ' <br/> Category: ' . $v->cate_title,
        'charge' => $v->point,
        'caps' => $v->quantities_remaining_today ?? 0,
        'monthly_cap' => 0
    ];

    if ($api->notExist($v->camp_id)) {

        $type = AdvApi::CPI;
        if (false !== strpos($v->ad_model, AdvApi::CPA)) {
            $type = AdvApi::CPA;
        }
        $bundle = $v->package_name;
        if ($v->os === 'ios') {
            $os = '["iOS"]';
            $ifa = '&npt_idfa={IDFA}';
            $bundle = substr($bundle, 2);
        } elseif ($v->os === 'android') {
            $os = '["Android"]';
            $ifa = '&npt_google_aid={GAID}';
        }

        $params['type'] = $type;
        $params['preview'] = $v->preview;
        $params['app_id'] = $bundle;
        $params['min_os_ver'] = 0;
        $params['device_id'] = 0;
        $params['icon'] = $v->thumbnail;
        $params['tracking'] = tracking($v->link, $ifa);
        $params['incent'] = $v->traffic_type === 'non-incent' ? 0 : 1;
        $params['device'] = $os;
        $params['country'] = $api->makeJson($v->target_geo);
        $params['category'] = '["General"]';
        $params['expiry'] = 0;

        $api->addNew($params);
    } else {
        $api->updateExist($params);
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));

function tracking($url, $ifa): string
{
    return $url . '?ssp_click_id={CLICK}&npt_partner_id={PUB}_{PUB2}' . $ifa;
}