<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/4/18
 * Time: 12:12 PM
 */

use helper\Tool;

require dirname(__DIR__).'/../lib/include.php';

$url = 'http://api.calltoadagency.affise.com/2.1/partner/offers?API-Key=117075523cf5354cb2b8219c48c1d815d7a79de9';
$network = 38;
$date = date('YmdHi');

$getData = Tool::hitCurl($url);

if ($getData === false)
    die('Failed'.__CLASS__.'');

$getData = json_decode($getData);
if ($getData->status === 1 && !empty($getData->offers)){

    $offers = $getData->offers;

    processAll($offers, $network, $date);

    $total = $getData->pagination->total_count;
    $per = $getData->pagination->per_page;
    $pageT = (int)ceil($total/$per);

    for($pg=2;$pg<=$pageT;$pg++) {
        $urlN = $url.'&page='.$pg;
        $getNext = Tool::hitCurl($urlN);
        if ($getNext === false)
            continue;

        $getNext = json_decode($getNext);

        processAll($getNext->offers, $network, $date);
    }

    $o = new \model\Offers();
    $o->setProperties([
        'active' => 0,
        'cron' => 1
    ]);
    $o->update($o, "WHERE last_updated < $date and network_ID = $network and active != 2 and cron = 1");
}

function processAll($offers, $network, $date){
    foreach ($offers as $v):

        /*if ($v->required_approval)
            continue;*/

        echo $v->id.'->';
        $oo = new \model\Offers();
        $oo->setProperties([
            'advID' => $v->id,
            'network_ID' => $network
        ]);
        $oo->setQueryParameters($oo);
        $apo = $oo->count();

        echo $apo . "  ";

        $expiry = 0;

        if (!$apo){

            if($v->is_cpi){
                $type = 'CPI';
            }else{
                if (false !== strpos($v->title, 'CPL')){
                    $type = 'CPL';
                }elseif (false !== strpos($v->title, 'CPR')){
                    $type = 'CPR';
                }elseif (false !== strpos($v->title, 'CPS')){
                    $type = 'CPS';
                }else{
                    $type = 'CPA';
                }
            }

            $incent = 1;
            if (false !== stripos($v->title, 'incent') && false !== stripos($v->title, 'non')) {
                $incent = 0;
            }

            if ($v->payments[0]->revenue === 0) {
                echo ' - payout 0 ----- dropped' . BR;
                continue;
            }

            $adult = 0;

            if (in_array('Adult', $v->categories, true) || in_array('adult', $v->categories, true) || in_array('*adult', $v->categories, true)) {
                echo ' - adult ---- dropped ' . BR;
                $adult = 1;
                continue;
            }

            $dd = ['All'];
            $dm = 0;
            if ($v->strictly_os->items !== null){
                $dd = [];
                if ($v->strictly_os->items->Android !== null) {
                    $dd[] = 'Android';
                    $dm = Tool::getMinVer($v->strictly_os->items->Android, 'AFFISE');
                }
                if ($v->strictly_os->items->iOS !== null) {
                    $dd[] = 'iOS';
                    $dm = Tool::getMinVer($v->strictly_os->items->iOS, 'AFFISE');
                }
                if ($v->strictly_os->items->windows !== null)
                    $dd[] = 'Windows';
            }

            $dev = Tool::sort($dd);
            $geo = array_map('strtoupper', $v->countries);
            $geo = Tool::sort($geo);

            $tracking = $v->link . '&sub1={CLICK}&sub2={PUB}_{PUB2}&sub3={IDFA}&sub4={GAID}';
            $oo->setProperties([
                'name' => $v->title,
                'description' => $v->description,
                'caps' => $v->cap,
                'count_daily' => $v->cap,
                'monthly_cap' => 0,
                'count_monthly' => 0,
                'charge' => $v->payments[0]->revenue,
                'type' => $type,
                'active' => 1,
                'preview' => $v->preview_url,
                'app_id' => Tool::getAppId($v->preview_url),
                'min_os_ver' => $dm,
                'icon' => $v->logo,
                'tracking' => $tracking,
                'date' => date('YmdHis'),
                'joined' => $date,
                'expiry' => $expiry,
                'network_ID' => $network,
                'advID' => $v->id,
                'incent' => $incent,
                'adult' => $adult,
                'cron' => 1,
                'last_updated' => $date,
                'device' => json_encode($dev),
                'country' => json_encode($geo),
                'category' => '["General"]'
            ]);
            $ID = $oo->insert($oo); echo " - $ID";

        }else {
            $o = new \model\Offers();
            $o->setProperties([
                'tracking' => $v->link . '&sub1={CLICK}&sub2={PUB}_{PUB2}&sub3={IDFA}&sub4={GAID}',
                'active' => 1,
                'cron' => 1,
                'charge' => $v->payments[0]->revenue,
                'caps' => $v->cap,
                'last_updated' => $date
            ]);
            $o->update($o, "WHERE advID = '$v->id' and network_ID = $network and cron = 1 and active != 2");
        }

        echo BR . "\n";
    endforeach;
}

if(!isset($argc)){
    echo "\n<br> Offers Updated";
    Tool::replicateOffers();
}
