<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/22/18
 * Time: 11:32 AM
 */

use model\Offers;

require dirname(__DIR__) . '/../lib/include.php';

require_once 'AdvApi.php';

$network = 71;
$link = 'http://api.c.avazunativeads.com/s2s?pagenum=100&campaigndesc=1&enforcedv=device_id&sourceid=33778&convflow=101&page=';

$api = new AdvApi($link, $network);

$pull = $api->pull();

if ($pull->status !== 'OK') {
    die('Failed'.__CLASS__.'');
}

$data = $pull->ads->ad;
$total = $pull->ads->total_records;

process($data, $api);

$n = $total / 100;
for ($i = 1; $i < $n; $i++) {

    $lnk = $link . ($i + 1);
    $api = new AdvApi($lnk, $network);
    $pull = $api->pull();
    if ($pull->status !== 'OK') {
        die('Failed'.__CLASS__.'');
    }
    echo "\n page: " . $i . "\n";

    $data = $pull->ads->ad;
    process($data, $api);
}

/**
 * @param $data
 * @param $api
 */
function process(array $data, AdvApi $api)
{

    foreach ($data as $v) {
        $device_id = '{GAID}';
        if ($v->market === 'apple') $device_id = '{IDFA}';

        $params = [
            'name' => $v->title,
            'description' => $v->description . ' ' . $v->campaigndesc,
            'charge' => rtrim($v->payout, '$'),
            'caps' => 0,
            'monthly_cap' => 0,
            'tracking' => $v->clkurlhttps . '&dv1={CLICK}&nw_sub_aff={PUB}_{PUB2}&device_id=' . $device_id
        ];

        if ($api->notExist($v->campaignid)) {

            $geo = explode('|', $v->countries);

            $device_id = 0;
            if (false !== strpos($v->enforcedv, 'device_id')) {
                $device_id = 1;
            }

            $geo = $api->makeJson($geo);
            if (strlen($geo) > 180) $geo = '["all"]';

            $params['type'] = AdvApi::CPI;
            $params['preview'] = $v->market === 'apple' ? 'https://itunes.apple.com/app/id' . $v->pkgname : 'https://play.google.com/store/apps/details?id=' . $v->pkgname;
            $params['app_id'] = $v->pkgname;
            $params['min_os_ver'] = $v->minosv;
            $params['device_id'] = $device_id;
            $params['icon'] = $v->icon;

            $params['incent'] = $v->incent === 'no' ? 0 : 1;
            $params['device'] = $v->os === 'android' ? '["Android"]' : '["iOS"]';
            $params['country'] = $geo;
            $params['category'] = '["General"]';
            $params['expiry'] = 0;

            $api->addNew($params);
        } else {
            $api->updateExist($params);
        }
    }
}

$check = 'http://api.c.avazunativeads.com/adcheck?sourceid=33778&campaignid=';

$o = new Offers();
$o->setProperties([
    'network_ID' => $network
])
    ->setQueryParameters($o, ['advID']);
$op = $o->query();

$advIDs = [];

$i = 0;
foreach ($op as $v) {
    $advIDs[] = $v->advID;
    $i++;
    if ($i === 50) {
        $i = 0;
        $ids = implode(',', $advIDs);
        $in = new AdvApi($check . $ids, $network);
        $pull = $in->pull();

        validate($pull->data);
        $advIDs = [];
    }
}

if ($i !== 0) {
    $ids = implode(',', $advIDs);
    $in = new AdvApi($check . $ids, $network);
    $pull = $in->pull();

    validate($pull->data);
}

function validate($data)
{
    $ids = implode(',', $data->offline);

    echo "\n $ids";

    $o = new Offers();
    $o->setProperties([
        'active' => 0,
        'cron' => 1
    ]);
    $o->update($o, "WHERE advID IN ($ids) and active != 2 and cron = 1");

}

$api->replicate(!isset($argc));
