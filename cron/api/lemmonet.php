<?php
/**
 * Created by PhpStorm.
 * User: tiwari_ut
 * Date: 24/4/19
 * Time: 3:41 PM
 */

require dirname(__DIR__) . '/../lib/include.php';

require_once 'AdvApi.php';

$network = 148;
$link = 'https://lemmonetapi.azurewebsites.net/myoffers';

$api = new AdvApi($link, $network);

$load = [
    'clientid' => 'Vinetool72635Ts',
    'apikey' => '3D7E29867A664D9A97692BF98A6A7271',
    'action' => 'QUERY'
];

$pull = $api->makePost($load)->pull();

if ($pull->status !== 'OK') {
    die('Failed'.__CLASS__.'');
}

$data = $pull->result;

foreach ($data as $v) {
    $params = [
        'name' => $v->OfferName,
        'description' => $v->AppDescritpion . "<br><strong>" . $v->Targetings[0] . "<br>" . $v->Notes . "</strong>",
        'charge' => $v->Payout,
        'caps' => $v->DailyCap,
        'monthly_cap' => 0
    ];

    if ($api->notExist($v->OfferID)) {

        if ($v->Platform !== null) {
            switch ($v->Platform) {
                case "IOS":
                    $dev = '["iOS"]';
                    break;
                case "ANDROID":
                    $dev = '["Android"]';
                    break;
                case "WINDOWS":
                    $dev = '["Windows"]';
                    break;
            }
        }

        $params['type'] = $v->Type;
        $params['preview'] = $v->AppPreview;
        $params['app_id'] = $v->PackageName;
        $params['min_os_ver'] = version($v->Targetings);
        $params['joined'] = date('YmdHi');
        $params['device_id'] = $v->DeviceIDRequired === 'true' ? 1 : 0;
        $params['icon'] = $v->AppIcon;
        $params['tracking'] = tracking($v->TrakingURL);
        $params['incent'] = $v->Restrictions[0] === 'No Incent' ? 0 : 1;
        $params['device'] = $dev;
        $params['country'] = $api->makeJson($v->Countries);
        $params['category'] = '["General"]';
        $params['expiry'] = 0;

        $api->addNew($params);
    } else {
        $api->updateExist($params);
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));

function tracking($url)
{
    $s = ['[INSERT_YOUR_CLICK]', '[INSERT_YOUR_PUBLISHER_ID]', '[INSERT_IDFA]', '[INSERT_GAID]'];
    $r = ['{CLICK}', '{PUB}_{PUB2}', '{IDFA}', '{GAID}'];
    return str_replace($s, $r, $url);
}

function version($v)
{
    if (isset($v) && !empty($v)) {
        foreach ($v as $j) {
            $j = strtolower($j);
            if (false !== strpos($j, 'ios') || false !== strpos($j, 'android')) {
                return str_replace(['ios', 'android', ' ', '+', '^'], '', $j);
            }
        }
    }
    return 0;
}
