<?php
/**
 * Created by PhpStorm.
 * User: tiwari_ut
 * Date: 24/4/19
 * Time: 6:09 PM
 */

use helper\Tool;

require dirname(__DIR__) . '/../lib/include.php';
require_once 'AdvApi.php';

$link = 'http://api-motorads.affise.com/3.0/offers?API-Key=69e7da660c26700f3ae6b24efb473e98efedf099&page=';
$network = 101;

$api = new AdvApi($link, $network);

$pull = $api->pull();

if ($pull->status !== 1) {
    die('Failed'.__CLASS__.'!');
}

$data = $pull->offers;
$n = $pull->pagination->total_count;

process($data, $api);

for ($i = 1; $i <= $n; $i++) {

    $lnk = $link . ($i + 1);
    $api = new AdvApi($lnk, $network);
    $pull = $api->pull();
    if ($pull->status !== 1) {
        die('Failed'.__CLASS__.'');
    }
    echo "\n page: " . $i . "\n";

    $data = $pull->offers;
    process($data, $api);
}

/**
 * @param $data
 * @param $api
 */
function process(array $data, AdvApi $api)
{

    foreach ($data as $v) {
        if ($v->link === '') {
            continue;
        }
        $appId = Tool::getAppId($v->preview_url);
        if ($appId === '') {
            continue;
        }

        $params = [
            'name' => $v->title,
            'description' => $v->description . $v->kpi->en,
            'charge' => $v->payments[0]->revenue,
            'caps' => cap($v->caps),
            'monthly_cap' => 0
        ];

        if ($api->notExist($v->id)) {

            $dd = ['All'];
            $dm = 0;
            $ifa = '';
            if ($v->strictly_os->items !== null) {
                $dd = [];
                if ($v->strictly_os->items->Android !== null) {
                    $dd[] = 'Android';
                    $dm = Tool::getMinVer($v->strictly_os->items->Android, 'AFFISE');
                    $ifa = '{GAID}';
                }
                if ($v->strictly_os->items->iOS !== null) {
                    $dd[] = 'iOS';
                    $dm = Tool::getMinVer($v->strictly_os->items->iOS, 'AFFISE');
                    $ifa = '{IDFA}';
                }
                if ($v->strictly_os->items->windows !== null)
                    $dd[] = 'Windows';
            }

            $tracking = $v->link . '&sub1={CLICK}&sub2={PUB}_{PUB2}&sub3=' . $ifa;

            $incent = 1;
            if (false !== stripos($v->title, 'incent') && false !== stripos($v->title, 'non')) {
                $incent = 0;
            }

            $geo = array_map('strtoupper', $v->countries);

            $params['type'] = $v->is_cpi ? AdvApi::CPI : AdvApi::CPA;
            $params['preview'] = $v->preview_url;
            $params['app_id'] = $appId;
            $params['min_os_ver'] = $dm;
            $params['device_id'] = infiltrate($v->title);
            $params['joined'] = date('Ymd');
            $params['icon'] = $v->logo;
            $params['tracking'] = $tracking;
            $params['incent'] = $incent;
            $params['device'] = $api->makeJson($dd);
            $params['country'] = $api->makeJson($geo);
            $params['category'] = '["General"]';
            $params['expiry'] = $v->stop_at;

            $api->addNew($params);
        } else {
            $api->updateExist($params);
            echo "<hr>";
        }
    }

}

$api->purgeThem();

$api->replicate(!isset($argc));

function cap($c)
{
    if (empty($c)) {
        return 0;
    }
    $c = $c[0];
    if ($c->period === 'day') {
        return $c->value;
    }
    return 0;
}

function infiltrate($t)
{
    if (false !== stripos($t, 'GAID') || false !== stripos($t, 'IDFA')) {
        return 1;
    }
    return 0;
}
