<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/9/17
 * Time: 11:45 AM
 */
use helper\Mysql;
use helper\Tool;

require dirname(__DIR__).'/../lib/include.php';

$url = 'http://api.mediashakers.affise.com/2.1/partner/offers?API-Key=fc9d0fd203ab3a406ab17ca72ac3fb96fb8fe39d';
$network = 4;
$date = date('YmdHi');

$getData = Tool::hitCurl($url);

if ($getData === false)
    die('Failed'.__CLASS__.'');

$getData = json_decode($getData);
if ($getData->status === 1 && !empty($getData->offers)){
    echo 'reached'.BR;
    $offers = $getData->offers;

    processAll($offers, $network, $date);

    $total = $getData->pagination->total_count;
    $per = $getData->pagination->per_page;
    $pageT = (int)ceil($total/$per);

    for($pg=2;$pg<=$pageT;$pg++) {
        $urlN = $url.'&page='.$pg;
        $getNext = Tool::hitCurl($urlN);
        if ($getNext === false)
            continue;

        $getNext = json_decode($getNext);

        processAll($getNext->offers, $network, $date);
    }

    $o = new \model\Offers();
    $o->setProperties([
        'active' => 0,
        'cron' => 1
    ]);
    $o->update($o, "WHERE last_updated < $date and network_ID = $network and active != 2 and cron = 1");
}

function processAll($offers, $network, $date){
    foreach ($offers as $v):

        if ($v->required_approval)
           continue;

        echo $v->id.BR;
        $oo = new \model\Offers();
        $oo->setProperties([
            'advID' => $v->id,
            'network_ID' => $network
        ]);
        $oo->setQueryParameters($oo);
        echo $oo->count() .BR;

        $expiry = 0;

        if (!$oo->count()){

            if($v->is_cpi){
                $type = 'CPI';
            }else{
                if (false !== strpos($v->title, 'CPL')){
                    $type = 'CPL';
                }elseif (false !== strpos($v->title, 'CPR')){
                    $type = 'CPR';
                }elseif (false !== strpos($v->title, 'CPS')){
                    $type = 'CPS';
                }else{
                    $type = 'CPA';
                }
            }

            $incent = 1;
            if (false !== strpos(strtolower($v->title), 'incent') && false !== strpos(strtolower($v->title), 'non')){
                $incent = 0;
            }

            $adult = 0;
            if (false !== strpos(strtolower($v->title), 'adult')){
                $adult = 1;
            }

            $tracking = $v->link.'&sub1={CLICK}&sub2={PUB}&sub3={IDFA}&sub4={GAID}';
            $oo->setProperties([
                'name' => $v->title,
                'description' => $v->description,
                'caps' => $v->cap,
                'count_daily' => $v->cap,
                'monthly_cap' => 0,
                'count_monthly' => 0,
                'charge' => $v->payments[0]->revenue,
                'type' => $type,
                'active' => 1,
                'preview' => $v->preview_url,
                'icon' => $v->logo,
                'tracking' => $tracking,
                'date' => date('YmdHis'),
                'joined' => $date,
                'expiry' => $expiry,
                'network_ID' => $network,
                'advID' => $v->id,
                'incent' => $incent,
                'adult' => $adult,
                'cron' => 1,
                'last_updated' => $date
            ]);
            $ID = $oo->insert($oo);

            $gg = implode('","',$v->countries);
            $g = new \model\Country();
            $g->setQueryParameters($g);
            $g->appendQuery("LOWER(code) IN (\"$gg\")");
            $country = $g->query();

            if ($v->strictly_os->items !== null){
                $dd = [];
                if ($v->strictly_os->items->android !== null)
                    $dd[] = 'Android';
                if ($v->strictly_os->items->ios !== null)
                    $dd[] = 'iOS';
                if ($v->strictly_os->items->windows !== null)
                    $dd[] = 'Windows';
                $dd = implode('","',$dd);
            }else
                $dd = 'All';

            $d = new \model\Device();
            $d->setQueryParameters($d);
            $d->appendQuery("name IN (\"$dd\")");
            $device = $d->query();

            foreach ($country as $item) {
                Mysql::put('insert', 'offers_country', ['offers_ID=?' => $ID, 'country_ID=?' => $item->ID]);
            }
            foreach ($device as $item) {
                Mysql::put('insert', 'offers_device', ['offers_ID=?' => $ID, 'device_ID=?' => $item->ID]);
            }
            Mysql::put('insert', 'offers_category', ['offers_ID=?' => $ID, 'category_ID=?' => 393]);
        }else {
            $o = new \model\Offers();
            $o->setProperties([
                'active' => 1,
                'cron' => 1,
                'charge' => $v->payments[0]->revenue,
                'caps' => $v->cap,
                'last_updated' => $date
            ]);
            $o->update($o, "WHERE advID = '$v->id' and network_ID = $network and cron = 1 and active != 2");
        }
    endforeach;
}
