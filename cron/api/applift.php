<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/2/19
 * Time: 2:47 PM
 */


require dirname(__DIR__) . '/../lib/include.php';

require_once 'AdvApi.php';

$network = 133;
$link = 'https://bapi.applift.com/bapi_v2?token=2HaGwDFUMVddbTA0h8B66A&format=json&v=3&per_page=100&page=';

$api = new AdvApi($link, $network);

$pull = $api->pull();

if ($pull->error) {
    die('Failed'.__CLASS__.'!');
}

$data = $pull->results;

process($data, $api);

for ($i = 1; $i <= 10; $i++) {

    $lnk = $link . ($i + 1);
    $api = new AdvApi($lnk, $network);
    $pull = $api->pull();

    if ($pull->error || empty($pull->results)) {
        break;
    }
    echo "\n page: " . $i . "\n";

    $data = $pull->results;
    process($data, $api);
}

/**
 * @param $data
 * @param $api
 */
function process(array $data, AdvApi $api)
{

    foreach ($data as $v) {

        foreach ($v->offers as $o):

            $v = $v->app_details;

            $params = [
                'name' => str_replace(['Applift', 'AppLift'], 'Offer', $o->offer_name),
                'description' => describe($o),
                'charge' => payout($o),
                'caps' => 0,
                'monthly_cap' => 0
            ];

            if ($api->notExist($o->offer_id)) {

                if ($o->global) {
                    $geo = '["all"]';
                } else {
                    $geo = geoData($o->geo_targeting, $api);
                    if (strlen($geo) > 180) {
                        $geo = '["all"]';
                    }
                }

                $preview = '';
                if ($v->platform === 'android') {
                    $preview = 'https://play.google.com/store/apps/details?id=' . $v->bundle_id;
                    $device = '["Android"]';
                } else if ($v->platform === 'ios') {
                    $preview = 'https://itunes.apple.com/app/id' . $v->bundle_id;
                    $device = '["iOS"]';
                }

                $params['type'] = $o->goal_type;
                $params['preview'] = $preview;
                $params['app_id'] = $v->bundle_id;
                $params['min_os_ver'] = $o->os_version ?? 0;
                $params['device_id'] = $o->device_id_required;
                $params['icon'] = icon($o->creatives);
                $params['tracking'] = tracking($o->click_url);
                $params['incent'] = $o->incent;
                $params['device'] = $device;
                $params['country'] = $geo;
                $params['category'] = '["General"]';
                $params['expiry'] = 0;

                $api->addNew($params);
            } else {
                $api->updateExist($params);
            }
        endforeach;
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));

function describe(&$o)
{
    $describe = '';
    if ($o->non_rebrokering) {
        $describe = '<h6>No re-brokering</h6> </br></br>';
    }
    if ($o->goal_type === 'CPA') {
        $describe .= '<h6>CPA payment: ' . $o->goal_payouts[0]->goal_name . '</h6> </br></br>';
    }
    if (!empty($o->devices)) {
        $describe .= addIf($o->devices, 'Devices', 'li');
    }
    if (!empty($o->preferences)) {
        $describe .= '<br/><br/><h5>Restrictions:</h5><ul>';
        foreach ($o->preferences as $p) {
            if ($p->preference_type === 'min_os_version') {
                $o->os_version = $p->value;
            } else {
                $ex = explode('_', $p->preference_type);
                $ex = '<li><strong>' . implode(' ', $ex) . ':</strong>&nbsp;' . $p->value . '</li>';
                $describe .= $ex;
            }
        }
        $describe .= '</ul>';
    }
    if (!empty($o->billing_kpis)) {
        $describe .= '<br/><br/><h5>KPIs:</h5><ul>';
        foreach ($o->billing_kpis as $p) {
            $ex = explode('_', $p->kpi_label);
            $ex = '<li><strong>' . implode(' ', $ex) . ':</strong>&nbsp;' . $p->target . '</li>';
            $describe .= $ex;
        }
        $describe .= '</ul>';
    }
    if (!empty($o->blacklisted_sources)) {
        $describe .= addIf($o->blacklisted_sources, 'Blocked sources', 'li');
    }
    return $describe;
}

function payout($o)
{
    $po = $o->goal_payouts[0]->payout;
    return $po / 1000;
}

function geoData($o, AdvApi $a)
{
    $in = [];
    foreach ($o as $g) {
        $in[] = $g->country_code;
    }
    return $a->makeJson($in);
}

function tracking($url): string
{
    $url = str_replace(array('{click_id}', '{idfa}', '{gaid}'), array('{CLICK}', '{IDFA}', '{GAID}'), $url);
    $url .= '&source={PUB}_{PUB2}';
    return $url;
}

function icon($v): string
{
    foreach ($v as $c) {
        if ($c->creative_id === null) {
            return $c->url;
        }
    }
    return 'http://';
}

function addIf($v, $k, $fn)
{
    if (!empty($v)) {
        return "<br/><br/><h5>$k:</h5>" . $fn($v);
    }
    return '';
}

function li(array $of): string
{
    $li = '';
    foreach ($of as $v) {
        $li .= "<li>$v</li>";
    }
    return "<ul>$li</ul>";
}
