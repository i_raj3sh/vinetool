<?php
/**
 * Created by PhpStorm.
 * User: tiwari_ut
 * Date: 13/3/19
 * Time: 11:27 AM
 */

require dirname(__DIR__) . '/../lib/include.php';
require_once 'AdvApi.php';

$url = 'http://api.blam.mobi/affiliate/offer/findAll/?token=Zptu9YSVaaK8db3q11pd1A7mMjWYTQd5&approved=1';
$network = 19;

$api = new AdvApi($url, $network);

$data = $api->pull();

//_r($data);

if (!$data->success) {
    die("API Error");
}
$data = $data->offers;

foreach ($data as $v) {
    if ($v->Status !== 'active' || $v->APP_ID === null) {
        continue;
    }

    $params = [
        'name' => $v->Name,
        'description' => $v->Description,
        'charge' => $v->Payout,
        'caps' => $v->Daily_cap ?? 0,
        'monthly_cap' => $v->Monthly_cap
    ];

    if ($api->notExist($v->ID)) {
        $getDev = str_replace(['iPhone,iPad', 'Windows phone', 'iPhone', 'iPad'], array('iOS', 'Windows', 'iOS', 'iOS'), $v->Platforms);
        $getDev = explode(',', $getDev);
        $bundle = $v->APP_ID;
        $minOS = 0;
        if (in_array('iOS', $getDev)) {
            $ifa = '&idfa={IDFA}';
            $bundle = substr($bundle, 2);
            if (strpos($v->Name, "+")) {
                $minOS = get_string_between($v->Name, "(iPhone ", "+");
            }
        } elseif (in_array('Android', $getDev)) {
            $ifa = '&idfa={GAID}';
            if (strpos($v->Name, "+")) {
                $minOS = get_string_between($v->Name, "(Android ", "+");
            }
        }

        $device_id = 0;
        if ((false !== strpos($v->Name, 'GAID')) || (false !== strpos($v->Name, 'IDFA'))) {
            $device_id = 1;
        }

        $geo = $api->makeJson($v->Countries, true);
        if (strlen($geo) > 180) {
            $geo = '["all"]';
        }

        $params['type'] = AdvApi::CPI;
        $params['preview'] = $v->Preview_url;
        $params['app_id'] = $bundle;
        $params['min_os_ver'] = $minOS;
        $params['device_id'] = $device_id;
        $params['joined'] = date('YmdHi');
        $params['icon'] = $v->Icon_url;
        $params['tracking'] = tracking($v->Tracking_url, $ifa);
        $params['incent'] = $v->Type === 'Non incent' ? 0 : 1;
        $params['device'] = $api->makeJson($getDev);
        $params['country'] = $geo;
        $params['category'] = '["General"]';
        $params['expiry'] = 0;

        $api->addNew($params);
    } else {
        $api->updateExist($params);
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));

function tracking($url, $ifa): string
{
    return $url . '&aff_sub={CLICK}&aff_sub2={PUB}_{PUB2}' . $ifa;
}

function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}