<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/22/18
 * Time: 11:32 AM
 */

require dirname(__DIR__) . '/../lib/include.php';

require_once 'AdvApi.php';

$network = 59;
$link = 'http://api.howdoesin.net/api/v1/get?code=b761c0f6-e486-4779-9864-545788e4296f&pageSize=100&offer_cap=true&page=';

$api = new AdvApi($link, $network);

$pull = $api->pull();

if (!$pull->success) {
    die('Failed'.__CLASS__.'!');
}

$data = $pull->offers;
$n = $pull->page_total;

process($data, $api);

for ($i = 1; $i <= $n; $i++) {

    $lnk = $link . ($i + 1);
    $api = new AdvApi($lnk, $network);
    $pull = $api->pull();
    if (!$pull->success) {
        die('Failed'.__CLASS__.'');
    }
    echo "\n page: " . $i . "\n";

    $data = $pull->offers;
    process($data, $api);
}

/**
 * @param $data
 * @param $api
 */
function process(array $data, AdvApi $api)
{

    foreach ($data as $v) {

        if ($v->status !== 'active') {
            continue;
        }

        $params = [
            'name' => $v->offer_name.' '.$v->incentive. ' '.$v->country. ' '. $v->platform. ' '.$v->required_os_version,
            'description' => $v->description . ' <br/>' . $v->terms . ' <br/>DL type:' . $v->download_type . ' <br/>Traffic allowed:' . $v->traffic_allowed,
            'charge' => $v->payout,
            'caps' => $v->offer_cap,
            'monthly_cap' => 0
        ];

        if ($api->notExist($v->offer_id)) {

            $geo = explode(',', $v->country);

            $device_id = 0;
            if (!empty($v->parameter_required)) {
                $device_id = 1;
            }

            $geo = $api->makeJson($geo);
            if (strlen($geo) > 180) {
                $geo = '["all"]';
            }

            $os = strtoupper($v->platform);
            if ($os === 'ANDROID') {
                $preview = 'https://play.google.com/store/apps/details?id=' . $v->package_name;
                $device = '["Android"]';
            } else {
                $preview = 'https://itunes.apple.com/app/id' . $v->package_name;
                $device = '["iOS"]';
            }

            $params['type'] = $v->payout_type;
            $params['preview'] = $preview;
            $params['app_id'] = $v->package_name;
            $params['min_os_ver'] = $v->required_os_version ?? 0;
            $params['device_id'] = $device_id;
            $params['icon'] = icon($v);
            $params['tracking'] = tracking($v->tracking_link);
            $params['incent'] = $v->incentive === 'Non-incent' ? 0 : 1;
            $params['device'] = $device;
            $params['country'] = $geo;
            $params['category'] = '["General"]';
            $params['expiry'] = 0;

            $api->addNew($params);
        } else {
            $api->updateExist($params);
        }
    }
}

$api->purgeThem();

$api->replicate(!isset($argc));

function tracking($url): string
{
    $url = str_replace(array('{idfa}', '{gaid}'), array('{IDFA}', '{GAID}'), $url);
    $url .= '&aff_sub2={CLICK}&aff_sub5={PUB}_{PUB2}&app_id={APP}';
    return $url;
}

function icon($v): string
{
    if ($v->icons !== null) {
        return $v->icons[0]->url;
    }
    if ($v->banners !== null) {
        return $v->banners[0]->url;
    }
    return 'http://';
}
