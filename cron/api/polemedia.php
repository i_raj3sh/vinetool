<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 07/20/18
 * Time: 9:39 PM
 */

use helper\Tool;
use model\Offers;

require dirname(__DIR__).'/../lib/include.php';

$network = 48;
$date = date('YmdHi');

function postCall($page = false){
    $data = [
        "api_id" => "vinetool media",
        "api_token" => "0d7ee83a212d2e8c0c35637a736d7b77",
        "approved" => true,
        "limit" => 200
    ];
    if($page){
        $data['page'] = $page;
    }
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'http://offermgr.polemedia.mobi/getOffers');
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($curl);
    curl_close($curl);

    return $result;
}

$getData = postCall();

//header('Content-Type: application/json');
//echo $getData; die;


if ($getData === false)
    die('Failed'.__CLASS__.'');

$getData = json_decode($getData);

if ($getData->state === 1 && !empty($getData->result->data)){
    echo 'reached'.BR;
    $offers = $getData->result->data;

    processAll($offers, $network, $date);

    for($pg=2;$pg<=20;$pg++) {
        $getNext = postCall($pg);
        $getNext = json_decode($getNext);
        if ($getNext->state !== 1 || empty($getNext->result->data))
            break;
        echo "\n page: ".$pg."\n";

        processAll($getNext->result->data, $network, $date);
    }

    $o = new Offers();
    $o->setProperties([
        'active' => 0,
        'cron' => 1
    ]);
    $o->update($o, "WHERE last_updated < $date and network_ID = $network and active != 2 and cron = 1");
}

function processAll($offers, $network, $date){
    foreach ($offers as $v):

        if (!$v->approved)
            continue;
        if (!$v->status)
            continue;

        echo $v->id.BR;
        $oo = new Offers();
        $oo->setProperties([
            'advID' => $v->id,
            'network_ID' => $network
        ]);
        $oo->setQueryParameters($oo);
        $cnt = $oo->count();
        echo $cnt .BR;

        if (!$cnt){

            $type = 'CPI';

            $incent = 0;
            if ($v->incent){
                $incent = 1;
            }

            $adult = 0;
            if ($v->adult){
                $adult = 1;
            }
            $cap = 0;
            if($v->cap){
                $cap = $v->capval->daily;
            }

            $expiry = 0;
            if($v->expiration_date){
                $expiry = $v->expiration_date;
            }

            $tracking = str_replace('{click_id}','{CLICK}',$v->tracklink);
            $tracking = str_replace('{extra}', '{PUB}_{PUB2}', $tracking);
            $tracking = str_replace('{idfa}','{IDFA}',$tracking);
            $tracking = str_replace('{gaid}','{GAID}',$tracking);

            $dd = '["All"]';
            if ($v->platform === 'and')
                $dd = '["Android"]';
            elseif ($v->platform === 'ios')
                $dd = '["iOS"]';

            $geo = \helper\Tool::sort($v->countries);
            $geo = json_encode($geo);

            if (strlen($geo) > 190) continue;

            $args = [
                'name' => $v->title,
                'description' => $v->description,
                'caps' => $cap,
                'count_daily' => $cap,
                'monthly_cap' => 0,
                'count_monthly' => 0,
                'charge' => $v->price,
                'type' => $type,
                'active' => 1,
                'preview' => $v->preview,
                'app_id' => Tool::getAppId($v->preview),
                'min_os_ver' => 0,
                'icon' => $v->image,
                'tracking' => $tracking,
                'date' => date('YmdHis'),
                'joined' => $date,
                'expiry' => $expiry,
                'network_ID' => $network,
                'advID' => $v->id,
                'incent' => $incent,
                'adult' => $adult,
                'cron' => 1,
                'last_updated' => $date,
                'category' => '["General"]',
                'device' => $dd,
                'country' => $geo
            ];

            $oo->setProperties($args);
            $ID = $oo->insert($oo);

        }else {

            $tracking = str_replace('{click_id}', '{CLICK}', $v->tracklink);
            $tracking = str_replace('{extra}', '{PUB}_{PUB2}', $tracking);
            $tracking = str_replace('{idfa}', '{IDFA}', $tracking);
            $tracking = str_replace('{gaid}', '{GAID}', $tracking);

            $o = new Offers();
            $o->setProperties([
                'tracking' => $tracking,
                'active' => 1,
                'cron' => 1,
                'charge' => $v->price,
                'last_updated' => $date
            ]);
            $o->update($o, "WHERE advID = '$v->id' and network_ID = $network and cron = 1 and active != 2");
        }
    endforeach;
}

if (!isset($argc)) {
    echo "\n<br> Offers Updated";
    Tool::replicateOffers();
}
