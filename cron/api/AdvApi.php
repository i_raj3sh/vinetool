<?php

use helper\Tool as T;
use model\Offers as O;

/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 9/29/18
 * Time: 5:03 PM
 */
class AdvApi
{

    private $link;
    private $network;
    private $advId;
    private $now;
    private $post = [];
    public const NIL = 0;
    public const CPI = 'CPI';
    public const CPA = 'CPA';

    public function __construct($link, $network)
    {
        $this->link = $link;
        $this->network = $network;
        $this->now = (int)date('YmdHi');
    }

    public function makePost($data): self
    {
        $this->post = $data;
        return $this;
    }

    public function pull(array $load = [])
    {
        if (empty($this->post)) {
            $getData = T::hitCurl($this->link);

            if ($getData === false) {
                die('Failed'.__CLASS__.'');
            }
        } else {
            $data = $this->post;
            $data = json_encode(array_merge($data, $load));
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $this->link);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );

            $getData = curl_exec($curl);
            curl_close($curl);

            if ($getData === false) {
                die('Failed'.__CLASS__.'');
            }
        }

        return json_decode($getData);
    }

    public function cookie()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->link);
        curl_setopt($ch, CURLOPT_COOKIE, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML,like Gecko) Chrome/33.0.1750");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);

        if ($data === false) {
            die('Failed'.__CLASS__.'---');
        }
        return json_decode($data);
    }

    public function notExist($id): bool
    {
        $this->advId = $id;
        $oo = new O();
        $oo->setProperties([
            'advID' => $id,
            'network_ID' => $this->network
        ])
            ->setQueryParameters($oo);
        $cn = $oo->count();
        echo "<br/> $cn ---> ";
        return !$cn;
    }

    public function makeJson($val, $is = false): string
    {
        if ($is) {
            $val = explode(',', $val);
        }
        return json_encode(T::sort($val));
    }

    public function updateExist(array $arg): self
    {
        $o = new O();

        $arg['count_daily'] = $arg['caps'];
        $arg['count_monthly'] = $arg['monthly_cap'];
        $arg['active'] = 1;
        $arg['cron'] = 1;
        $arg['last_updated'] = $this->now;
        $o->setProperties($arg)
            ->update($o, "WHERE advID = '$this->advId' and network_ID = $this->network and cron = 1 and active != 2");

        return $this;
    }

    public function purgeThem(): self
    {
        $o = new O();
        $o->setProperties([
            'active' => 0,
            'cron' => 1
        ]);
        $o->update($o, "WHERE last_updated < $this->now and network_ID = $this->network and active != 2 and cron = 1");

        return $this;
    }

    public function addNew(array $arg): self
    {
        $oo = new O();

        $arg['count_daily'] = $arg['caps'];
        $arg['count_monthly'] = $arg['monthly_cap'];
        $arg['active'] = 1;
        $arg['cron'] = 1;
        $arg['advID'] = $this->advId;
        $arg['network_ID'] = $this->network;
        $arg['date'] = date('YmdHis');
        $arg['last_updated'] = $this->now;

        $oo->setProperties($arg);
        $ID = $oo->insert($oo);
        echo " $ID ";

        return $this;
    }

    /**
     * @param $is
     * @return mixed
     */
    public function replicate($is): void
    {
        if ($is) {
            echo "\n<br> Offers Updated";
            T::replicateOffers();
        }
    }
}
