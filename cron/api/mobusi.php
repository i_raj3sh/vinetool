<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/24/18
 * Time: 9:05 AM
 */

require dirname(__DIR__) . '/../lib/include.php';

require_once 'AdvApi.php';

$network = 34;
$link = 'http://api.leadzu.com/offer.find';

$api = new AdvApi($link, $network);

$load = [
    'api_key' => '692a214848674306d87920affb617646',
    'user_id' => 23854,
    'currency' => 'USD',
    'device' => 'mobile',
    'type' => 'cpi',
    'adult' => false,
    'approved' => true,
    'limit' => 100
];
$pull = $api->makePost($load)->pull();

if ($pull->type !== 'ok') {
    die('Failed'.__CLASS__.'');
}

$data = $pull->answer;
$total = $pull->pagination->total;

process($data, $api);

$n = floor($total / 100);
for ($i = 1; $i < $n; $i++) {

    $load['skip'] = 100 * $i;
    $api = new AdvApi($link, $network);
    $pull = $api->makePost($load)->pull();
    if ($pull->type !== 'ok') {
        die('Failed'.__CLASS__.'');
    }
    echo "\n page: " . $i . "\n";

    $data = $pull->answer;
    process($data, $api);
}

/**
 * @param $data
 * @param $api
 */
function process($data, AdvApi $api)
{

    foreach ($data as $v) {

        $params = [
            'name' => $v->title,
            'description' => describe($v),
            'charge' => rtrim($v->payout, '$'),
            'caps' => $v->daily_cap,
            'monthly_cap' => $v->monthly_cap
        ];

        if ($api->notExist($v->id)) {

            $tracking = str_replace(['{clickID}', '{sourceID}'], ['{CLICK}', '{PUB}_{PUB2}'], $v->tracking);
            $params['type'] = AdvApi::CPI;
            $params['preview'] = $v->preview;
            $params['app_id'] = $v->appid;
            $params['min_os_ver'] = $v->os_ver ?? 0;
            $params['device_id'] = $v->device_id;
            $params['icon'] = $v->icon;
            $params['tracking'] = $tracking;
            $params['incent'] = (int)$v->incent;
            $params['device'] = $v->os_type;
            $params['country'] = $v->geo;
            $params['category'] = '["General"]';
            $params['expiry'] = 0;

            $api->addNew($params);
        } else {
            $api->updateExist($params);
        }
    }
}

function describe(&$v): string
{

    if ($v->os_version->android) {
        $v->os_type = '["Android"]';
        $os = $v->os_version->android;
    } else {
        $v->os_type = '["iOS"]';
        $os = $v->os_version->ios;
    }
    if ($os->ge) {
        $v->os_ver = $os->ge;
        $v->os_ver_text = $os->ge . '+';
    } elseif ($os->gt) {
        $v->os_ver = $os->gt;
        $v->os_ver_text = $os->gt . '+';
    }

    geoData($v);

    $describe = $v->description;
    $describe .= '<h5>Tags:</h5>' . li($v->tags);
    $describe .= '<h5>Restrictions:</h5>' . li($v->restrictions);
    $describe .= addIf($v->os->allowed, 'OS allowed', 'li');
    $describe .= addIf($v->os->denied, 'OS denied', 'li');
    $describe .= '<h5>OS version:</h5>' . $v->os_ver_text;
    $describe .= '<h4>KPI:</h4>' . $v->kpi;
    $describe .= addIf($v->postevents, 'Post events', 'li');

    return $describe;
}

function geoData(&$v)
{
    foreach ($v->countries as $index => $country) {
        if ($country->urlStatus !== 'approved') {
            continue;
        }
        $v->geo = '["' . $index . '"]';
        $v->payout = $country->payout;
        $v->tracking = $country->url;
        if ($country->capping) {
            $v->daily_cap = $country->capValue->daily;
            $v->monthly_cap = $country->capValue->monthly;
        } else {
            $v->daily_cap = 0;
            $v->monthly_cap = 0;
        }
        if ($country->cityTargeting) {
            $v->description .= addIf($country->cityTargetingValues->region->allowed, 'Region allowed', 'li');
            $v->description .= addIf($country->cityTargetingValues->region->denied, 'Region denied', 'li');
            $v->description .= addIf($country->cityTargetingValues->city->allowed, 'City allowed', 'li');
            $v->description .= addIf($country->cityTargetingValues->city->denied, 'City denied', 'li');
        }
        $v->description .= addIf($country->carriers->allowed, 'Carrier allowed', 'lo');
        if ($country->carriers->wifi) {
            $v->description .= 'Wifi allowed<br/>';
        } else {
            $v->description .= 'Wifi denied<br/>';
        }
        $v->description .= addIf($country->pubids->allowed, 'Pub ids allowed', 'li');
        $v->description .= addIf($country->pubids->denied, 'Pub ids denied', 'li');

        $v->device_id = $country->required_idfa_gaid;
        break;
    }
}

function addIf($v, $k, $fn)
{
    if (!empty($v)) {
        return "<br/><br/><h5>$k:</h5>" . $fn($v);
    }
    return '';
}

function li($of): string
{
    $li = '';
    foreach ($of as $v) {
        $li .= "<li>$v</li>";
    }
    return "<ul>$li</ul>";
}

function lo($of): string
{
    $li = '';
    foreach ($of as $v) {
        $li .= "<li>$v->name</li>";
    }
    return "<ul>$li</ul>";
}

$api->purgeThem();

$api->replicate(true);
