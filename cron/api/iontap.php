<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/1/18
 * Time: 11:35 AM
 */


use helper\Mysql;
use helper\Tool;

require dirname(__DIR__).'/../lib/include.php';

$url = 'https://api.iontap.io/affiliate/offer/findAll/?token=M4QlaoaMsnoHACGKZs59EGLtVwkfIj7r&approved=1';
$network = 9;
$date = (int)date('YmdHi');

$getData = Tool::hitCurl($url);

if ($getData === false)
    die('Failed'.__CLASS__.'');

$getData = json_decode($getData);
if ($getData->success){

    $offers = $getData->offers;
    foreach ($offers as $k=>$v):
        echo $k. ' - ';
        $oo = new \model\Offers();
        $oo->setProperties([
            'advID' => $k,
            'network_ID' => $network
        ]);
        $oo->setQueryParameters($oo);
$cccc = $oo->count();
        echo $cccc.' - ';

        $replace = [
            '-',
            ' ',
            ':'
        ];
        $expiry = str_replace($replace, '', $v->Expiration_date);

        if (!$cccc){

            if($v->APP_ID){
                $type = 'CPI';
            }else{
                if (false !== strpos($v->Name, 'CPL')){
                    $type = 'CPL';
                }else{
                    $type = 'CPA';
                }
            }

            $tracking = $v->Tracking_url.'&aff_sub={CLICK}&aff_sub2={PUB}&idfa={IDFA}&android_id={GAID}';
            $oo->setProperties([
                'name' => $v->Name,
                'description' => $v->Description,
                'caps' => $v->Daily_cap,
                'count_daily' => $v->Daily_cap,
                'monthly_cap' => $v->Monthly_cap,
                'count_monthly' => $v->Monthly_cap,
                'charge' => $v->Payout,
                'type' => $type,
                'active' => 1,
                'preview' => $v->Preview_url,
                'icon' => $v->Icon_url,
                'tracking' => $tracking,
                'date' => date('YmdHis'),
                'joined' => $date,
                'expiry' => $expiry,
                'network_ID' => $network,
                'advID' => $k,
                'cron' => 1,
                'incent' => $v->Type === 'Non incent'?0:1,
                'last_updated' => $date
            ]);
            $ID = $oo->insert($oo); echo " $ID ";

            $gg = str_replace(',','","',$v->Countries);
            $g = new \model\Country();
            $g->setQueryParameters($g);
            $g->appendQuery("code IN (\"$gg\")");
            $country = $g->query();

            $getDev = str_replace(['iPhone,iPad','Windows phone'],['iOS','Windows'],$v->Platforms);
            $getDev = str_replace(['iPhone','iPad'],'iOS',$getDev);
            $dd = str_replace(',','","',$getDev);
            $d = new \model\Device();
            $d->setQueryParameters($d);
            $d->appendQuery("name IN (\"$dd\")");
            $device = $d->query();

            foreach ($country as $item) {
                Mysql::put('insert', 'offers_country', ['offers_ID=?' => $ID, 'country_ID=?' => $item->ID]);
            }
            foreach ($device as $item) {
                Mysql::put('insert', 'offers_device', ['offers_ID=?' => $ID, 'device_ID=?' => $item->ID]);
            }
            Mysql::put('insert', 'offers_category', ['offers_ID=?' => $ID, 'category_ID=?' => 393]);
        }else {
            $o = new \model\Offers();
            $o->setProperties([
                'name' => $v->Name,
                'active' => 1,
                'cron' => 1,
                'charge' => $v->Payout,
                'caps' => $v->Daily_cap,
                'monthly_cap' => $v->Monthly_cap,
                'last_updated' => $date,
                'expiry' => $expiry
            ]);
            $o->update($o, "WHERE advID = '$k' and network_ID = $network and cron = 1 and active != 2");
        }
echo BR."\n";
    endforeach;

    $o = new \model\Offers();
    $o->setProperties([
        'active' => 0,
        'cron' => 1
    ]);
    $o->update($o, "WHERE last_updated < $date and network_ID = $network and active != 2 and cron = 1");
}

if(!isset($argc)){
echo "\n<br> Offers Updated";
exec("php /var/www/html/reoxey/op.vinetool.com/cron/replicateOffers.php");
}
