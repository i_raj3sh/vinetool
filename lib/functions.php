<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/6/18
 * Time: 3:03 PM
 */

use controller\Controller as C;
use helper\Cache;
use helper\Request;
use model\Admin;
use model\Affiliate;
use model\Event;
use model\Network;
use model\Offers as O;

define('CACHE_KEY', 'functions');

function _r($data)
{
    ob_start();
    print_r($data);
    $data = ob_get_contents();
    ob_end_clean();
    echo '<section style="width:1200px;margin: auto;"><pre style="display: block;
    padding: 9.5px;
    margin: 0 0 10px;
    font-size: 13px;
    line-height: 1.428571429;
    color: darkred;
    word-break: break-all;
    word-wrap: break-word;
    background-color: #f5f5f5;
    border: 1px solid #cccccc;
    border-radius: 4px;white-space: pre-wrap;"><code style="padding: 0;
    font-size: inherit;
    color: inherit;
    white-space: pre-wrap;
    background-color: transparent;
    border-radius: 0;
    font-family:monospace;">' . $data . '</code></pre></section>';
    die('died');
}

function campaign(): string
{
    $out = '';
    if ($of = Request::getField('of', Request::GET)) {
        $o = new O();
        $o->setProperties(['ID' => $of])
            ->setQueryParameters($o, ['ID', 'name']);
        $offer = $o->one();
        $out = '<option value="' . $offer->ID . '">#' . $offer->ID . ' ' . $offer->name . '</option>';
    }
    return '
    <div class="form-group">
    <label for="campaign">Offer:</label>
    <select id="campaign" name="of" class="form-control">' . $out . '</select>
    </div>
    <script>
    $("#campaign").select2({
        ajax: {
            url: "' . s() . 'offers/search",
            dataType: "json",
            delay: 250,
            data: function (params) {
            return {
                q: params.term
            };
            },
            processResults: function (data, params) {
            return {
                results: $.map(data, function(obj) {
                    return {
                        id:  obj.id,
                        text : obj.value
                };
                })
            }
            },
            cache: true
        },
        placeholder: "--select--",
        allowClear: true,
        minimumInputLength: 3
    });
    </scr' . 'ipt>';
}

function event()
{
    $cObj = new Cache(Cache::STORE);

    $key = CACHE_KEY . __FUNCTION__;

    if (!($cache = $cObj->retrieve($key))) {
        $g = new Event();
        $g->setQueryParameters($g, ['name']);
        $all = $g->query();
        $cache = json_encode($all);

        $cObj->enter($key, $cache);
    }

    $all = json_decode($cache);

    $in = Request::getField('ev', 'GET');
    $html = '<div class="form-group"><label>' . ucfirst(__FUNCTION__) . ':</label>';
    $html .= '<select id="event" name="ev" class="form-control selector selected5"><option value="" selected>--select-' . __FUNCTION__ . '--</option>';
    foreach ($all as $item):
        $selected = '';
        if ($in === $item->name) {
            $selected = 'selected';
        }
        $html .= '<option value="' . $item->name . '" ' . $selected . '>' . $item->name . '</option>';
    endforeach;
    $html .= '</select></div>';

    return $html;
}

function network()
{
    $cObj = new Cache(Cache::STORE);

    $key = CACHE_KEY . __FUNCTION__;

    if (!($cache = $cObj->retrieve($key))) {
        $g = new Network();
        $g->setProperties([
            'active' => 1
        ]);
        $g->setQueryParameters($g, ['ID', 'name', 'company']);
        $all = $g->query();
        $cache = json_encode($all);

        $cObj->enter($key, $cache);
    }

    $all = json_decode($cache);

    $in = Request::getField('nw', 'GET');
    $html = '<div class="form-group"><label>' . ucfirst(__FUNCTION__) . ':</label>';
    $html .= '<select id="network" name="nw" class="form-control selector selected5"><option value="" selected>--select-' . __FUNCTION__ . '--</option>';
    foreach ($all as $item):

        $selected = '';
        if ($in === $item->ID) {
            $selected = 'selected';
        }
        $html .= '<option value="' . $item->ID . '" ' . $selected . '>' . $item->name . " ($item->company)" . '</option>';
    endforeach;
    $html .= '</select></div>';

    return $html;
}

function affiliate()
{
    $cObj = new Cache(Cache::STORE);

    $key = CACHE_KEY . __FUNCTION__;

    if (!($cache = $cObj->retrieve($key))) {
        $g = new Affiliate();
        $g->setProperties([
            'active' => 1
        ]);
        $g->setQueryParameters($g, ['ID', 'name', 'company']);
        $all = $g->query();
        $cache = json_encode($all);

        $cObj->enter($key, $cache);
    }

    $all = json_decode($cache);

    $in = Request::getField('af', 'GET');
    $html = '<div class="form-group"><label>' . ucfirst(__FUNCTION__) . ':</label>';
    $html .= '<select id="affiliate" name="af" class="form-control selector selected5"><option value="" selected>--select-' . __FUNCTION__ . '--</option>';
    foreach ($all as $item):

        $selected = '';
        if ($in === $item->ID) {
            $selected = 'selected';
        }
        $html .= '<option value="' . $item->ID . '" ' . $selected . '>' . $item->name . " ($item->company)" . '</option>';
    endforeach;
    $html .= '</select></div>';

    return $html;
}

function admin()
{
    $cObj = new Cache(Cache::STORE);

    $key = CACHE_KEY . __FUNCTION__;

    if (!($cache = $cObj->retrieve($key))) {
        $g = new Admin();
        $g->setProperties([
            'active' => 1
        ]);
        $g->setQueryParameters($g, ['ID', 'name']);
        $all = $g->query();
        $cache = json_encode($all);

        $cObj->enter($key, $cache);
    }

    $all = json_decode($cache);

    $in = Request::getField('am', 'GET');
    $html = '<div class="form-group"><label>' . ucfirst(__FUNCTION__) . ':</label>';
    $html .= '<select name="am" class="form-control selector selected5"><option value="" selected>--select-' . __FUNCTION__ . '--</option>';
    foreach ($all as $item):

        $selected = '';
        if ($in === $item->ID) {
            $selected = 'selected';
        }
        $html .= '<option value="' . $item->ID . '" ' . $selected . '>' . $item->name . '</option>';
    endforeach;
    $html .= '</select></div>';

    return $html;
}

function select2()
{
    return
        '
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    ';
}

function datepicker()
{
    return
        '
    <script src="https://cdn.jsdelivr.net/npm/moment@2.19.2/moment.min.js"></script>
<link href="' . s() . 'assets/css/picker.css" rel="stylesheet"/>
<script src="' . s() . 'assets/js/picker.min.js"></script>
    ';
}

function tableexport()
{
    return
        '
    <link href="' . s() . 'assets/css/tableexport.min.css" rel="stylesheet"/>
    <script src="https://cdn.rawgit.com/eligrey/FileSaver.js/5ed507ef8aa53d8ecfea96d96bc7214cd2476fd2/FileSaver.min.js"></script>
<script type="text/javascript" src="' . s() . 'assets/js/tableexport.min.js"></script>
    ';
}

function ckeditor()
{
    return
        '
    <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    ';
}

function flags()
{
    return
        '
    <link
        rel="stylesheet"
        type="text/css"
        href="//cloud.github.com/downloads/lafeber/world-flags-sprite/flags16.css"
/>
    ';
}

function datatables()
{
    return
        '
<link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet"/>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
    ';
}

function datatables_responsive()
{
    return
        '
    <script src="' . s() . 'assets/js/datatables-responsive.js"></script> 
    ';
}

function highmaps()
{
    return
        '<script src="https://code.highcharts.com/maps/modules/map.js"></script>
    <script src="https://code.highcharts.com/mapdata/custom/world.js"></script>
    ';
}

function s()
{
    return MY_SERVER;
}

function clc()
{
    return 'https://adtrackr.co/c?t=';
}

function tkn($join = '?')
{
    $tkn = '';
    if (C::tknSession()) {
        $tkn = $join . TKN . '=' . $_GET[TKN];
    }
    return $tkn;
}

function tknInp()
{
    if (C::tknSession()) {
        return '<input type="hidden" name="' . TKN . '" value="' . $_GET[TKN] . '" />';
    }
    return '';
}

function dateSpan($t = 'Stats for', $all = false)
{
    $date = Request::getField('date', 'GET');
    $date1 = Request::getField('date1', 'GET');
    $q = Request::getField('q', 'GET');
    $today = $yesterday = '';
    $seven = $previous = '';
    $month = $last = '';
    $custom = '';
    switch ($q):
        case 't':
            $today = 'selected';
            break;
        case 'y':
            $yesterday = 'selected';
            break;
        case 'w':
            $seven = 'selected';
            break;
        case 'p':
            $previous = 'selected';
            break;
        case 'm':
            $month = 'selected';
            break;
        case 'l':
            $last = 'selected';
            break;
        case 'c':
            $custom = 'selected';
            break;
        default:
    endswitch;

    $extra = '';
    if ($all) {
        $extra = '<option value="" data-val="_">All</option>';
    }

    $html = '
    <style>.timepicker-picker,.dater{display: none}</style>
    <div class="col-md-2">
                <label>' . $t . ':</label>
                <select id="date_type" class="form-control" name="q">
                    ' . $extra . '
                    <option value="t" data-val="' . date('Ymd') . '" ' . $today . '>Today</option>
                    <option value="y" data-val="' . date('Ymd', strtotime('-1 day')) . '" ' . $yesterday . '>Yesterday</option>
                    <option value="w" data-val="' . date('Ymd', strtotime('-6 days')) . '_' . date('Ymd') . '" ' . $seven . '> Last 7 days</option>
                    <option value="p" data-val="' . date('Ymd', strtotime('-13 days')) . '_' . date('Ymd', strtotime('-7 days')) . '" ' . $previous . '>Last Week</option>
                    <option value="m" data-val="' . date('Ym') . '" ' . $month . '>This Month</option>
                    <option value="l" data-val="' . date('Ym', strtotime('last month')) . '" ' . $last . '>Last Month</option>
                    <option value="c" data-val="" ' . $custom . '>Custom</option>
                </select>
            </div>
            <div class="w-100 dater"></div>
            <div class="col-md-3 dater">
                <label>Start Date:</label>
                <input id="date0" class="form-control" type="text" name="date" value="' . $date . '" placeholder="Date Start: YYYYMMDD">
            </div>
            <div class="col-md-3 dater">
                <label>End Date:</label>
                <input id="date1" class="form-control" type="text" name="date1" value="' . $date1 . '" placeholder="Date End: YYYYMMDD">
            </div>
    ';

    $script = '<script>
    $("#date0,#date1").datetimepicker({format:"YYYYMMDD"});
    $("#date_type").on("change",function (e) {
        var vu = $(this).find(":selected").data("val")+"", $v = vu.split("_"),
            $date0 = $("#date0"),
            $date1 = $("#date1"),
            $dater = $(".dater");

        if ($v.length === 2){
            $date0.val($v[0]);
            $date1.val($v[1]);
        }else{
            $date0.val($v);
            $date1.val("");
        }

        if (!vu){
            $dater.slideDown();
        }else{
            $dater.slideUp();
        }
    });
    </scr' . 'ipt>';
    if ($custom === 'selected') {
        $script .= '<script>$(".dater").slideDown().find("#date0").attr("required","true");</scr' . 'ipt>';
    }

    return $html . $script;
}

function timeLeft(): int
{
    $lastDay = date("d", strtotime('last day of this month'));
    $today = date('d');
    $daysLeft = $lastDay - $today;
    $timeLeft1 = $daysLeft * 24 * 60 * 60;

    $secsNow = date("s");
    $secsLeft = 60 - $secsNow;
    $minsNow = date("i");
    $minsLeft = 60 - $minsNow;
    $hrsNow = date("H");
    $hrsLeft = 24 - $hrsNow;
    $timeLeft2 = ($hrsLeft * 60 * 60) + ($minsLeft * 60) + $secsLeft;

    $totalTimeLeft = $timeLeft1 + $timeLeft2;

    return $totalTimeLeft;
}

function checkUnique($table, $entity)
{
    foreach ($table as $v) {
        foreach ($v as $key => $value) {
            if ($value === $entity) {
                return false;
            }
        }
    }
    return true;
}