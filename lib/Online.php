<?php

use helper\RedisFy;

/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/5/18
 * Time: 5:01 PM
 */
class Online
{

    public function __construct()
    {
        $redis = new RedisFy();
        $redis->remove($redis->getStart('Resource'));
    }

    public function unseal($socketData): string
    {
        $length = ord($socketData[1]) & 127;
        if ($length === 126) {
            $masks = substr($socketData, 4, 4);
            $data = substr($socketData, 8);
        } elseif ($length === 127) {
            $masks = substr($socketData, 10, 4);
            $data = substr($socketData, 14);
        } else {
            $masks = substr($socketData, 2, 4);
            $data = substr($socketData, 6);
        }
        $socketData = '';
        $size = strlen($data);
        for ($i = 0; $i < $size; ++$i) {
            $socketData .= $data[$i] ^ $masks[$i % 4];
        }
        return $socketData;
    }

    public function seal($socketData): string
    {
        $b1 = 0x80 | (0x1 & 0x0f);
        $length = strlen($socketData);

        if ($length <= 125)
            $header = pack('CC', $b1, $length);
        elseif ($length > 125 && $length < 65536)
            $header = pack('CCn', $b1, 126, $length);
        elseif ($length >= 65536)
            $header = pack('CCNN', $b1, 127, $length);
        return $header . $socketData;
    }

    public function doHandshake($received_header, $client_socket_resource, array $host): void
    {
        $headers = array();
        $lines = preg_split("/\r\n/", $received_header);
        foreach ($lines as $line) {
            $line = rtrim($line);
            if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                $headers[$matches[1]] = $matches[2];
            }
        }

        $secKey = $headers['Sec-WebSocket-Key'];
        $secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
        $buffer = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
            "Upgrade: websocket\r\n" .
            "Connection: Upgrade\r\n" .
            'WebSocket-Origin: ' . $host['name'] . "\r\n" .
            'WebSocket-Location: ws://' . $host['name'] . "/myws\r\n" .
            "Sec-WebSocket-Accept:$secAccept\r\n\r\n";
        socket_write($client_socket_resource, $buffer, strlen($buffer));
    }

    public function close($ip): void
    {
        $redis = new RedisFy();
        $redis->remove($ip);
    
    echo "\n-------__CLOSED__-------\n";

        $this->show($redis);
    }

    public function receiver($ip, $name): void
    {
        $redis = new RedisFy();
    
    echo "\n $name \n";

        $name = json_decode($name);

        $redis->setKV($ip, '{"name":"' . $name->n . '", "time":' . time() . ', "admin": "' . $name->a . '"}');

        $this->show($redis);
    }

    private function show(RedisFy $redis): void
    {
     global $clientSocketArray; 
    
        $now = time();
        $send = [];

        $all = $redis->getStart('Resource');
        print_r($all);
    	print_r($clientSocketArray);

        foreach ($all as $k => $v) {

            $obj = json_decode($redis->getV($v));
          
               $send[$k]['n'] = $obj->name;
               $send[$k]['a'] = $obj->admin;

        }

        $message = $this->seal(base64_encode(json_encode($send)));

      
        $messageLength = strlen($message);
        foreach ($clientSocketArray as $clientSocket) {
            @socket_write($clientSocket, $message, $messageLength);
        }
    }
}