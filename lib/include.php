<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/17/17
 * Time: 7:45 PM
 */

## PATHS
define('MY_BASE', dirname(__DIR__) . '/');
define('LOG_PATH','./web/log/');
define('INDEX', './web/index/');

## DEBUG
define('LOG', 1);
define('BR', '<br/>');

## SESSIONS
define('SESSION',md5(date('Ymd')));
define('TKN',md5(date('Ymd')));
define('COMMISSION', 0.8);

session_name('hell');
session_start();

/**
 * @param $classlogin
 */
function autoload($class)
{
    [$dir, $name] = explode("\\", $class, 2);
    $file = dirname(__DIR__) . "/$dir/$name.php";
    if (is_file($file))
        include_once $file;
}
spl_autoload_register('autoload');

require_once dirname(__DIR__) . '/config/local.php';

if (defined('ENVIRONMENT')) {
    ini_set('display_errors', 1);
    switch (ENVIRONMENT) {
        case 'DD':
            error_reporting(E_ALL);
            break;
        case 'TT':
            error_reporting(E_ERROR);
            break;
        case 'D':
            error_reporting(E_ALL);
            break;
        case 'T':
            error_reporting(E_ERROR);
            break;
        case 'P':
            ini_set('display_errors', 0);
            error_reporting(0);
            break;
        default:
            die('The application environment is not set correctly.');
    }
}

include 'functions.php';
//require_once '../vendor/autoload.php';
