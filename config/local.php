<?php

## MYSQL
define('MYSQL_USER', 'root');
define('MYSQL_PSWD', '');

## MONGO
define('MONGO_DSN', '');

## Other
define('SQL_DEBUG', 1);
define('MY_SERVER', 'http://localhost/vt/');

define('ENVIRONMENT', 'TT');
