require 'capistrano/ext/multistage'
set :application, "distro"
set :repository, "git@bitbucket.org:reoxey/#{application}.git"
set :user, "root"
set :use_sudo, false
#set :keep_releases, 3
set :deploy_via, :remote_cache
set :copy_exclude, [ '.git' ]
set :normalize_asset_timestamps, false

set :stages, ["pm", "pre"]
set :default_stage, "pre"

set :shared_children, fetch(:shared_children, []).push('images');

logger.level = Logger::MAX_LEVEL