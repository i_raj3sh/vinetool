server "mine", :app, :web, :db, :primary => true
set :deploy_to, "/web/pre_#{application}"
set :branch, fetch(:branch, "dev")

after "deploy", "deploy:configs"
after "deploy", "deploy:fix_permissions"
after "deploy", "deploy:cleanup"

namespace :deploy do
	task :configs, :roles => :web do
  		run "cp /web/pre_#{application}/current/config/develop.php /web/pre_#{application}/current/config/local.php"
	end
end

namespace :deploy do
	task :fix_permissions, :roles => :web do
  		run "chown -R apache:apache /web/pre_#{application}"
	end
end

role :web, "mine"                          # Your HTTP server, Apache/etc
role :app, "mine"                          # This may be the same as your `Web` server
role :db,  "mine"