<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 25/1/18
 * Time: 10:56 PM
 */

namespace controller;


use helper\Mongo;
use helper\Request;
use helper\Template;
use helper\Tool;
use model\ActivityLog_;
use model\Visit;

class TestAffiliateLink extends Controller
{
    public static function index(){
        $template = new Template('testlink');
        $template->setTemplateField('title','Test Affiliate Link');
        $template->setTemplateField('html','index.html.php');

        if (($l = Request::getField('l','POST')) && ($o = Request::getField('t','POST'))){

            $h = date('YmdH');

            // $got = Tool::hitCurl($l, [
            //     'ct' => 'plain',
            //     'fl' => true
            // ]);
            //

            $db = new Mongo([
                'db' => 'db_dump',
                'server' => 'traffic',
                'timeout' => 900000,
                'authentication' => 'db_dump'
            ]);

            $id = $db->query(new Visit(), ['tid' => $o], ['projection' => ['_id' => 1, 'subID' => 1, 'source' => 1, 'subSource' => 1, 'appName' => 1, 'IDFA' => 1, 'GAID' => 1], 'sort' => ['_id' => -1], 'limit' => 1]);

            if (!$id) {
                $postback['er'] = 'Something went wrong. Please check if the TID is related to this test link';
            } else {
                $iterator = $id->toArray();
                if (empty($iterator)) {
                    $postback['er'] = 'No click found. Please check if the TID is related to this test link';
                } else {

                    $col = $iterator[0];
                    $id = $col->{'_id'}->__toString();
                    $postback['clickid'] = $col->{'subID'};
                    $postback['sub_affid'] = $col->{'source'};
                    $postback['sub_sourceid'] = $col->{'subSource'};
                    $postback['app_name'] = $col->{'appName'};
                    $postback['idfa'] = $col->{'IDFA'};
                    $postback['gaid'] = $col->{'GAID'};

                    $arg = [];
                    $arg[0] = [
                        'AID' => self::getSession(),
                        'name' => self::getSessionName(),
                        'type' => 'OPERATION',
                        'date' => (int)date('Ymd'),
                        'time' => time(),
                        'extra' => [
                            'what' => "tested Affiliate test link $l"
                        ]
                    ];
                    $db = new Mongo([
                        'db' => 'db_dump',
                        'server' => 'localhost',
                        'timeout' => 900000,
                        'authentication' => 'admin'
                    ]);
                    try {
                        $db->insert(new ActivityLog_(), $arg);
                    } catch (\Exception $e) {
                    }

                    if ($postback['clickid']) {
                        $today = date('YmdH');
                        $id = $today . '-' . $id;
                        $postback['pb'] = 'http://postback.vinetool.com/?click=' . $id . '&cam={}&payout={}&test=true';
                    } else {
                        $postback['er'] = 'Click id not sent in the TL. Can\'t fire postback.';
                    }
                }
            }

            $template->render($postback);
        } elseif ($pb = Request::getField('pb','POST')){

            $got = Tool::hitCurl($pb, [
                'ct' => 'plain',
                'fl' => true
            ]);

            $form['pb'] = $pb;
            $form['msg'] = 'Postback fired. See response below.';
            $form['res'] = $got;

            $template->render($form);

        } else
            $template->render();
    }

}