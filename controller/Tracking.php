<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/11/17
 * Time: 2:21 PM
 */

namespace controller;

use helper\Request;

class Tracking extends Controller
{
    public static function index():void{
        if ($oid = Request::getField('oid')){
            $sub1 = Request::getField('sub1');
            $sub2 = Request::getField('sub2');

            $our = clc();
            $link = "Location: $our$oid&clickid=$sub1&sub_affid=$sub2";

            header($link);
        }
    }
}