<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 8:29 AM
 */

namespace controller;


use helper\Mongo;
use helper\MyException;
use helper\Mysql;
use helper\Paginator;
use helper\Request;
use helper\Template;
use model\ActivityLog_;
use model\Network as N;

class OfferBlock extends Controller
{

    public static function index(){

        $template = new Template('offerblock');
        $template->setTemplateField('title', 'Blocked by offer');
        $template->setTemplateField('html', 'offer_index.html.php');

        $str = 'WHERE 1';
        if ($af = Request::getField('af','GET')){
            $str .= " AND affiliate_ID = $af";
        }
        if ($of = Request::getField('of','GET')){
            $str .= " AND offers_ID = $of";
        }

        $sql = 'SELECT a.email affiliate,o.name offer,bo.* FROM vine_ad_db.affiliate a 
                JOIN vine_ad_db.blocked_offer_rules bo ON a.ID = bo.affiliate_ID
                JOIN vine_ad_db.offers o ON o.ID = bo.offers_ID '.$str;

        $paginator = new Paginator();
        $paginator->setDate(50);
        $paginator->setLink();
        $paginator->total = Mysql::numRows($sql,[]);
        $paginator->paginate();

        $skip = $paginator->getStart();

        $sql .= " LIMIT $skip, $paginator->itemsPerPage";

        $get['table'] = Mysql::select($sql, (new N())->getMyClass());
        $get['total'] = $paginator->total;

        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();

        $template->render($get);
    }

    public static function edit(){
        $template = new Template('offerblock');
        $template->setTemplateField('title', 'Blocked by offer');
        $template->setTemplateField('html', 'offer_edit.html.php');

        $template->render();
    }

    public static function add(){
        if (Request::getRequiredField('oblocked', 'POST')) {
            $af = Request::getRequiredField('af', 'POST');
            $of = Request::getRequiredField('of', 'POST');

            $args = [
                'affiliate_ID=?' => $af,
                'offers_ID=?' => $of,
                'at=?' => time()
            ];
            $loc = 'offerBlock/edit';

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'INSERT',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => " affiliateID[$af] blocked by offer[$of]"
                ]
            ];

            if (!Mysql::put('INSERT','blocked_offer_rules',$args)) {
                MyException::setSessionError('Something went wrong.');
                self::redirect($loc);
            }

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Successful');
            self::redirect('offerBlock');
        }
    }

    public static function del(){
        if ($id = Request::getRequiredField('id','GET')) {

            [$af,$of] = explode('_',$id);
            $sql = "DELETE FROM blocked_offer_rules WHERE affiliate_ID = $af AND offers_ID = $of";
            if (!Mysql::delete($sql)){
                MyException::setSessionError('Delete failed');
            }else {
                MyException::setSessionSuccess('Delete successful');

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' =>  "affiliateID[$af] unblocked for offer[$of]"
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }
            }
            self::redirect('offerBlock');
        }
    }

    public static function isBlocked($id): bool
    {
        $sql = 'SELECT * FROM vine_ad_db.blocked_offer_rules WHERE offers_ID = :o AND affiliate_ID = :a';

        $res = Mysql::select($sql, (new N())->getMyClass(), [
            ':o' => $id,
            ':a' => self::getSession()
        ]);

        if (empty($res)) {
            return false;
        }
        return true;
    }
}