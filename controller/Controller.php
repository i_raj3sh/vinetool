<?php

/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/17/17
 * Time: 7:43 PM
 */

namespace controller;

abstract class Controller
{
    public const ADMIN = 'ADMIN';
    public const AFFILIATE = 'AFFILIATE';

    public static function getSession()
    {

        if (isset($_SESSION[SESSION])) {
            if (self::tknSession()) {
                return $_GET[TKN];
            }
            return $_SESSION[SESSION];
        }
        return false;
    }

    public static function getSessionType()
    {
        if (!empty($_GET[TKN])) {
            return self::AFFILIATE;
        }
        if (($id = self::getSession()) !== false) {
            return $_SESSION[md5($id)];
        }
        return false;
    }

    public static function getSessionName()
    {
        if (!empty($_GET[TKN])) {
            $a = new \model\Affiliate();
            $a->setProperties([
                'ID' => $_GET[TKN]
            ]);
            $a->setQueryParameters($a, ['name']);
            return $a->one()->name;
        }
        if (($id = self::getSession()) !== false) {
            return $_SESSION[md5($id . '_XXX_')];
        }
        return false;
    }

    public static function getAdminType()
    {
        if (($id = self::getSession()) !== false) {
            return $_SESSION[md5($id . '__TYPE__')];
        }
        return false;
    }

    public static function getAccountManager()
    {
        $a = new \model\Affiliate();
        $d = new \model\Admin();
        $a->setProperties([
            'affiliate.ID' => self::getSession()
        ]);
        $a->setQueryParameters($a, ['affiliate.ID']);
        $a->setQueryParameters($d, ['admin.*']);
        return $a->one();
    }

    public static function tknSession(): bool
    {
        if (!empty($_GET[TKN])) {
            if ($_SESSION[md5($_SESSION[SESSION])] === self::AFFILIATE) {
                self::redirect(false);
            }
            return true;
        }
        return false;
    }

    public static function redirect($loc = ''): void
    {
        if ($loc === true) {
            // \var_dump('before everything');
            echo 'direct' . $_SESSION['DIRECT'];
            // die();
            var_dump($_SESSION['DIRECT']) ;
            if (isset($_SESSION['DIRECT']) && $_SESSION['DIRECT'] != 1) {
                $loc = 'Location: ' . MY_SERVER . ltrim($_SESSION['DIRECT'], '/');
                unset($_SESSION['DIRECT']);
                header($loc);
                die;
            } else if (isset($_SERVER['HTTP_REFERER'])) {
                unset($_SESSION['DIRECT']);
                $loc = 'Location: ' . $_SERVER['HTTP_REFERER'];
                header($loc);
                die;
            } else {
                unset($_SESSION['DIRECT']);
                $loc = 'Location: ' . MY_SERVER;
                header($loc);
                die;
            }
        }
        if ($loc === false) {
            $loc = 'Location:' . s();
            header($loc);
            die;
        }

        if (!empty($_GET[TKN])) {
            $link = MY_SERVER . self::getJoin($loc) . TKN . '=' . $_GET[TKN];
        } else {
            $link = MY_SERVER . $loc;
        }
        $loc = 'Location: ' . $link;
        header($loc);
        die;
    }

    public static function href($link = ''): void
    {
        if (!empty($_GET[TKN])) {
            echo MY_SERVER . self::getJoin($link) . TKN . '=' . $_GET[TKN];
        } else echo MY_SERVER . $link;
    }

    private static function getJoin($l): string
    {
        if (false !== strpos($l, '?')) {
            return $l . '&';
        }
        return $l . '?';
    }

    public static function verifyAccess(string $controller): void
    {

        $type = self::getSessionType();

        if ($type === self::AFFILIATE) {
            switch ($controller):
                case 'Dashboard':
                case 'Home':
                case 'Account':
                case 'Offers':
                case 'MyOffers':
                case 'Postback':
                case 'ApiKey':
                case 'Invoice':
                case 'OfferStats':
                case 'DailyStats':
                case 'MonthlyStats':
                case 'GeoStats':
                case 'OsStats':
                case 'ConversionsStats':
                case 'Async':
                case 'Tracking':
                case 'Api':
                case 'Billing':
                case 'PostbackLogs':
                case 'Login':
                case 'SourceStats':
                    return;
                default:
                    self::redirect();
            endswitch;
        }
    }
}
