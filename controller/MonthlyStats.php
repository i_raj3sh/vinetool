<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 9/30/18
 * Time: 9:47 AM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\HourlyStats;

class MonthlyStats extends Controller
{
    public static function index(): void
    {

        $template = new Template('stats');
        $template->setTemplateField('title', 'Monthly Stats');
        $template->setTemplateField('html', 'monthly.html.php');

        $params = [];
        if (!($year = Request::getField('date', 'GET'))) {
            $year = date('Y');
        }

        $query = "month_ >= ${year}01 AND month_ <= ${year}12";

        Mysql::setDbName(Mysql::STATS);
        $gh = new HourlyStats();

        if ($of = Request::getField('of', 'GET')) {
            $params['offers_ID'] = $of;
        }

        if (self::getSessionType() === 'AFFILIATE') {
            $params['affiliate_ID'] = self::getSession();
        } else {
            if ($af = Request::getField('af', 'GET')) {
                $params['affiliate_ID'] = $af;
            }

            if ($nw = Request::getField('nw', 'GET')) {
                $params['network_ID'] = $nw;
            }
        }
        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['*', 'SUM(dropped) lost', 'SUM(clicks) visit', 'SUM(sales) conver', 'SUM(payout) push', 'SUM(charge) pull'], '', 'GROUP BY month_', 'ORDER BY month_');
        $gh->appendQuery($query);
        $stats = $gh->query();

        Mysql::setDbName();

        $template->render($stats);
    }

}