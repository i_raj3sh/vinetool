<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 17/3/18
 * Time: 1:17 PM
 */

namespace controller;


use helper\Form;
use helper\Mongo;
use helper\MyException;
use helper\Request;
use helper\Template;
use model\ActivityLog_;

class ApiKey extends Controller
{
    public static function index():void {
        $template = new Template('apikey');
        $template->setTemplateField('title','API');
        $template->setTemplateField('html','index.html.php');

        if (self::getSessionType() === 'AFFILIATE'){
            $id = self::getSession();
        }else {
            $id = Request::getField('id', 'GET');
        }

        $a = new \model\Affiliate();
        $a->setProperties([
           'ID' => $id
        ]);
        $a->setQueryParameters($a);

        $form = new Form();
        $get = $a->one();
        $form->setForm($get);

        $template->render($form);
    }

    public static function add():void {
        if (Request::getRequiredField('generate','POST')){
            $id = Request::getRequiredField('id','POST');

            $a = new \model\Affiliate();
            $args = [
                'ID' => $id,
                'akey' => md5(time()),
            ];
            $a->setProperties($args);
            if (!$a->update($a)){
                MyException::setSessionError('Something went wrong.');
                self::redirect(true);
            }
            $arg = [];
            $arg[0] = [
                'name' => self::getSessionName(),
                'type' => 'UPDATE',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => 'API key of Affiliate #' . $id
                ]
            ];
            if (self::getSessionType() === 'ADMIN') {
                $arg[0]['AID'] = self::getSession();
            } else {
                $arg[0]['PID'] = self::getSession();
            }

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }
            MyException::setSessionSuccess('Successful');

            self::redirect(true);
        }
    }
}