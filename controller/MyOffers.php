<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/13/17
 * Time: 9:48 PM
 */

namespace controller;

use helper\Paginator;
use helper\Request as R;
use helper\Template;

class MyOffers extends Controller
{

    public static function index(){

        if (self::getSessionType() === 'ADMIN'){
            self::redirect();
        }

        $template = new Template('myoffers');
        $template->setTemplateField('title','My Live Offers');
        $template->setTemplateField('html','index.html.php');

        $name = R::getField('n','GET');

        $ao = new \model\ApproveOffers();
        $o = new \model\Offers();
        $a = new \model\Affiliate();

            $args = [
                'affiliate_ID' => self::getSession(),
                'status' => 1,
                'affiliate.active' => 1,
                'offers.active' => 1
            ];

        if ($id = R::getField('i', 'GET')) {
            $args['offers.ID'] = $id;
        }
        if ($tid = R::getField('t', 'GET')) {
            $args['affiliate_offers.ID'] = $tid;
        }

        $o->setProperties($args);
        $ao->setQueryParameters($o,['offers.*']);
        $ao->setQueryParameters($ao,['status','percentage']);
        $ao->setQueryParameters($a,['affiliate.ID AID','affiliate.name pub','affiliate.active state']);
        $ao->appendQuery('offers.name LIKE "%'.$name.'%"');

        $paginator = new Paginator();
        $paginator->setLink();
        $paginator->total = $ao->count();
        $paginator->paginate();

        $start = $paginator->getStart();

        $ao = new \model\ApproveOffers();
        $o = new \model\Offers();
        $a = new \model\Affiliate();

        $o->setProperties($args);
        $ao->setQueryParameters($o,['offers.*']);
        $ao->setQueryParameters($ao, ['affiliate_offers.ID TID', 'status', 'percentage', 'cap']);
        $ao->setQueryParameters($a,['affiliate.ID AID','affiliate.name pub','affiliate.active state']);
        $ao->appendQuery('offers.name LIKE "%'.$name.'%"'
            ." ORDER BY offers.ID DESC LIMIT $start, $paginator->itemsPerPage");

        $get['table'] = $ao->query();
        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();

        $template->render($get);
    }

}