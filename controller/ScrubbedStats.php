<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 24/2/18
 * Time: 11:59 AM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\Scrubbed;

class ScrubbedStats extends Controller
{
    public static function index(): void
    {

        $template = new Template('stats');
        $template->setTemplateField('title', 'Scrub Stats');
        $template->setTemplateField('html', 'scrubbed.html.php');


        if (!($date = Request::getField('date', 'GET'))) {
            $date = date('Ymd');
        }

        $join = 'WHERE';
        $sql = 'SELECT * FROM scrubbed s ';

        if (self::getSessionType() === 'AFFILIATE') {
            $sql .= $join . ' s.affiliate_ID = ' . self::getSession();
            $join = ' AND';
        } else {
            if ($af = Request::getField('af', 'GET')) {
                $sql .= $join . ' s.affiliate_ID = ' . $af;
                $join = ' AND';
            }

            if ($nw = Request::getField('nw', 'GET')) {
                $sql .= $join . ' s.network_ID = ' . $nw;
                $join = ' AND';
            }
        }
        if ($of = Request::getField('of', 'GET')) {
            $sql .= $join . ' s.offers_ID = ' . $of;
            $join = ' AND';
        }

        if ($date1 = Request::getField('date1', 'GET')) {
            $sql .= $join . ' day_sale >= :d1 AND day_sale <= :d2';
            $args = [':d1' => $date, ':d2' => $date1];
        } else {
            if (\strlen($date) === 8) {
                $sql .= $join . ' day_sale = :d';
                $args = [':d' => $date];
            } else {
                if (date('Ym') === $date) {
                    $sql .= $join . ' day_sale >= :d1 AND day_sale <= :d2';
                    $args = [':d1' => $date . '01', ':d2' => $date . date('d')];
                } else {
                    $sql .= $join . ' day_sale >= :d1 AND day_sale <= :d2';
                    $args = [':d1' => $date . '01', ':d2' => $date . '31'];
                }
            }
        }
        $sql .= ' AND affiliate_ID != 0';
        Mysql::setDbName(Mysql::STATS);
        $stats = Mysql::select($sql, (new Scrubbed())->getMyClass(), $args);
        Mysql::setDbName();

        $template->render($stats);
    }
}