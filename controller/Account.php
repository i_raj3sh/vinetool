<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/13/17
 * Time: 10:30 PM
 */

namespace controller;

use helper\Form;
use helper\Mongo;
use helper\MyException;
use helper\Request;
use helper\Template;
use model\ActivityLog_;

class Account extends Controller
{

    public static function index(){

        if (self::getSessionType() === 'ADMIN'){
            self::redirect();
        }

        $template = new Template('account');
        $template->setTemplateField('title','Edit Account Details');
        $template->setTemplateField('html','index.html.php');

        $form = new Form();
            $a = new \model\Affiliate();
            $a->setProperties([
                'ID' => self::getSession()
            ]);
            $a->setQueryParameters($a);
            $form->setForm($a->one());
        $template->render($form);
    }

    public static function add(){

        if (self::getSessionType() === 'ADMIN'){
            self::redirect();
        }

        if (Request::getRequiredField('account','POST')){
            $name = Request::getRequiredField('name','POST');
            $username = Request::getRequiredField('username','POST');
            $email = Request::getRequiredField('email','POST');
            $password = Request::getRequiredField('password','POST');
            $skype = Request::getField('skype','POST');
            $company = Request::getField('company','POST');
            $website = Request::getField('website','POST');
            $country = Request::getRequiredField('country','POST');

            $password = hash('sha512', $password);

            $a = new \model\Affiliate();
            $args = [
                'ID' => self::getSession(),
                'name' => $name,
                'username' => $username,
                'email' => $email,
                'password' => $password,
                'skype' => $skype,
                'company' => $company,
                'website' => $website,
                'country' => $country,
            ];

            $a->setProperties($args);
            if (!$a->update($a)){
                MyException::setSessionError('Something went wrong.');
            }
            else MyException::setSessionSuccess('Successful');

            $arg = [];
            $arg[0] = [
                'PID' => self::getSession(),
                'name' => $name . " ($company) ",
                'type' => 'UPDATE',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => 'account details'
                ]
            ];
            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            self::redirect('account');
        }
    }

}
