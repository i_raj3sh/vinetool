<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 28/4/18
 * Time: 9:04 AM
 */

namespace controller;


use helper\Mongo;
use helper\Request as R;
use helper\Template;
use model\ActivityLog_;

class ActivityLog extends Controller
{

    public static function index(): void
    {
        $template = new Template('logs');
        $template->setTemplateField('title', 'Activity Logs');
        $template->setTemplateField('html', 'activity_log_index.html.php');

        $args = [];
        if ($af = R::getField('af', 'GET')) {
            $args['PID'] = $af;
        }
        if ($am = R::getField('am', 'GET')) {
            $args['AID'] = $am;
        }
        if ($d = R::getField('date', 'GET')) {
            $args['date'] = (int)$d;
        } else {
            $args['date'] = (int)date('Ymd');
        }
        if ($t = R::getField('t', 'GET')) {
            $args['type'] = $t;
        }

        $db = new Mongo();
        $get = $db->query(new ActivityLog_(), $args, ['sort' => ['_id' => -1]]);

        $template->render($get);
    }
}