<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 8:29 AM
 */

namespace controller;


use helper\Mongo;
use helper\MyException;
use helper\Mysql;
use helper\Paginator;
use helper\Request;
use helper\Template;
use model\ActivityLog_;
use model\Network as N;

class NetworkBlock extends Controller
{

    public static function index(){

        $template = new Template('networkblock');
        $template->setTemplateField('title', 'Blocked by network');
        $template->setTemplateField('html', 'network_index.html.php');

        $str = 'WHERE 1';
        if ($af = Request::getField('af','GET')){
            $str .= " AND affiliate_ID = $af";
        }
        if ($nw = Request::getField('nw','GET')){
            $str .= " AND network_ID = $nw";
        }

        $sql = 'SELECT a.email affiliate,n.email network,bn.* FROM vine_ad_db.affiliate a 
                JOIN vine_ad_db.blocked_network_rules bn ON a.ID = bn.affiliate_ID
                JOIN vine_ad_db.network n ON n.ID = bn.network_ID '.$str;

        $paginator = new Paginator();
        $paginator->setDate(50);
        $paginator->setLink();
        $paginator->total = Mysql::numRows($sql,[]);
        $paginator->paginate();

        $skip = $paginator->getStart();

        $sql .= " LIMIT $skip, $paginator->itemsPerPage";

        $get['table'] = Mysql::select($sql, (new N())->getMyClass());
        $get['total'] = $paginator->total;

        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();

        $template->render($get);
    }

    public static function edit(){
        $template = new Template('networkblock');
        $template->setTemplateField('title', 'Blocked by network');
        $template->setTemplateField('html', 'network_edit.html.php');

        $template->render();
    }

    public static function add(){
        if (Request::getRequiredField('nblocked', 'POST')) {
            $af = Request::getRequiredField('af', 'POST');
            $nw = Request::getRequiredField('nw', 'POST');

            $args = [
                'affiliate_ID=?' => $af,
                'network_ID=?' => $nw,
                'at=?' => time()
            ];
            $loc = 'networkBlock/edit';

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'INSERT',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => " affiliateID[$af] blocked by networkID[$nw]"
                ]
            ];

            if (!Mysql::put('INSERT','blocked_network_rules',$args)) {
                MyException::setSessionError('Something went wrong.');
                self::redirect($loc);
            }

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Successful');
            self::redirect('networkBlock');
        }
    }

    public static function del(){
        if ($id = Request::getRequiredField('id','GET')) {

            [$af,$nw] = explode('_',$id);
            $sql = "DELETE FROM blocked_network_rules WHERE affiliate_ID = $af AND network_ID = $nw";
            if (!Mysql::delete($sql)){
                MyException::setSessionError('Delete failed');
            }else {
                MyException::setSessionSuccess('Delete successful');

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' =>  "affiliateID[$af] unblocked for networkID[$nw]"
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }
            }
            self::redirect('networkBlock');
        }
    }
}