<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/2/18
 * Time: 12:44 PM
 */

namespace controller;


use helper\LogTraffic;
use helper\Mysql;
use helper\Request;
use model\Click;
use model\Passback;
use model\ReRoute;
\define('START',microtime(true));

class Cb extends Controller{

    public static function index(){

        if ($t = Request::getField('t')) {

            $log = new LogTraffic('passback_click.log');

            $h = new Click();
            $h->setProperties([
               'ID' => $t
            ]);
            $h->setQueryParameters($h);
            $click = $h->one();

            $tid = $click->affiliate_offers_ID;
            $dev = $click->device;
            $geo = $click->country;

            $p = new Passback();
            $p->setProperties([
                'OS' => $dev,
                'geo'=> $geo
            ]);
            $p->setQueryParameters($p);
            $get = $p->one();

            if ($get){
                Mysql::put('update','passback',['count=count+1'=>''],'WHERE OS = "'.$dev.'" AND geo = "'.$geo.'"');
            }else{
                $pc = new Passback();
                $pc->setProperties([
                    'OS' => $dev,
                    'geo'=> $geo,
                    'count' => 1
                ]);
                $pc->insert($pc);
                die;
            }

            $rand = random_int(1,100);

            $r = new ReRoute();
            $r->setProperties([
                'passback_ID' => $get->ID
            ]);
            $r->setQueryParameters($r,['*'],'','','ORDER BY weight');
            $out = $r->query();

            $l = 0;
            $tot = '';
            foreach ($out as $v){
                if ($rand > $l && $rand <= ($v->weight+$l)){
                    $tot = $v->tot;
                }
                $l += (int)$v->weight;
            }

            if ($l){

                $log->setLogData($tot,'[U]','blue');
                $log->setLogData($geo,'[G]','green');
                $log->setLogData($dev,'[D]','cyan');
                $log->setLogData($tid,'[T]','yellow');
                $log->setLogData($rand,'[R]','magenta');
                $log->setLogTime(START,'[L]','bold');
                $log->writeLog();

                $loc = 'Location: '.$tot;
                header($loc);
            }
        }
    }
}