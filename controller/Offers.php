<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/9/17
 * Time: 3:19 PM
 */

namespace controller;

use helper\Form;
use helper\LogTraffic;
use helper\Mailer;
use helper\Mongo;
use helper\MyException;
use helper\Mysql;
use helper\Paginator;
use helper\Request as R;
use helper\Template;
use helper\Tool;
use model\ActivityLog_;
use model\Country;
use model\Creative;
use model\Model;
use model\Offers as O;

class Offers extends Controller
{
    public static function index(): void
    {

        $template = new Template('offers');
        $template->setTemplateField(Template::TITLE, 'Live Offers on VineTool');
        $template->setTemplateField(Template::HTML, 'index.html.php');

        $isAf = self::getSessionType() === self::AFFILIATE;
        $args = [];
        if ($isAf) {
            $args['active']  = '1';
            $args['private']  = '0';
            $args['charge']['$gt'] = 1;
        }
        $tags = self::setArgs($args);

        if ($isAf) {
            $sql = 'SELECT network_ID FROM blocked_network_rules WHERE affiliate_ID = '.self::getSession();
            $get = Mysql::select($sql, (new O())->getMyClass());
            $args['network_ID']['$nin'] = Tool::hydrate($get);
        }

        $offerObj = new O();
        $db = new Mongo();
        $paginator = new Paginator();
        $paginator->setDate(50);
        $paginator->setLink();
        $paginator->total = $db->command($offerObj, 'count', $args)->n;
        $paginator->paginate();

        $skip = $paginator->getStart();

        $what = 'ID';
        $how = -1;
        if (isset($_GET['sort'])){
            [$what,$how] = explode('_',$_GET['sort']);
            $how = $how === 'a' ? 1 : -1;
        }

        $offers['table'] = $db->query($offerObj, $args, ['sort' => [$what => $how], 'skip' => $skip, 'limit' => $paginator->itemsPerPage]);

        $offers['total'] = $paginator->total;

        $offers['pages'] = $paginator->pageNumbers();
        $offers['perPage'] = $paginator->itemsPerPage();

        $offers['tags'] = $tags;

        $template->render($offers);
    }

    public static function edit()
    {
        $_SESSION['DIRECT'] = str_replace(MY_SERVER,'',$_SERVER['HTTP_REFERER']);

        $template = new Template('offers');
        $template->setTemplateField('title', 'Edit an Offer');
        $template->setTemplateField('html', 'edit.html.php');

        $form = new Form();
        if ($id = R::getField('id', 'GET')) {
            $a = new O();
            $a->setProperties([
                'ID' => $id
            ]);
            $a->setQueryParameters($a);

            $s = "SELECT event_ID FROM vine_ad_db.offers_event WHERE offers_ID = '$id'";
            $res = Mysql::select($s, (new O())->getMyClass());
            $eve = [];
            foreach ($res as $r) {
                $eve[] = $r->event_ID;
            }
            $of = $a->one();
            $of->events = $eve;
            $form->setForm($of);
        }
        $template->render($form);
    }

    public static function add(): void
    {
        if (R::getRequiredField('offers', 'POST')) {
            $name = R::getRequiredField('name', 'POST');
            $advID = R::getRequiredField('advID', 'POST');
            $desc = R::getField('desc', 'POST');
            $caps = R::getRequiredField('caps', 'POST');
            $monthly = R::getRequiredField('monthly_cap', 'POST');
            $charge = R::getRequiredField('charge', 'POST');
            $preview = R::getRequiredField('preview', 'POST');
            $icon = R::getRequiredField('icon', 'POST');
            $tracking = R::getRequiredField('tracking', 'POST');
            $expiry = R::getRequiredField('expiry', 'POST');
            $country = R::getRequiredField('country', 'POST');
            $category = R::getRequiredField('category', 'POST');
            $device = R::getRequiredField('device', 'POST');
            $network = R::getRequiredField('network', 'POST');
            $type = R::getRequiredField('type', 'POST');
            $active = R::getRequiredField('active', 'POST');
            $private = R::getRequiredField('private', 'POST');
            $min_os_ver = R::getRequiredField('min_os_ver', 'POST');
            $device_id = R::getField('device_id', 'POST');
            $incent = R::getField('incent', 'POST');
            $api = R::getField('api', 'POST');
            $fallback = R::getField('fallback', 'POST');
            $events = R::getField('event', 'POST');

            $replace = [
                '-',
                ' ',
                ':'
            ];
            $expiry = str_replace($replace, '', $expiry);

            $loc = 'offers/edit';
            $date = date('Ymd');

            $country = Tool::sort($country);
            $device = Tool::sort($device);
            $category = Tool::sort($category);

            $a = new O();
            $args = [
                'name' => $name,
                'description' => $desc,
                'caps' => $caps,
                'count_daily' => $caps,
                'monthly_cap' => $monthly,
                'count_monthly' => $monthly,
                'charge' => $charge,
                'type' => $type,
                'active' => $active,
                'private' => $private,
                'preview' => $preview,
                'icon' => $icon,
                'min_os_ver' => $min_os_ver,
                'device_id' => $device_id,
                'tracking' => $tracking,
                'date' => date('YmdHis'),
                'joined' => $date,
                'expiry' => $expiry,
                'network_ID' => $network,
                'advID' => $advID,
                'incent' => $incent,
                'cron' => $api,
                'last_updated' => date('YmdHi'),
                'country' => json_encode($country),
                'device' => json_encode($device),
                'category' => json_encode($category),
                'fallback' => $fallback
            ];
            if ($id = R::getField('id', 'POST')) {
                $args['ID'] = $id;
                unset($args['date'], $args['joined'], $args['count_daily'], $args['count_monthly']);
                $loc = 'offers/edit?id=' . $id;

                Mysql::delete("DELETE FROM offers_event WHERE offers_ID = '$id'");

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'UPDATE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Offer #$id $name's details"
                    ]
                ];

            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'INSERT',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Offer $name"
                    ]
                ];

            }
            if (strpos($tracking, '{CLICK}') === false) {
                MyException::setSessionError('Please check tracking url. No macros found.');
                self::redirect($loc);
            }
            $a->setProperties($args);
            if (!($get = $a->save($a))) {
                MyException::setSessionError('Something went wrong.');
                self::redirect($loc);
            }

            if (is_numeric($get) && $get > 1)
                $id = $get;

            $args['ID'] = $id;
            /*self::mongoUpdate($a, $args, [
                'geo' => $geo,
                'cat' => $cat,
                'dev' => $dev
            ]);*/

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            foreach ($events as $e) {
                $args = [
                    'offers_ID=?' => $id,
                    'event_ID=?' => $e
                ];
                Mysql::put('INSERT', 'offers_event', $args);
            }

            Tool::replicateOffers();

            MyException::setSessionSuccess('Successful');
            self::redirect(true);
        }
    }

    public static function del()
    {
        if ($id = R::getRequiredField('id', 'GET')) {

            $a = new O();
            $a->setProperties([
                'ID' => $id
            ]);
            if (!$a->remove()) {
                MyException::setSessionError('Need SUPER privilege to delete.');
            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'Offer #' . $id
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }

                Tool::replicateOffers();
                MyException::setSessionSuccess('Delete successful');
            }

            self::redirect(true);
        }
    }

    public static function request($id = '')
    {
        if (!$id){
            $id = R::getRequiredField('id', R::GET);
            $redirect = true;
        }
        $o = new O();
        $n = new \model\Network();
        $o->setProperties([
            'offers.ID' => $id,
            'tracking' => ''
        ]);
        $o->setQueryParameters($o,['tracking','advID','network_ID']);
        $o->setQueryParameters($n,['approvalLink']);

        $get = $o->one();

        if ($get){
            $getData = Tool::hitCurl($get->approvalLink.$get->advID);
            $getStatus = json_decode($getData);

            $status = 0;

            if($getStatus->approvals->{$get->advID}->Status === 'approved'){
                $status = 1;

                self::offerTrackURL($get->network_ID, $id);
            }

            elseif($getStatus->approvals->{$get->advID}->Status !== 'sent'){
                $log = new LogTraffic('requested.log');
                $log->setLogData($id,'ID');
                $log->setLogData($getData,'RESPONSE');
                $log->writeLog();
                MyException::setSessionError('Unable to request this offer. Please contact Account Manager to request manually.');
                $id = false;
            }

            $hidden = true;
        }

        if ($id) {
            $param = [
                'offers_ID' => $id,
                'affiliate_ID' => self::getSession()
            ];
            $a = new \model\ApproveOffers();
            $a->setProperties($param);
            $a->setQueryParameters($a);
            if ($a->count()) {
                MyException::setSessionError('This offer is already requested or approved.');
            }else{
                if ($hidden){
                    $ao = new \model\ApproveOffers();
                    $ao->setProperties([
                        'offers_ID' => $id,
                        'affiliate_ID' => self::getSession(),
                        'api' => 1,
                        'percentage' => COMMISSION * 100,
                        'added' => date('YmdH'),
                        'requested' => time(),
                        'status' => $status
                    ]);
                    $ao->setQueryParameters($ao);
                    $ao->insert($ao);
                }else{
                    $param['requested'] = time();
                    $param['percentage'] = COMMISSION * 100;
                    if ($a->setProperties($param)->insert($a)) {

                        $arg = [];
                        $arg[0] = [
                            'PID' => self::getSession(),
                            'name' => self::getSessionName(),
                            'type' => 'OPERATION',
                            'date' => (int)date('Ymd'),
                            'time' => time(),
                            'extra' => [
                                'what' => "requested offer #$id"
                            ]
                        ];
                        $db = new Mongo();
                        try {
                            $db->insert(new ActivityLog_(), $arg);
                        } catch (\Exception $e) {
                        }

                        MyException::setSessionSuccess('Request sent');
//                    self::mailRequest(self::getSession(), $id);
                    }else{
                        MyException::setSessionError('Something went wrong.');
                    }
                }
            }
        }
        if($redirect){
            self::redirect(true);
        }
    }

    public static function reqAll(){
        if ($id = R::getRequiredField('ids', R::GET)) {
            $ids = explode(',',$id);
            foreach ($ids as $id){
                self::request($id);
            }
        }
        self::redirect(true);
    }

    public static function pause(): void
    {
        if ($id = R::getRequiredField('id', 'GET')) {
            $offer = new O();
            $offer->setProperties([
                'ID' => $id,
                'active' => 2
            ])
                ->update($offer);

            $arg = [];
            $arg[] = [
                'ID' => $id,
                '$set' => [
                    'active' => '2'
                ]
            ];
            $db = new Mongo();
            try {
                $db->save($offer, $arg);
            } catch (\Exception $e) {
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Paused offer $id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Offer has been paused');
        }
        self::redirect(true);
    }

    public static function resume(): void
    {
        if ($id = R::getRequiredField('id', 'GET')) {
            $offer = new O();
            $offer->setProperties([
                'ID' => $id,
                'active' => 1
            ])
                ->update($offer);

            $arg = [];
            $arg[] = [
                'ID' => $id,
                '$set' => [
                    'active' => '1'
                ]
            ];
            $db = new Mongo();
            try {
                $db->save($offer, $arg);
            } catch (\Exception $e) {
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Resumed offer $id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Offer has been activated');
        }
        self::redirect(true);
    }

    public static function pauseAll()
    {
        if ($ids = R::getRequiredField('ids', 'GET')) {
            Mysql::put('UPDATE', 'offers', ['active=?' => 2], "WHERE ID IN ($ids)");
            Tool::replicateOffers();

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Paused offers [$ids]"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Selected offers paused');
        }
        self::redirect(true);
    }

    private static function mailRequest($aff, $off){

        $af = new \model\Affiliate();
        $af->setProperties([
            'ID' => $aff
        ])->setQueryParameters($af,['name']);

        $affiliate = $af->one();

        $of = new O();
        $of->setProperties([
            'ID' => $off
        ])->setQueryParameters($of,['ID','name','icon']);

        $offer = $of->one();

        $arr = [
            'name' => $affiliate->name,
            'ID' => $offer->ID,
            'icon' => $offer->icon,
            'title' => $offer->name,
        ];

        $m = new Mailer();
        $m
            ->setHeaders()
            ->setMessage('offerRequest',$arr)
            ->send('support@vinetool.com','An offer is requested by '.$affiliate->name);
    }

    public static function view()
    {
        if ($id = R::getRequiredField('id', 'GET')) {
            $template = new Template('offers');

            $form = new Form();
            $o = new O();
            $o->setProperties([
                'ID' => $id
            ]);
            $o->setQueryParameters($o);
            $offers = $o->one();

            $dev = json_decode($offers->device);
            $geo = json_decode($offers->country);

            if (self::getSessionType() !== 'ADMIN') {
                $ao = new \model\ApproveOffers();
                $ao->setProperties([
                    'affiliate_ID' => self::getSession(),
                    'offers_ID' => $offers->ID,
                ]);
                $ao->setQueryParameters($ao);
                $pull = $ao->one();
                if ($pull) {
                    $offers->charge *= $pull->percentage/100;
                    if ($pull->status == 1) {

                        $device_id = '';
                        if (\in_array('iOS', $dev, true)) {
                            $device_id .= '&idfa=';
                        }
                        if (\in_array('Android', $dev, true)) {
                            $device_id .= '&gaid=';
                        }

                        $offers->tracking = clc() . $pull->ID . '&p=' . self::getSession() . '&clickid=[_YOUR_CLICK_ID_]&sub_affid=[_YOUR_SUB_AFFILIATE_ID_]&sub_sourceid=[_YOUR_AFFILIATES_SOURCE_ID_]&app_name=[_APP_NAME_]' . $device_id;
                        $offers->button = '<strong class="badge badge-success">Approved</strong>';

                        $offers->tracking = '<span class="copy_cat"><textarea id="popUrl" rows="3" readonly>' . $offers->tracking . '</textarea><br/><button type="button" id="copyBack" class="btn btn-dark btn-sm" title="Link is copied"><i class="fa fa-copy"></i></button></span>';
                    } else {
                        $offers->tracking = '<span class="text-danger">Offer not approved</span>';
                        $offers->button = '<strong class="badge badge-warning">Pending</strong>';
                    }
                } else {
                    $offers->charge *= COMMISSION;
                    $offers->tracking = '<span class="text-danger">Offer not approved</span>';
                    $offers->button = '<a class="btn btn-info" href="' . MY_SERVER . 'offers/request?id=' . $offers->ID . '">Request Offer</a>';
                }
                $offers->macros = '<p><strong>Allowed params:</strong></p>
                <ul class="list-group">
                    <li class="list-group-item">&clickid=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your click id</span></li>
                    <li class="list-group-item">&sub_affid=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your affiliate\'s id in your tool</span></li>
                    <li class="list-group-item">&sub_sourceid=&nbsp;<span class="text-danger">Your affiliate\'s source id</span></li>
                    <li class="list-group-item">&app_name=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;App name ran by publisher</span></li>
                    <li class="list-group-item">&idfa=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apple\'s advertiser identifier (IDFA)</span></li>
                    <li class="list-group-item">&gaid=&nbsp;<span class="text-danger">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Google\'s advertising identifier (GAID)</span></li>
                    <li class="list-group-item">Below is the list extra parameters available back in postback.</li>
                    <li class="list-group-item">&extra1=&nbsp;<span class="text-danger">Extra parameter 1</span></li>
                    <li class="list-group-item">&extra2=&nbsp;<span class="text-danger">Extra parameter 2</span></li>
                    <li class="list-group-item">&extra3=&nbsp;<span class="text-danger">Extra parameter 3</span></li>
                    <li class="list-group-item">&extra4=&nbsp;<span class="text-danger">Extra parameter 4</span></li>
                </ul>';
            }else{
                $c = new Creative();
                $c->setProperties([
                    'offers_ID' => $id
                ])
                    ->setQueryParameters($c);
                $x = $c->count();
                $offers->tracking = '<span class="copy_cat"><textarea id="popUrl" rows="3" readonly>' . $offers->tracking . '</textarea><br/><button type="button" id="copyBack" class="btn btn-success btn-sm" title="Link is copied">Copy</button></span>';
                $offers->button = '<a class="btn btn-info btn-sm" href="' . s() . 'offers/edit?id=' . $offers->ID . '"><i class="fa fa-pencil"></i> Edit offer</a>&nbsp;<a class="btn btn-success btn-sm" href="#!"  data-toggle="modal" data-target="#creaMode"><span class="badge badge-light">' . $x . '</span> Creatives</a>';
            }

            $offers->device = json_decode($offers->device);

            $country = [];
            foreach ($geo as $item) {
                $g = new Country();
                $g->setProperties([
                    'code' => $item
                ])->setQueryParameters($g, ['country.name']);
                $datum = $g->one();
                $country[] = '<span class="badge badge-primary">' . $datum->name . '</span>';
            }

            $offers->country = implode(' ', $country);

            $cat = json_decode($offers->category);

            $cate = [];
            foreach ($cat as $item) {
                $cate[] = '<span class="badge badge-primary">' . $item . '</span>';
            }

            $offers->category = implode(' ', $cate);

            $offers->device_id = $offers->device_id > 0 ? '<span class="badge badge-primary" style="font-weight: 500">yes</span>' : 'No';

            $sale = '';
            if ($offers->caps > 0) {
                $sale = $offers->caps - $offers->count_daily;
                $sale = $sale > $offers->caps ? $offers->caps . ' / ' : $sale . ' / ';
            }
            $offers->sale = $sale;

            $form->setForm($offers);

            $template->setTemplateField('title', $offers->name);
            $template->setTemplateField('html', 'view.html.php');

            $template->render($form);
        }
    }

    /**
     * @param Model $model
     * @param array $param
     * @param array $merge
     */
    private static function mongoUpdate(Model $model, array $param, array $merge):void{
        unset($param['description'], $param['cron'], $param['monthly_cap']);
        $param['geo'] = $merge['geo'];
        $param['cat'] = $merge['cat'];
        $param['dev'] = $merge['dev'];
        $db = new Mongo();
        $db->save($model, [$param]);
    }

    private static function offerTrackURL($net, $id): void
    {
        switch($net){
            case '19': $url = 'https://track.blam.mobi/?aff_id=549974&offer_id='.$id.'&aff_sub={CLICK}&aff_sub2={PUB}&idfa={IDFA}&android_id={GAID}'; break;
            default: $url = '';
        }

        $a = new O();
        $args = [
            'ID' => $id,
            'tracking' => $url,
            'last_updated' => date('YmdHi')
        ];

        $a->setProperties($args);
        $a->save($a);
    }

    public static function assigned($id): bool
    {
        $a = new \model\ApproveOffers();
        $a->setProperties(['offers_ID' => $id])
            ->setQueryParameters($a);
        $a->appendQuery('status != 0');
        return (bool)$a->count();
    }

    private static function setArgs(&$args): string
    {
        $filters = [];
        if (($h = R::getField('h', R::GET)) !== '') {
            $args['active'] = $h;
            $filters[] = '<span class="filter-tag" data-filter="select" data-what="state"><b>x</b>Status</span>';
        }
        if ($i = R::getField('of', R::GET)) {
            $args['ID'] = (int)$i;
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="offi"><b>x</b>ID: <strong>' . $i . '</strong></span>';
        }
        if ($m = R::getField('m', R::GET)) {
            $args['advID'] = $m;
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="adv"><b>x</b>Adv: <strong>' . $m . '</strong></span>';
        }
        if ($n = R::getField('n', R::GET)) {
            $args['name'] = [
                '$regex' => $n,
                '$options' => 'i'
            ];
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="nami"><b>x</b>Name: <strong>' . $n . '</strong></span>';
        }
        if ($f = R::getField('f', R::GET)) {
            $args['network_ID']['$in'] = $f;
            $filters[] = '<span class="filter-tag"  data-filter="select" data-what="netw"><b>x</b>Network</span>';
        }
        if ($t = R::getField('t', R::GET)) {
            $args['type'] = $t;
            $filters[] = '<span class="filter-tag"  data-filter="select" data-what="kinda"><b>x</b>Type: <strong>' . $t . '</strong></span>';
        }
        if ($t = R::getField('z', R::GET)) {
            $args['app_id'] = $t;
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="appi"><b>x</b>Bundle: <strong>' . $t . '</strong></span>';
        }
        if ($e = R::getField('e', R::GET)) {
            $args['assigned'] = $e === '1';
            $filters[] = '<span class="filter-tag"  data-filter="select" data-what="assign"><b>x</b>Assigned: <strong>' . $e . '</strong></span>';
        }
        if ($a = R::getField('r', R::GET)) {
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="above"><b>x</b>$ above: <strong>' . $a . '</strong></span>';
            $a /= .5;
            $args['charge']['$gte'] = $a;
        }
        if ($b = R::getField('j', R::GET)) {
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="below"><b>x</b>$ below: <strong>' . $b . '</strong></span>';
            $b /= .5;
            $args['charge']['$lte'] = $b;
        }
        if ($g = R::getField('g', R::GET)) {
            $args['geo']['$in'] = $g;
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="gea"><b>x</b>Geo</span>';
        }
        if ($c = R::getField('c', R::GET)) {
            $args['cat']['$in'] = $c;
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="coti"><b>x</b>Category</span>';
        }
        if ($d = R::getField('d', R::GET)) {
            $args['dev']['$in'] = $d;
            $filters[] = '<span class="filter-tag"  data-filter="text" data-what="davi"><b>x</b>Device</span>';
        }
        if ($q = R::getField('q', R::GET)) {
            $date = R::getField('date', R::GET);
            if ($date1 = R::getField('date1', R::GET)) {
                $date .= '000000';
                $date1 .= '240000';
            } else {
                if (\strlen($date) === 8) {
                    $date1 = $date . '240000';
                    $date .= '000000';
                } else {
                    $date1 = $date . '31240000';
                    $date .= '01000000';
                }
            }
            $args['date'] = [
                '$gte' => $date,
                '$lte' => $date1
            ];
            switch ($q) {
                case 't':
                    $which = 'Today';
                    break;
                case 'y':
                    $which = 'Yesterday';
                    break;
                case 'w':
                    $which = 'Last 7 days';
                    break;
                case 'p':
                    $which = 'Last week';
                    break;
                case 'm':
                    $which = 'This month';
                    break;
                case 'l':
                    $which = 'Last month';
                    break;
                default:
                    $which = 'Custom';
            }
            $filters[] = '<span class="filter-tag"  data-filter="date"><b>x</b>' . $which . '</span>';
        }
        return implode('', $filters);
    }

    public static function search(): void
    {
        if ($q = R::getRequiredField('q', R::GET)) {
            $offerObj = new O();
            $db = new Mongo();
            $args = [
                '$or' => [
                    [
                        'name' => [
                            '$regex' => $q,
                            '$options' => 'i'
                        ],
                    ],
                    ['ID' => (int)$q]
                ]
            ];
            $res = $db->query($offerObj, $args);
            $at = [];
            foreach ($res as $v) {
                $at[] = [
                    'id' => $v->ID,
                    'value' => "#$v->ID $v->name"
                ];
            }
            echo json_encode($at);
        }
    }

    public static function eventsList(): void
    {
        if ($nid = R::getRequiredField('nid', R::GET)) {

            $str = '';
            $g = new \model\NetworkEvent();
            $g->setProperties(['network_ID' => $nid])
                ->setQueryParameters($g);
            $all = $g->query();
            foreach ($all as $item):
                $str .= "<option value=\"$item->ID \">$item->event_id : $item->event_name</option>";
            endforeach;

            echo $str;
        }
    }
}