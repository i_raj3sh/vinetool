<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 8:10 PM
 */

namespace controller;


use helper\Form;
use helper\Mongo;
use helper\MyException;
use helper\Request;
use helper\Template;
use model\ActivityLog_;
use model\NetworkEvent;

class Network extends Controller
{

    public static function index(){

        $template = new Template('network');
        $template->setTemplateField('title','Networks');
        $template->setTemplateField('html','index.html.php');

        /*$isID = false;

        $name = Request::getField('n','GET');
        if ($id = Request::getField('id', 'GET')) {
            $isID = true;
        }

        $a = new \model\Network();
        $a->setQueryParameters($a);
        if ($name) {
            $a->appendQuery('name LIKE "%' . $name . '%" OR company LIKE "%' . $name . '%"');
        }

        if (!$isID) {
            $total = $a->count();
        } else {
            $total = 1;
        }

        $paginator = new Paginator();
        $paginator->setLink();
        $paginator->total = $total;
        $paginator->paginate();

        $start = $paginator->getStart();

        $a = new \model\Network();
        $m = new \model\Admin();
        if ($isID) {
            $a->setProperties([
                'network.ID' => $id
            ]);
        }
        $a->setQueryParameters($a, ['network.*']);
        $a->setQueryParameters($m, ['admin.name manager'], '', '', '', "LIMIT $start, $paginator->itemsPerPage");
        if ($name) {
            $a->appendQuery('network.name LIKE "%' . $name . '%" OR company LIKE "%' . $name . '%"');
        }

        $get['table'] = $a->query();
        $get['total'] = $total;
        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();*/

        $a = new \model\Network();
        $m = new \model\Admin();
        if ($id = Request::getField('id', 'GET')) {
            $a->setProperties([
                'network.ID' => $id
            ]);
        }
        $a->setQueryParameters($a, ['network.*']);
        $a->setQueryParameters($m, ['admin.name manager']);
        if ($name = Request::getField('n', 'GET')) {
            $a->appendQuery('network.name LIKE "%' . $name . '%" OR company LIKE "%' . $name . '%"');
        }
        $get['table'] = $a->query();

        $template->render($get);
    }

    public static function edit(){

        $template = new Template('network');
        $template->setTemplateField('title','Edit Network');
        $template->setTemplateField('html','edit.html.php');

        $form = new Form();
        if ($id = Request::getField('id','GET')){
            $a = new \model\Network();
            $a->setProperties([
                'ID' => $id
            ]);
            $a->setQueryParameters($a);
            $form->setForm($a->one());
        }
        $template->render($form);
    }

    public static function add(){
        if (Request::getRequiredField('network','POST')){
            $name = Request::getRequiredField('name','POST');
            $username = Request::getField('username','POST');
            $email = Request::getField('email','POST');
            $password = Request::getField('password','POST');
            $skype = Request::getField('skype','POST');
            $company = Request::getField('company','POST');
            $website = Request::getField('website','POST');
            $country = Request::getField('country','POST');
            $active = Request::getRequiredField('active','POST');
            $admin = Request::getField('admin', 'POST');
            $apiKey = Request::getField('apiKey', 'POST');
            $apiDomain = Request::getField('apiDomain', 'POST');
            $fallback = Request::getField('fallback', 'POST');


            $password = $password = hash('sha512', $password);

            $a = new \model\Network();
            $args = [
                'name' => $name,
                'username' => $username,
                'email' => $email,
                'password' => $password,
                'skype' => $skype,
                'company' => $company,
                'website' => $website,
                'country' => $country,
                'active' => $active,
                'admin_ID' => $admin,
                'apiKey' => $apiKey,
                'apiDomain' => $apiDomain,
                'fallback' => $fallback,
                'joined' => time(),
                'last_active' => 0
            ];
            $loc = 'network/edit';
            if ($id = Request::getField('id','POST')){
                $args['ID'] = $id;
                unset($args['joined'],$args['last_active']);
                $loc = 'network/edit?id='.$id;

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'UPDATE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Network $name ($company) 's details"
                    ]
                ];

            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'INSERT',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Network $name ($company)"
                    ]
                ];

            }
            $a->setProperties($args);
            if (!$a->save($a)){
                MyException::setSessionError('Something went wrong.');
                self::redirect($loc);
            }

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Successful');
            self::redirect('network');
        }
    }

    public static function del(){
        if ($id = Request::getRequiredField('id','GET')) {

            $a = new \model\Network();
            $a->setProperties([
                'ID' => $id
            ]);
            if (!$a->remove()){
                MyException::setSessionError('Need SUPER privilege to delete.');
            }else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'Network #' . $id
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }

                MyException::setSessionSuccess('Delete successful');
            }
            self::redirect('network');
        }
    }

    public static function events(): void
    {
        if (Request::getRequiredField('nameEvents', Request::GET)) {
            $n = Request::getField('network_id', Request::GET);
            $e = new NetworkEvent();
            $e->remove("network_ID = $n");

            foreach ($_GET as $k => $v) {
                $v = trim($v);
                if (empty($v)) {
                    continue;
                }
                $f = explode('ev_', $k, 2);
                if (count($f) === 2) {
                    $e = new NetworkEvent();
                    $e->setProperties([
                        'event_id' => $v,
                        'event_name' => $f[1],
                        'network_ID' => $n
                    ])
                        ->insert($e);
                }
            }
            self::redirect(true);
        }
    }

    public static function pullEvents()
    {
        if ($id = Request::getRequiredField('id', Request::GET)) {
            $e = new NetworkEvent();
            $e->setProperties([
                'network_ID' => $id
            ])
                ->setQueryParameters($e, ['event_id', 'event_name']);
            $res = $e->query();

            $json = [];
            foreach ($res as $v) {
                $json[] = [
                    'val' => $v->event_id,
                    'id' => $v->event_name
                ];
            }
            echo json_encode($json);
        }
    }

    public static function deactivate(): void
    {
        if ($id = Request::getRequiredField('id', 'GET')) {
            $a = new \model\Network();
            $a->setProperties([
                'ID' => $id,
                'active' => 0
            ])
                ->update($a);

            $arg = [];
            $arg[] = [
                'ID' => $id,
                '$set' => [
                    'active' => '0'
                ]
            ];
            $db = new Mongo();
            try {
                $db->save($a, $arg);
            } catch (\Exception $e) {
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Deactivated network $id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Network has been deactivated');
        }
        self::redirect(true);
    }

    public static function activate(): void
    {
        if ($id = Request::getRequiredField('id', 'GET')) {
            $a = new \model\Network();
            $a->setProperties([
                'ID' => $id,
                'active' => 1
            ])
                ->update($a);

            $arg = [];
            $arg[] = [
                'ID' => $id,
                '$set' => [
                    'active' => '1'
                ]
            ];
            $db = new Mongo();
            try {
                $db->save($a, $arg);
            } catch (\Exception $e) {
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Activated network $id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Network has been activated');
        }
        self::redirect(true);
    }
}