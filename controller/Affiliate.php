<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 7:40 PM
 */

namespace controller;


use helper\Form;
use helper\Mailer;
use helper\Mongo;
use helper\MyException;
use helper\Request;
use helper\Template;
use model\ActivityLog_;

class Affiliate extends Controller
{

    public static function index(){

        $template = new Template('affiliate');
        $template->setTemplateField('title','Affiliates');
        $template->setTemplateField('html','index.html.php');

        /*$isID = false;

        $name = Request::getField('n','GET');
        if ($id = Request::getField('id', 'GET')) {
            $isID = true;
        }

        $a = new \model\Affiliate();
        $a->setQueryParameters($a);
        if ($name) {
            $a->appendQuery('name LIKE "%' . $name . '%" OR company LIKE "%' . $name . '%"');
        }

        if (!$isID) {
            $total = $a->count();
        } else {
            $total = 1;
        }

        $paginator = new Paginator();
        $paginator->setLink();
        $paginator->total = $total;
        $paginator->paginate();

        $start = $paginator->getStart();

        $a = new \model\Affiliate();
        $m = new \model\Admin();
        if ($isID) {
            $a->setProperties([
                'affiliate.ID' => $id
            ]);
        }
        $a->setQueryParameters($a, ['affiliate.*']);
        $a->setQueryParameters($m, ['admin.name manager'], '', '', '', "LIMIT $start, $paginator->itemsPerPage");
        if ($name) {
            $a->appendQuery('affiliate.name LIKE "%' . $name . '%" OR company LIKE "%' . $name . '%"');
        }

        $get['table'] = $a->query();
        $get['total'] = $total;
        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();*/

        $a = new \model\Affiliate();
        $m = new \model\Admin();
        if ($id = Request::getField('id', 'GET')) {
            $a->setProperties([
                'affiliate.ID' => $id
            ]);
        }
        $a->setQueryParameters($a, ['affiliate.*']);
        $a->setQueryParameters($m, ['admin.name manager']);
        if ($name = Request::getField('n', 'GET')) {
            $a->appendQuery('affiliate.name LIKE "%' . $name . '%" OR company LIKE "%' . $name . '%"');
        }
        $get['table'] = $a->query();

        $template->render($get);
    }

    public static function edit(){

        $template = new Template('affiliate');
        $template->setTemplateField('title','Edit Affiliate');
        $template->setTemplateField('html','edit.html.php');

        $form = new Form();
        if ($id = Request::getField('id','GET')){
            $a = new \model\Affiliate();
            $a->setProperties([
                'ID' => $id
            ]);
            $a->setQueryParameters($a);
            $form->setForm($a->one());
        }
        $template->render($form);
    }

    public static function add(){
        if (Request::getRequiredField('affiliate','POST')){
            $name = Request::getRequiredField('name','POST');
            $username = Request::getRequiredField('username','POST');
            $email = Request::getRequiredField('email','POST');
            $pass = Request::getField('password', 'POST');
            $skype = Request::getField('skype','POST');
            $company = Request::getField('company','POST');
            $website = Request::getField('website','POST');
            $country = Request::getRequiredField('country','POST');
            $active = Request::getRequiredField('active','POST');
            $admin = Request::getField('admin','POST');
            $scrub = Request::getRequiredField('scrub', 'POST');
            $notification = Request::getRequiredField('notification', 'POST');

            $password = hash('sha512', $pass);

            $a = new \model\Affiliate();
            $args = [
                'name' => $name,
                'username' => $username,
                'email' => $email,
                'password' => $password,
                'skype' => $skype,
                'company' => $company,
                'website' => $website,
                'country' => $country,
                'active' => $active,
                'admin_ID' => $admin,
                'scrub' => $scrub,
                'notification' => $notification,
                'joined' => time(),
                'last_active' => 0
            ];
            $loc = 'affiliate/edit';
            if ($id = Request::getField('id','POST')){
                $args['ID'] = $id;
                unset($args['joined'], $args['last_active'], $args['password']);
                $loc = 'affiliate/edit?id='.$id;

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'UPDATE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Affiliate $name ($company) 's details"
                    ]
                ];

            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'INSERT',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Affiliate $name ($company)"
                    ]
                ];

                $m = new Mailer();
                $m->setHeaders()
                    ->setMessage('affiliateJoined', [
                        'name' => $name,
                        'username' => $username,
                        'password' => $pass
                    ])
                    ->send($email, 'Greetings from VineTool');

            }

            //Check for unique username

            $ad = new \model\Admin();
            $ad->setQueryParameters($ad);
            $adm = $ad->query();

            $a->setQueryParameters($a);
            $aff = $a->query();

            if (checkUnique($aff, $username) == false || !checkUnique($adm, $username) !== false) {
                MyException::setSessionError('Username already exists.');
                self::redirect($loc);
            }

            $a->setProperties($args);
            if (!$a->save($a)){
                MyException::setSessionError('Something went wrong.');
                self::redirect($loc);
            }

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Successful');

            self::redirect('affiliate');
        }
    }

    public static function del(){
        if ($id = Request::getRequiredField('id','GET')) {

            $a = new \model\Affiliate();
            $a->setProperties([
                'ID' => $id
            ]);
            if (!$a->remove()){
                MyException::setSessionError('Need SUPER privilege to delete.');
            }else {
                MyException::setSessionSuccess('Delete successful');

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'Affiliate #' . $id
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }
            }
            self::redirect('affiliate');
        }
    }

    public static function password(): void
    {
        if ($p = Request::getRequiredField('p', Request::GET)) {

            $id = Request::getField('i', Request::GET);
            $e = new \model\Affiliate();
            $e->setProperties([
                'ID' => $id,
                'password' => hash('sha512', $p)
            ]);
            if ($e->update($e)) {
                echo 'OK';
            } else {
                echo '';
            }
        }
    }

    public static function deactivate(): void
    {
        if ($id = Request::getRequiredField('id', 'GET')) {
            $a = new \model\Affiliate();
            $a->setProperties([
                'ID' => $id,
                'active' => 0
            ])
                ->update($a);

            $arg = [];
            $arg[] = [
                'ID' => $id,
                '$set' => [
                    'active' => '0'
                ]
            ];
            $db = new Mongo();
            try {
                $db->save($a, $arg);
            } catch (\Exception $e) {
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Deactivated affiliate $id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Affiliate has been deactivated');
        }
        self::redirect(true);
    }

    public static function activate(): void
    {
        if ($id = Request::getRequiredField('id', 'GET')) {
            $a = new \model\Affiliate();
            $a->setProperties([
                'ID' => $id,
                'active' => 1
            ])
                ->update($a);

            $arg = [];
            $arg[] = [
                'ID' => $id,
                '$set' => [
                    'active' => '1'
                ]
            ];
            $db = new Mongo();
            try {
                $db->save($a, $arg);
            } catch (\Exception $e) {
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Activated affiliate $id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Affiliate has been activated');
        }
        self::redirect(true);
    }
}