<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/26/17
 * Time: 2:57 PM
 */

namespace controller;


use helper\Cache;
use helper\Mysql;
use helper\Request;
use helper\Template;
use model\HourlyStats;
use model\HourlyStats as HS;
use model\ScrubbedGroup as SB;

class Async extends Controller
{
    public static function widget(): void
    {

        self::head();

        $cObj = new Cache();

        Mysql::setDbName(Mysql::STATS);

        if (!($cache = $cObj->retrieve(__METHOD__))) {
            $day = date('Ymd');
            $yesterday = date('Ymd', strtotime('-1 day'));

            $thisWeek = date("Ymd", strtotime('-6 days'));

            $thisMonth = date('Ym');

            $all = [];

            if (self::getSessionType() === 'AFFILIATE') {

                $params1 = [
                    'day_' => $day,
                    'affiliate_ID' => self::getSession()
                ];
                $params2 = [
                    'day_' => $yesterday,
                    'affiliate_ID' => self::getSession()
                ];
                $params3 = [
                    'affiliate_ID' => self::getSession()
                ];
                $params4 = [
                    'month_' => $thisMonth,
                    'affiliate_ID' => self::getSession()
                ];
            } else {
                $params1 = [
                    'day_' => $day,
                ];
                $params2 = [
                    'day_' => $yesterday,
                ];
                $params3 = [];
                $params4 = [
                    'month_' => $thisMonth,
                ];
                $params12 = [
                    'day' => $day,
                ];
                $params23 = [
                    'day' => $yesterday,
                ];
                $params45 = [
                    'month' => $thisMonth,
                ];
            }

            //Queries for profit
            $pToday = self::stats($params1);
            $pYester = self::stats($params2);

            $gh = new HS();
            $gh->setProperties($params3);
            $gh->setQueryParameters($gh, ['SUM(stats_grouped_hourly.payout) push,SUM(stats_grouped_hourly.charge) pull']);
            $gh->appendQuery("day_ >= $thisWeek and day_ <= $day");
            $pWeek = $gh->one();

            $pMonth = self::stats($params4);

            //Queries for scrubbed
            $sToday = self::scrubs($params12);
            $sYester = self::scrubs($params23);

            $gh = new SB();
            $gh->setProperties($params3);
            $gh->setQueryParameters($gh, ['SUM(payout) pull']);
            $gh->appendQuery("day >= $thisWeek and day <= $day");
            $sWeek = $gh->one();

            $sMonth = self::scrubs($params45);

            if (self::getSessionType() !== 'ADMIN') {
                //Profit
                $all['pToday'] = number_format($pToday->push, 2);
                $all['pYesterday'] = number_format($pYester->push, 2);
                $all['pWeek'] = number_format($pWeek->push, 2);
                $all['pMonth'] = number_format($pMonth->push, 2);
            } else {
                //Profit
                $all['pToday'] = number_format($pToday->pull - $pToday->push, 2);
                $all['pYesterday'] = number_format($pYester->pull - $pYester->push, 2);
                $all['pWeek'] = number_format($pWeek->pull - $pWeek->push, 2);
                $all['pMonth'] = number_format($pMonth->pull - $pMonth->push, 2);

                //Revenue
                $all['rToday'] = number_format($pToday->pull, 2);
                $all['rYesterday'] = number_format($pYester->pull, 2);
                $all['rWeek'] = number_format($pWeek->pull, 2);
                $all['rMonth'] = number_format($pMonth->pull, 2);

                //Scrubbed
                $all['sToday'] = number_format($sToday->pull, 2);
                $all['sYesterday'] = number_format($sYester->pull, 2);
                $all['sWeek'] = number_format($sWeek->pull, 2);
                $all['sMonth'] = number_format($sMonth->pull, 2);
            }

            $cache = json_encode($all);
            $cObj->enter(__METHOD__, $cache);
        }

        $over = json_decode($cache, true);

        $cObj = new Cache(Cache::STORE);

        if (!($cache = $cObj->retrieve(__METHOD__))) {
            $lastMonth = date("Ym", strtotime('first day of last month'));

            $all = [];

            if (self::getSessionType() === 'AFFILIATE') {

                $params3 = [
                    'affiliate_ID' => self::getSession()
                ];
                $params5 = [
                    'month_' => $lastMonth,
                    'affiliate_ID' => self::getSession()
                ];
            } else {
                $params3 = [];
                $params5 = [
                    'month_' => $lastMonth,
                ];
                $params56 = [
                    'month' => $lastMonth,
                ];
            }

            //Queries for revenue & profit
            $pMonthL = self::stats($params5);
            $pTotal = self::stats($params3);

            //Queries for scrubbed
            $sMonthL = self::scrubs($params56);
            $sTotal = self::scrubs($params3);

            if (self::getSessionType() !== 'ADMIN') {
                //Profit
                $all['pMonthL'] = number_format($pMonthL->push, 2);
                $all['pTotal'] = number_format($pTotal->push, 2);
            } else {
                //Profit
                $all['pMonthL'] = number_format($pMonthL->pull - $pMonthL->push, 2);
                $all['pTotal'] = number_format($pTotal->pull - $pTotal->push, 2);

                //Revenue
                $all['rMonthL'] = number_format($pMonthL->pull, 2);
                $all['rTotal'] = number_format($pTotal->pull, 2);

                //Scrubbed
                $all['sMonthL'] = number_format($sMonthL->pull, 2);
                $all['sTotal'] = number_format($sTotal->pull, 2);
            }

            $cache = json_encode($all);
            $cObj->enterTemp(__METHOD__, $cache, 84600 * 30);
        }

        $final = json_decode($cache, true);

        $all = array_merge($over, $final);

        echo json_encode($all);

        Mysql::setDbName();
    }

    private static function stats($params): HS{
        $gh = new HS();
        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['SUM(payout) push,SUM(charge) pull']);
        return $gh->one();
    }

    private static function scrubs($params): SB{
        $gh = new SB();
        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['SUM(payout) pull']);
        return $gh->one();
    }

    public static function dash(): void
    {

        self::head();
        $month = Request::getField('m', 'GET');

        $cObj = new Cache();

        if (!($cache = $cObj->retrieve(__METHOD__)) || $month !== date('Ym')) {
            $gh = new HS();
            $data = [];
            Mysql::setDbName(Mysql::STATS);

            Mysql::setDbName(Mysql::STATS);

            if (self::getSessionType() === 'ADMIN') {
                $param = [
                    'month_' => $month
                ];

                /*Affiliate*/
                self::affiliate($month, $data);
                self::affiliateToday($data);
                /*Network*/
                self::network($month, $data);
                self::networkToday($data);
                /*PAM*/
                self::publisherAccountManager($month, $data);
                self::publisherAccountManagerToday($data);

                /*AAM*/
                self::advertiserAccountManager($month, $data);
                self::advertiserAccountManagerToday($data);

            } else {
                $param = [
                    'month_' => $month,
                    'affiliate_ID' => self::getSession()
                ];
            }
            $gh->setProperties($param)
                ->setQueryParameters($gh, ['campaign', 'SUM(payout) push', 'SUM(charge) pull', 'SUM(clicks) visits', 'SUM(sales) conver'], '', 'GROUP BY offers_ID ORDER BY pull DESC LIMIT 10');

            $offers = $gh->query();

            $i = 0;
            foreach ($offers as $offer) {
                if ($offer->pull == 0) {
                    continue;
                }
                $data['o'][$i]['name'] = $offer->campaign;
                if (self::getSessionType() === 'ADMIN') {
                    $data['o'][$i]['y'] = (float)$offer->pull;
                } else {
                    $data['o'][$i]['y'] = (float)$offer->push;
                }
                $data['o'][$i]['v'] = (int)$offer->visits;
                $data['o'][$i]['s'] = (int)$offer->conver;
                $i++;
            }

            self::offersToday($data);

            Mysql::setDbName();

            $cache = json_encode($data);
            if ($month === date('Ym')) {
                $cObj->enter(__METHOD__, $cache);
            }
        }
        echo $cache;
    }

    private static function affiliate(string $month, array &$data): void
    {
        $gh = new HS();

        $param = [
            'month_' => $month
        ];
        $gh->setProperties($param)
            ->setQueryParameters($gh,['affiliate','SUM(payout) push', 'SUM(clicks) visits', 'SUM(sales) conver'], '', 'GROUP BY affiliate_ID ORDER BY push DESC');

        $aff = $gh->query();

        $i = 0;
        foreach ($aff as $af) {
            if ($af->push == 0) {
                continue;
            }
            $data['a'][$i]['name'] = $af->affiliate;
            $data['a'][$i]['y'] = (float)$af->push;
            $data['a'][$i]['v'] = (int)$af->visits;
            $data['a'][$i]['s'] = (int)$af->conver;
            $i++;
        }
    }

    private static function affiliateToday(array &$data): void
    {
        $gh = new HS();

        $param = [
            'day_' => date('Ymd')
        ];
        $gh->setProperties($param)
            ->setQueryParameters($gh, ['affiliate', 'SUM(payout) push', 'SUM(clicks) visits', 'SUM(sales) conver'], '', 'GROUP BY affiliate_ID HAVING visits > 0 ORDER BY push DESC LIMIT 5');

        $aff = $gh->query();

        $i = 0;
        foreach ($aff as $af) {
            if ($af->push == 0) {
                continue;
            }
            $data['at'][$i]['name'] = '' . $af->affiliate;
            $data['at'][$i]['y'] = (float)$af->push;
            $data['at'][$i]['z'] = ($af->conver * 100) / $af->visits;
            $i++;
        }
    }

    private static function offersToday(array &$data): void
    {
        $gh = new HS();
        $o = new \model\Offers();

        $param = [
            'day_' => date('Ymd')
        ];

        $gh->setProperties($param)
            ->setQueryParameters($gh, ['campaign', 'SUM(payout) push', 'SUM(charge) pull', 'SUM(clicks) visits', 'SUM(sales) conver'], '', 'GROUP BY offers_ID HAVING visits > 0 ORDER BY pull DESC LIMIT 5');

        $aff = $gh->query();

        $i = 0;
        $arr = [40, 20, 15, 15, 10];
        $cr = [0.3, 0.08, 0.7, 0.99, 1.2];
        foreach ($aff as $k => $af) {
            if ($af->push == 0){
                continue;
            }
            $data['ot'][$i]['name'] = '' . $af->campaign;
            if (self::getSessionType() === 'ADMIN') {
                $data['ot'][$i]['y'] = (float)$af->pull;
            }else{
                $data['ot'][$i]['y'] = (float)$af->push;
            }
//            $data['ot'][$i]['y'] = $arr[$k];
//            $data['ot'][$i]['z'] = $cr[$k];
            $data['ot'][$i]['z'] = ($af->conver * 100)/$af->visits;
            $i++;
        }
    }

    private static function network(string $month, array &$data): void
    {

        $gh = new HS();

        $param = [
            'month_' => $month
        ];
        $gh->setProperties($param)
            ->setQueryParameters($gh,['network','SUM(charge) pull', 'SUM(clicks) visits', 'SUM(sales) conver'], '', 'GROUP BY network_ID ORDER BY pull DESC');

        $aff = $gh->query();

        $i = 0;
        foreach ($aff as $af) {
            if ($af->pull == 0) {
                continue;
            }
            $data['n'][$i]['name'] = $af->network;
            $data['n'][$i]['y'] = (float)$af->pull;
            $data['n'][$i]['v'] = (int)$af->visits;
            $data['n'][$i]['s'] = (int)$af->conver;
            $i++;
        }
    }

    private static function networkToday(array &$data): void
    {

        $gh = new HS();

        $param = [
            'day_' => date('Ymd')
        ];
        $gh->setProperties($param)
            ->setQueryParameters($gh, ['network', 'SUM(charge) pull', 'SUM(clicks) visits', 'SUM(sales) conver'], '', 'GROUP BY network_ID HAVING visits > 0 ORDER BY pull DESC LIMIT 5');

        $aff = $gh->query();

        $i = 0;
        foreach ($aff as $af) {
            if ($af->pull == 0) {
                continue;
            }
            $data['nt'][$i]['name'] = '' . $af->network;
            $data['nt'][$i]['y'] = (float)$af->pull;
            $data['nt'][$i]['z'] = ($af->conver * 100) / $af->visits;
            $i++;
        }
    }

    private static function publisherAccountManager(string $month, array &$data): void
    {

        $sql = "
        SELECT SUM(s.charge) pull, SUM(s.payout) push, SUM(s.clicks) visits, SUM(s.sales) conver, a.name FROM vine_ad_stats.stats_grouped_hourly s 
        JOIN vine_ad_db.admin a ON a.ID = s.admin_ID
        WHERE s.month_ = $month AND a.name != 'ADMIN'
        GROUP BY admin_ID
        ORDER BY pull DESC
        ";

        $aff = Mysql::select($sql, (new HourlyStats())->getMyClass());

        $i = 0;
        foreach ($aff as $af) {
            if ($af->pull == 0) {
                continue;
            }
            $data['pam'][$i]['name'] = $af->name;
            $data['pam'][$i]['y'] = (float)($af->pull - $af->push);
            $data['pam'][$i]['v'] = (int)$af->visits;
            $data['pam'][$i]['s'] = (int)$af->conver;
            $i++;
        }
    }

    private static function publisherAccountManagerToday(array &$data): void
    {

        $date = date('Ymd');
        $sql = "
        SELECT SUM(s.charge) pull, SUM(s.payout) push, m.name FROM vine_ad_stats.stats_grouped_hourly s 
        JOIN vine_ad_db.admin m ON s.admin_ID = m.ID
        WHERE s.day_ = $date AND m.name != 'ADMIN'
        GROUP BY admin_ID
        ORDER BY pull DESC
        ";

        $aff = Mysql::select($sql, (new HourlyStats())->getMyClass());

        foreach ($aff as $af) {
            $rev = (float)($af->pull - $af->push);

            if ($rev > 0) {
                $data['pamt']['cate'][] = $af->name;
                $data['pamt']['data'][] = $rev;
            }
        }
    }

    private static function advertiserAccountManager(string $month, array &$data): void
    {

        $sql = "SELECT SUM(clicks) visit, SUM(sales) conver, SUM(payout) push, SUM(charge) pull, m.name 
                FROM vine_ad_stats.stats_grouped_hourly sh 
                JOIN vine_ad_db.network n ON n.ID = sh.network_ID
                JOIN vine_ad_db.admin m ON m.ID = n.admin_ID
                WHERE sh.month_ = $month AND m.ID NOT IN (1,9)
                GROUP BY m.ID
                HAVING conver > 0";

        $aff = Mysql::select($sql, (new HourlyStats())->getMyClass());

        $i = 0;
        foreach ($aff as $af) {
            if ($af->pull == 0) {
                continue;
            }
            $data['aam'][$i]['name'] = $af->name; //Name**
            $data['aam'][$i]['y'] = (float)($af->pull - $af->push); //Earning**
            $data['aam'][$i]['v'] = (int)$af->visit; //Visits
            $data['aam'][$i]['s'] = (int)$af->conver;  //Installs**
            $i++;
        }
    }


    private static function advertiserAccountManagerToday(array &$data): void
    {

        $date = date('Ymd');
        $sql = "SELECT SUM(clicks) visit, SUM(sales) conver, SUM(payout) push, SUM(charge) pull, m.name 
                FROM vine_ad_stats.stats_grouped_hourly sh 
                JOIN vine_ad_db.network n ON n.ID = sh.network_ID
                JOIN vine_ad_db.admin m ON m.ID = n.admin_ID
                WHERE sh.day_ = $date AND m.ID NOT IN (1,9)
                GROUP BY m.ID
                HAVING conver > 0";

        $aff = Mysql::select($sql, (new HourlyStats())->getMyClass());

        foreach ($aff as $af) {
            $rev = (float)($af->pull - $af->push);

            if ($rev > 0) {
                $data['aamt']['cate'][] = $af->name; //Name
                $data['aamt']['data'][] = $rev; //Earning
            }
        }
    }

    private static function head(): void
    {
        header('Content-Type: application/json');
    }

    public static function view(): void
    {
        $page = Request::getRequiredField('p', Request::GET);
        $load = [];
        if ($id = Request::getField('i', Request::GET)) {
            $class = '\model\\' . ucfirst($page);
            $load['id'] = $id;
            $load['table'] = $class::load($id);
        }
        Template::getView($page, $load);
    }

}