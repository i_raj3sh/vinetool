<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 2:13 PM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\HourlyStats;

class DailyStats extends Controller{
    public static function index(){

        $template = new Template('stats');
        $template->setTemplateField('title','Daily Stats');
        $template->setTemplateField('html','daily.html.php');

        $true = false;
        if (!($date = Request::getField('date','GET'))){
            $date1 = date('Ymd');
            $date = date("Ymd", strtotime("-7 days"));
            $true = true;
        }

        if ($dateq = Request::getField('date1','GET')){
            $date1 = $dateq;
            $params = [];
        }else {
            if (\strlen($date) === 8) {
                $params = [
                    'day_' => $date
                ];
            }else{
                $params = [
                    'month_' => $date
                ];
            }
        }

        if($true){
            $params = [];
        }

        Mysql::setDbName(Mysql::STATS);
        $gh = new HourlyStats();

        if ($of = Request::getField('of','GET')){
            $params['offers_ID'] = $of;
        }

        if (self::getSessionType() === 'AFFILIATE'){
            $params['affiliate_ID'] = self::getSession();
        }
        else{
            if ($af = Request::getField('af','GET')){
                $params['affiliate_ID'] = $af;
            }

            if ($nw = Request::getField('nw','GET')){
                $params['network_ID'] = $nw;
            }
        }
        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['*', 'SUM(dropped) lost', 'SUM(clicks) visit', 'SUM(sales) conver', 'SUM(payout) push', 'SUM(charge) pull'], '', 'GROUP BY day_', 'ORDER BY day_');
        if($date1){
            $gh->appendQuery("day_ >= $date and day_ <= $date1");
        }
        $stats = $gh->query();
        Mysql::setDbName();

        $template->render($stats);
    }
}