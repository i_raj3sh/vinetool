<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 5:03 PM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\HourlyStats;

class OsStats extends Controller
{
    public static function index(){

        $template = new Template('stats');
        $template->setTemplateField('title','OS Stats');
        $template->setTemplateField('html','os.html.php');


        if (!($date = Request::getField('date','GET'))){
            $date = date('Ymd');
        }

        if ($date1 = Request::getField('date1','GET')){
            $params = [];
        }else {
            if (\strlen($date) === 8) {
                $params = [
                    'day_' => $date
                ];
            }else{
                $params = [
                    'month_' => $date
                ];
            }
        }

        $gh = new HourlyStats();

        if (self::getSessionType() === 'AFFILIATE'){
            $params['affiliate_ID'] = self::getSession();
        } else {
            if ($af = Request::getField('af', 'GET')) {
                $params['affiliate_ID'] = $af;
            }

            if ($nw = Request::getField('nw', 'GET')) {
                $params['network_ID'] = $nw;
            }
        }
        if ($of = Request::getField('of','GET')){
            $params['offers_ID'] = $of;
        }
        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['*', 'SUM(dropped) lost', 'SUM(clicks) visit', 'SUM(sales) conver', 'SUM(payout) push', 'SUM(charge) pull'], '', 'GROUP BY device', 'ORDER BY pull DESC');
        if($date1){
            $gh->appendQuery("day_ >= $date and day_ <= $date1");
        }
        Mysql::setDbName(Mysql::STATS);
        $stats = $gh->query();
        Mysql::setDbName();

        $template->render($stats);
    }
}