<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/24/17
 * Time: 7:54 AM
 */

namespace controller;

use helper\Mysql;
use helper\Request as R;
use helper\Template as T;
use model\BillingAffiliate;
use model\InvoiceAffiliate as IA;

class Invoice extends Controller
{
    public static function index(){

        if (self::getSessionType() === 'ADMIN'){
            self::redirect();
        }

        $template = new T('invoiceaff');
        $template->setTemplateField(T::TITLE, 'Invoices - Paid | Unpaid');
        $template->setTemplateField(T::HTML, 'index.html.php');

        $a = new IA();

        $a->setProperties([
            'affiliate_ID' => self::getSession(),
        ]);
        $a->setQueryParameters($a);
        $a->appendQuery('amount >= 100');
        Mysql::setDbName(Mysql::STATS);
        $get = $a->query();
        Mysql::setDbName();

        $template->render($get);
    }

    public static function view(): void
    {
        if ($i = R::getRequiredField('i', R::GET)) {

            $in = new IA();
            $in->setProperties([
                'ID' => $i
            ])
                ->setQueryParameters($in);

            Mysql::setDbName(Mysql::STATS);
            $res = $in->one();

            $b = new BillingAffiliate();
            $b->setProperties([
                'affiliate_ID' => self::getSession(),
                'month' => $res->month
            ])
                ->setQueryParameters($b);

            $bill = $b->query();

            $confirmed = [];
            $unconfirmed = [];
            $deduction = [];
            foreach ($bill as $q) {
                $q->per = $q->payout / $q->sales;
                $confirmed[] = $q;
                if ($q->status === '1') {
                    if ($q->deduction > 0) {
                        $q->d_sales = $q->deduction;
                        $q->per = $q->payout / $q->sales;
                        $q->d_payout = $q->d_sales * $q->per;
                        $deduction[] = $q;
                    }
                } else {
                    $q->f_sales = $q->sales;
                    $q->per = $q->payout / $q->sales;
                    $q->f_payout = $q->f_sales * $q->per;
                    $unconfirmed[] = $q;
                }
            }

            $res->confirmed = $confirmed;
            $res->unconfirmed = $unconfirmed;
            $res->deducted = $deduction;

            $ia = new IA();
            $ia->setProperties([
                'affiliate_ID' => self::getSession(),
                'status' => 0,
                'month' => $res->month - 1
            ])
                ->setQueryParameters($ia, ['amount']);
            $ia->appendQuery('amount < 100');
            $res->lastInvoice = (float)$ia->one()->amount;

            Mysql::setDbName();

            $a = new \model\Affiliate();
            $a->setProperties([
                'ID' => self::getSession()
            ])
                ->setQueryParameters($a);
            $res->af = $a->one();

            $template = new T('invoiceaff');
            $template->setTemplateField(T::TITLE, 'Invoices - billed campaigns');
            $template->setTemplateField(T::HTML, 'view.html.php');
            $template->render($res);
        }
    }

}