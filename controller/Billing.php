<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 9/2/18
 * Time: 12:51 PM
 */

namespace controller;


use helper\Mysql;
use helper\Paginator;
use helper\Request;
use helper\Template;
use model\BillingAffiliate;

class Billing extends Controller
{
    public static function index(): void
    {
        if (self::getSessionType() === 'ADMIN'){
            self::redirect();
        }

        $template = new Template('billing');
        $template->setTemplateField('title', 'Final Billing numbers');
        $template->setTemplateField('html', 'billing_index.html.php');

        $a = new BillingAffiliate();
        Mysql::setDbName(Mysql::STATS);

        $args = [];
        $st = Request::getField('st', 'GET');
        if ($of = Request::getField('of', 'GET')) {
            $args['offers_ID'] = $of;
        }
        if ($st !== '-' && $st !== '') {
            $args['status'] = $st;
        }
        if ($d = Request::getField('date', 'GET')) {
            $args['month'] = $d;
        }else{
            $args['month'] = date('Ym',strtotime('-1 month'));
        }
        $args['affiliate_ID'] = self::getSession();

        $a->setProperties($args)->setQueryParameters($a);

        $paginator = new Paginator();
        $paginator->setDate(25);
        $paginator->setLink();
        $paginator->total = $a->count();
        $paginator->paginate();

        $skip = $paginator->getStart();

        $a = new BillingAffiliate();
        $a->setProperties($args)->setQueryParameters($a,['*'],'','','',"LIMIT $skip, $paginator->itemsPerPage");

        $arr = $a->query();

        if (!empty($arr) && $paginator->isLast()) {

            $b = new BillingAffiliate();
            $b->setProperties($args)
                ->setQueryParameters($b);
            $all = $b->query();

            $sales = $nSales = 0;
            $payout = $nPayout = 0;
            $deduction = $nDeduction = 0;
            $total = $nTotal = 0;
            foreach ($all as $v) {
                self::total($v);
                if ($v->status === '1') {
                    $sales += $v->sales;
                    $payout += $v->payout;
                    $deduction += $v->deduction;
                    $total += $v->total;
                } else {
                    $nSales += $v->sales;
                    $nPayout += $v->payout;
                    $nDeduction += $v->deduction;
                    $nTotal += $v->total;
                }
            }

            $v = new BillingAffiliate();
            $v->offer = '<strong class="text-success float-right">confirmed:</strong>';
            $v->sales = $sales;
            $v->payout = $payout;
            $v->deduction = $deduction;
            $v->total = $total;
            $v->bd = 'style="border-top:2px solid #333;"';
            $arr[] = $v;

            $v = new BillingAffiliate();
            $v->offer = '<strong class="text-danger float-right">unconfirmed:</strong>';
            $v->sales = $nSales;
            $v->payout = $nPayout;
            $v->deduction = $nDeduction;
            $v->total = $nTotal;
            $arr[] = $v;
        }

        $get['table'] = $arr;
        $get['total'] = $paginator->total;
        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();
        Mysql::setDbName();
        $template->render($get);
    }

    public static function total($o):string {
        if ($o === null){
            die;
        }
        if ($o->sales > 0) {
            $one = $o->payout / $o->sales;
            $final = $o->sales - $o->deduction;
            $o->total = $one * $final;
            $total = number_format($o->total, 2);
        } else {
            $total = 0;
        }
        return "<code>$</code><strong>$total</strong>";
    }

    public static function status($s):string {
        if ($s === '0'){
            return '<div class="text-danger">uncomfirmed</div>';
        }else{
            return '<div class="text-success">comfirmed</div>';
        }
    }

}