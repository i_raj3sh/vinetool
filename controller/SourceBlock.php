<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 8:29 AM
 */

namespace controller;


use helper\Mongo;
use helper\MyException;
use helper\Mysql;
use helper\Paginator;
use helper\Request;
use helper\Template;
use model\ActivityLog_;
use model\Affiliate as A;
use model\Network as N;
use model\Offers as O;

class SourceBlock extends Controller
{

    public static function index(){

        $template = new Template('sourceblock');
        $template->setTemplateField('title', 'Blocked by source');
        $template->setTemplateField('html', 'source_index.html.php');

        $str = 'WHERE 1';
        if ($af = Request::getField('af','GET')){
            $str .= " AND affiliate_ID = $af";
        }
        if ($of = Request::getField('of','GET')){
            $str .= " AND offers_ID = $of";
        }
        if ($nw = Request::getField('nw', 'GET')) {
            $str .= " AND network_ID = $nw";
        }

        $sql = "SELECT ID, affiliate_ID, GROUP_CONCAT(`source` separator ',')  source, network_ID, offers_ID, at FROM vine_ad_db.blocked_sources $str GROUP BY affiliate_ID, network_ID, offers_ID";

        $paginator = new Paginator();
        $paginator->setDate(50);
        $paginator->setLink();
        $paginator->total = Mysql::numRows($sql,[]);
        $paginator->paginate();

        $skip = $paginator->getStart();

        $sql .= " LIMIT $skip, $paginator->itemsPerPage";

        $res = Mysql::select($sql, (new N())->getMyClass());

        $all = [];
        foreach ($res as $re) {
            $a = new A();
            $a->setProperties(['ID' => $re->affiliate_ID])
                ->setQueryParameters($a, ['email']);
            $re->affiliate = $a->one()->email;

            if ($re->offers_ID) {
                $a = new O();
                $a->setProperties(['ID' => $re->offers_ID])
                    ->setQueryParameters($a, ['name']);
                $re->campaign = $a->one()->name;
            } else {
                $a = new N();
                $a->setProperties(['ID' => $re->network_ID])
                    ->setQueryParameters($a, ['email']);
                $re->network = $a->one()->email;
            }
            $all[] = $re;
        }

        $get['table'] = $all;
        $get['total'] = $paginator->total;

        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();

        $template->render($get);
    }

    public static function edit(){
        $template = new Template('sourceblock');
        $template->setTemplateField('title', 'Blocked by source');
        $template->setTemplateField('html', 'source_edit.html.php');

        $template->render();
    }

    public static function add(){
        if (Request::getRequiredField('sblocked', 'POST')) {
            $af = Request::getRequiredField('af', 'POST');
            $of = Request::getField('of', 'POST');
            $nw = Request::getField('nw', 'POST');
            $so = Request::getRequiredField('so', 'POST');

            $of = $of === '' ? 0 : $of;
            $nw = $nw === '' ? 0 : $nw;
            $source = implode("','", $so);

            if ($nw) {
                $sql = "DELETE FROM vine_ad_db.blocked_sources WHERE affiliate_ID = $af AND `source` IN ('$source') AND offers_ID IN (" .
                    "SELECT ID FROM vine_ad_db.offers WHERE network_ID = $nw )";
                Mysql::delete($sql);
            }

            foreach ($so as $s) {

                $sql = "SELECT ID FROM vine_ad_db.blocked_sources WHERE affiliate_ID = '$af' AND offers_ID = '$of' AND network_ID = '$nw' AND `source` = '$s'";
                $res = Mysql::select($sql, (new N())->getMyClass());

                if ($res) {
                    continue;
                }

                if ($of) {
                    $sql = "SELECT * FROM vine_ad_db.`blocked_sources` WHERE affiliate_ID = '$af' AND source = '$s' AND network_ID IN (SELECT network_ID FROM vine_ad_db.offers WHERE ID = '$of')";
                    $res = Mysql::select($sql, (new N())->getMyClass());
                    if ($res) {
                        continue;
                    }
                }

                $args = [
                    'affiliate_ID=?' => $af,
                    'source=?' => $s,
                    'offers_ID=?' => $of,
                    'network_ID=?' => $nw,
                    'at=?' => time()
                ];

                Mysql::put('INSERT', 'blocked_sources', $args);
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'INSERT',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => " affiliateID[$af] blocked by source"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Successful');
            self::redirect('sourceBlock');
        }
    }

    public static function del(){
        if ($id = Request::getRequiredField('id','GET')) {

            [$af,$of] = explode('_',$id);
            $sql = "DELETE FROM blocked_offer_rules WHERE affiliate_ID = $af AND offers_ID = $of";
            if (!Mysql::delete($sql)){
                MyException::setSessionError('Delete failed');
            }else {
                MyException::setSessionSuccess('Delete successful');

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' =>  "affiliateID[$af] unblocked for offer[$of]"
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }
            }
            self::redirect('offerBlock');
        }
    }
}