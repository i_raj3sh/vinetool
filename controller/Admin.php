<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 8:29 AM
 */

namespace controller;


use helper\Form;
use helper\Mongo;
use helper\MyException;
use helper\Request;
use helper\Template;
use model\ActivityLog_;

class Admin extends Controller
{

    public static function index(){

        if (self::getAdminType() === 'SUPER') {
            $template = new Template('admin');
            $template->setTemplateField('title', 'Admins & Account managers');
            $template->setTemplateField('html', 'index.html.php');

            $a = new \model\Admin();
            $a->setQueryParameters($a);
            $get = $a->query();
            $template->render($get);
        } else {
            MyException::setSessionError('Access forbidden.');
            Controller::redirect(true);
        }
    }

    public static function edit(){

        if (self::getAdminType() === 'SUPER') {
            $template = new Template('admin');
            $template->setTemplateField('title', 'Edit Admin Details');
            $template->setTemplateField('html', 'edit.html.php');

            $form = new Form();
            if ($id = Request::getField('id', 'GET')) {
                $a = new \model\Admin();
                $a->setProperties([
                    'ID' => $id
                ]);
                $a->setQueryParameters($a);
                $form->setForm($a->one());
            }
            $template->render($form);
        } else {
            MyException::setSessionError('Access forbidden.');
            Controller::redirect(true);
        }
    }

    public static function add(){
        if (self::getAdminType() === 'SUPER') {
            if (Request::getRequiredField('admin', 'POST')) {
                $name = Request::getRequiredField('name', 'POST');
                $username = Request::getRequiredField('username', 'POST');
                $email = Request::getRequiredField('email', 'POST');
                $password = Request::getField('password', 'POST');
                $type = Request::getRequiredField('type', 'POST');
                $active = Request::getRequiredField('active', 'POST');

                $password = $password = hash('sha512', $password);

                $a = new \model\Admin();
                $args = [
                    'name' => $name,
                    'username' => $username,
                    'email' => $email,
                    'password' => $password,
                    'type' => $type,
                    'active' => $active,
                    'joined' => date('YmdHis'),
                    'last_active' => 0
                ];
                $loc = 'admin/edit';
                if ($id = Request::getField('id', 'POST')) {
                    $args['ID'] = $id;
                    unset($args['joined'], $args['last_active'], $args['password']);
                    $loc = 'admin/edit?id=' . $id;

                    $arg = [];
                    $arg[0] = [
                        'AID' => self::getSession(),
                        'name' => self::getSessionName(),
                        'type' => 'UPDATE',
                        'date' => (int)date('Ymd'),
                        'time' => time(),
                        'extra' => [
                            'what' => "$name's account details"
                        ]
                    ];

                } else {

                    $arg = [];
                    $arg[0] = [
                        'AID' => self::getSession(),
                        'name' => self::getSessionName(),
                        'type' => 'INSERT',
                        'date' => (int)date('Ymd'),
                        'time' => time(),
                        'extra' => [
                            'what' => $name
                        ]
                    ];

                }

                //Check for unique username

                $af = new \model\Affiliate();
                $af->setQueryParameters($af);
                $aff = $af->query();

                $a->setQueryParameters($a);
                $adm = $a->query();

                if (checkUnique($aff, $username) == false || !checkUnique($adm, $username) !== false) {
                    MyException::setSessionError('Username already exists.');
                    self::redirect($loc);
                }

                $a->setProperties($args);
                if (!$a->save($a)) {
                    MyException::setSessionError('Something went wrong.');
                    self::redirect($loc);
                }

                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }

                MyException::setSessionSuccess('Successful');
                self::redirect('admin');
            }
        } else {
            MyException::setSessionError('Access forbidden.');
            Controller::redirect(true);
        }
    }

    public static function del(){
        if ($id = Request::getRequiredField('id','GET')) {

            if (self::getSession() == $id){
                MyException::setSessionError('Can\'t delete your account.');
                self::redirect('admin');
            }

            $a = new \model\Admin();
            $a->setProperties([
                'ID' => $id
            ]);
            if (!$a->remove()){
                MyException::setSessionError('Need SUPER privilege to delete.');
            }else {
                MyException::setSessionSuccess('Delete successful');

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'Admin #' . $id
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }
            }
            self::redirect('admin');
        }
    }

    public static function password(): void
    {
        if ($p = Request::getRequiredField('p', Request::GET)) {

            $id = Request::getField('i', Request::GET);
            $e = new \model\Admin();
            $e->setProperties([
                'ID' => $id,
                'password' => hash('sha512', $p)
            ]);
            if ($e->update($e)) {
                echo 'OK';
            } else {
                echo '';
            }
        }
    }

    public static function deactivate(): void
    {
        if ($id = Request::getRequiredField('id', 'GET')) {
            $a = new \model\Admin();
            $a->setProperties([
                'ID' => $id,
                'active' => 0
            ])
                ->update($a);

            $arg = [];
            $arg[] = [
                'ID' => $id,
                '$set' => [
                    'active' => '0'
                ]
            ];
            $db = new Mongo();
            try {
                $db->save($a, $arg);
            } catch (\Exception $e) {
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Deactivated admin $id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Admin has been deactivated');
        }
        self::redirect(true);
    }

    public static function activate(): void
    {
        if ($id = Request::getRequiredField('id', 'GET')) {
            $a = new \model\Admin();
            $a->setProperties([
                'ID' => $id,
                'active' => 1
            ])
                ->update($a);

            $arg = [];
            $arg[] = [
                'ID' => $id,
                '$set' => [
                    'active' => '1'
                ]
            ];
            $db = new Mongo();
            try {
                $db->save($a, $arg);
            } catch (\Exception $e) {
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Activated admin $id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Admin has been activated');
        }
        self::redirect(true);
    }
}

