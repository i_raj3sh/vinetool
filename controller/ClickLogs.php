<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 28/4/18
 * Time: 9:04 AM
 */

namespace controller;


use helper\Mongo;
use helper\Request as R;
use helper\Template;
use model\Visit;

class ClickLogs extends Controller
{

    public static function index(): void
    {
        $template = new Template('logs');
        $template->setTemplateField('title', 'Click Logs');
        $template->setTemplateField('html', 'click_log_index.html.php');

        $args = [];
        if ($af = R::getField('af', 'GET')) {
            $args['affiliateID'] = $af;
        }
        if ($of = R::getField('of', 'GET')) {
            $args['offerID'] = $of;
        }
        if ($t = R::getField('t', 'GET')) {
            $args['tid'] = $t;
        }

        if ($h = R::getField('h', 'GET')) {
            if ($h >= 0 && $h < 10) {
                $hours = "0" . $h;
            } else {
                $hours = $h;
            }
        }
        $date = date('Ymd') . $hours;
        // $date = "20190326" . $hours;

        if (!($l = R::getField('l', 'GET'))) {
            $l = 10;
        }

        if ($of || $t) {
            $db = new Mongo([
                'db' => 'db_dump',
                'server' => 'traffic',
                'timeout' => 900000,
                'authentication' => 'db_dump'
            ]);
            $get = $db->query(new Visit($date), $args, ['sort' => ['_id' => -1], 'limit' => $l]);

            $template->render($get);
        } else {
            $template->render();
        }
    }
}