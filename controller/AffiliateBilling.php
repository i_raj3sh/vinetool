<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 9/2/18
 * Time: 12:51 PM
 */

namespace controller;


use helper\Mysql as M;
use helper\Paginator;
use helper\Request;
use helper\Template;
use model\BillingAffiliate;
use model\InvoiceAffiliate as IA;

class AffiliateBilling extends Controller
{
    public static function index(): void
    {

        $template = new Template('affiliatebilling');
        $template->setTemplateField('title', 'Affiliate Billing numbers');
        $template->setTemplateField('html', 'billing_index.html.php');

        $a = new BillingAffiliate();
        M::setDbName(M::STATS);

        $args = [];
        $st = Request::getField('st', 'GET');
        if ($af = Request::getField('af', 'GET')) {
            $args['affiliate_ID'] = $af;
        }
        if ($of = Request::getField('of', 'GET')) {
            $args['offers_ID'] = $of;
        }
        if ($nw = Request::getField('nw', 'GET')) {
            $args['network_ID'] = $nw;
        }
        if ($st !== '-' && $st !== '') {
            $args['status'] = $st;
        }
        if ($d = Request::getField('date', 'GET')) {
            $args['month'] = $d;
        }else{
            $args['month'] = date('Ym',strtotime('-1 month'));
        }

        $a->setProperties($args)->setQueryParameters($a);

        $paginator = new Paginator();
        $paginator->setDate(25);
        $paginator->setLink();
        $paginator->total = $a->count();
        $paginator->paginate();

        $skip = $paginator->getStart();

        $a = new BillingAffiliate();
        $a->setProperties($args)->setQueryParameters($a,['*'],'','','',"LIMIT $skip, $paginator->itemsPerPage");

        $arr = $a->query();

        if (!empty($arr) && $paginator->isLast()) {

            $b = new BillingAffiliate();
            $b->setProperties($args)
                ->setQueryParameters($b);
            $all = $b->query();

            $sales = $nSales = 0;
            $payout = $nPayout = 0;
            $deduction = $nDeduction = 0;
            $total = $nTotal = 0;
            foreach ($all as $v) {
                self::total($v);
                if ($v->status === '1') {
                    $sales += $v->sales;
                    $payout += $v->payout;
                    $deduction += $v->deduction;
                    $total += $v->total;
                } else {
                    $nSales += $v->sales;
                    $nPayout += $v->payout;
                    $nDeduction += $v->deduction;
                    $nTotal += $v->total;
                }
            }

            $v = new BillingAffiliate();
            $v->network = '<strong class="text-success float-right">confirmed:</strong>';
            $v->sales = $sales;
            $v->payout = $payout;
            $v->deduction = $deduction;
            $v->total = $total;
            $v->bd = 'style="border-top:2px solid #333;padding:.7rem !important;"';
            $arr[] = $v;

            $v = new BillingAffiliate();
            $v->network = '<strong class="text-danger float-right">unconfirmed:</strong>';
            $v->sales = $nSales;
            $v->payout = $nPayout;
            $v->deduction = $nDeduction;
            $v->total = $nTotal;
            $v->bd = 'style="padding:.7rem !important;"';
            $arr[] = $v;
        }

        $get['table'] = $arr;
        $get['total'] = $paginator->total;
        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();
        M::setDbName();
        $template->render($get);
    }

    public static function add():void{
        if ($i = Request::getRequiredField('i')){

            $v = Request::getRequiredField('v');
            M::setDbName(M::STATS);
            $ab = new BillingAffiliate();
            $ab->setProperties([
                'deduction' => $v,
                'ID' => $i
            ]);

            if($ab->update($ab)){
                $ab = new BillingAffiliate();
                $ab->setProperties(['ID'=>$i])->setQueryParameters($ab);
                $get = $ab->one();
                echo '{"total":"'.self::total($get).'","deduction":"'.number_format($get->deduction).'"}';

                self::generate_invoice($i);
            }
            else
                echo '{"error":"Update failed."}';

            M::setDbName();
        }
    }

    public static function toggle():void{
        if ($i = Request::getRequiredField('i')){

            $v = Request::getRequiredField('v');
            M::setDbName(M::STATS);
            $ab = new BillingAffiliate();
            $ab->setProperties([
                'status' => $v,
                'ID' => $i
            ]);

            if($ab->update($ab)){
                echo '{"error":false}';

                self::generate_invoice($i);
            }
            else
                echo '{"error":"Update failed."}';

            M::setDbName();
        }
    }

    public static function total(&$o): string
    {
        if ($o === null){
            die;
        }
        if ($o->sales > 0) {
            $one = $o->payout / $o->sales;
            $final = $o->sales - $o->deduction;
            $o->total = $one * $final;
            $total = number_format($o->total, 2);
        } else {
            $total = 0;
        }
        return "<code>$</code><strong>$total</strong>";
    }

    public static function status($s):string {
        if ($s === '0'){
            return '<div class="text-danger">uncomfirmed</div>';
        }
            return '<div class="text-success">comfirmed</div>';
    }

    public static function uiStatus($s):string {
        if ($s === '0'){
            return 'text-success fa-play';
        }
            return 'text-danger fa-stop';
    }

    public static function generate_invoice($id): void
    {

        $b = new BillingAffiliate();
        $b->setProperties([
            'ID' => $id
        ])
            ->setQueryParameters($b, ['month', 'affiliate_ID', 'affiliate']);
        $ok = $b->one();
        $af = $ok->affiliate_ID;
        $email = $ok->affiliate;
        $month = $ok->month;

        $sql = 'SELECT SUM(sales) s, SUM(payout) p, SUM(deduction) d, affiliate email
FROM vine_ad_stats.billing_affiliate 
WHERE status = 1 AND month = :m AND affiliate_ID = :a 
GROUP BY offers_ID';

        M::setDbName(M::STATS);

        $results = M::select($sql, (new BillingAffiliate())->getMyClass(), [':m' => $month, ':a' => $af]);

        $iaa = new \model\InvoiceAffiliate();
        $iaa->setProperties([
            'affiliate_ID' => $af,
            'month' => $month
        ])
            ->setQueryParameters($iaa);
        $isOne = $iaa->one();

        $total = 0;
        foreach ($results as $o) {
            $one = $o->p / $o->s;
            $final = $o->s - $o->d;
            $total += $one * $final;
        }

        $ia = new IA();
        $ia->setProperties([
            'affiliate_ID' => $af,
            'status' => 0,
            'month' => date('Ym', strtotime('-2 month'))
        ])
            ->setQueryParameters($ia, ['amount']);
        $ia->appendQuery('amount < 100');
        $oldAmt = (float)$ia->one()->amount;

        $total += $oldAmt;
        if ($isOne) {
            if ($isOne->status === '0') {
                $sql = "UPDATE vine_ad_stats.invoice_affiliate SET amount = $total WHERE affiliate_ID = $af AND month = $month";
                M::update($sql);
            }
        } elseif ($total > 0) {
            $ia = new IA();
            $ia->setProperties([
                'affiliate_ID' => $af,
                'affiliate' => $email,
                'month' => $month,
                'amount' => $total,
                'at' => time()
            ])
                ->insert($ia);
        }

        M::setDbName();
    }

}