<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 28/4/18
 * Time: 9:04 AM
 */

namespace controller;


use helper\Mysql;
use helper\Request as R;
use helper\Template;
use model\PostbackLog;

class PostbackLogs extends Controller
{

    public static function index(): void
    {
        $template = new Template('logs');
        $template->setTemplateField('title', 'Postback Logs');
        $template->setTemplateField('html', 'postback_log_index.html.php');

        $args = [];
        if (self::getSessionType() === 'AFFILIATE') {
            $args['affiliate_ID'] = self::getSession();
        } elseif ($af = R::getField('af', 'GET')) {
            $args['affiliate_ID'] = $af;
        }
        if ($of = R::getField('of', 'GET')) {
            $args['offers_ID'] = $of;
        }
        if ($d = R::getField('day', 'GET')) {
            $args['day'] = $d;
        } else {
            $args['day'] = date('Ymd');
        }

        Mysql::setDbName(Mysql::STATS);
        $l = new PostbackLog();
        $l->setProperties($args)
            ->setQueryParameters($l);
        $get = $l->query();

        $template->render($get);
    }
}