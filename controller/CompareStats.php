<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 26/1/18
 * Time: 7:17 PM
 */

namespace controller;


use helper\Request;
use helper\Template;
use model\HourlyStats;

class CompareStats extends Controller
{
    public static function index(){die('Work in progress');

        $template = new Template('stats');
        $template->setTemplateField('title','Compare Stats');
        $template->setTemplateField('html','compare.html.php');

        $push = false;
        $flag = false;
        $by = 'hour_';

        if (!($date = Request::getField('date', 'GET'))) {
            $date = date('Ymd');
        }
        if (!($date0 = Request::getField('date0', 'GET'))) {
            $date0 = date('Ymd');
        }

        $gh = new HourlyStats();
        $gh0 = new HourlyStats();

        if (self::getSessionType() === 'AFFILIATE'){
            $params = $params1 = [
                'affiliate_ID' => self::getSession()
            ];
            $push = true;
        }else{
            $params = $params1 = [];
        }

        if (!($date1 = Request::getField('date1','GET'))){
            if (\strlen($date) === 8) {
                $params['day_'] = $date;
            }else{
                $params['month_'] = $date;
                $flag = true;
                $by = 'day_';
                $first = (int)$date.'01';
                if ($date == date('Ym')){
                    $last = (int)date('Ymd');
                }else{
                    $last = (int)$date.'31';
                }
            }
        }elseif($date !== $date1){
            $flag = true;
            $by = 'day_';
            $first = $date;
            $last = $date1;
        }
        if (!($date10 = Request::getField('date10','GET'))){
            if (\strlen($date0) === 8) {
                $params1['day_'] = $date0;
            }else{
                $params1['month_'] = $date0;
                $flag = true;
                $by = 'day_';
                $first0 = (int)$date0.'01';
                if ($date0 == date('Ym')){
                    $last0 = (int)date('Ymd');
                }else{
                    $last0 = (int)$date0.'31';
                }
            }
        }elseif($date0 !== $date10){
            $flag = true;
            $by = 'day_';
            $first0 = $date0;
            $last0 = $date10;
        }

        if ($of = Request::getField('of', 'GET')) {
            $params['offers_ID'] = $of;
            $params1['offers_ID'] = $of;
        }
        if ($af = Request::getField('af','GET')){
            $params['affiliate_ID'] = $af;
            $params1['affiliate_ID'] = $af;
        }

        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['*,SUM(stats_grouped_hourly.clicks) visit,SUM(stats_grouped_hourly.sales) conver,SUM(stats_grouped_hourly.payout) push,SUM(stats_grouped_hourly.charge) pull'], '', 'GROUP BY '.$by);
        if($date1){
            $gh->appendQuery("day_ >= $date and day_ <= $date1");
        }

        $stats = $gh->query();

        $gh0->setProperties($params1);
        $gh0->setQueryParameters($gh0, ['*,SUM(stats_grouped_hourly.clicks) visit,SUM(stats_grouped_hourly.sales) conver,SUM(stats_grouped_hourly.payout) push,SUM(stats_grouped_hourly.charge) pull'], '', 'GROUP BY '.$by);
        if($date10){
            $gh0->appendQuery("day_ >= $date0 and day_ <= $date10");
        }

        $stats0 = $gh0->query();

        $series = $cate = [];
        $series[0]['name'] = 'Total ConversionsStats';
        $series[1]['name'] = 'Total Earnings';
        $series[2]['name'] = 'Total Visits';
        if ($flag){
            for($i=$first,$j=0;$i<=$last;$i++){
                $day = (int)$stats[$j]->day_;
                if ($i == $day && $stats[$j]->ID){
                    $series[0]['data'][] = (int)$stats[$j]->conver;
                    if ($push)
                        $series[1]['data'][] = round((float)$stats[$j]->push, 2);
                    else
                        $series[1]['data'][] = round($stats[$j]->pull - $stats[$j]->push, 2);
                    $series[2]['data'][] = (int)$stats[$j]->visit;

                    $j++;
                }else{
                    $series[0]['data'][] = '<span class="text-danger">0</span>';
                    $series[1]['data'][] = '<span class="text-danger">0</span>';
                    $series[2]['data'][] = '<span class="text-danger">0</span>';
                }
                $cate[] = $i.'';
            }
        }else {
            for ($i = 0, $j = 0; $i < 24; $i++) {
                $hour = (int)$stats[$j]->hour_;
                if ($i === $hour && $stats[$j]->ID) {
                    $series[0]['data'][] = (int)$stats[$j]->conver;
                    if ($push)
                        $series[1]['data'][] = round((float)$stats[$j]->push, 2);
                    else
                        $series[1]['data'][] = round($stats[$j]->pull - $stats[$j]->push, 2);
                    $series[2]['data'][] = (int)$stats[$j]->visit;
                    $j++;
                } else {
                    $series[0]['data'][] = '<span class="text-danger">0</span>';
                    $series[1]['data'][] = '<span class="text-danger">0</span>';
                    $series[2]['data'][] = '<span class="text-danger">0</span>';
                }
                $cate[] = $i . '';
            }
        }

        $series0 = $cate0 = [];
        $series0[0]['name'] = 'Total ConversionsStats';
        $series0[1]['name'] = 'Total Earnings';
        $series0[2]['name'] = 'Total Visits';
        if ($flag){
            for($i=$first0,$j=0;$i<=$last0;$i++){
                $day = (int)$stats0[$j]->day_;
                if ($i == $day && $stats0[$j]->ID){
                    $series0[0]['data'][] = (int)$stats0[$j]->conver;
                    if ($push)
                        $series0[1]['data'][] = round((float)$stats0[$j]->push, 2);
                    else
                        $series0[1]['data'][] = round($stats0[$j]->pull - $stats0[$j]->push, 2);
                    $series0[2]['data'][] = (int)$stats0[$j]->visit;
                    $j++;
                }else{
                    $series0[0]['data'][] = '<span class="text-danger">0</span>';
                    $series0[1]['data'][] = '<span class="text-danger">0</span>';
                    $series0[2]['data'][] = '<span class="text-danger">0</span>';
                }
                $cate0[] = $i.'';
            }
        }else {
            for ($i = 0, $j = 0; $i < 24; $i++) {
                $hour = (int)$stats0[$j]->hour_;
                if ($i === $hour && $stats0[$j]->ID) {
                    $series0[0]['data'][] = (int)$stats0[$j]->conver;
                    if ($push)
                        $series0[1]['data'][] = round((float)$stats0[$j]->push, 2);
                    else
                        $series0[1]['data'][] = round($stats0[$j]->pull - $stats0[$j]->push, 2);
                    $series0[2]['data'][] = (int)$stats0[$j]->visit;
                    $j++;
                } else {
                    $series0[0]['data'][] = '<span class="text-danger">0</span>';
                    $series0[1]['data'][] = '<span class="text-danger">0</span>';
                    $series0[2]['data'][] = '<span class="text-danger">0</span>';
                }
                $cate0[] = $i . '';
            }
        }

        $well = $date;
        $well0 = $date0;
        if ($flag){
            $well = $first.' - '.$last;
            $well0 = $first0.' - '.$last0;
        }
        $all['for'] = 'UTC Timezone Hours for '.$well;
        $all['for0'] = 'UTC Timezone Hours for '.$well0;
        $all['series'] = json_encode($series);
        $all['series0'] = json_encode($series0);
        $all['cate'] = json_encode($cate);
        $all['cate0'] = json_encode($cate0);

        $template->render($all);
    }

}