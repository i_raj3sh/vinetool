<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/14/17
 * Time: 7:56 AM
 */

namespace controller;


use helper\Form;
use helper\Mongo;
use helper\MyException;
use helper\Request;
use helper\Template;
use model\ActivityLog_;

class Postback extends Controller
{

    public static function index()
    {
        if (self::getSessionType() === 'ADMIN'){
            self::redirect();
        }

        $template = new Template('postback');
        $template->setTemplateField('title', 'My Postback URL');
        $template->setTemplateField('html', 'index.html.php');

        $form = new Form();
        $a = new \model\Postback();
        $a->setProperties([
            'affiliate_ID' => self::getSession()
        ]);
        $a->setQueryParameters($a);
        $form->setForm($a->one());
        $template->render($form);
    }

    public static function add(){

        if (self::getSessionType() === 'ADMIN'){
            self::redirect();
        }

        if (Request::getRequiredField('postback','POST')){
            $url = Request::getRequiredField('posturl','POST');

            if(false === strpos($url, '{clickid}')){
                MyException::setSessionError('{clickid} must be present.');
                self::redirect('postback');
            }

            $a = new \model\Postback();
            $args = [
                'affiliate_ID' => self::getSession(),
                'url' => $url,
                'global' => 1
                ];

            if ($id = Request::getField('id','POST')){
                $args['ID'] = $id;

                $arg = [];
                $arg[0] = [
                    'PID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'UPDATE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'Postback "' . $url . '"'
                    ]
                ];

            } else {
                $arg = [];
                $arg[0] = [
                    'PID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'INSERT',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'Postback "' . $url . '"'
                    ]
                ];
            }

            $a->setProperties($args);
            if (!$a->save($a)){
                MyException::setSessionError('Nothing changed.');
            } else {


                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }

                MyException::setSessionSuccess('Successful');
            }

            self::redirect('postback');
        }
    }
}