<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/12/17
 * Time: 8:53 PM
 */

namespace controller;


use helper\Form;
use helper\LogTraffic;
use helper\Mailer;
use helper\Mongo;
use helper\MyException;
use helper\Mysql;
use helper\Request;
use helper\Template;
use helper\Tool;
use model\ActivityLog_;

class ApproveOffers extends Controller
{

    public static function index()
    {

        $template = new Template('approve');
        $template->setTemplateField('title', 'Approve a new Offer to an Affiliate');
        $template->setTemplateField('html', 'index.html.php');

        $ao = new \model\ApproveOffers();
        $o = new \model\Offers();
        $a = new \model\Affiliate();

        if (!($date = Request::getField('date','GET'))){
            $date = date('Ymd');
        }

        if (\strlen($date) === 6) {
            $str = "added >= ${date}0100 AND added <= ${date}3100";

        }elseif ($date1 = Request::getField('date1','GET')){
            $str = "added >= ${date}00 AND added <= ${date1}23";
        }else{
            $str = "added >= ${date}00 AND added <= ${date}23";
        }

        $args = [
            'status' => 1,
            'affiliate.active' => 1,
            'offers.active' => 1
        ];

        if ($af = Request::getField('af', Request::GET)) {
            $args['affiliate_ID'] = $af;
        } elseif (!isset($_GET['as'])) {
            $args['status'] = 0;
            $str = '';
        }
        if ($of = Request::getField('of', Request::GET)) {
            $args['offers_ID'] = $of;
        }

        if ($nw = Request::getField('nw', Request::GET)) {
            $args['network_ID'] = $nw;
        }


        if ($ap = Request::getField('ap', Request::GET)) {
            $args = [
                'affiliate_offers.ID' => $ap,
                'affiliate.active' => 1
            ];
            $str = '';
        }

        $o->setProperties($args);
        $ao->setQueryParameters($o, ['offers.ID', 'offers.type', 'offers.charge', 'offers.active', 'offers.name','offers.network_ID']);
        $ao->setQueryParameters($ao, ['affiliate_offers.ID AOID', 'status', 'percentage', 'api','requested', 'approved']);
        $ao->setQueryParameters($a, ['affiliate.ID AID', 'affiliate.name pub', 'affiliate.active state'],'','','ORDER BY AOID DESC');
        $ao->appendQuery($str);
        $get = $ao->query();
        $template->render($get);
    }

    public static function edit()
    {

        $template = new Template('approve');
        $template->setTemplateField('title', 'Edit Approved Offer to an Affiliate');
        $template->setTemplateField('html', 'edit.html.php');

        $form = new Form();
        if ($id = Request::getField('id', 'GET')) {
            $ao = new \model\ApproveOffers();
            $o = new \model\Offers();
            $a = new \model\Affiliate();

            $args = [
                'affiliate.active' => 1,
                'offers.active' => 1,
                'affiliate_offers.ID' => $id
            ];

            $o->setProperties($args);
            $ao->setQueryParameters($o, ['offers.name', 'offers.ID OID']);
            $ao->setQueryParameters($ao, ['affiliate_offers.ID AOID', 'status', 'percentage', 'affiliate_ID AID', 'cap']);
            $ao->setQueryParameters($a, ['affiliate.ID', 'affiliate.name pub']);
            $form->setForm($ao->one());
        }
        $template->render($form);
    }

    public static function add()
    {
        if (Request::getRequiredField('approve', 'POST')) {
            $affiliate = Request::getRequiredField('affiliate', 'POST');
            $offer = Request::getRequiredField('offer', 'POST');
            $cap = Request::getField('cap', 'POST');
            $percent = Request::getField('percent', 'POST');
            $status = Request::getField('status', 'POST');

            if (empty($percent))
                $percent = COMMISSION * 100;
            if (empty($cap))
                $cap = -1;

            $a = new \model\ApproveOffers();
            $args = [
                'affiliate_ID' => $affiliate,
                'offers_ID' => $offer,
                'cap' => $cap,
                'percentage' => $percent,
                'status' => $status,
                'added' => date('YmdH'),
                'requested' => time(),
                'approved' => time()
            ];
            $loc = 'approveOffers/edit';
            if ($id = Request::getField('id', 'POST')) {
                $args['ID'] = $id;
                unset($args['added'],$args['approved'],$args['requested']);
                $loc = 'approveOffers/edit?id=' . $id;
            } else {
                $o = new \model\Offers();
                $n = new \model\Network();
                $o->setProperties([
                    'offers.ID' => $offer,
                    'tracking' => ''
                ]);
                $o->setQueryParameters($o, ['tracking', 'advID']);
                $o->setQueryParameters($n, ['approvalLink', 'network.name ADV']);

                $get = $o->one();

                if ($get) {
                    $getData = Tool::hitCurl($get->approvalLink . $get->advID);
                    $getStatus = json_decode($getData);

                    if ($getStatus->approvals->{$get->advID}->Status !== 'sent') {
                        $log = new LogTraffic('requested.log');
                        $log->setLogData($offer, 'ID');
                        $log->setLogData($getData, 'RESPONSE');
                        $log->writeLog();

                        MyException::setSessionError('Could not request this offer. Approval Failed. Contact: ' . $get->ADV);
                        self::redirect('approveOffers');
                    }

                    $args['status'] = 0;
                    $args['api'] = 1;

                    $hidden = true;
                }

                $sql = "SELECT * FROM vine_ad_db.blocked_offer_rules WHERE offers_ID = $offer AND affiliate_ID = $affiliate";
                if (Mysql::numRows($sql,[])){
                    MyException::setSessionError('This offer is blocked for this affiliate.');
                    self::redirect($loc);
                }

                $sql = "SELECT o.ID FROM vine_ad_db.blocked_network_rules bn 
                        JOIN vine_ad_db.offers o ON bn.network_ID = o.network_ID 
                        WHERE o.ID = $offer AND affiliate_ID = $affiliate";
                if (Mysql::numRows($sql,[])){
                    MyException::setSessionError('This network is blocked for this affiliate.');
                    self::redirect($loc);
                }
            }
            $a->setProperties($args);

            $aa = new \model\ApproveOffers();
            $aa->setProperties(['affiliate_ID' => $affiliate,
                'offers_ID' => $offer]);
            $aa->setQueryParameters($aa);
            if (!$id && $aa->count()) {
                MyException::setSessionError('This offer is already set to this affiliate.');
                self::redirect($loc);
            }
            if (!($i = $a->save($a))) {
                MyException::setSessionError('No changes made.');
                self::redirect($loc);
            }
            if (!$id) {
                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'INSERT',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "TID #$i with Affiliate #$affiliate & Offer #$offer"
                    ]
                ];
                self::mailApprove($affiliate, $offer, $i);
            } else {
                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'UPDATE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "TID #$id with Affiliate #$affiliate & Offer #$offer"
                    ]
                ];
            }

            if ($hidden) {
                MyException::setSessionError('This offer will be approved after Advertiser ' . $get->ADV . '\'s approval. This will take at most 30 mins.');
            } else {
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }
                MyException::setSessionSuccess('Successful');
            }
            self::redirect('approveOffers');
        }
    }

    public static function del($id = '')
    {
        if (!$id) {
            $id = Request::getRequiredField('id', 'GET');
            $redirect = true;
        }
        if ($id) {

            $a = new \model\ApproveOffers();
            $a->setProperties([
                'ID' => $id
            ]);
            if (!$a->remove()) {
                MyException::setSessionError('Need SUPER privilege to delete.');
            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'TID #' . $id
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }

                MyException::setSessionSuccess('Delete successful.');
            }
        }
        if ($redirect) {
            self::redirect('approveOffers');
        }
    }

    public static function tick($id = '')
    {
        if (!$id) {
            $id = Request::getRequiredField('id', 'GET');
            $redirect = true;
        }
        if ($id) {

            $a = new \model\ApproveOffers();
            $a->setProperties([
                'ID' => $id,
                'status' => 1,
                'added' => date('YmdH'),
                'approved' => time()
            ]);
            if (!$a->update($a)) {
                MyException::setSessionError('Something went wrong.');
            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'OPERATION',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'Approved TID #' . $id
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }

                MyException::setSessionSuccess('Approved');

                $a->setQueryParameters($a);
                $get = $a->one();

                self::mailApprove($get->affiliate_ID, $get->offers_ID, $id);
            }
        }
        if ($redirect) {
            self::redirect('approveOffers');
        }
    }

    private static function mailApprove($aff, $off, $id)
    {

        $af = new \model\Affiliate();
        $af->setProperties([
            'ID' => $aff
        ])->setQueryParameters($af, ['name', 'email']);

        $affiliate = $af->one();

        $of = new \model\Offers();
        $of->setProperties([
            'ID' => $off
        ])->setQueryParameters($of, ['ID', 'name', 'icon', 'charge', 'caps']);

        $offer = $of->one();

        $arr = [
            'name' => $affiliate->name,
            'ID' => $offer->ID,
            'icon' => $offer->icon,
            'title' => $offer->name,
            'payout' => $offer->charge * COMMISSION,
            'cap' => $offer->caps == 0 ? 'n/a' : $offer->caps,
            'tracking' =>  clc() . $id,
            'macros' => '<em>&clickid=</em> Your click id<br/>
            <em>&sub_affid=</em> Sub affiliate id<br/>
            <em>&sub_sourceid=</em> Your affiliate\'s source id<br/>
            <em>&idfa=</em> Apple\'s advertiser identifier (IDFA)<br/>
            <em>&gaid=</em> Google\'s advertising identifier (GAID)<br/>'
        ];

        $m = new Mailer();
        $m
            ->setHeaders()
            ->setMessage('approvedMailPub', $arr)
            ->send($affiliate->email, 'An offer Approved from VineTool');
    }

    public static function tickAll()
    {
        if ($id = Request::getRequiredField('ids', 'GET')) {
            $ids = explode(',', $id);
            foreach ($ids as $id) {
                self::tick($id);
            }
        }
        self::redirect('approveOffers');
    }

    public static function delAll()
    {
        if ($id = Request::getRequiredField('ids', 'GET')) {
            $ids = explode(',', $id);
            foreach ($ids as $id) {
                self::del($id);
            }
        }
        self::redirect('approveOffers');
    }

    public static function getPub():void{
        if ($id = Request::getRequiredField('id', 'GET')) {
            $a = new \model\ApproveOffers();
            $a->setProperties([
                'offers_ID' => $id
            ])
                ->setQueryParameters($a, ['ID','affiliate_ID','percentage','status']);
            $a->appendQuery('status != 0');
            $all = $a->query();

            $arr = [];
            foreach ($all as $v){
                $arr[] = [
                    'id' => $v->ID,
                    'affid' => $v->affiliate_ID,
                    'per' => $v->percentage/100,
                    'status' => $v->status
                ];
            }
            echo json_encode($arr);
        }
    }

    public static function toggle():void{
        if ($id = Request::getRequiredField('i', 'GET')) {
            $s = Request::getRequiredField('s', 'GET');

            $a = new \model\ApproveOffers();
            $a->setProperties([
                'ID' => $id,
                'status' => $s
            ]);
            $row = $a->update($a);

            $l = 'Paused';
            if ($s === '1'){
                $l = 'Active';
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'UPDATE',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "status of TID[$id] to $l"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            if ($row){
                echo '{"error":false}';
            }else{
                echo '{"error":"Update failed"}';
            }
        }
    }

    public static function assign():void{
        if ($af = Request::getRequiredField('af', 'GET')) {
            $c = Request::getField('c', 'GET');
            $p = Request::getRequiredField('p', 'GET');
            $ids = Request::getRequiredField('ids', 'GET');

            if (!$c){
                $c = -1;
            }

            $in = false;
            $t = time();

            $offers = explode(',',$ids);

            foreach ($offers as $offer){

                $sql = "SELECT * FROM vine_ad_db.blocked_offer_rules WHERE offers_ID = $offer AND affiliate_ID = $af";
                if (Mysql::numRows($sql,[])){
                    $in = true;
                    continue;
                }

                $sql = "SELECT o.ID FROM vine_ad_db.blocked_network_rules bn 
                        JOIN vine_ad_db.offers o ON bn.network_ID = o.network_ID 
                        WHERE o.ID = $offer AND affiliate_ID = $af";
                if (Mysql::numRows($sql,[])){
                    $in = true;
                    continue;
                }

                $aa = new \model\ApproveOffers();
                $aa->setProperties(['affiliate_ID' => $af, 'offers_ID' => $offer])
                    ->setQueryParameters($aa);
                if ($aa->count()) {
                    $in = true;
                    continue;
                }

                $a = new \model\ApproveOffers();
                $a->setProperties([
                    'offers_ID' => $offer,
                    'affiliate_ID' => $af,
                    'percentage' => $p,
                    'cap' => $c,
                    'requested' => $t,
                    'approved' => $t,
                    'added' => date('YmdH'),
                    'status' => 1
                ])
                    ->insert($a);
            }

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'INSERT',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "[$ids] to affiliate[$af]"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            if ($in){
                echo '{"error":"Some offers are not assigned (exists in blocked rules)"}';
            }else {
                echo '{"error":false}';
            }
        }
    }

    public static function countPub(){
        if ($ids = Request::getField('ids', 'POST')) {

            $a = new \model\ApproveOffers();
            $a->setQueryParameters($a,['offers_ID id','count(affiliate_ID) c'],'','GROUP BY offers_ID');
            $a->appendQuery("offers_ID IN ($ids) AND status != 0");
            $res = $a->query();

            $json = [];
            foreach ($res as $v){
                $json[] = [
                    'id' => $v->id,
                    'count' => $v->c
                ];
            }
            echo json_encode($json);
        }
    }
}