<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/13/17
 * Time: 10:30 PM
 */

namespace controller;

use helper\Form;
use helper\Mongo;
use helper\MyException;
use helper\Mysql;
use helper\Request;
use helper\Template;
use model\ActivityLog_;
use model\Offers as O;
use model\Target as TG;

class Target extends Controller
{

    public static function index()
    {

        $template = new Template('target');
        $template->setTemplateField('title', 'Target');
        $template->setTemplateField('html', 'target_index.html.php');

        $where = '1';
        if ($af = Request::getField('af', Request::GET)) {
            $where .= ' and affiliate_ID = ' . $af;
        }
        if ($af = Request::getField('geo', Request::GET)) {
            $where .= " and geo = '$af'";
        }
        if ($af = Request::getField('os', Request::GET)) {
            $where .= " and os = '$af'";
        }

        $sql = "select count(ot.offers_ID) o, t.* from target t left join target_offers ot on t.ID = ot.target_ID where $where group by t.ID";

        $template->render(Mysql::select($sql, (new TG())->getMyClass()));
    }

    public static function edit()
    {

        $template = new Template('target');
        $template->setTemplateField('title', 'Edit Target Details');
        $template->setTemplateField('html', 'target_edit.html.php');

        $form = new Form();
        if ($id = Request::getField('id', 'GET')) {
            $a = new TG();
            $a->setProperties([
                'ID' => $id
            ]);
            $a->setQueryParameters($a);
            $form->setForm($a->one());
        }
        $template->render($form);
    }

    public static function add()
    {

        if (Request::getRequiredField('target', 'POST')) {
            $affiliate_ID = Request::getRequiredField('affiliate_ID', 'POST');
            $geo = Request::getRequiredField('geo', 'POST');
            $region = Request::getRequiredField('region', 'POST');
            $device = Request::getRequiredField('device', 'POST');
            $os = Request::getRequiredField('os', 'POST');
            $category = Request::getField('category', 'POST');
            $status = Request::getRequiredField('status', 'POST');

            $a = new TG();
            $args = [
                'affiliate_ID' => $affiliate_ID,
                'geo' => $geo,
                'region' => $region,
                'device' => $device,
                'os' => $os,
//                'category' => $category,
                'status' => $status,
            ];

            $loc = 'target/edit';
            if ($id = Request::getField('id', 'POST')) {
                $args['ID'] = $id;
                $loc = 'target/edit?id=' . $id;

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'UPDATE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Target {$id}'s details"
                    ]
                ];

            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'INSERT',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Target $id"
                    ]
                ];

            }
            $a->setProperties($args);
            if (!$a->save($a)) {
                MyException::setSessionError('Something went wrong.');
                self::redirect($loc);
            }

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Successful');
            self::redirect('target');
        }
    }

    public static function del()
    {
        if ($id = Request::getRequiredField('id', 'GET')) {

            $a = new TG();
            $a->setProperties([
                'ID' => $id
            ]);
            if (!$a->remove()) {
                MyException::setSessionError('Need SUPER privilege to delete.');
            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'DELETE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => 'Target #' . $id
                    ]
                ];
                $db = new Mongo();
                try {
                    $db->insert(new ActivityLog_(), $arg);
                } catch (\Exception $e) {
                }

                MyException::setSessionSuccess('Delete successful');
            }
            self::redirect('target');
        }
    }

    public static function pullCampaigns(): void
    {
        if ($id = Request::getRequiredField('id', Request::GET)) {
            $sql = "select o.ID, o.name from target_offers ot join offers o on ot.offers_ID = o.ID where target_ID = $id";
            $offers = Mysql::select($sql, (new \model\Offers())->getMyClass());
            echo json_encode($offers);
        }
    }

    public static function pushCampaigns(): void
    {
        if (($id = Request::getRequiredField('id', Request::GET)) && $c = Request::getRequiredField('cid', Request::GET)) {

            $args = [
                'target_id=?' => $id,
                'offers_id=?' => $c
            ];
            if (Mysql::put('insert', 'target_offers', $args))
                die('true');
        }
    }

    public static function offer(): void
    {
        if ($q = Request::getRequiredField('q', Request::GET)) {
            $g = Request::getRequiredField('g', Request::GET);
            $d = Request::getRequiredField('d', Request::GET);
            $a = Request::getRequiredField('p', Request::GET);

            $sql = 'select o.ID, name from vine_ad_db.offers o ' .
                'join vine_ad_db.affiliate_offers ao on o.ID = ao.offers_ID ' .
                "where ao.affiliate_ID = $a and ao.status = 1 and " .
                "o.country like '%$g%' and o.device like '%$d%' and o.active = 1 " .
                "and (name like '%$q%' or o.ID = '$q')"; //echo $sql;

            $res = Mysql::select($sql, (new O())->getMyClass());
            $at = [];
            foreach ($res as $v) {
                $at[] = [
                    'id' => $v->ID,
                    'value' => "#$v->ID $v->name"
                ];
            }
            echo json_encode($at);
        }
    }

    public static function delOffs()
    {
        if ($i = Request::getRequiredField('i', Request::GET)) {
            $o = Request::getRequiredField('o', Request::GET);

            $sql = "delete from vine_ad_db.target_offers where target_ID = $i and offers_ID = $o";
            Mysql::delete($sql);
        }
    }

}