<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/14/17
 * Time: 11:10 AM
 */

namespace controller;


use helper\Cache;
use helper\Mysql;
use helper\Request;
use helper\Template;
use helper\Tool;
use model\HourlyStats;

class Dashboard extends Controller
{

    public static function index()
    {

        if (self::getSessionType() === 'ADMIN'){
            self::redirect();
        }

        $all = [];

        $template = new Template('dash');
        $template->setTemplateField('title', 'Dashboard');
        $template->setTemplateField('html', 'affiliate.html.php');

        if (self::getSessionType() === 'AFFILIATE') {

            $flag = false;
            $isCache = true;
            $by = 'hour_';
            $type = 'Hour';
            $title = 'Stats breakdown based on ';

            $Today = date('Ymd');

            if (!($date = Request::getField('date', 'GET'))) {
                $date = $Today;
            }

            if ($date !== $Today){
                $isCache = false;
            }

            Mysql::setDbName(Mysql::STATS);
            $gh = new HourlyStats();

            if ($date1 = Request::getField('date1', 'GET')) {
                $params = [
                    'affiliate_ID' => self::getSession()
                ];
                $flag = true;
                $by = 'day_';
                $first = $date;
                $last = $date1;
                $title .= "days from $first to $last";
            } else {
                if (\strlen($date) === 8) {
                    $params = [
                        'day_' => $date,
                        'affiliate_ID' => self::getSession()
                    ];
                    $title .= "hours for $date";
                } else {
                    $params = [
                        'month_' => $date,
                        'affiliate_ID' => self::getSession()
                    ];
                    $flag = true;
                    $by = 'day_';
                    $type = 'Date';
                    $first = (int)$date . '01';
                    if ($date == date('Ym')) {
                        $last = (int)date('Ymd');
                    } else {
                        $last = (int)$date . '31';
                    }
                    $title .= 'days for a month of ' . date('M', strtotime($first));
                }
            }

            if ($of = Request::getField('of', 'GET')) {
                $params['offers_ID'] = $of;
                $isCache = false;
            }

            $gh->setProperties($params);
            $gh->setQueryParameters($gh, ['*', 'SUM(dropped) lost', 'SUM(clicks) visit', 'SUM(sales) conver', 'SUM(payout) push'], '', 'GROUP BY ' . $by);
            if ($date1) {
                $gh->appendQuery("day_ >= $date and day_ <= $date1");
                $type = 'Date';
                $isCache = false;
            }

            $cObj = new Cache();
            if (!$isCache || !($cache = $cObj->retrieve(__METHOD__))) {
                $stats = $gh->query();

                $series = $series2 = [];
                $cate = $tat = [];
                $series2[0]['name'] = 'Total Conversions';
                $series2[1]['name'] = 'Total Earnings';
                $series[0]['name'] = 'Total Visits';
                if ($flag) {
                    for ($i = $first, $j = 0; $i <= $last; $i++) {
                        $day = (int)$stats[$j]->day_;
                        $tat[$i] = new \stdClass();
                        $tat[$i]->hour = '<strong>' . Tool::hourFormat($i) . '</strong>';
                        if ($i == $day && $stats[$j]->ID) {
                            $series2[0]['data'][] = (int)$stats[$j]->conver;
                            $series2[1]['data'][] = round($stats[$j]->push, 2);
                            $series[0]['data'][] = (int)$stats[$j]->visit;
                            $tat[$i]->total = '<strong class="">' . number_format($stats[$j]->visit + $stats[$j]->lost) . '</strong>';
                            $tat[$i]->visit = '<strong class="">' . number_format($stats[$j]->visit) . '</strong>';
                            $tat[$i]->convert = '<strong class="">' . number_format($stats[$j]->conver) . '</strong>';
                            $tat[$i]->push = '<strong class="">' . number_format( $stats[$j]->push,2) . '</strong>';
                            $tat[$i]->EPC = '<strong>' . number_format( $stats[$j]->push / $stats[$j]->visit,4) . '</strong>';
                            $tat[$i]->CR = '<strong>' . number_format( ($stats[$j]->conver / $stats[$j]->visit) * 100,4) . '</strong>';
                            $tat[$i]->ECPM = '<strong>' . number_format( $stats[$j]->push * 1000 / $stats[$j]->visit,4) . '</strong>';
                            $j++;
                        } else {
                            $series2[0]['data'][] = '<span class="text-danger">0</span>';
                            $series2[1]['data'][] = '<span class="text-danger">0</span>';
                            $series[0]['data'][] = '<span class="text-danger">0</span>';
                            $tat[$i]->total = '<span class="text-danger">0</span>';
                            $tat[$i]->visit = '<span class="text-danger">0</span>';
                            $tat[$i]->convert = '<span class="text-danger">0</span>';
                            $tat[$i]->push = '<span class="text-danger">0</span>';
                            $tat[$i]->EPC = '<span class="text-danger">0</span>';
                            $tat[$i]->CR = '<span class="text-danger">0</span>';
                            $tat[$i]->ECPM = '<span class="text-danger">0</span>';
                        }
                        $cate[] = $i . '';
                        Tool::dateOverflowCheck($i);
                    }
                } else {
                    for ($i = 0, $j = 0; $i < 24; $i++) {
                        $hour = (int)$stats[$j]->hour_;
                        $tat[$i] = new \stdClass();
                        $tat[$i]->hour = '<strong>' . Tool::hourFormat($i) . '</strong>';
                        if ($i === $hour && $stats[$j]->ID) {
                            $series2[0]['data'][] = (int)$stats[$j]->conver;
                            $series2[1]['data'][] = round((float)$stats[$j]->push, 2);
                            $series[0]['data'][] = (int)$stats[$j]->visit;
                            $tat[$i]->total = '<strong class="">' . number_format($stats[$j]->visit + $stats[$j]->lost) . '</strong>';
                            $tat[$i]->visit = '<strong class="">' . number_format($stats[$j]->visit) . '</strong>';
                            $tat[$i]->convert = '<strong class="">' . number_format($stats[$j]->conver) . '</strong>';
                            $tat[$i]->push = '<strong class="">' . number_format( $stats[$j]->push,2) . '</strong>';
                            $tat[$i]->EPC = '<strong>' . number_format( $stats[$j]->push / $stats[$j]->visit,4) . '</strong>';
                            $tat[$i]->CR = '<strong>' . number_format( ($stats[$j]->conver / $stats[$j]->visit) * 100,4) . '</strong>';
                            $tat[$i]->ECPM = '<strong>' . number_format( $stats[$j]->push * 1000 / $stats[$j]->visit,4) . '</strong>';
                            $j++;
                        } else {
                            $series2[0]['data'][] = '<span class="text-danger">0</span>';
                            $series2[1]['data'][] = '<span class="text-danger">0</span>';
                            $series[0]['data'][] = '<span class="text-danger">0</span>';
                            $tat[$i]->total = '<span class="text-danger">0</span>';
                            $tat[$i]->visit = '<span class="text-danger">0</span>';
                            $tat[$i]->convert = '<span class="text-danger">0</span>';
                            $tat[$i]->push = '<span class="text-danger">0</span>';
                            $tat[$i]->EPC = '<span class="text-danger">0</span>';
                            $tat[$i]->CR = '<span class="text-danger">0</span>';
                            $tat[$i]->ECPM = '<span class="text-danger">0</span>';
                        }
                        $cate[] = $i . '';
                    }
                }

                $geo = new HourlyStats();
                $geo->setProperties($params);
                $geo->setQueryParameters($geo, ['ID','country','SUM(sales) conver', 'SUM(clicks) visits', 'SUM(payout) push', 'SUM(charge) pull'], '', 'GROUP BY country');
                if ($date1) {
                    $geo->appendQuery("day_ >= $date and day_ <= $date1");
                }
                $geos = $geo->query();

                $set = [];
                foreach ($geos as $k => $v) {
                    if ($v->country === '-')
                        continue;
                    $set[$k]['code'] = $v->country;
                    $set[$k]['name'] = $v->country;
                    $set[$k]['value'] = round((float)$v->push, 2);
                    $set[$k]['visit'] = (int)$v->visits;
                    $set[$k]['sale'] = (int)$v->conver;
                    $set[$k]['color'] = '#' . dechex(mt_rand(0x000000, 0xFFFFFF));
                }

                $all['geos'] = json_encode(array_values($set));
                $all['forVisits'] = 'Visits';
                $all['forSales'] = 'Conversions';
                $all['table'] = $tat;
                $all['visits'] = json_encode($series);
                $all['sales'] = json_encode($series2);
                $all['cate'] = json_encode($cate);
                $all['type'] = $type;

                $cache = json_encode($all);
                if ($isCache) {
                    $cObj->enter(__METHOD__, $cache);
                }
            }
            Mysql::setDbName();

            $all = json_decode($cache, true);
            $template->render($all);
        } else {
            self::redirect();
        }
    }
}