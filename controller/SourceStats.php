<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 5:07 PM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\SourceGrouped;

class SourceStats extends Controller
{

    public static function index()
    {

        $template = new Template('stats');
        $template->setTemplateField('title', 'Source Stats');
        $template->setTemplateField('html', 'source.html.php');


        if (!($date = Request::getField('date', 'GET'))) {
            $date = date('Ymd');
        }

        if ($date1 = Request::getField('date1', 'GET')) {
            $params = [];
        } else {
            if (\strlen($date) === 8) {
                $params = [
                    'day_' => $date
                ];
            } else {
                $params = [
                    'month_' => $date
                ];
            }
        }

        if (self::getSessionType() === self::ADMIN) {
            if ($nw = Request::getField('nw', 'GET')) {
                $params['network_ID'] = $nw;
            }
            if ($af = Request::getField('af', 'GET')) {
                $params['affiliate_ID'] = $af;
            }
        } else {
            $params['affiliate_ID'] = self::getSession();
        }
        if ($sr = Request::getField('sr', 'GET')) {
            $params['source'] = $sr;
        }
        if ($of = Request::getField('of', 'GET')) {
            $params['offers_ID'] = $of;
        }

        $gh = new SourceGrouped();

        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['*','SUM(clicks) visit','SUM(sales) conver','SUM(payout) push','SUM(charge) pull'], '', 'GROUP BY source, affiliate_ID', 'ORDER BY affiliate, pull DESC');
        if ($date1) {
            $gh->appendQuery("day_ >= $date and day_ <= $date1");
        }
        Mysql::setDbName(Mysql::STATS);
        $stats = $gh->query();
        Mysql::setDbName();

        $template->render($stats);
    }
}