<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 5:07 PM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\HourlyStats;

class AMStats extends Controller
{

    public static function index()
    {

        $template = new Template('stats');
        $template->setTemplateField('title', 'AM Stats');
        $template->setTemplateField('html', 'amstats.html.php');

        if (!($date = Request::getField('date','GET'))){
            $date = date('Ymd');
        }


        if ($date1 = Request::getField('date1','GET')){
            $where = "day_ >= $date and day_ <= $date1 ";
        }else {
            if (\strlen($date) === 8) {
                $where = "day_ = $date ";
            }else{
                $where = "month_ = $date ";
            }
        }

        if ($am = Request::getField('am', 'GET')) {
            $where .= "AND m.ID = $am";
        }
        $who = Request::getField('w', Request::GET);

        if ($who === 'Network') {
            $sql = "SELECT SUM(clicks) visit, SUM(sales) conver, SUM(payout) push, SUM(charge) pull, m.name 
                FROM vine_ad_stats.stats_grouped_hourly sh 
                JOIN vine_ad_db.network n ON n.ID = sh.network_ID
                JOIN vine_ad_db.admin m ON m.ID = n.admin_ID
                WHERE $where AND m.ID NOT IN (1,9)
                GROUP BY m.ID
                HAVING conver > 0";
        } else {
            $sql = "SELECT SUM(clicks) visit, SUM(sales) conver, SUM(payout) push, SUM(charge) pull, m.name 
                FROM vine_ad_stats.stats_grouped_hourly sh 
                JOIN vine_ad_db.affiliate a ON a.ID = sh.affiliate_ID
                JOIN vine_ad_db.admin m ON m.ID = a.admin_ID
                WHERE $where AND m.ID NOT IN (1,9)
                GROUP BY m.ID
                HAVING conver > 0";
        }

        $stats = Mysql::select($sql, (new HourlyStats())->getMyClass());

        $template->render($stats);
    }
}