<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 2:13 PM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\ScrubbedGroup;

class ScrubbedGroupedStats extends Controller
{
    public static function index()
    {

        $template = new Template('stats');
        $template->setTemplateField('title', 'Scrubbed Grouped Stats');
        $template->setTemplateField('html', 'scrubbed_group.html.php');

        $true = false;
        if (!($date = Request::getField('date', 'GET'))) {
            $date1 = date('Ymd');
            $date = date("Ymd", strtotime('-7 days'));
            $true = true;
        }

        if ($dateq = Request::getField('date1', 'GET')) {
            $date1 = $dateq;
            $params = [];
        } else {
            if (\strlen($date) === 8) {
                $params = [
                    'day' => $date
                ];
            } else {
                $params = [
                    'month' => $date
                ];
            }
        }

        if ($true) {
            $params = [];
        }

        Mysql::setDbName(Mysql::STATS);
        $gh = new ScrubbedGroup();

        if ($of = Request::getField('of', 'GET')) {
            $params['offers_ID'] = $of;
        }

        if ($af = Request::getField('af', 'GET')) {
            $params['affiliate_ID'] = $af;
        }

        if ($nw = Request::getField('nw', 'GET')) {
            $params['network_ID'] = $nw;
        }

        if ($group = Request::getField('g', Request::GET)) {
            $group = rtrim($group, '_ID');
        } else {
            $group = 'day';
        }

        $order = 'ORDER BY pull desc';
        if ($group === 'day') {
            $order = 'ORDER BY day';
        } elseif ($group === 'month') {
            $order = 'ORDER BY month';
        }

        $gh->setProperties($params);
        $gh->setQueryParameters($gh, [$group, 'SUM(sale) conver', 'SUM(payout) pull'], '', "GROUP BY $group", $order);
        if ($date1) {
            $gh->appendQuery("day >= $date and day <= $date1");
        }
        $stats['group'] = $group;
        $stats['table'] = $gh->query();
        Mysql::setDbName();

        $template->render($stats);
    }
}