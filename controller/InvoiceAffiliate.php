<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/23/17
 * Time: 9:55 PM
 */

namespace controller;


use helper\Form;
use helper\Mongo;
use helper\MyException;
use helper\Mysql;
use helper\Request as R;
use helper\Template as T;
use model\ActivityLog_;
use model\InvoiceAffiliate as IA;

class InvoiceAffiliate extends Controller
{

    public static function index(){

        $template = new T('invoiceaffiliate');
        $template->setTemplateField('title','Invoice for an Affiliate');
        $template->setTemplateField('html','index.html.php');

        $args = [];
        if ($id = R::getField('af', 'GET')) {
            $args = [
                'affiliate_ID' => $id,
            ];
        }
        $a = new IA();

        if ($paid = R::getField('paid', 'GET')) {
            $args['status'] = $paid;
        }

        $a->setProperties($args);
        $a->setQueryParameters($a, ['invoice_affiliate.*']);
        $a->appendQuery('amount >= 100');
        Mysql::setDbName(Mysql::STATS);
        $get = $a->query();
        Mysql::setDbName();

        $template->render($get);
    }

    public static function edit(){

        $template = new T('invoiceaffiliate');
        $template->setTemplateField('title','Edit Invoice for an Affiliate');
        $template->setTemplateField('html','edit.html.php');

        $form = new Form();
        if ($id = R::getField('id','GET')){
            $a = new IA();
            $a->setProperties([
                'ID' => $id
            ])
                ->setQueryParameters($a);
            Mysql::setDbName(Mysql::STATS);
            $form->setForm($a->one());
            Mysql::setDbName();
        }
        $template->render($form);
    }

    public static function add(){
        if (R::getRequiredField('invoice_aff','POST')){
            $af = R::getRequiredField('af', 'POST');
            $month = R::getRequiredField('month', 'POST');
            $amount = R::getRequiredField('amount','POST');
            $paid = R::getField('paid','POST');
            $payment_method = R::getField('payment_method', 'POST');
            $paid_on = R::getField('paid_on', 'POST');
            $transaction_id = R::getField('transaction_id', 'POST');

            $aO = new \model\Affiliate();
            $aO->setProperties(['ID' => $af])->setQueryParameters($aO, ['email']);
            $affiliate = $aO->one()->email;

            $a = new IA();
            $args = [
                'affiliate_ID' => $af,
                'affiliate' => $affiliate,
                'month' => $month,
                'amount' => $amount,
                'status' => $paid,
                'at' => time()
            ];

            if ($paid === '1') {
                $args['paid_on'] = $paid_on;
                $args['payment_method'] = $payment_method;
                $args['transaction_id'] = $transaction_id;
            }

            $loc = 'invoiceAffiliate/edit';
            if ($id = R::getField('id','POST')){
                $args['ID'] = $id;
                unset($args['joined'],$args['last_active']);
                $loc = 'invoiceAffiliate/edit?id='.$id;

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'UPDATE',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Invoice for Affiliate #$affiliate with $paid"
                    ]
                ];

            } else {

                $arg = [];
                $arg[0] = [
                    'AID' => self::getSession(),
                    'name' => self::getSessionName(),
                    'type' => 'INSERT',
                    'date' => (int)date('Ymd'),
                    'time' => time(),
                    'extra' => [
                        'what' => "Invoice for Affiliate #$affiliate"
                    ]
                ];

            }
            $a->setProperties($args);

            Mysql::setDbName(Mysql::STATS);
            if (!$a->save($a)){
                MyException::setSessionError('Something went wrong.');
                self::redirect($loc);
            }
            Mysql::setDbName();
            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }
            NULL
            MyException::setSessionSuccess('Successful');
            self::redirect('invoiceAffiliate?af=' . $af);
        }
    }

    public static function del(){
        if ($id = R::getRequiredField('id','GET')) {

            $a = new IA();
            $a->setProperties([
                'ID' => $id
            ]);
            if (!$a->remove()){
                MyException::setSessionError('Something went wrong.');
            }else {
                MyException::setSessionSuccess('Delete successful');
            }
            self::redirect('invoiceAffiliate');
        }
    }
}
