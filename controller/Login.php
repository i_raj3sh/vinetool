<?php

/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/11/17
 * Time: 12:35 PM
 */

namespace controller;

use helper\Mongo;
use helper\MyException;
use helper\Request;
use helper\Template;
use helper\Tool;
use model\ActivityLog_;
use model\Admin;
use model\Affiliate;

class Login extends Controller
{

    public static function index()
    {

        if (self::getSession() === false) {

            $template = new Template('login');
            $template->setTemplateField('title', 'AdServer Login | VineTool');
            $template->setTemplateField('html', 'index.html');
            if (Request::getField('login', 'POST')) {

                $email = Request::getRequiredField('username', 'POST');
                $password = hash('sha512', Request::getRequiredField('password', 'POST'));
                if (($provider = self::getLoginStatus($email, $password)) !== false) {
                    self::setSession($provider);
                    // var_dump($provider);
                    self::preRedirect();
                } else {
                    MyException::setSessionError('Invalid username or password.');
                }
            }
            $template->render();
        } else {
            self::redirect();
        }
    }

    private static function getLoginStatus($e, $p)
    {
        $o = new Admin();
        $arg = [':e' => $e, ':p' => $p];
        $obj = $o->login($arg);
        // var_dump($obj);
        if (!$obj) {
            $o = new Affiliate();
            if (!$obj = $o->login($arg)) {
                // var_dump($o);
                return false;
            }
        }
        return $obj;
    }

    private static function setSession($obj)
    {
        $db = new Mongo();

        $arg = [];
        $arg[0] = [
            'name' => $obj['obj'][0]->name,
            'type' => 'LOGIN',
            'date' => (int)date('Ymd'),
            'time' => time(),
            'extra' => [
                'ip' => Tool::getIp()
            ]
        ];

        if ($obj['type'] === 'ADMIN') {
            $_SESSION[md5($obj['obj'][0]->ID . '__TYPE__')] = $obj['obj'][0]->type;
            //setcookie(SESSION,time(),time()+60*60*24);

            $arg[0]['AID'] = $obj['obj'][0]->ID;
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }
        } else {
            $arg[0]['PID'] = $obj['obj'][0]->ID;
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }
        }
        $_SESSION[SESSION] = $obj['obj'][0]->ID;
        $_SESSION[md5($obj['obj'][0]->ID)] = $obj['type'];
        $_SESSION[md5($obj['obj'][0]->ID . '_XXX_')] = $obj['obj'][0]->name;
    }

    private static function preRedirect(): void
    {
        // $_GET[TKN] = false;

        if (isset($_GET[TKN])) {
            self::redirect(false);
        }
        self::redirect(true);
    }

    private static function setTknSession($obj)
    {
        $_SESSION[SESSION] = $obj->ID;
        $_SESSION[md5($obj->ID)] = 'AFFILIATE';
        $_SESSION[md5($obj->ID . '_XXX_')] = $obj->name;
    }

    public static function dead()
    {
        if (self::getSession()) {
            die('{"status":false}');
        }
        die('{"status":true}');
    }

    public static function relive(): void
    {
        if ($email = Request::getRequiredField('u', 'POST')) {

            $password = Request::getRequiredField('p', 'POST');
            $password = base64_decode($password);
            $password = hash('sha512', $password);
            if (($provider = self::getLoginStatus($email, $password)) !== false) {
                self::setSession($provider);
                die('{"status":true}');
            }
            die('{"status":false}');
        }
    }
}
