<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/19/17
 * Time: 7:59 AM
 */

namespace controller;


use helper\Mongo;
use helper\MyException;
use helper\Mysql;
use helper\Request;
use helper\Template;
use model\ActivityLog_;
use model\AffiliateMonthly as AM;

class AffiliateMonthly extends Controller
{

    public static function index(){
        $template = new Template('affiliatemonthly');
        $template->setTemplateField('title','Affiliate Monthly Report');
        $template->setTemplateField('html','index.html.php');

        Mysql::setDbName(Mysql::STATS);
        if ($id = Request::getField('af','GET')) {
            $a = new AM();

            if (!($year = Request::getField('y')))
                $year = date('Y');

            $a->setProperties([
                'affiliate_ID' => $id,
                'year_' => $year
            ])
                ->setQueryParameters($a);
            $get = $a->query();

            $a1 = new AM();

            $y = $year - 1;

            $a1->setProperties([
                'affiliate_ID' => $id,
                'year_' => $y,
                'paid' => 0
            ])
                ->setQueryParameters($a1,['amount'],'','','ORDER BY ID DESC','LIMIT 1');
            $due = (float)$a1->one()->amount;

            $all = [];
            for ($i=1, $j=0; $i<13; $i++){
                $all[$i] = new \stdClass();
                $month = self::getMonthName($i);
                if ($month === $get[$j]->month){
                    $total = $get[$j]->amount;
                    $all[$i]->ID = $get[$j]->ID;
                    $all[$i]->visits = number_format($get[$j]->visits);
                    $all[$i]->sales = number_format($get[$j]->sales);
                    $all[$i]->amount = '$'.number_format($get[$j]->amount,4);
                    $all[$i]->person = $get[$j]->affiliate;
                    $all[$i]->paid = $get[$j]->paid;
                    $all[$i]->due = 0;

                    if($due){
                        $all[$i]->due = '$'.number_format($due,4);
                        $total += $due;
                        if (!$get[$j]->paid){
                            $due += $get[$j]->amount;
                        }
                    }elseif (!$get[$j]->paid){
                        $due += $get[$j]->amount;
                    }

                    $all[$i]->total = '<strong><cite>$</cite>'.number_format($total,4).'</strong>';
                    $j++;
                }else{
                    $all[$i]->visits = '-';
                    $all[$i]->sales = '-';
                    $all[$i]->amount = '-';
                    $all[$i]->person = '-';
                    $all[$i]->paid = '-';
                    $all[$i]->due = '-';
                    $all[$i]->total = '-';
                }
                $all[$i]->month = $month;
            }

            $template->render($all);
        } else{
            $a = new AM();

            $a->setProperties([
                'paid' => 0
            ]);
            $a->setQueryParameters($a,['SUM(amount) amount','affiliate','affiliate_ID'],'','GROUP BY affiliate_ID', 'ORDER BY amount DESC');
            $a->appendQuery('amount > 0');
            $get = $a->query();
            $template->render($get);
        }
        Mysql::setDbName();
    }

    private static function getMonthName($m){
        switch ($m){
            case 1: $month = 'Jan'; break;
            case 2: $month = 'Feb'; break;
            case 3: $month = 'Mar'; break;
            case 4: $month = 'Apr'; break;
            case 5: $month = 'May'; break;
            case 6: $month = 'Jun'; break;
            case 7: $month = 'Jul'; break;
            case 8: $month = 'Aug'; break;
            case 9: $month = 'Sep'; break;
            case 10: $month = 'Oct'; break;
            case 11: $month = 'Nov'; break;
            default: $month = 'Dec';
        }
        return $month;
    }

    public static function tick(){
        if ($id = Request::getRequiredField('id','GET')) {

            Mysql::setDbName(Mysql::STATS);
            $a = new AM();
            $a->setProperties([
                'ID' => $id,
                'paid' => Request::getField('p','GET')
            ]);

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'OPERATION',
                'date' => (int)date('Ymd'),
                'time' => time()
            ];

            if (!$a->update($a)){

                MyException::setSessionError('Something went wrong.');
                self::redirect(true);

            }elseif (Request::getField('p','GET')) {

                $arg[0]['extra']['what'] = "marked Affiliate paid #$id";

                MyException::setSessionSuccess('Marked Paid');
            }else{
                $arg[0]['extra']['what'] = "marked Affiliate unpaid #$id";
                MyException::setSessionSuccess('Marked UnPaid');
            }

            Mysql::setDbName();

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            self::redirect(true);
        }
    }
}