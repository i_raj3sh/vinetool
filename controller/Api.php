<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/20/17
 * Time: 8:31 PM
 */

namespace controller;


use helper\Mailer;
use helper\Mysql;
use helper\Request as R;
use helper\Tool;

class Api extends Controller
{
    private static function keyCheck(){
        header('Content-Type: application/json');

        if ($k = R::getField('k')) {

            $a = new \model\Affiliate();
            $a->setProperties([
                'akey' => $k,
                'active' => 1
            ])
                ->setQueryParameters($a);
            if (!$a->count()) {
                die('{
            "status":false,
            "message":"Invalid key or inactive account. Hit the road."
            }');
            }

            return [
                'key' => $k,
                'id' => $a->one()->ID
            ];
        }

        die('{
            "status":false,
            "message":"No goddamn key. Hit the road."
            }');
    }
    public static function index(){
        $k = self::keyCheck();

        $array = [
            'offer' => MY_SERVER.'api/offer?k='.$k['key'],
            'offer_request' => MY_SERVER.'api/offer_request?k='.$k['key'].'&id=',
            'offer_approved' => MY_SERVER.'api/offer_approved?k='.$k['key'],
        ];

        echo(json_encode($array));
    }

    public static function offer()
    {
        $k = self::keyCheck();

        $param = [];
        $of = 'o.active = 1 ';

        if ($i = R::getField('id', 'GET')) {
            $of .= "AND o.ID = $i ";
        }
        if ($n = R::getField('title', 'GET')) {
            $of .= "AND o.name LIKE \"%$n%\" ";
        }
        if ($t = R::getField('type', 'GET')) {
            $of .= "AND o.type = \"$t\" ";
        }
        if ($a = R::getField('min_pay', 'GET')) {
            $a /= COMMISSION;
            $of .= "AND o.charge >= $a ";
        }
        if ($b = R::getField('max_pay', 'GET')) {
            $b /= COMMISSION;
            $of .= "AND o.charge <= $b ";
        }
        if ($g = R::getField('countries', 'GET')) {
            $g = explode(',',$g);
            asort($g);
            $g = implode('%', $g);
            $of .= "AND o.country LIKE \"%$g%\" ";
        }
        if ($d = R::getField('devices', 'GET')) {
            $d = explode(',',$d);
            asort($d);
            $d = implode('%', $d);
            $of .= "AND o.device LIKE \"%$d%\" ";
        }

        $sql = 'SELECT network_ID FROM blocked_network_rules WHERE affiliate_ID = '.$k['id'];
        $get = Mysql::select($sql,(new \model\Offers())->getMyClass());
        $network_ids = implode("','",Tool::hydrate($get));
        $of .= "AND o.network_ID NOT IN ('$network_ids')";

        $count = "
        SELECT count(o.ID) total FROM `offers` o 
        WHERE $of";

        $all = 100;
        $start = (int)R::getField('current','GET');
        $first = $start * $all;
        $total = (int)Mysql::count($count, $param);

        $sql = "
        SELECT o.* FROM `offers` o 
        WHERE $of
        LIMIT $first, $all
        ";

        $offers = Mysql::select($sql, (new \model\Offers())->getMyClass(), $param);

        $off = [];
        $i = 0;
        foreach ($offers as $item){
            $off[$i] = [
                'ID' => (int)$item->ID,
                'title' => $item->name,
                'description' => $item->description,
                'payout' => (float)$item->charge * COMMISSION,
                'type' => $item->type,
                'preview' => $item->preview,
                'icon' => $item->icon,
                'incent' => $item->incent !== '0',
                'bundle_id' => $item->app_id,
                'min_os_version' => $item->min_os_ver === '0' ? null : $item->min_os_ver,
                'deviceid_strictly_required' => $item->device_id !== '0',
                'cap_remaining_today' => $item->count_daily === '0' ? null : (int)$item->count_daily,
                'date_added' => (int)$item->date,
                'last_updated' => (int)($item->last_updated . '00'),
                'categories' => json_decode($item->category),
                'devices' => json_decode($item->device),
                'countries' => json_decode($item->country),
                'expiry' => $item->expiry == 0? null : (int) $item->expiry,
                'daily_cap' => $item->caps == 0? null : (int) $item->caps,
                'monthly_cap' => $item->monthly == 0? null : (int) $item->monthly_cap,
                'request_offer' => s() . 'api/offer_request?k=' . $k['key'] . '&id=' . $item->ID
            ];
            $i++;
        }

        $array = [
            'status' => true,
            'pagination' => [
                'current' => $start,
                'last' => (int)($total/$all),
                'total' => $total
            ],
            'offers' => $off
        ];

        echo json_encode($array);
    }

    /**
     *
     */
    public static function offer_approved(){
        $k = self::keyCheck()['id'];

        $param = [];
        $of = 'o.active = 1 AND ao.status = 1 ';

        if ($i = R::getField('id', 'GET')) {
            $of .= "AND o.ID = $i ";
        }

        $count = "
        SELECT count(o.ID) total FROM `offers` o
        JOIN affiliate_offers ao
        ON o.ID = ao.offers_ID AND ao.affiliate_ID = $k
        WHERE $of";

        $all = 100;
        $start = (int)R::getField('current','GET');
        $first = $start * $all;
        $total = (int)Mysql::count($count, $param);

        $sql = "
        SELECT o.*,ao.ID TID FROM `offers` o 
        JOIN affiliate_offers ao
	    ON o.ID = ao.offers_ID AND ao.affiliate_ID = $k
        WHERE $of
        LIMIT $first, $all
        ";

        $offers = Mysql::select($sql, (new \model\Offers())->getMyClass(), $param);

        $off = [];
        $i = 0;
        foreach ($offers as $item){
            $off[$i] = [
                'ID' => (int)$item->ID,
                'title' => $item->name,
                'description' => $item->description,
                'payout' => (float)$item->charge * COMMISSION,
                'type' => $item->type,
                'preview' => $item->preview,
                'icon' => $item->icon,
                'tracking' => clc() . $item->TID . "&p=$k",
                'macros' => [
                    'clickid' => 'CLICK',
                    'sub_affid' => 'PUB',
                    'sub_sourceid' => 'SOURCE',
                    'app_name' => 'APP_NAME',
                    'idfa' => 'IDFA',
                    'gaid' => 'GAID',
                    'extra1' => 'EXTRA1',
                    'extra2' => 'EXTRA2',
                    'extra3' => 'EXTRA3',
                    'extra4' => 'EXTRA4'
                ],
                'incent' => $item->incent != 0,
                'bundle_id' => $item->app_id,
                'min_os_version' => $item->min_os_ver === '0' ? null : $item->min_os_ver,
                'deviceid_strictly_required' => $item->device_id !== '0',
                'cap_remaining_today' => $item->count_daily === '0' ? null : (int)$item->count_daily,
                'date_added' => (int)$item->date,
                'last_updated' => (int)($item->last_updated . '00'),
                'categories' => json_decode($item->category),
                'devices' => json_decode($item->device),
                'countries' => json_decode($item->country),
                'expiry' => $item->expiry == 0? null : (int) $item->expiry,
                'daily_cap' => $item->caps == 0? null : (int) $item->caps,
                'monthly_cap' => $item->monthly == 0? null : (int) $item->monthly_cap,
            ];
            $i++;
        }

        $array = [
            'status' => true,
            'pagination' => [
                'current' => $start,
                'last' => (int)($total/$all),
                'total' => $total
            ],
            'offers' => $off
        ];

        echo json_encode($array);
    }

    private static function bulkRequest($ids, $af): void
    {
        foreach ($ids as $id) {
            $a = new \model\ApproveOffers();
            $args = [
                'affiliate_ID' => $af,
                'offers_ID' => $id,
                'percentage' => COMMISSION * 100,
                'cap' => -1,
                'status' => 1,
                'added' => date('YmdH'),
                'requested' => time()
            ];
            $a->setProperties($args);

            $aa = new \model\ApproveOffers();
            $aa->setProperties([
                'affiliate_ID' => $af,
                'offers_ID' => $id
            ])
                ->setQueryParameters($aa);
            $well = $aa->one();

            if ($well) {
                continue;
            }
            $a->save($a);
        }

        die('{
            "status":true,
            "message":"Mentioned offers requested"
            }');
    }

    public static function offer_request(): void
    {
        $k = self::keyCheck();

        if ($id = R::getField('id')){

            if (false !== strpos($id, ',')) {
                $id = explode(',', $id);

                self::bulkRequest($id, $k['id']);
            }

            $a = new \model\ApproveOffers();
            $args = [
                'affiliate_ID' => $k['id'],
                'offers_ID' => $id,
                'percentage' => COMMISSION * 100,
                'cap' => -1,
                'status' => 1,
                'added' => date('YmdH'),
                'requested' => time()
            ];
            $a->setProperties($args);

            $aa = new \model\ApproveOffers();
            $aa->setProperties([
                'affiliate_ID' => $k['id'],
                'offers_ID' => $id
            ])
                ->setQueryParameters($aa);
            $well = $aa->one();

            if ($well){
                if ($well->status == 1)
                    self::showOffer($id,$k['id'],'Approved');
                else
                    self::showOffer($id,$k['id'],'Pending');
                return;
            }
            if (!($i = $a->save($a))){
                die('{
            "status":false,
            "message":"Something went wrong"
            }');
            }
            self::mailRequest($k['id'], $i);

            self::showOffer($id,$k['id'],'Pending');

        }else{
            die('{
            "status":false,
            "message":"Please provide offer id to be approved"
            }');
        }
    }

    private static function mailRequest($aff, $off){

        $af = new \model\Affiliate();
        $af->setProperties([
            'ID' => $aff
        ])->setQueryParameters($af,['name']);

        $affiliate = $af->one();

        $of = new \model\Offers();
        $of->setProperties([
            'ID' => $off
        ])->setQueryParameters($of,['ID','name','icon']);

        $offer = $of->one();

        $arr = [
            'name' => $affiliate->name,
            'ID' => $offer->ID,
            'icon' => $offer->icon,
            'title' => $offer->name,
        ];

        $m = new Mailer();
        $m
            ->setHeaders()
            ->setMessage('offerRequest',$arr)
            ->send('support@vinetool.com','API - An offer is requested by '.$affiliate->name);
    }

    /**
     * @param $id
     * @param $k
     * @param $state
     */
    private static function showOffer($id, $k, $state){

        $sql = "
        SELECT o.*,ao.ID TID FROM `offers` o 
        JOIN affiliate_offers ao
        WHERE active = 1 AND o.ID = $id
        ";

        $offers = Mysql::select($sql, (new \model\Offers())->getMyClass(), []);

        $off = [];
        foreach ($offers as $item){
            $off = [
                'ID' => (int)$item->ID,
                'title' => $item->name,
                'description' => $item->description,
                'payout' => (float)$item->charge * COMMISSION,
                'type' => $item->type,
                'preview' => $item->preview,
                'icon' => $item->icon,
                'tracking' => clc() . $item->TID . "&p=$k",
                'macros' => [
                    'clickid' => 'CLICK',
                    'sub_affid' => 'PUB',
                    'sub_sourceid' => 'SOURCE',
                    'app_name' => 'APP_NAME',
                    'idfa' => 'IDFA',
                    'gaid' => 'GAID',
                    'extra1' => 'EXTRA1',
                    'extra2' => 'EXTRA2',
                    'extra3' => 'EXTRA3',
                    'extra4' => 'EXTRA4'
                ],
                'incent' => $item->incent != 0,
                'bundle_id' => $item->app_id,
                'min_os_version' => $item->min_os_ver === '0' ? null : $item->min_os_ver,
                'deviceid_strictly_required' => $item->device_id !== '0',
                'cap_remaining_today' => $item->count_daily === '0' ? null : (int)$item->count_daily,
                'date_added' => (int)$item->date,
                'last_updated' => (int)($item->last_updated . '00'),
                'categories' => json_decode($item->category),
                'devices' => json_decode($item->device),
                'countries' => json_decode($item->country),
                'expiry' => $item->expiry == 0? null : (int) $item->expiry,
                'daily_cap' => $item->caps == 0? null : (int) $item->caps,
                'monthly_cap' => $item->monthly == 0? null : (int) $item->monthly_cap,
            ];
        }

        $array = [
            'status' => true,
            'offers' => $off,
            'offer_state' => $state
        ];

        echo json_encode($array);
    }

}