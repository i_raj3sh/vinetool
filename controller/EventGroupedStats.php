<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 2:13 PM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\EventGroup;

class EventGroupedStats extends Controller
{
    public static function index()
    {

        $template = new Template('stats');
        $template->setTemplateField('title', 'Event Stats');
        $template->setTemplateField('html', 'event.html.php');

        if (!($date = Request::getField('date', 'GET'))) {
            $date = date('Ymd');
        }

        if ($date1 = Request::getField('date1', 'GET')) {
            $params = [];
        } else {
            if (\strlen($date) === 8) {
                $params = [
                    'day' => $date
                ];
            } else {
                $params = [
                    'month' => $date
                ];
            }
        }

        Mysql::setDbName(Mysql::STATS);
        $gh = new EventGroup();

        if ($of = Request::getField('of', 'GET')) {
            $params['offers_ID'] = $of;
        }
        if ($af = Request::getField('af', 'GET')) {
            $params['affiliate_ID'] = $af;
        }
        if ($nw = Request::getField('nw', 'GET')) {
            $params['network_ID'] = $nw;
        }
        if ($ev = Request::getField('ev', 'GET')) {
            $params['event'] = $ev;
        }

        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['event', 'SUM(sale) conver', 'SUM(charge) pull', 'SUM(payout) push'], '', 'GROUP BY event', 'ORDER BY pull DESC');
        if ($date1) {
            $gh->appendQuery("day >= $date and day <= $date1");
        }
        $stats['table'] = $gh->query();
        Mysql::setDbName();

        $template->render($stats);
    }
}