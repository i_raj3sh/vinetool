<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/23/17
 * Time: 5:07 PM
 */

namespace controller;


use helper\Mysql;
use helper\Request;
use helper\Template;
use model\HourlyStats;

class NetworkStats extends Controller
{

    public static function index()
    {

        $template = new Template('stats');
        $template->setTemplateField('title', 'Network Stats');
        $template->setTemplateField('html', 'network.html.php');

        if (!($date = Request::getField('date','GET'))){
            $date = date('Ymd');
        }

        if ($date1 = Request::getField('date1','GET')){
            $params = [];
        }else {
            if (\strlen($date) === 8) {
                $params = [
                    'day_' => $date
                ];
            }else{
                $params = [
                    'month_' => $date
                ];
            }
        }

        if ($nw = Request::getField('nw','GET')){
            $params['network_ID'] = $nw;
        }
        if ($of = Request::getField('of', 'GET')) {
            $params['offers_ID'] = $of;
        }
        if ($af = Request::getField('af', 'GET')) {
            $params['affiliate_ID'] = $af;
        }

        $gh = new HourlyStats();

        $gh->setProperties($params);
        $gh->setQueryParameters($gh, ['*', 'SUM(dropped) lost', 'SUM(clicks) visit', 'SUM(sales) conver', 'SUM(payout) push', 'SUM(charge) pull'], '', 'GROUP BY network_ID', 'ORDER BY pull DESC');
        if($date1){
            $gh->appendQuery("day_ >= $date and day_ <= $date1");
        }
        Mysql::setDbName(Mysql::STATS);
        $stats = $gh->query();
        Mysql::setDbName();

        $template->render($stats);
    }
}