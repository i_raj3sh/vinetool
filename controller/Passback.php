<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/2/18
 * Time: 6:00 PM
 */

namespace controller;


use helper\Mongo;
use helper\MyException;
use helper\Paginator;
use helper\Request;
use helper\Template;
use model\ActivityLog_;
use model\Passback as PB;
use model\ReRoute;

class Passback extends Controller
{
    public static function index(){

        $template = new Template('passback');
        $template->setTemplateField('title','Passback Traffic');
        $template->setTemplateField('html','index.html.php');

        $param = [];
        if($g = Request::getField('g','GET')){
            $param['geo'] = $g;
        }
        if($d = Request::getField('d','GET')){
            $param['OS'] = $d;
        }

        $a = new PB();
        $a->setProperties($param);
        $a->setQueryParameters($a);

        $paginator = new Paginator();
        $paginator->setLink();
        $paginator->total = $a->count();
        $paginator->paginate();

        $start = $paginator->getStart();

        $a = new PB();
        $a->setProperties($param);
        $a->setQueryParameters($a,['*'],'','','',"LIMIT $start, $paginator->itemsPerPage");

        $get['table'] = $a->query();
        $get['pages'] = $paginator->pageNumbers();
        $get['perPage'] = $paginator->itemsPerPage();
        $template->render($get);
    }

    public static function add(){
        if (($p = Request::getRequiredField('p','GET')) && ($t = Request::getRequiredField('t','GET')) && ($w = Request::getRequiredField('w','GET'))){

            $k = Request::getRequiredField('k','GET');

            $r = new ReRoute();
            $r->setProperties([
                'ID' => $k,
                'passback_ID' => $p
            ]);
            $r->setQueryParameters($r);
            $get = $r->one();

            if ($get){
                if ($get->weight === $w && $get->tot === $t){
                    die('{"error":"Same data sent"}');
                }
                $r = new ReRoute();
                $r->setProperties([
                    'passback_ID' => $p,
                    'tot' => $t,
                    'ID' => $k,
                    'weight' => $w
                ]);
                $r->update($r);
                die('{"success":"Updated Successfully"}');
            }
            $r = new ReRoute();
            $r->setProperties([
                'passback_ID' => $p,
                'tot' => $t,
                'weight' => $w
            ]);
            $id = $r->insert($r);

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'INSERT',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Passback #$p with Route #$id"
                ]
            ];

            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            die('{"success":"Added Successfully", "id":'.$id.'}');

        }
    }

    public static function del(){
        if (($p = Request::getRequiredField('p','GET')) && ($k = Request::getRequiredField('k','GET'))){

            $r = new ReRoute();
            $r->setProperties([
                'ID' => $k,
                'passback_ID' => $p
            ]);
            $r->remove();

            $arg = [];
            $arg[0] = [
                'AID' => self::getSession(),
                'name' => self::getSessionName(),
                'type' => 'DELETE',
                'date' => (int)date('Ymd'),
                'time' => time(),
                'extra' => [
                    'what' => "Route #$k for Passback #$p"
                ]
            ];
            $db = new Mongo();
            try {
                $db->insert(new ActivityLog_(), $arg);
            } catch (\Exception $e) {
            }

            MyException::setSessionSuccess('Deleted Successfully. Please distribute weight to 100.');
            self::redirect(true);
        }
    }
}