<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/9/17
 * Time: 3:05 PM
 */

namespace model;


use helper\Mysql;

class Admin extends Model
{

    public function getMyTable()
    {
        return 'admin';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }

    public function login($arg) {
        $sql = "select * from vine_ad_db.`" . $this->getMyTable() . "` where (email = :e and password = :p) or (username = :e and password = :p) and active = 1";
        if (Mysql::numRows($sql, $arg)){
            return ['obj'=>Mysql::select($sql, $this->getMyClass(), $arg),'type'=>'ADMIN'];
        }
        return false;
    }
}