<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 1:47 PM
 */

namespace model;


class HourlyStats extends Model
{
    public function getMyTable()
    {
        return 'stats_grouped_hourly';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}