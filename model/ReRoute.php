<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/2/18
 * Time: 3:17 PM
 */

namespace model;


class ReRoute extends Model
{

    public function getMyTable()
    {
        return 'reroute';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}