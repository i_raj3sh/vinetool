<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/9/17
 * Time: 3:05 PM
 */

namespace model;


class ActivityLog_ extends Model
{

    public function getMyTable(): string
    {
        return 'activitylog';
    }

    public function getMyClass(): string
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}