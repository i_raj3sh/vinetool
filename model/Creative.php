<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/11/17
 * Time: 6:31 PM
 */

namespace model;


class Creative extends Model
{
    public function getMyTable()
    {
        return 'creative';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }

    public static function load($id)
    {
        $c = new self();
        $f = new Format();
        $c->setProperties([
            'offers_ID' => $id
        ])
            ->setQueryParameters($c, ['creative.*']);
        $c->setQueryParameters($f, ['format.*']);
        return $c->query();
    }
}