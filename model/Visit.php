<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 7/8/18
 * Time: 2:48 PM
 */

namespace model;


class Visit extends Model
{
    private $col;

    public function __construct(string $col = null)
    {
        $this->col = $col;
    }

    public function setProperties($arg)
    {
    }

    public function getMyTable()
    {
        if ($this->col === null) {
            $this->col = date('YmdH');
        }
        return 'redirect_visit_' . $this->col;
    }

    public function getMyClass()
    {
        return __CLASS__;
    }
}