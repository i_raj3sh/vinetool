<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/18/17
 * Time: 11:21 AM
 */

namespace model;

use controller\Controller;
use helper\Mysql;

abstract class Model
{
    private $_where = "";
    private $_arg = [];
    private $_select = "";

    abstract public function setProperties($arg);

    public function setQueryParameters($o, $select = ["*"], $join = "", $group = "", $order = "", $limit = "")
    {
        if ($select)
            $this->_select .= implode(',', $select) . ",";
        if (is_array($join)) {
            if (isset($this->_join)) $this->_join .= " left join " . $o->getMyTable();
            else $this->_join = " left join " . implode(' left join ', $join);
            $where = "on";
            foreach ($join as $v) {
                if (strpos($v, '_')) $where .= " $v." . $o->getMyTable() . "_ID = " . $o->getMyTable() . ".ID and";
                else $where .= " " . $o->getMyTable() . "." . $v . "_ID = $v.ID and";
            }
            $this->where($o, $where);
        } elseif (is_object($join)) {
            $this->_join .= "left join " . $o->getMyTable() . " on " . $o->getMyTable() . ".ID = " . $join->getMyTable() . "." . $o->getMyTable() . "_ID ";
            $this->where($o);
        }
        if ($this != $o && !is_object($join)) {
            $this->_join .= "left join " . $o->getMyTable() . " on " . $o->getMyTable() . ".ID = " . $this->getMyTable() . "." . $o->getMyTable() . "_ID ";
            $this->where($o);
        } elseif ($this == $o) {
            //$this->_join = "";
            $this->_join = str_replace('left','',$this->_join);
            if(empty($this->_join))
                $this->where($o);
        }
        $this->_group = $group;
        $this->_order = $order;
        $this->_limit = $limit;
    }

    private function where($o, $where = "where "): void
    {
        $arr = [];
        $arg = [];
        $puts = get_object_vars($o);
        foreach ($puts as $k => $v) {
            if ($this->exclude($k)) {
                array_push($arr, " $k = ? ");
                array_push($arg, $v);
            }
        }
        if (false !== strpos($this->_where, 'where'))
            $where = " and";
        if (!empty($arr)) {
            $this->_where .= $where . implode(' and ', $arr);
            $this->_arg = array_merge($this->_arg, $arg);
        }
    }

    private function exclude($k)
    {
        $noVars = [
            "_select", "_arg", "_group", "_order", "_where", "_limit", "_join"
        ];
        return !\in_array($k, $noVars, true);
    }

    abstract public function getMyTable();

    public function appendQuery($query, $join = 'and'): void
    {
        if (!empty($query)) {
            if (!empty($this->_where)) $this->_where .= ' '.$join.' ' . $query;
            else $this->_where = "where " . $query;
        }
    }

    public function query(): ?array
    {
        $where = rtrim($this->_where);
        $where = preg_replace('/and$/','',$where);
        $select = rtrim($this->_select, ',');
        $sql = "select $select from " . $this->getMyTable() . " $this->_join $where $this->_group $this->_order $this->_limit";
        return Mysql::select($sql, $this->getMyClass(), $this->_arg);
    }

    public function one(){
        return $this->query()[0];
    }

    abstract public function getMyClass();

    public function count(): int
    {
        $where = rtrim($this->_where, "and");
        $select = rtrim($this->_select, ",");
        $sql = "select $select from " . $this->getMyTable() . " $this->_join $where";
        return Mysql::numRows($sql, $this->_arg);
    }

    public function save($o)
    {
        if (!isset($o->ID))
            return $this->insert($o);
        else
            return $this->update($o);

    }

    public function insert($o): string
    {
        $puts = get_object_vars($o);
        $arg = [];
        foreach ($puts as $k => $v) {
            if ($this->exclude($k))
                $arg["$k = ?"] = $v;
        }
       $a = Mysql::put(__FUNCTION__, $this->getMyTable(), $arg) ;
       $m = Mysql::lastId();

        return $m;
    }

    public function update($o,$cond='where')
    {
        $puts = get_object_vars($o);
        $arg = [];
        foreach ($puts as $k => $v) {
            if ($this->exclude($k))
                $arg["$k = ?"] = $v;
        }
        if (isset($o->ID))
            $cond = "$cond ID = $o->ID";
        return Mysql::put(__FUNCTION__, $this->getMyTable(), $arg, $cond);
    }

    public function remove($cond = '', $go = false): bool
    {
        if ($go || Controller::getAdminType() === 'SUPER') {
            if (!empty($cond)){
                $sql = 'DELETE FROM ' . $this->getMyTable() . ' WHERE ' . $cond;
                return Mysql::delete($sql);
            }
            if (isset($this->ID)) {
                $sql = 'DELETE FROM ' . $this->getMyTable() . ' WHERE ID = ' . $this->ID;
                return Mysql::delete($sql);
            }
        }
        return false;
    }


}
