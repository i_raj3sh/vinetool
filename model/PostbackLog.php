<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/17/17
 * Time: 10:49 AM
 */

namespace model;


class PostbackLog extends Model
{

    public function getMyTable()
    {
        return 'postback_log';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}