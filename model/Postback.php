<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/14/17
 * Time: 8:01 AM
 */

namespace model;


class Postback extends Model
{
    public function getMyTable()
    {
        return 'postback';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}