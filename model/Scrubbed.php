<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/11/17
 * Time: 1:45 PM
 */

namespace model;


class Scrubbed extends Model
{
    public function getMyTable()
    {
        return 'scrubbed';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }

}