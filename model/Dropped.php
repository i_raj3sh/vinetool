<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/17/17
 * Time: 8:41 AM
 */

namespace model;


class Dropped extends Model
{

    public function getMyTable()
    {
        return 'dropped';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}