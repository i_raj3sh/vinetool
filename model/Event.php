<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 12/30/18
 * Time: 2:28 PM
 */

namespace model;


class Event extends Model
{

    public function getMyTable()
    {
        return 'event';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }

    public static function getEventListHtml(): string
    {
        $e = new self();
        $e->setQueryParameters($e);
        $all = $e->query();
        $str = '';
        foreach ($all as $one) {
            $n = ucfirst($one->name);
            $str .= '<div class="col-3"><strong class="float-right mt-2">' . $n . ':</strong></div><div class="col-8"><div class="form-group"><input id="' . $one->name . '" class="form-control event_in" type="number" name="ev_' . $one->name . '" placeholder="' . $n . ' event ID"/></div></div>';
        }
        return $str;
    }
}