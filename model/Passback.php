<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 3/2/18
 * Time: 1:19 PM
 */

namespace model;


class Passback extends Model
{

    public function getMyTable()
    {
        return 'passback';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}