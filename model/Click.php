<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 11:53 AM
 */

namespace model;


class Click extends Model
{
    public function getMyTable()
    {
        return 'click';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}