<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 7:32 PM
 */

namespace model;


class AffiliateMonthly extends Model
{
    public function getMyTable()
    {
        return 'monthly_affiliate_report';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }

}