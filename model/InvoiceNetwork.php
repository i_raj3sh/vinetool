<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/23/17
 * Time: 9:57 PM
 */

namespace model;


class InvoiceNetwork extends Model
{
    public function getMyTable()
    {
        return 'invoice_network';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}