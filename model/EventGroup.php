<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/11/17
 * Time: 6:31 PM
 */

namespace model;


class EventGroup extends Model
{
    public function getMyTable()
    {
        return 'event_group';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}