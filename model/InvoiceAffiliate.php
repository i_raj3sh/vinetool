<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/23/17
 * Time: 9:58 PM
 */

namespace model;


class InvoiceAffiliate extends Model
{
    public function getMyTable()
    {
        return 'invoice_affiliate';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }

    public function pop(): string
    {

        if ($this->status === '1') {
            return '&nbsp;<button class="btn btn-default btn-sm" data-toggle="popover" data-content="<table class=\'table\'><thead><tr><th>Paid On<th>Payment Method<th>Transaction ID</thead><tbody><tr><td>' . $this->paid_on . '<td>' . $this->payment_method . '<td>' . $this->transaction_id . '</tbody></table>"><i class="fa fa-eye text-info"></i></button>';
        }
        return '';
    }
}