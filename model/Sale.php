<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 4/1/18
 * Time: 3:43 PM
 */

namespace model;


class Sale extends Model
{
    public function getMyTable()
    {
        return 'sale';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }
}