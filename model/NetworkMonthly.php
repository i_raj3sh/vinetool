<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/18/17
 * Time: 7:34 PM
 */

namespace model;


class NetworkMonthly extends Model
{
    public function getMyTable()
    {
        return 'monthly_network_report';
    }

    public function getMyClass()
    {
        return __CLASS__;
    }

    public function setProperties($arg)
    {
        foreach ($arg as $k => $v) {
            $this->$k = $v;
        }
        return $this;
    }

}