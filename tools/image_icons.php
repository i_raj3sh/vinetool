<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 11/21/17
 * Time: 10:50 AM
 */
require dirname(__DIR__).'/lib/include.php';

if ($fnm = \helper\Request::getField('f','GET')){
    $tp = \helper\Request::getField('t','GET');

    $path = MY_BASE.'images/icons/';

    if (!$tp){
        $tp = 'jpg';
    }

    $title = md5(microtime(true)).'.'.$tp;

    $img = $path.$title;
    $out = MY_SERVER.'images/icons/'.$title;

    if(isset($_SESSION['_U_'])){
        if ($fnm !== $_SESSION['_U_']){
            copy($fnm, $img);
            $_SESSION['out'] = $out;
        }
        else{
            $out = $_SESSION['out'];
        }
    }else{
        copy($fnm, $img);
        $_SESSION['out'] = $out;
    }


    $_SESSION['_U_'] = $fnm;
}
?>
<!doctype html>
<html lang="en">
<head>
    <title>Image download from link</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <style>
        input,select{width: 50rem;padding: 1rem;border:1px solid #aaa; border-radius: 5px;color:#444 }
        select{width: 6rem}
        button{width: 7rem;padding: 1rem;background:green;border-radius: 5px;color:aliceblue }
    </style>
</head>
<body>

<main class="container-fluid">
    <form>
        <h2>Image downloader</h2>
        <p><input type="url" name="f" placeholder="Enter Image URL to be downloaded" required></p>
        <p><label>Image Type: </label><select name="t">
                <option value="png">PNG</option>
                <option value="jpg">JPG</option>
                <option value="gif">GIF</option>
                <option value="webm">WEBM</option>
            </select></p>
        <p><button type="submit">Proceed</button></p>
    </form>
    <h3><?php echo $out ?></h3>
</main>
</body>
</html>