<?php

use model\Creative;

header('Access-Control-Allow-Origin: cache-control,x-requested-with');

define('MAX_SIZE', 1);
define('PATH', dirname(__DIR__) . '/tools/');

$allowed_image_types = [
    'jpg' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
    'png' => 'image/png',
    'gif' => 'image/gif',
];

$error = 'Invalid image request';
if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])) {

    $userfile_name = $_FILES['file']['name'];
    $userfile_tmp = $_FILES['file']['tmp_name'];
    $userfile_size = $_FILES['file']['size'];
    $userfile_type = $_FILES['file']['type'];
    $n = explode('.', $_FILES['file']['name']);

    $file_ext = $n[1];

    [$width, $height, $type, $attr] = getimagesize($userfile_tmp);

    if ($_FILES['file']['error'] === 0) {
        $error = 'Only jpg, png, gif images accepted!';
        foreach ($allowed_image_types as $ext => $mime_type) {
            if ($file_ext === $ext && $userfile_type === $mime_type) {
                $error = '';
                break;
            }
        }
        if ($userfile_size > (MAX_SIZE * 1048576)) {
            $error .= 'Images must be under ' . MAX_SIZE . 'MB in size';
        }
    } else {
        $error = 'No image / image with errors';
    }
    if (empty($error) && !($type === 1 || $type === 2 || $type === 3)) {
        $error = 'Only jpg, png, gif images accepted';
    }

    if (!($f = checkDimensions($width, $height))) {
        $error = 'Not valid dimensions';
        $userfile_name = $_FILES['file']['name'];
        $userfile_tmp = $_FILES['file']['tmp_name'];
    } else {

        $filename = $f . '_' . $_GET['oid'];
        $EXT = '.' . $file_ext;
        $large_image_location = PATH . $filename . $EXT;

        require_once dirname(__DIR__) . '/lib/include.php';

        $url = 'https://cdn.adtrackr.co/img/' . $width . 'x' . $height . '/' . $filename . '.' . $file_ext;

        $crea = new Creative();
        $crea->setProperties([
            'format_ID' => $f,
            'offers_ID' => $_GET['oid'],
            'url' => $url
        ])
            ->setQueryParameters($crea);
        $crea->insert($crea);

        move_uploaded_file($userfile_tmp, $large_image_location);
        chmod($large_image_location, 0777);

        echo $f . '|' . $url . '|' . $width . 'x' . $height;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://cdn.adtrackr.co/upload.php");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
//            'file' => new CURLFile(realpath($large_image_location))
            'file' => curl_file_create($large_image_location, $file_ext, $filename)
        ));
        $result = curl_exec($ch);
        curl_close($ch);

        unlink($large_image_location);

    }
} else {
    $error = 'Banner Image is required. please upload it.';
}

if ($error) {
    http_response_code(500);
    echo $error;
}

function checkDimensions($w, $h)
{
    $formats = [
        1 => '320x50',
        2 => '300x50',
        3 => '320x480',
        4 => '480x320'
    ];

    $k = $w . 'x' . $h;
    foreach ($formats as $f => $s) {
        if ($k === $s) {
            return $f;
        }
    }

    return 0;
}

