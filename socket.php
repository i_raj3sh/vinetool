<?php
/**
 * Created by PhpStorm.
 * User: reoxey
 * Date: 5/5/18
 * Time: 1:05 PM
 */

define('HOST_NAME', 'localhost');
define('PORT', '8090');
$null = NULL;

require_once 'lib/include.php';
require_once MY_BASE . '/lib/Online.php';
require_once MY_BASE . '/helper/RedisFy.php';
$chatHandler = new Online();

			ini_set('display_errors', 1);
            error_reporting(E_ALL);

$socketResource = socket_create(AF_INET, SOCK_STREAM, 0);
#socket_set_option($socketResource, SOL_SOCKET, SO_REUSEADDR, 1);
$connect_timeval = array(
        "sec"=>30,
        "usec" => 0
    );
socket_set_option(
        $socketResource,
        SOL_SOCKET,
        SO_SNDTIMEO,
        $connect_timeval
    );
    socket_set_option(
        $socketResource,
        SOL_SOCKET,
        SO_RCVTIMEO,
        $connect_timeval
    );
socket_set_option($socketResource, SOL_SOCKET, SO_KEEPALIVE, 1);

socket_bind($socketResource, 0, PORT);
socket_listen($socketResource);

$clientSocketArray = array($socketResource);
while (true) {
    $newSocketArray = $clientSocketArray;
    socket_select($newSocketArray, $null, $null, 0, 10);

    if (in_array($socketResource, $newSocketArray)) {echo '1';
        $newSocket = socket_accept($socketResource);
        $clientSocketArray[] = $newSocket;

        $header = socket_read($newSocket, 1024);
        $chatHandler->doHandshake($header, $newSocket, [
            'name' => HOST_NAME,
            'port' => PORT
        ]);

        socket_getpeername($newSocket, $client_ip_address);

        $newSocketIndex = array_search($socketResource, $newSocketArray);
        unset($newSocketArray[$newSocketIndex]);
    }

    foreach ($newSocketArray as $newSocketArrayResource) {
    
    $soc = (string) $newSocketArrayResource;

        while (socket_recv($newSocketArrayResource, $socketData, 1024, 0) >= 1) {
            $socketMessage = $chatHandler->unseal($socketData);
            $chatHandler->receiver($soc, $socketMessage);
            break 2;
        }

        $socketData = @socket_read($newSocketArrayResource, 1024, PHP_NORMAL_READ);
        if ($socketData === false) {
        socket_getpeername($newSocketArrayResource, $client_ip_address);
            $chatHandler->close($soc);
            $newSocketIndex = array_search($newSocketArrayResource, $clientSocketArray);
            unset($clientSocketArray[$newSocketIndex]);
        }
    }
}
socket_close($socketResource);