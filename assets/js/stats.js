/**
 * Created by reoxey on 11/16/17.
 */
Highcharts.setOptions({
    lang: {
        thousandsSep: ','
    }
});

function dash(id,cate,series,statsfor) {
    Highcharts.chart(id, {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Stats for '+statsfor
        },
        xAxis: {
            categories: cate,
            tickmarkPlacement: 'on',
            title: {
                text: ''
            }
        },
        yAxis: {
            title: {
                text: statsfor
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            }
        },
        tooltip: {
            split: true
        },
        plotOptions: {
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },
        series: series
    });
}

function pieMulti(data,t,id) {
    if (data === undefined)
        return;
    if (data.length === 0)
        return;
    Highcharts.chart('contain_'+id+'_top', {
        chart: {
            type: 'variablepie'
        },
        title: {
            text: t
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'Overall Earning (%): <b>{point.percentage:.2f}</b><br/>' +
            'Overall CR (%): <b>{point.z:.2f}</b><br/>'
        },
        plotOptions: {
            variablepie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            minPointSize: 10,
            innerSize: '20%',
            zMin: 0,
            name: 'performance',
            data: data
        }]
    });
}

function pieStats(data,d,t) {
    if (data === undefined)
        return;
    if (data.length === 0)
        return;
    var month = [];
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var n = month[d];
    Highcharts.chart('contain_'+t, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: t+' Performance for month '+n
        },
        tooltip: {
            pointFormat: 'Visits: {point.v:,.0f}<br/>Installs: {point.s:,.0f}<br/>Earning: ${point.y:,.2f}<br/>Percent: <b>{point.percentage:.2f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true,
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Conversion',
            colorByPoint: true,
            data: data
        }]
    });
}

// function getGeoChart(a,tit) {
//     for (var t,
//              l = 5, o = 50, i = 1 / 0, d = -(1 / 0), n = 0; n < a.length; n++) {
//         var u = a[n].value;
//         i > u && (i = u), u > d && (d = u)
//     }
//     AmCharts.ready(function () {
//         AmCharts.theme = AmCharts.themes.dark;
//             t = new AmCharts.AmMap;
//             // t.pathToImages = "amcharts/ammap/images/";
//             // t.fontFamily = "Open Sans";
//         t.fontSize = "13";
//         t.color = "#888";
//             t.addTitle(tit, 14);
//             t.areasSettings = {
//                 unlistedAreasColor: "#000000",
//                 unlistedAreasAlpha: .1
//             };
//             t.imagesSettings.balloonText = "<span style='font-size:14px;'><b>[[title]]</b>: [[value]]</span>";
//
//         for (var n = {mapVar: AmCharts.maps.worldLow, images: []}, u = 0; u < a.length; u++) {
//             var r = a[u], c = r.value,
//                 s = c * o / d;
//                 // s = (c - i) / (d - i) * (o - l) + l;
//             l > s && (s = l);
//             var g = r.code;
//         if(!e[g]) continue;
//             n.images.push({
//                 type: "circle",
//                 width: s,
//                 height: s,
//                 color: r.color,
//                  longitude: e[g].longitude,
//                  latitude: e[g].latitude,
//                 title: '<h4>Geo Stats for '+r.name+'</h4><strong>Visits</strong><span style="font-weight: 100 !important;">:&nbsp;'+r.visit+'</span><br/><strong>Installs</strong><span style="font-weight: 100 !important;">:&nbsp;'+r.sale+'</span><br/><strong>Earning</strong><span style="font-weight: 100 !important;">:&nbsp;$'+r.value+'</span><br/><strong>CR</strong>',
//                 value: (r.sale*100/r.visit).toFixed(4)
//             })
//         }
//         t.dataProvider = n;
//         t.write("geos")
//     });
//     // $("#geos").closest(".portlet").find(".fullscreen").click(function () {
//     //     t.invalidateSize()
//     // });
// }

function highmap(geoData) {
    //Get Data
    var country = [],
        installs = [],
        visits = [],
        earning = [],
        cr = [];

    for (var i = 0; i < geoData.length; i++) {
        country[i] = geoData[i].code.toLowerCase();
        installs[i] = geoData[i].sale;
        visits[i] = geoData[i].visit;
        earning[i] = geoData[i].value;
        cr[i] = (geoData[i].sale * 100 / geoData[i].visit).toFixed(4);
        if (isNaN(cr[i]) || cr[i] == 0) {
            cr[i] = 0;
        }
    }

    var data = [];

    for (i = 0; i <= geoData.length; i++) {
        data[i] = [country[i], earning[i], visits[i], installs[i], cr[i]];
    }

    // Create the chart
    Highcharts.mapChart('container', {
        chart: {
            map: 'custom/world-highres3'
        },

        title: {
            text: 'Earning Based on Countries'
        },

        // subtitle: {
        //     text: 'Source map: <a href="http://code.highcharts.com/mapdata/custom/world-highres3.js">World, Miller projection, ultra high resolution</a>'
        // },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'left'
            },
            enableMouseWheelZoom: false
        },

        colorAxis: {
            min: 0
        },

        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'bottom'
        },

        series: [{
            data: data,
            name: 'Geo Stats',
            keys: ['hc-key', 'value', 'visits', 'installs', 'cr'],
            states: {
                hover: {
                    color: '#a4edba'
                }
            },
            dataLabels: {
                enabled: true,
                format: '' //'{point.name}'
            }
        }],

        tooltip: {
            pointFormat: '<span>Country: <b>{point.name} <span style="text-transform:uppercase; font-weight: bold;">({point.hc-key})</span></b><br> Visits: <b>{point.visits}</b><br> Installs: <b>{point.installs}</b><br> Earning: $<b>{point.value}</b><br>  CR: <b>{point.cr}%</b></span>'
        }
    });
}

function barSpline(id, cate, series, statsfor, s) {
    Highcharts.chart(id, {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Stats for '+statsfor
        },
        xAxis: [{
            categories: cate,
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: s,
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Earnings',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} $',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        series: [
            {
                name: series[1].name,
                type: 'column',
                yAxis: 1,
                data: series[1].data,
                tooltip: {
                    valueSuffix: ' $'
                }
            },
            {
                name: series[0].name,
                type: 'spline',
            data: series[0].data,
            tooltip: {
                valueSuffix: ''
            }
        }]
    });
}

function bar2Spline(id, cate, series, statsfor, s1, s2) {
    Highcharts.chart(id, {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Stats for ' + statsfor
        },
        xAxis: [{
            categories: cate,
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            },
            title: {
                text: s1,
                style: {
                    color: Highcharts.getOptions().colors[2]
                }
            }

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: 'Earnings',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                format: '{value} $',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }, { // Tertiary yAxis
            gridLineWidth: 0,
            title: {
                text: s2,
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],
        tooltip: {
            shared: true
        },
        series: [{
            name: series[1].name,
            type: 'column',
            yAxis: 1,
            data: series[1].data,
            tooltip: {
                valueSuffix: ' $'
            }

        }, {
            name: series[2].name,
            type: 'spline',
            yAxis: 2,
            data: series[2].data,
            marker: {
                enabled: false
            },
            dashStyle: 'shortdot',
            tooltip: {
                valueSuffix: ' %'
            }

        }, {
            name: series[0].name,
            type: 'spline',
            data: series[0].data,
            tooltip: {
                valueSuffix: ''
            }
        }]
    });
}

function bar(id, t, series) {
    if (!series){
        series = {};
        series.cate = [];
        series.date = [];
    }
    Highcharts.chart(id, {

        title: {
            text: t
        },

        xAxis: {
            categories: series.cate
        },
        yAxis: {
            title: {
                text: 'Earnings'
            }
        },
        series: [{
            name: "Earnings",
            type: 'column',
            colorByPoint: true,
            data: series.data,
            showInLegend: false,
            tooltip: {
                valueSuffix: ' $'
            }
        }]

    });
}

function world(a, tit) {
    // Initiate the chart
    console.log(a);
    Highcharts.mapChart('geos', {

        chart: {
            map: 'custom/world'
        },

        title: {
            text: tit
        },

        legend: {
            title: {
                text: 'conversion density',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },

        tooltip: {
            backgroundColor: 'none',
            borderWidth: 0,
            shadow: false,
            useHTML: true,
            padding: 0,
            pointFormat: '<span class="f32"><span class="flag {point.properties.hc-key}">' +
            '</span></span> {point.name}<br>' +
            '<span style="font-size:30px">{point.value}</span>',
            positioner: function () {
                return { x: 0, y: 250 };
            }
        },

        colorAxis: {
            min: 1,
            max: 1000,
            type: 'logarithmic'
        },

        series: [{
            data: a,
            joinBy: ['code2'],
            name: 'Conversions',
            states: {
                hover: {
                    color: '#a4edba'
                }
            }
        }]
    });
}

'use strict';

(function ($) {
    $.fn.easy_number_animate = function (_options) {
        var val = (_options.end_value + '').replace(',', '');
        var ok = val.split('.');
        _options.end_value = parseInt(ok[0]);
        _options.last = ok[1];
        var defaults = {
                start_value: 0
                , end_value: 100
                , duration: 1000  // Milliseconds
                , delimiter: ','
                , round: true
                , before: null
                , after: null
            }

            , options = $.extend(defaults, _options)

            , UPDATES_PER_SECOND = 60
            , ONE_SECOND = 1000  // Milliseconds
            , MILLISECONDS_PER_FRAME = ONE_SECOND / UPDATES_PER_SECOND
            , DIRECTIONS = {DOWN: 0, UP: 1}
            , ONE_THOUSAND = 1000

            , $element = $(this)
            , interval = Math.ceil(options.duration / MILLISECONDS_PER_FRAME)
            , current_value = options.start_value
            , final = ''
            , increment_value = (options.end_value - options.start_value) / interval
            , direction = options.start_value < options.end_value ? DIRECTIONS.UP : DIRECTIONS.DOWN
        ;

        function format_thousand(_value) {
            var _THOUSAND_GROUP_LENGTH = 3
                , _number_string = _value.toString();

            if (_number_string.length > _THOUSAND_GROUP_LENGTH) {
                var _remainder = _number_string.length % _THOUSAND_GROUP_LENGTH
                    , _index = _remainder ? _remainder : _THOUSAND_GROUP_LENGTH
                    , _number_string_formatted = _number_string.slice(0, _index)
                ;

                for (; _index < _number_string.length; _index += _THOUSAND_GROUP_LENGTH) {
                    _number_string_formatted += options.delimiter + _number_string.slice(_index, _index + _THOUSAND_GROUP_LENGTH);
                }

                return _number_string_formatted;
            } else {
                return _value;
            }
        }

        function needs_formatting(_value) {
            return _value >= ONE_THOUSAND;
        }

        function animate() {
            if (current_value !== options.end_value) {
                var new_value = current_value + increment_value;

                if (direction === DIRECTIONS.UP) {
                    current_value = new_value > options.end_value ? options.end_value : new_value;
                } else {
                    current_value = new_value < options.end_value ? options.end_value : new_value;
                }

                if (options.round) {
                    new_value = Math.trunc(current_value);
                }

                if (options.delimiter && needs_formatting(new_value)) {
                    new_value = format_thousand(new_value);
                }

                $element.text(new_value + '.00');
                final = new_value;
                requestAnimationFrame(animate);
            } else {
                $element.text(final + '.' + options.last);
                if (typeof options.after === 'function') {
                    options.after($element, current_value);
                }
            }
        }

        if (typeof options.before === 'function') {
            options.before($element);
        }

        animate();
    };
}(jQuery));